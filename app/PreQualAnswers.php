<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PreQualAnswers extends Model
{
    //Set table for model
  protected $table = 'pre_qual_answers';

   /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    ' AnswersID','PreQualTestID', 'Answers', 'Marks'
  ];

  public function questions()
  {
    return $this->belongsTo(JobTest::class);
  }
  public function candidateAnswers()
  {
    return $this->hasMany(CandidateAnswers::class, 'AnswerID','AnswersID');
  }
  
  
  public function convert($marks){

    if ($marks == 10){
      $c = 100;

    }else if ($marks == 9){
        
      $c = 90;
      
    }else if ($marks == 8){
        
      $c = 80;
        
    }else if ($marks == 7){
        
      $c = 70;
         
    }else if ($marks == 6){
        
      $c = 60;
      
    }else if ($marks == 5){
        
      $c = 50;
      
    }else if ($marks == 4){
        
      $c = 40;
        
    }else if ($marks == 3){
        
      $c = 30;
      
    }else if ($marks == 2){
        
      $c = 20;
    }else if ($marks == 1){
        
      $c = 10;
    }

    return $c;
  }
}
