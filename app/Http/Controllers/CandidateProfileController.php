<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Auth;
use DB;
use App\RegistrationDetails;
use App\PersonalSummary;
use Redirect;
use Toastr;
use App\User;
use App\LanguageList;
use App\Country;
use App\Location;
use App\CvTable;
use App\HardSkill;
use App\Language;
use App\FurtherEducation;
use App\Subject;
use App\Specialization;
use App\ProfQualTitles;
use App\ProfQual;
use App\ProfBodies;
use App\ProfBodiesTitles;
use App\Industry;
use App\IndustryFunctions;
use App\Currency;
use App\WorkExperience;
use App\WorkExperienceResponsibility;
use App\OtherInterest;

class CandidateProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function create()
    {
        $userdetails = RegistrationDetails::where('AccUserID', Auth::user()->id)->where('EmailAddress',Auth::user()->email)->first();
        $cvdets = CvTable::where('CandidateRegID', $userdetails->CanditateRegID)->first();
        $countries = Country::all();
        $locations = Location::all();


        return view('candidates.create', compact('userdetails','countries','cvdets','locations'));
    }
    public function createSummary()
    {
        $userdetails = RegistrationDetails::where('AccUserID', Auth::user()->id)->where('EmailAddress',Auth::user()->email)->first();
      	$cvdets = CvTable::where('CandidateRegID', $userdetails->CanditateRegID)->first();

        if($cvdets == null){

            $redirectError = [
                'title' => 'Sorry!',
                'content' => 'Kindly fill the following information.',
            ];
            Session::flash('redirectError', $redirectError);

            return Redirect::route('profile.create');
        }

      	$summary = PersonalSummary::where('CV_ID', $cvdets->CV_ID)->get();
        $y = 0;
        $lang = Language::where('CV_ID', $cvdets->CV_ID)->first();
        if(!empty($lang) && $lang->Language2 != null){
          $y++;
        }if(!empty($lang) && $lang->Language3 != null){
          $y++;
        }if(!empty($lang) && $lang->Language4 != null){
          $y++;
        }
        $attrs = [];
        $skills = [];
        $hskills = [];
        foreach ($summary as $key => $value) {
          array_push($attrs, $value->Attributes);
          array_push($skills, $value->Skills);
          array_push($hskills, $value->HardSkills);
        }
        $languages = LanguageList::orderBy('LanguageName', 'ASC')->get();
        $hardskills = HardSkill::orderBy('Name', 'ASC')->get();

        return view('candidates.createSummary', compact('languages','summary','hardskills','cvdets','attrs','skills','hskills','lang','y'));
    }
        public function createEducation()
    {

        $userdetails = RegistrationDetails::where('AccUserID', Auth::user()->id)->where('EmailAddress',Auth::user()->email)->first();
      	$cvdets = CvTable::where('CandidateRegID', $userdetails->CanditateRegID)->first();

        //dd(PersonalSummary::where('CV_ID',$cvdets->CV_ID)->first());
        if(PersonalSummary::where('CV_ID',$cvdets->CV_ID)->first() == null){
            $redirectError = [
                'title' => 'Sorry!',
                'content' => 'You can not move to the next step until Education details are complete.',
            ];
            Session::flash('redirectError', $redirectError);

            return Redirect::route('profile.create.summary');
        }

        $education = FurtherEducation::where('CV_ID', $cvdets->CV_ID)->get();
        //dd($education);
        $subjects = Subject::all();
        $specializations = Specialization::all();
        $profs = ProfQualTitles::all();
        $profBodiesTitles = ProfBodiesTitles::all();
        $qualifications = ProfQual::where('CV_ID', $cvdets->CV_ID)->get();
        $profbodies = ProfBodies::where('CV_ID', $cvdets->CV_ID)->get();
        //dd($profBodiesTitles);

        return view('candidates.createEducation', compact('subjects','userdetails','cvdets','education','specializations','profs','qualifications','profbodies','profBodiesTitles'));
    }

    public function createWork()
    {
        $userdetails = RegistrationDetails::where('AccUserID', Auth::user()->id)->where('EmailAddress',Auth::user()->email)->first();
      	$cvdets = CvTable::where('CandidateRegID', $userdetails->CanditateRegID)->first();

        if(FurtherEducation::where('CV_ID',$cvdets->CV_ID)->first() == null){
            $redirectError = [
                'title' => 'Sorry!',
                'content' => ' Kindly complete Education details to move to the next step.',
            ];
            Session::flash('redirectError', $redirectError);
            return Redirect::route('profile.create.education');
        }
        $industry = Industry::all();
        $functions = IndustryFunctions::orderBy('Name','ASC')->get();
        $currency = Currency::all();
        $work = WorkExperience::where('CV_ID', $cvdets->CV_ID)->get();
        $interests = OtherInterest::where('CV_ID', $cvdets->CV_ID)->get();
        
            $workresps = [];
            foreach ($work as $key => $value)
            {
              $workresps[] = WorkExperienceResponsibility::where('WorkExpID', $value->WorkExpID)->get();
            }
            

        return view('candidates.createWork', compact('industry','userdetails','cvdets','functions','currency','work','interests','workresps'));
    }


    public function addcontact(Request $request)
    {
      $data = $request->all();
      // dd($data);
      $user = RegistrationDetails::where('AccUserID', Auth::user()->id)->where('EmailAddress',Auth::user()->email)->first();

      $validator = Validator::make($data, [
        'fname' => ['required', 'string', 'max:255'],
        'lname' => ['required', 'string', 'max:255'],
        'email1' => ['string', 'email', 'max:255'],
      ]);

      if ($validator->fails()) {
        $errors = $validator->errors();
        //dd($errors);
        return Redirect::back()->withErrors($errors);
      }

      if(isset($data['contactable'])){
        $contactable = 'Yes';
      }else{
        $contactable = 'No';
      }

      $cvdets = CvTable::where('CandidateRegID', $user->CanditateRegID)->first();
      if($cvdets != null){

      $details = DB::table('cv_table')
	      ->where('CandidateRegID', $user->CanditateRegID)
	      ->update([
		 "Gender" => $data['gender'],
		 "DOB" => $data['date'],
		 "PhoneNumber" => $data['full_phone'],
         "PhoneNumberOther" => @$data['full_phone1'],
         "EmailAddressOther" => @$data['email1'],
		 "Contactable" => $contactable,
		 "PO_BOX" => $data['pobox'],
		 "PhysicalAddress" => $data['address'],
         "location" => @$data['location'],
		 "Nationality" => $data['nationality'],
		 "Identification" => $data['identification'],
	     "Passport_No" => $data['passport'],
		 "Passport_Country" => $data['passport_country'],
		 "ID_No"  => $data['id'],
		 "ID_Country" => $data['country'],
		 "DL"  => $data['licence'],
		 "CarOwner" => $data['owner'],
	      ]);

      	 return Redirect::route('profile.create.summary');
      }else{

      $details = CvTable::create([
	     "Gender" => $data['gender'],
	     "DOB" => $data['date'],
         "PhoneNumber" => $data['full_phone'],
         "PhoneNumberOther" => @$data['full_phone1'],
         "EmailAddressOther" => @$data['email1'],
	     "Contactable" => $contactable,
	     "PO_BOX" => $data['pobox'],
	     "PhysicalAddress" => $data['address'],
	     "Nationality" => $data['nationality'],
	     "Identification" => $data['identification'],
	     "Passport_No" => $data['passport'],
	     "Passport_Country" => $data['passport_country'],
	     "ID_No"  => $data['id'],
	     "ID_Country" => $data['country'],
	     "DL"  => $data['licence'],
	     "CarOwner" => $data['owner'],
	     "CandidateRegID" => $user->CanditateRegID
      ]);

      }

      if($details){
      	 return Redirect::route('profile.create.summary');
      }else{
      	return Redirect::back()->withErrors("Sorry, something seems to have gone wrong. Please refresh and try again.");
      }
    }
    public function addSummary(Request $request)
    {
      $data = $request->all();
      //dd($data);
      $user = RegistrationDetails::where('AccUserID', Auth::user()->id)->where('EmailAddress',Auth::user()->email)->first();

      $cvdets = CvTable::where('CandidateRegID', $user->CanditateRegID)->first();

      $validator = Validator::make($data, [
        'language1' => ['required'],
        'fluency1' => ['required'],
      ]);
      if($cvdets->PersonalSummary == null){
        $errors = [
          'Please fill in your profile summary information. This is helpful for matching you with available opportunities.'
        ];
        return Redirect::back()->withErrors($errors);
      }

      if ($validator->fails()) {
        $errors = $validator->errors();
      	//dd($errors);
        return Redirect::back()->withErrors($errors);
      }

        DB::table('cv_table')
	      ->where('CV_ID', $cvdets->CV_ID)
	      ->update([
		     "SkypeContact" => $data['skype'],
		     "LinkedInContact" => $data['linkedin']
	      ]);

      if(Language::where('CV_ID', $cvdets->CV_ID)->first() == null){
	      Language::create([
		  	"CV_ID" => $cvdets->CV_ID,
		    "Language1" => $data['language1'],
		    "Fluency1" => $data['fluency1'],
		    "Language2" => @$data['language2'],
		    "Fluency2" => @$data['fluency2'],
		    "Language3" => @$data['language3'],
		    "Fluency3" => @$data['fluency3'],
		    "Language4" => @$data['language4'],
		    "Fluency4" => @$data['fluency4']
		  ]);
      }else{

      $language = DB::table('language')
	      ->where('CV_ID', $cvdets->CV_ID)
	      ->update([
		    "Language1" => $data['language1'],
		    "Fluency1" => $data['fluency1'],
		    "Language2" => @$data['language2'],
		    "Fluency2" => @$data['fluency2'],
		    "Language3" => @$data['language3'],
		    "Fluency3" => @$data['fluency3'],
		    "Language4" => @$data['language4'],
		    "Fluency4" => @$data['fluency4']
		  ]);
      }

      if(PersonalSummary::where('CV_ID', $cvdets->CV_ID)->first() != null){

      	PersonalSummary::where('CV_ID', $cvdets->CV_ID)->where('Skills','!=', '')->delete();
      	PersonalSummary::where('CV_ID', $cvdets->CV_ID)->where('Attributes','!=', '')->delete();
      	PersonalSummary::where('CV_ID', $cvdets->CV_ID)->where('HardSkills','!=', null)->delete();
      }

      foreach ($data['skill'] as $key => $value) {
      PersonalSummary::create([
	    'CV_ID' => $cvdets->CV_ID,
	    'Skills' => $value,
	    'Attributes' => ''
	  	]);
      }


      foreach ($data['attr'] as $key => $value) {
      PersonalSummary::create([
	    'CV_ID' => $cvdets->CV_ID,
	    'Attributes' => $value,
	    'Skills' => ''
	  	]);
      }


      foreach ($data['hardskill'] as $key => $value) {
        //dd($value);
      PersonalSummary::create([
	    'CV_ID' => $cvdets->CV_ID,
	    'HardSkills' => $value,
	    'Attributes' => '',
	    'Skills' => ''
	  	]);
      }

      return Redirect::route('profile.create.education')->with('status','Education Details Created');

  	}

    public function addFurtherEducation(Request $request)
    {
      $data = $request->all();
      
      $user = RegistrationDetails::where('AccUserID', Auth::user()->id)->where('EmailAddress',Auth::user()->email)->first();

      $cvdets = CvTable::where('CandidateRegID', $user->CanditateRegID)->first();
    //   dd($request);
      	if($data['subject'] === 'Others'){
      			$others = $data['subject'];
      			$subject = $data['othersubjecttitle'];
      		}else{
      			$others = '';
      			$subject = $data['subject'];
      		}
      		
      	if($data['specialization'] === 'Others'){
      			$ot = $data['specialization'];
      			$specialization = $data['otherspecialization'];
      		}else{
      			$ot = '';
      			$specialization = $data['specialization'];
      		}
      			
      		

   $save =     FurtherEducation::create([
        "CV_ID" => $cvdets->CV_ID,
        "FormalEducation" => @$data['level'],
        "FurtherEducation" => @$data['furthered'],
        "QualificationTitle" => @$data['type'],
        "QualStartGradDate" => @$data['start_date'],
        "QualEndGradDate" => @$data['end_date'],
        "Institution" => @$data['institution'],
        "Subjects" => $subject,
        
        // "Subjects" => @$data['subject'],
        "Specialization" => $specialization,
      ]);
    
       return Redirect::back();
	}

    public function addProffession(Request $request)
    {
      $data = $request->all();
      //dd($data);
      $user = RegistrationDetails::where('AccUserID', Auth::user()->id)->where('EmailAddress',Auth::user()->email)->first();

      $cvdets = CvTable::where('CandidateRegID', $user->CanditateRegID)->first();

      		if($data['qualification'] == 'Others'){
      			$others = $data['qualification'];
      			$qualification = $data['qualification-title'];
      		}else{
      			$others = '';
      			$qualification = $data['qualification'];
      		}

	      ProfQual::create([
		  	'CV_ID' => $cvdets->CV_ID,
		  	'ProfessionalQualifications' => @$qualification,
		  	'Other' => $others,
		  	'StartDate' => @$data['start_date'],
		  	'EndDate' => @$data['end_date'],
		  	'ProfQualTitle' => @$qualification,
		  ]);


       return Redirect::back();
	}

  public function addProffessionBodies(Request $request)
    {
      $data = $request->all();
      //dd($data);
      $user = RegistrationDetails::where('AccUserID', Auth::user()->id)->where('EmailAddress',Auth::user()->email)->first();

      $cvdets = CvTable::where('CandidateRegID', $user->CanditateRegID)->first();

        ProfBodies::create([
          'CV_ID' => $cvdets->CV_ID,
          'ProfessionalBody' => $data['bodies'],
          'Membership_number'=> @$data['membership-number'],
          'StartDate' => @$data['start_date'],
  		  	'EndDate' => @$data['end_date'],
        ]);


       return Redirect::back();
  }
    public function addWork(Request $request)
    {
        $userdetails = RegistrationDetails::where('AccUserID', Auth::user()->id)->where('EmailAddress',Auth::user()->email)->first();
      	$cvdets = CvTable::where('CandidateRegID', $userdetails->CanditateRegID)->first();

    // if(WorkExperience::where('CV_ID',$cvdets->CV_ID)->first() != null){
    //         $redirectError = [
    //             'title' => 'Sorry!',
    //             'content' => ' Kindly complete Work details to move to the next step.',
    //         ];
    //         Session::flash('redirectError', $redirectError);
    //         return Redirect::route('profile.create.work');
    //     }
        
        
        
      $data = $request->all();

      
      $user = RegistrationDetails::where('AccUserID', Auth::user()->id)->where('EmailAddress',Auth::user()->email)->first();

      $cvdets = CvTable::where('CandidateRegID', $user->CanditateRegID)->first();
      if(isset($data['current'])){
        $current = 'Yes';
      }else{
        $current = 'No';
      }
      if(isset($data['contactref1'])){
        $contactref1 = 'Yes';
      }else{
        $contactref1 = 'No';
      }
      
//   if($data['subject'] == 'Others'){
//       			$others = $data['subject'];
//       			$subject = $data['othersubjecttitle'];
//       		}else{
//       			$others = '';
//       			$subject = $data['subject'];
//       		}

   
      if(isset($data['industryfunc'])){
          
          if($data['industryfunc'] == 'Others'){
      			$others = $data['industryfunc'];
      			$industryfunc = $data['otherposition'];
      		}else{
      			$others = '';
      			$industryfunc= $data['industryfunc'];
      		}
      }else{
          
          $industryfunc= " ";
      }
      
      
      

     $wid = new WorkExperience();
     $wid->CV_ID = $cvdets->CV_ID;
     $wid->Title = $request->input('industryfunc');
     $wid->JobType = $request->input('jobtype');
     $wid->OtherWork = $industryfunc;
     $wid->Company = $request->input('company');
     $wid->Sector = $request->input('sector');
     $wid->StartDate = $request->input('start_date');
     $wid->CurrentDate = $current;
     $wid->EndDate = $request->input('end_date');
     $wid->Line_Manager_Name = $request->input('line_name');
     $wid->Line_Manager_Designation = $request->input('line_designation');
     $wid->linemanageremail = $request->input('linemanageremail');
     $wid->linemanagercontact = $request->input('linemanagercontact');
     $wid->CurrencyID = $request->input('currency');
     $wid->Allowances = $request->input('allowances');
     $wid->Benefits = $request->input('benefits');
     $wid->reasonsleaving = $request->input('reasonsleaving');
     $wid->MonthlyGrossSalary = str_replace( ',', '',$request->input('salary'));
     $wid->save();
     
     $responsib = new WorkExperienceResponsibility();
      $responsib->WorkExpID = $wid->id;
      $responsib->Responsibility =  $request->input('responsibility');
      $responsib->Achievement = $request->input('achivements');
      $responsib->save();

      Toastr::success('Work Details Created');

       return Redirect::back();
	}
  public function candidatehome()
  {
      $userdetails = RegistrationDetails::where('AccUserID', Auth::user()->id)->where('EmailAddress',Auth::user()->email)->first();
      $cvdets = CvTable::where('CandidateRegID', $userdetails->CanditateRegID)->first();

      if(WorkExperience::where('CV_ID',$cvdets->CV_ID)->first() == null){
          $redirectError = [
              'title' => 'Sorry!',
              'content' => 'Kindly fill Work Details To Complete Your Profile',
          ];
          Session::flash('redirectError', $redirectError);

          return Redirect::route('profile.create.work');
      }else{

        return Redirect::route('home');
      }
      // $industry = Industry::all();
      // $functions = IndustryFunctions::orderBy('Name')->get();
      // $currency = Currency::all();
      // $work = WorkExperience::where('CV_ID', $cvdets->CV_ID)->get();
      // $interests = OtherInterest::where('CV_ID', $cvdets->CV_ID)->get();
      //     $workresps = [];
      //     foreach ($work as $key => $value) {
      //         //dd($value);
      //       $workresps = WorkExperienceResponsibility::where('WorkExpID', $value->WorkExpID)->get();
      //     }

  }

    public function addInterest(Request $request)
    {
      $data = $request->all();
      //dd($data);
      $user = RegistrationDetails::where('AccUserID', Auth::user()->id)->first();

      $cvdets = CvTable::where('CandidateRegID', $user->CanditateRegID)->first();

      if(OtherInterest::where('CV_ID', $cvdets->CV_ID)->first() != null){

      OtherInterest::where('CV_ID', $cvdets->CV_ID)->update([
        'Interests' => @$data['Interest1'],
        'Interest2' => @$data['Interest2'],
        'Interest3' => @$data['Interest3'],
        'Interest4' => @$data['Interest4'],
        'Interest5' => @$data['Interest5'],
      ]);
      }else{

      OtherInterest::create([
        'CV_ID' => $cvdets->CV_ID,
        'Interests' => @$data['Interest1'],
        'Interest2' => @$data['Interest2'],
        'Interest3' => @$data['Interest3'],
        'Interest4' => @$data['Interest4'],
        'Interest5' => @$data['Interest5'],
      ]);
      }

       return Redirect::route('home')->with('status','Work Details Created');
    }

    public function deleteFurtherEducation(Request $request)
    {
        $data= $request->id;
        $userinfo = FurtherEducation::where('FurtherEducationID', $data)->first();
        if($userinfo->FurtherEducation == 'Degree'){

        FurtherEducation::where('CV_ID', $userinfo->CV_ID)->where('FurtherEducation', '!=',null)->delete();

        }else{

        FurtherEducation::where('FurtherEducationID', $data)->delete();
        }


        return response()->json(['success'=>'Record Deleted.']);

    }

    public function deleteProffession(Request $request)
    {
        $qid= $request->qid;

        ProfQual::where('ProfQualID', $qid)->delete();

        return response()->json(['success'=>'Record Deleted.']);

    }

    public function deleteProffessionBodies(Request $request)
    {
        $bid= $request->bid;

        ProfBodies::where('ProfBodyID', $bid)->delete();

        return response()->json(['success'=>'Record Deleted.']);

    }

    public function deleteWork(Request $request)
    {
        $wid= $request->wid;

        WorkExperienceResponsibility::where('WorkExpID', $wid)->delete();
        WorkExperience::where('WorkExpID', $wid)->delete();

        return response()->json(['success'=>'Record Delete.']);

    }

}
