<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\Eloquent\Collection;

use App\Clients;
use App\Country;
use App\Currency;
use App\Industry;
use App\Location;
use App\JobAd;
use App\SummitStaff;
use App\JobRequirement;
use App\JobDuty;
use App\PreQualTest;
use App\PreQualAnswers;
use App\CandidateAnswers;
use App\RegistrationDetails;
use App\IndustryFunctions;
use App\CvTable;
use App\JobCV;
use App\User;
use App\Interview;
use App\HardSkill;
use App\ProfQualTitles;
use App\PersonalSummary;


use Redirect;
use Toastr;
use Session;
use Validator;
use Auth;
use Notifications;
use DB;

use Carbon\Carbon;

class CandidateManagementController extends Controller
{

    public function index()
    {
      $user=User::all();
      $jobs=JobAd::all();
      $industry=Industry::all();
      $industryfunctions=IndustryFunctions::orderBy('Name')->get();
      $hardskills=HardSkill::all();
      $profqualtitles = ProfQualTitles::all();
      $interview=Interview::paginate(20);

    	return view ('candidatemanagement.index',compact('user','jobs','interview','industry','industryfunctions','hardskills','profqualtitles'));

    }
     public function json(Request $request)
     {
  	$collections = CVTable::where('updated_at','>=','2020-10-01 00:00:00')->get();
  //	$collections = CVTable::limit(8000)->get();
        $value = '#';

        foreach ($collections as $key =>$value)
        {
          $firstname = $request->input('firstname');
          $lastname = $request->input('lastname');
          $phonenumber = $request->input('phonenumbercandidate');
          $gender  = $request->input('gender');
          $nationality = $request->input('nationality');
          $skills = $request->input('skills');
          $education = $request->input('education');
          $profqualtitles = $request->input('profqualtitles');
          $industrysector = $request->input('industrysector');
          $industryfunctions = $request->input('industryfunctions');
          $hardskills = $request->input('hardskills');
          $companyname = $request->input('companyname');



          //search whole string using just a subsuffix
          if($firstname)
          {
          
              $searchTerm = strtolower($value->RegDetails->Firstname);
              $str = strtolower($firstname);
              $pos = strstr($searchTerm, $str);

              if($pos === false){
              unset($collections[$key]);
             }
             
          }
          if($lastname)
          {
             
              $searchTerm = strtolower($value->RegDetails->Lastname);
              $str = strtolower($lastname);
              $pos = strstr($searchTerm, $str);
              if($pos === false){
              unset($collections[$key]);
             }
              
          }
          if($companyname)
          {
             $company = array();
             foreach($value->WorkExp as $func){
             $company[]= strtolower($func->Company);
            }
             
          if(empty($company) || !in_array(strtolower($companyname),$company))
          {
             unset($collections[$key]);
          }
          }

          if($phonenumber)
          {
              
              if($value->PhoneNumber != $phonenumber){
              unset($collections[$key]);
             }
          }

          if($gender)
          {
              
          if ( $value->Gender != $gender)
          {
              unset($collections[$key]);
          }
          }

          if($nationality)
          {
              
          if($value->Nationality != $nationality)
          {
             unset($collections[$key]);
          }
          }

         if($skills)
         {
          
          $s = array();
            foreach($value->PersonalSum as $sum){
             $s[]= $sum->Skills;
            }

          if(!in_array($skills,$s))
          {
             unset($collections[$key]);
          }
         }

         if($education)
         {
             
          $all = array();
            foreach($value->FurtherEducation as $edu){
             $all[]= $edu->FurtherEducation;
            }

          if(!in_array($education,$all))
          {
             unset($collections[$key]);
          }
         }

         if($profqualtitles)
         {
          
          $pr = array();

            foreach($value->ProfessionalQuals as $qual){
             $pr[]= $qual->ProfessionalQualifications;
            }

          if(empty($pr) || !in_array($profqualtitles,$pr))
          {
             unset($collections[$key]);
          }
         }
         if($industrysector)
         {
             $sector = array();
             foreach($value->WorkExp as $func){
             $sector[]= $func->Sector;
            }
             
          if(empty($sector) || !in_array($industrysector,$sector))
          {
             unset($collections[$key]);
          }
         }
         if($industryfunctions)
         {
          
          $fc = array();
            foreach($value->IndustryFunc as $func){
             $fc[]= $func->Title;
            }

          if(empty($fc) || !in_array($industryfunctions,$fc))
          {
             unset($collections[$key]);
          }

         }

         if($hardskills)
         {
           
          $hs = array();
            foreach($value->PersonalSum as $hard){
                if(!empty($hard->HardSkills)){
                    $hs[]= $hard->HardSkills;
                }
            }
            
            
          if(empty($hs) || !in_array($hardskills, $hs))
          {
             unset($collections[$key]);
          }
         }

        }


       $query = new Collection($collections);
         return Datatables::of($query)
          ->addColumn('CV_ID', function($list){
              if(!empty($list->RegDetails)){
               return $list->RegDetails->Firstname.' '.$list->RegDetails->Lastname;
              }
          
       })

      ->addColumn('Gender', function($list){
          
         return $list->Gender;
       })
      ->addColumn('Phonenumber', function($list){
           return $list->PhoneNumber;
       })
        ->addColumn('Actions', function ($list)
          {
             $inter = Interview::where('CV_ID',$list->CV_ID)->first(); 
          if (Auth::user()->role == 0)
          {
          $a =   '
        <span style="overflow: visible; position: relative; width: 100px;">
        
        
         <a title="View details" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="'.route('candidatesCV',[$list->CV_ID]).'"><i class="fas fa-eye" style="color:#80CFFF"></i></a>
          <a title="view Applied Jobs" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="'.route('jobAds.jobsapplied',[$list->CV_ID]).'">
            <i class="fa fa-briefcase" style="color: #3069AB"></i>
          </a>';
          if($inter){
            
            $b ='  <a title="Set Actions" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="'.route('interviewmanagement.edit',[$inter->InterviewID]).'">
             <i class="fas fa-calendar" style="color: #5cb85c"></i></a>';
          }else{
           $b = ' <a title="Set Actions" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="'.route('interviewmanagement.noCreate',[$list->CV_ID,$list->Job_adID]).'">
             <i class="fas fa-calendar" style="color: #FFCB00"></i></a>';
          }
          
         if($list->Blacklisted == 1){
               $c ='  <a title="Remove Blacklist" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="'.route('candidatemanagement.blacklist',[$list->CV_ID, 0]).'">
             <i class="fa fa-user-times" style="color: #FA0000"></i></a>';
          }else{
               $c ='  <a title="Blacklist" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="'.route('candidatemanagement.blacklist',[$list->CV_ID, 1]).'">
             <i class="fa fa-user" style="color: #5cb85c"></i></a>';
          }
          
          
         $d =' <a
            title="Delete candidate details"
            class="btn btn-sm btn-clean btn-icon btn-icon-sm"
            data-href="#"
            data-toggle="modal"
            data-target="#deleteConfirmModal"
            >
            <i class="far fa-trash-alt" style="color:#FA0000"></i>
          </a>
      </span>';
      
      return $a.$b.$c.$d;
            
         }
         else{
              
          $a = '
        <span style="overflow: visible; position: relative; width: 100px;">
         <a title="View details" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="'.route('candidatesCV',[$list->CV_ID]).'"><i class="fas fa-eye" style="color:#80CFFF"></i></a>
          <a title="View Applied Jobs" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="'.route('jobAds.jobsapplied',[$list->CV_ID]).'">
            <i class="fa fa-briefcase" style="color: #3069AB"></i>
          </a>';
          if($inter){
            $b ='  <a title="Set Actions" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="'.route('interviewmanagement.edit',[$inter->InterviewID]).'">
             <i class="fas fa-calendar" style="color: #5cb85c"></i></a>';
          }else{
           $b = ' <a title="Set Actions" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="'.route('interviewmanagement.noCreate',[$list->CV_ID,$list->Job_adID]).'">
             <i class="fas fa-calendar" style="color: #FFCB00"></i></a>';
          }
          
          if($list->Blacklisted == 1){
               $c ='  <a title="Remove Blacklist" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="'.route('candidatemanagement.blacklist',[$list->CV_ID, 0]).'">
             <i class="fa fa-user-times" style="color: #FA0000"></i></a>';
          }else{
               $c ='  <a title="Blacklist" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="'.route('candidatemanagement.blacklist',[$list->CV_ID, 1]).'">
             <i class="fa fa-user" style="color: #5cb85c"></i></a>';
          }
          
      $d ='</span>';
      
      
      return $a.$b.$c.$d;
              }
          
        })
        

        ->rawColumns(['Actions'])
        ->make();


     }
     
     
     public function Blacklist($id,$val){
         $date = Carbon::now();
         
             $details = DB::table('cv_table')
	      ->where('CV_ID', $id)
	      ->update([
		     "Blacklisted" => $val,
		     "updated_at" => $date,

	      ]);
	      if($val == 1){
	      Toastr::success('Candidate has been blacklisted.');
	      }else{
	          Toastr::success('Candidate has been removed from blacklist.');
	      }
	      
	       return redirect()->route('candidatemanagement.index');
	      
	    
	      
         
         
     }
}
