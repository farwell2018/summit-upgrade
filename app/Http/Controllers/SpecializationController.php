<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Clients;
use App\Country;
use App\Currency;
use App\Industry;
use App\Location;
use App\JobAd;
use App\SummitStaff;
use App\JobRequirement;
use App\JobDuty;
use App\PreQualTest;
use App\PreQualAnswers;
use App\CandidateAnswers;
use App\RegistrationDetails;
use App\CvTable;
use App\JobCV;
use App\User;
use App\HardSkill;
use App\Specialization;

use Redirect;
use Toastr;
use Session;
use Validator;
use Auth;
use Notifications;
use DB;

class SpecializationController extends Controller
{
    //

    public function index(){

        $specializations = Specialization::all();
        return view ('specialization.index', compact('specializations'));

    }

    // public function json()
    // {
    //     print_r("Adadasdasd");die;
    //     $query = Industry::orderBy('id','ASC')->get();
    //     print_r($query);die;
    //     return Datatables::of($query)

    //         ->addColumn('full_salary', function ($list) {
    //             return $list->SalCurrency.', '.$list->GrossMonthSal;
    //         })
    //         ->addColumn('client', function ($list) {
    //             return $list->Clients->CompanyName;
    //         })
    //         ->rawColumns(['tools'])
    //         ->make();

    // }
//nimefika hapa kuanzia 

    public function create(){

        // $industries = Industry::all();
        return view ('specialization.create');
    }

    public function store(Request $request){


        $validator = Validator::make($request->except('_token'), [
            'specializationname' => ['required', 'string'],
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();

            // dd($errors);
            return Redirect::back()->withInput()->withErrors($errors);
        }else {

            $specialization = new Specialization();
            $specialization->SpecializationTitle = $request->input('specializationname');
            $specialization->save();
            Toastr::success('Specialization has been created.');
            return redirect()->route('specialization.index');
        }
    }


    public function edit($id){

        $specialization = Specialization::where('SpecializationID','=',$id)->first();
        return view ('specialization.edit',compact('specialization'));

    }

    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->except('_token'), [
            'specializationname' => ['required', 'string'],
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();

            // dd($errors);
            return Redirect::back()->withInput()->withErrors($errors);
        }else {

            $insertdata = [
                'Name' => $request->input('specializationname')];

            DB::table('specialization')->where('SpecializationID', $id)->update($insertdata);

            Toastr::success('Specialization has been update.');
            return redirect()->route('specialization.index');
        }
    }


    // public function view($id){
    //     $industry = Industry::where('ID','=', $id)->first();
    //     return view('industry.view',compact('industry'));
    // }
     

     public function delete($id){
        Specialization::where('SpecializationID','=', $id)->delete();

        Toastr::success('Specialization has been deleted.');
        return redirect()->route('specialization.index');
    }

    
}
