<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use App\User;
use App\VerifyUser;
use App\PasswordReset;
use App\RegistrationDetails;
use Mail;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;


    public function sendResetLinkEmail(Request $request)
    {

      $this->validateEmail($request);

      $user= User::where('email','=', $request->input('email'))->first();

      if(empty($user)){

        $response = 'We cannot find a user with that e-mail address.';
            // 'action'=>'Google',
            // 'link'=>'google.com'


        return back()->withInput($request->only('email'))->withErrors(['email' => trans($response)]);

      }else{

      $token = self::generateRandomString(16);
       $password = new PasswordReset();
       $password->email = $request->input('email');
       $password->token = $token;

       $password->save();

         $details = RegistrationDetails::where('AccUserID',$user->id)->first();

          if($details){
              $name = $details->Firstname.' '.$user->Lastname;
          }else{
              $name = '';
          }

      Mail::send('emails.password_reset',['name' => $name, 'url' => route('password.change', $password->token)],
       function ($message) use ($user) {
         $message->from('application@summitrecruitment-search.com', 'Reset Password Request');
         $message->to($user->email, $user->first_name);
         $message->subject('Password Reset');
       });


       if($password){
         $response= 'An email has been sent to your email address. Please follow the instructions in the email to reset your password.If you do not see the email in your inbox, please check your junk, spam or promotions folder.';
             // 'action'=>'Google',
             // 'link'=>'google.com'

         return back()->with('status', trans($response));
       }


      }



        // $this->validateEmail($request);
        //
        // // We will send the password reset link to this user. Once we have attempted
        // // to send the link, we will examine the response then see the message we
        // // need to show to the user. Finally, we'll send out a proper response.
        // $response = $this->broker()->sendResetLink(
        //     $this->credentials($request)
        // );
        //
        // return $response == Password::RESET_LINK_SENT
        //             ? $this->sendResetLinkResponse($request, $response)
        //             : $this->sendResetLinkFailedResponse($request, $response);
    }

    protected function generateRandomString($length = 10)
    {
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[ rand(0, $charactersLength - 1) ];
      }

      return $randomString;
    }
}
