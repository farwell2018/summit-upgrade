<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\Eloquent\Collection;
use App\JobAd;
use App\JobTests;
use App\CandidateAnswers;
use App\PreQualAnswers;
use Redirect;
use Toastr;
use Session;
use Validator;
use Auth;
use Notifications;
use DB;

class JobTestController extends Controller
{
    public function jobtestsindex()
    {
    	$questions = JobTests::all();
    	$jobads = JobAd::all();

    	return view('jobtests.index',compact('questions','jobads'));
    }
    public function json(Request $request)
    {
         $collections = JobTests::orderBy('ID','Desc')->distinct()->get(['ID']);
         $value = '#';

        $query = new Collection($collections);
        return Datatables::of($query)

        ->addColumn('ID', function($list)
            {
                if(!empty($list->jobs)){
                    return $list->jobs->JobTitle;
                }
                
            })
        ->addColumn( 'Question',function($list)
            {
              $q = array();
              $questions = JobTests::where('ID', $list->ID)->get();
              foreach($questions as $question){
                $q[] = $question->Question;
              }
              $a = implode(' , ', $q);
              return $a;
            })
        ->addColumn('Actions', function ($list) { /*list here refers to interview in question for the view blade*/
          if (Auth::user()->role == 0)
          {
               if(!empty($list->jobs)){
             return
             '
        <span style="overflow: visible; position: relative; width: 100px;">
        <a title="View candidate details" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="'.route('jobtests.viewquestions',[$list->jobs->ID]).'">
           <i class="fas fa-eye" style="color:#80CFFF"></i>
          </a>
          <a title="Edit Client Status" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="'.route('jobtests.editviewquestions',[$list->jobs->ID]).'">
            <i class="fas fa-edit" style="color: #3069AB"></i>
          </a>
          <a
            title="Delete candidate details"
            class="btn btn-sm btn-clean btn-icon btn-icon-sm"
            data-href="#"
            data-toggle="modal"
            data-target="#deleteConfirmModal"
            >
            <i class="far fa-trash-alt" style="color:#FA0000"></i>
          </a>

      </span>';
               }else{
                   
                   
         return
             '
        <span style="overflow: visible; position: relative; width: 100px;">
        <a title="View candidate details" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="">
           <i class="fas fa-eye" style="color:#80CFFF"></i>
          </a>
          <a title="Edit Client Status" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="">
            <i class="fas fa-edit" style="color: #3069AB"></i>
          </a>
          <a
            title="Delete candidate details"
            class="btn btn-sm btn-clean btn-icon btn-icon-sm"
            data-href="#"
            data-toggle="modal"
            data-target="#deleteConfirmModal"
            >
            <i class="far fa-trash-alt" style="color:#FA0000"></i>
          </a>

      </span>' ;          
               }
            
         ;}

         else{
          if(!empty($list->jobs)){
          return '
        <span style="overflow: visible; position: relative; width: 100px;">
        <a title="View details" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="'.route('jobtests.viewquestions',[$list->jobs->ID]).'">
           <i class="fas fa-eye" style="color:#80CFFF"></i>
          </a>
          <a title="Edit details" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="'.route('jobtests.editviewquestions',[$list->jobs->ID]).'">
            <i class="fas fa-edit" style="color: #3069AB"></i>
          </a>
      </span>';
          }else{
               return '
        <span style="overflow: visible; position: relative; width: 100px;">
        <a title="View details" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="">
           <i class="fas fa-eye" style="color:#80CFFF"></i>
          </a>
          <a title="Edit details" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="">
            <i class="fas fa-edit" style="color: #3069AB"></i>
          </a>
      </span>';
          }

              }

        })
        ->rawColumns(['Actions'])
        ->make();

    }
    public function jobtestscreate($id)
    {
    	$questions = JobTests::all();
    	$jobads = JobAd::where('ID',$id)->first();

    	return view ('jobtests.create',compact('questions','jobads'));
    }

    public function jobtestsstore(Request $request)
    {

      $requirement= $_POST['Question'];
      //dd($requirement);
      foreach ($requirement as $req){


          $quest = new JobTests();
          $quest->ID = $request->input('jobadselect');
          $quest->Question = $req['Question'];
          $quest->save();

          $answers = $req['Answers'];
           $marks =  $req['Marks'];
           $tog = array_filter(array_combine($answers, $marks));
          foreach ($tog as $key=>$value)
          {
            $answers = new PreQualAnswers();
            $answers->Answers = $key;
            $answers->PreQualTestID = $quest->id;
            $answers->Marks = $value;
            $answers->save();

           }

      }
            // return redirect()->route('jobAds.show',$quest->ID);            
        return redirect()->route('jobAds.show',$quest->ID)->with('status','Job Test Successfully Created');
    }

    public function jobtestsView($id,$job)
    {
      $job=JobAd::where('ID','=',$job)->first();
      $prequalquestions =jobtests::where('ID','=',$job->ID)->get();

      return view('jobtests.view',compact('job','prequalquestions','id'));
    }
    public function jobtestsviewquestions($job)
    {
      $job=JobAd::where('ID','=',$job)->first();
      $prequalquestions =JobTests::where('ID','=',$job->ID)->get();
      $answers = array();
      foreach ($prequalquestions as $key => $value)
      {
        $answers[] = PreQualAnswers::where('PreQualTestID','=', $value->PreQualTestID)->get();
      }
        
        return view('jobtests.viewjobtests',compact('job','prequalquestions','answers'));
    }
    public function editjobtestsviewquestions($job)
    {
     $job=JobAd::where('ID','=',$job)->first();
     $prequalquestions =JobTests::where('ID','=',$job->ID)->get();
     $answers = array();
     foreach ($prequalquestions as $key => $value)
      {
        $answers[] = PreQualAnswers::where('PreQualTestID','=', $value->PreQualTestID)->get();
      }

     return view('jobtests.editjobtests',compact('job','prequalquestions','answers'));
        
    }
      public function updatejobtestsviewquestions(Request $request, $id)
  {
     //dd($request->all());
     $job=JobAd::where('ID','=',$id)->first();

    //  $prequalquestions = jobtests::where('ID','=',$job->ID)->first();

     $prequal= jobtests::where('ID','=',$job->ID)->get();
    //  $a = PreQualAnswers::where('PreQualTestID', $prequalquestions->PreQualTestID)->get();

    //  foreach($a as $aa){
    //     PreQualAnswers::where('AnswersID', $aa->AnswersID)->delete();

    //  }

     foreach($prequal as $p){
      PreQualAnswers::where('PreQualTestID', $p->PreQualTestID)->delete();
         jobtests::where('PreQualTestID','=',$p->PreQualTestID)->delete();
     }

     //
     // jobtests::where('ID','=',$job->ID)->delete();

     $requirement= $_POST['Question'];
     //dd($requirement);
     foreach ($requirement as $req){
      
      //dd($req);


         $quest = new JobTests();
         $quest->ID = $id;
         $quest->Question = $req['Question'];
         $quest->save();

         $answers = isset($req['Answers']) ? $req['Answers'] : array();
          $marks =  isset($req['Marks']) ? $req['Marks'] : array();
          $tog = array_filter(array_combine($answers, $marks));
         foreach ($tog as $key=>$value)
         {
           $answers = new PreQualAnswers();
           $answers->Answers = $key;
           $answers->PreQualTestID = $quest->id;
           $answers->Marks = $value;
           $answers->save();

          }
  }

    return redirect()->route('jobtests.index')->with('status','Job Test Successfully Updated');

}

 public function jobtestsdestroy($id,$jobAD){
      
     $job = JobTests::where('PreQualTestID', $id)->first();
     
     $answered = CandidateAnswers::where('ID',$job->ID)->get();
     
     if(count($answered) > 0){
       return redirect()->route('jobtests.viewquestions',$job->ID)->with('status','Question can not be deleted after candidates have answered');  
         
     }else{
        $a = PreQualAnswers::where('PreQualTestID', $id)->get();
        foreach ($a as $ans){
            PreQualAnswers::where('AnswersID', $ans->AnswersID)->delete();
             
        }
        JobTests::where('PreQualTestID','=',$job->PreQualTestID)->delete();
       
        
         return redirect()->route('jobtests.viewquestions',$jobAD)->with('status','Question has been deleted');   
     }
     
 }


}
