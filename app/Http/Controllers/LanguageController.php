<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Clients;
use App\Country;
use App\Currency;
use App\Industry;
use App\Location;
use App\Language;
use App\LanguageList;
use App\JobAd;
use App\SummitStaff;
use App\JobRequirement;
use App\JobDuty;
use App\PreQualTest;
use App\PreQualAnswers;
use App\CandidateAnswers;
use App\RegistrationDetails;
use App\CvTable;
use App\JobCV;
use App\User;

use Redirect;
use Toastr;
use Session;
use Validator;
use Auth;
use Notifications;
use DB;

class LanguageController extends Controller
{
    //

    public function index(){

        $languages = LanguageList::all();
        return view ('language.index', compact('languages'));

    }


    // }

    public function create(){

            //can change from here
           // $language = LanguageList::all();
            return view ('language.create');
    }

    public function store(Request $request){


        $validator = Validator::make($request->except('_token'), [
            'languagename' => ['required', 'string'],
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();

            // dd($errors);
            return Redirect::back()->withInput()->withErrors($errors);
        }else {

            $language = new LanguageList();
            $language->LanguageName = $request->input('languagename');
            $language->save();
            Toastr::success('Language has been created.');
            return redirect()->route('language.index');
        }
    }


    public function edit($id){

        $language = LanguageList::where('LanguageListID','=',$id)->first();
        return view ('language.edit',compact('language'));

    }

    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->except('_token'), [
            'languagename' => ['required', 'string'],
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();

            // dd($errors);
            return Redirect::back()->withInput()->withErrors($errors);
        }else {

            $insertdata = [
                'LanguageName' => $request->input('languagename')];

            // DB::table('languages')->where('LanguageListID', $id)->update($insertdata);
                DB::table('languagelist')->where('LanguageListID', $id)->update($insertdata);


            Toastr::success('Language has been update.');
            return redirect()->route('language.index');
        }
    }


    // public function view($id){
    //     $industry = Industry::where('ID','=', $id)->first();
    //     return view('industry.view',compact('industry'));
    // }
     

     public function delete($id){
        LanguageList::where('LanguageListID','=', $id)->delete();

        Toastr::success('Language has been deleted.');
        return redirect()->route('language.index');
    }

    
}
