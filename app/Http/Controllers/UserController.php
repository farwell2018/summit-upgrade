<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Ixudra\Curl\Facades\Curl;
use Illuminate\Database\Eloquent\Collection;

use App\User;
use App\SummitStaff;
use App\VerifyUser;
use App\Country;
use App\JobAd;
use App\Interview;
use App\Clients;
use Yajra\Datatables\Datatables;
use Auth;
use Toastr;
use Session;
use Validator;
use Mail;
use Redirect;
use DB;

class UserController extends Controller
{
  public function index(Request $request)
  {
     $countries = Country::all();
     $staff = SummitStaff::all();
     $jobss = JobAd::all();
     $users = User::where('role','=',0)->orWhere('role','=',1)->orderBy('id')->paginate(20);
    return view('user.index', compact('countries', 'users','staff','jobss'));
  } 
  public function json($companyId = null)
  {
      $query = User::where('role','=',0)->orWhere('role','=',1)->get();

      return Datatables::of($query)
          ->addColumn('first_name', function ($list) {
                return $list->SummitStaff->Firstname;
            })
          ->addColumn('last_name', function ($list) {
                return $list->SummitStaff->Lastname;
            })
          ->addColumn('role', function ($list) {
            if($list->role == 0){

              return 'Administrator';
            }else{
              return 'Staff';
            }
          })
          ->addColumn('status', function ($list) {
            if($list->status == 0){
              return 'Not Active';
            }else{
              return 'Active';
            }
            })
        ->addColumn('tools', function ($list) {
          return '
        <span style="overflow: visible; position: relative; width: 110px;">
          <a title="Edit details" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="' . route('users.edit', $list->SummitStaff->id) . '">
            <i class="fas fa-edit"></i>
          </a>

      </span>';
        })
        ->rawColumns(['tools'])
        ->make();
  }
    public function json2(Request $request)
  { 
      $collections = Interview::all();
      $value = '#';
      //var_dump(Auth::user()->SummitStaff->id);die;
      foreach ($collections as $key => $value)
      {

        $status = $request->input('status');
        if ($status) 
        {
          if ($value->Status != $status) 
          {
            unset($collections[$key]);
          }
        }
        if(Auth::user()->SummitStaff->StaffID != $value->StaffID){
            unset($collections[$key]);
        }
      }
      $query = new Collection($collections);

      return Datatables::of($query)
      ->addColumn('CV_ID', function($list){

        /*The candidates object is referenced from the interview's model*/
         return $list->candidates->RegDetails->Firstname.' '.$list->candidates->RegDetails->Lastname;
       })
       ->addColumn('JobAD_ID', function($list){
         return $list->jobs->JobTitle;
       })
       ->addColumn('StaffID', function($list){
         if(empty($list->staffs)){
           return '';
         }else{
           return $list->staffs->Firstname.' '.$list->staffs->Lastname;
         }
       })
        ->addColumn('InterviewDate',function($list){
         return date("d-m-Y", strtotime($list->InterviewDate));
       })
       ->addColumn('Actions', function ($list) { 
              return '
        <span style="overflow: visible; position: relative; width: 100px;">
        <a title="View details" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="'.route('candidateCV',[$list->CV_ID, $list->JobAD_ID]).'">
           <i class="fas fa-eye" style="color:#80CFFF"></i>
          </a>
          <a title="Edit details" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="' . route('interviewmanagement.edit', $list->InterviewID) . '">
            <i class="fas fa-edit" style="color: #3069AB"></i>
          </a>
      </span>';              
        })
        ->rawColumns(['Actions'])
        ->make();
  }
  public function create()
  {
    return view ('user.create');
  }
  public function store(Request $request)
  {
    $validator = Validator::make($request->except('_token'), [
    'first_name' => ['required', 'string', 'max:255'],
    'last_name' => ['required', 'string', 'max:255'],
    'email_address' => ['required', 'string', 'email', 'max:255'],
    'password' => ['required', 'string', 'min:8', 'confirmed'],
    'account_type' => ['required'],
        ],
      [
        'email.unique' => 'An account with a similar email address already exists.'
      ]
      );
    if ($validator->fails()) 
      {
        $errors = $validator->errors();
        return Redirect::back()->withInput()->withErrors($errors);
      }else
      {
        $user = new User();
        $user->email = $request->input('email_address');
        $user->password = Hash::make($request->input('password'));
        $user->confirmpassword = Hash::make($request->input('password'));
        $user->role = $request->input('account_type');
        $user->status = 0;
        $user->save();
        $staff= new SummitStaff();
        $staff->AccUserID = $user->id;
        $staff->Firstname = $request->input('first_name');
        $staff->Lastname = $request->input('last_name');
        $staff->EmailAddress = $request->input('email_address');
        $staff->PhoneNumber = $request->input('phone');
        $staff->designation = $request->input('designation');
        $staff->status = 1;
        $staff->save();
        $token = self::generateRandomString(16);
        $validate = new VerifyUser();
        $validate->user_id = $user->id;
        $validate->token = $token;
        $validate->save();
        Mail::send('emails.staff_verify',['name' => $staff->Firstname.' '.$staff->Lastname,'email'=>$staff->EmailAddress, 'password'=> $request->input('password'), 'url' => route('verify.account', $validate->token)],
              function ($message) use ($user) {
                $message->from('support@farwell-consultants.com', 'Welcome to Summit Recruitment');
                $message->to($user->email, $user->first_name);
                $message->subject('Account Verification');
        });
          Toastr::success('User has been created.');
          return redirect()->route('users.index');
      }
  }
public function show($id)
{
  $user = User::where('id','=', $id)->first();
  $staff = SummitStaff::where('AccUserID','=',$user->id)->first();
  if(!empty($user->SummitStaff)){
     $interviewed = Interview::where('Status','=','Interviewed')->where('StaffID','=',$user->SummitStaff->StaffID)->get();
  $placed = Interview::where('Status','=','Placed')->where('StaffID','=',$user->SummitStaff->StaffID)->get();
  $jobss = JobAd::where('StaffID','=',$user->SummitStaff->StaffID)->get();
  $jobs = JobAd::where('StaffID','=',$user->SummitStaff->StaffID)->orderBy('ID','DESC')->paginate(60);
  $clients = Clients::where('staff','=',$user->SummitStaff->StaffID)->get();
  return view('user.view', compact('user','jobs','interviewed','placed','jobss','clients')); 
      
  }else{
    
  return view('user.view2', compact('user'));
  }
  
}
public function edit($id)
{
  $user = SummitStaff::where('StaffID','=', $id)->first();
  return view('user.edit', compact('user'));
}
public function update(Request $request, $id)
{
  $validator = Validator::make($request->except('_token'), [
  'first_name' => ['required', 'string', 'max:255'],
  'last_name' => ['required', 'string', 'max:255'],
  'email_address' => ['required', 'string', 'email', 'max:255'],
  'password' => ['required', 'string', 'min:8', 'confirmed'],
  'account_type' => ['required'],
      ],
    [
      'email.unique' => 'An account with a similar email address already exists.'
    ]
    );
  if ($validator->fails()) {
      $errors = $validator->errors();
      return Redirect::back()->withInput()->withErrors($errors);
    }else{
        
        $data = $request->all();
        $staff = SummitStaff::where('StaffID','=', $id)->first();
        
        DB::table('summit_staff')
        ->where('StaffID', $id)
        ->update([
      'Firstname' => $data['first_name'],
      'Lastname'=> $data['last_name'],
      'EmailAddress' => $data['email_address'],
      'PhoneNumber' => $data['phone'],
      'designation' => $data['designation'],
      'status' => 1,
      ]);
      
      DB::table('users')
        ->where('id', $staff->AccUserID)
        ->update([
      'email' => $data['email_address'],
      'password' => Hash::make($data['password']),
      'role' => $data['account_type'],
     'status' => 1,
      ]);
      if(Auth::user()->role == 0){
        Toastr::success('User successfully updated.');
        return redirect()->route('users.index');
      }else{
        Toastr::success('User successfully updated.');
        return redirect()->route('dashboard');
      }
    }
}
  public function profile(Request $request)
  {
    $user = Auth::user();
    if ($request->isMethod('post')) {
      $request->validate([
        'first_name' => ['required', 'string', 'max:255'],
        'last_name' => ['required', 'string', 'max:255'],
      ]);
      $staff = SummitStaff::where('AccUserID','=',$user->id)->first();
      $staff->Firstname = $request->input('first_name');
      $staff->Lastname = $request->input('last_name');
      $staff->PhoneNumber = $request->input('phone');
      $staff->designation = $request->input('designation');
      if ($staff->update()) {
         Toastr::success('Your profile has been successfully updated.');
        return  view('user.profile', ['user' => $user]);
      } else {
        Toastr::error('There was a problem updating your profile. Please try again later.');
        return  view('user.profile', ['user' => $user]);
      }
    }
    if(strpos(Auth::user()->email,'noemail')){
      $user->email = '';
    }
    return view('user.profile', ['user' => $user]);
  }
  public function profileImage(Request $request, $id)
  {
     if (!is_null($request->file('profile_image'))) 
    {
      $extension = $request->file('profile_image')->getClientOriginalExtension();
      $user = User::where('id','=',$id)->first();
      $fileName = $user->SummitStaff->Firstname.'-'.$user->SummitStaff->Lastname.'.' . $extension;
      $originalName = $fileName;
      $originalNameWithoutExt = substr($originalName, 0, strlen($originalName) - strlen($extension) - 1);
      $upload = $request->file('profile_image')->move('uploads/staff/', $originalName);//
        if ($upload)
        {
          $staff = SummitStaff::where('AccUserID','=',$id)->first();
          $staff->profilePhoto = $originalName;
          $staff->update();
          Toastr::success('User profile image updated.');
          return redirect()->back();
        }
    }
  }
  public function password(Request $request)
  {
    //Get user
    $user = Auth::user();
    if ($request->isMethod('post')) {
      //Validate
      $request->validate([
        'password' => ['required', 'string', 'min:8'],
      ]);

      //Get password
      $password = $request->input('password');

      //Save changes
      if ($user->update(['password' => Hash::make($password)])) {
        $this->resetEdxPassword($user, $password);
        Toastr::success('Password successfully updated.');
      } else {
        Toastr::error('There was a problem updating your password.');
      }
    }
    return view('user.password', ['user' => $user]);
  }
  public function picture(Request $request)
  {
    if (!Auth::check()) {
      return response('Unauthorized', 403);
    }
    $user = Auth::user();
    //Save to storage
    $ppic = $request->file('ppic');
    $extension = $ppic->getClientOriginalExtension();
    $filename = $ppic->getFilename() . '.' . $extension;
    Storage::disk('public')->put($filename,  File::get($ppic));
    //Delete old file
    Storage::disk('public')->delete($user->ppicFilename);
    //Save to user
    $user->ppicFilename = $filename;
    if ($user->save()) {
      //Show session message
      $redirectMessage = [
        'title' => 'Profile photo updated',
        'content' => 'Your profile photo has been successfully updated.',
      ];
    } else {
      //Show session message
      $redirectMessage = [
        'title' => 'Profile photo update failed',
        'content' => 'Profile photo updating failed. Please try again later.',     
      ];
    }
    Session::flash('redirectMessage', $redirectMessage);
    return back();
  }
  public function permissions($userId,Request $request){
    //Get user
    $user = User::findOrFail($userId);
    if ($request->isMethod('put')) {
        //Validate
        $request->validate([
          'general' => ['required'],
        ]);
        //Update general
        $user->is_admin = $request->input('general');
        // $user->is_ikadmin = $request->input('general');
        $user->save();
        //Delete all user related company admins
        CompanyAdmin::where('user_id',$user->id)->delete();
        if($request->input('company')){
          //Set company admin
          $companyAdmin = new CompanyAdmin;
          $companyAdmin->user_id = $user->id;
          $companyAdmin->company_id = $request->input('company');
          $companyAdmin->save();
        }
        Toastr::success('User successfully updated.');
      }
    return view('user.permissions',['user'=>$user,'companies'=>Company::all()]);
  }
  
  
 public function delete($userId,Request $request){
     DB::table('users')
        ->where('id', $userId)
        ->update(['status' => 0,]);
     Toastr::success('User successfully deactivated.');
     return redirect()->route('users.index');
     
     
 }
 
  public function activate($userId,Request $request){
     DB::table('users')
        ->where('id', $userId)
        ->update(['status' => 1,]);
     Toastr::success('User successfully deactivated.');
     return redirect()->route('users.index');
     
     
 }
 
  protected function generateRandomString($length = 10)
  {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[ rand(0, $charactersLength - 1) ];
    }
    return $randomString;
  }
}
