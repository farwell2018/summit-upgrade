<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Clients;
use App\Country;
use App\Currency;
use App\Industry;
use App\Location;
use App\JobAd;
use App\SummitStaff;
use App\JobRequirement;
use App\JobDuty;
use App\PreQualTest;
use App\PreQualAnswers;
use App\CandidateAnswers;
use App\RegistrationDetails;
use App\CvTable;
use App\JobCV;
use App\User;
use App\HardSkill;

use paginate;
use Redirect;
use Toastr;
use Session;
use Validator;
use Auth;
use Notifications;
use DB;

class CountryController extends Controller
{
    //

    public function index(){
        
        $countries = Country::all();
        return view ('country.index', compact('countries'));

    }



    public function create(){

        // $industries = Industry::all();
        return view ('country.create');
    }

    public function store(Request $request){


        $validator = Validator::make($request->except('_token'), [
            'countryname' => ['required', 'string'],
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();

            // dd($errors);
            return Redirect::back()->withInput()->withErrors($errors);
        }else {

            $country = new Country();
            $country->CountryName = $request->input('countryname');
            $country->save();
            Toastr::success('country has been created.');
            return redirect()->route('country.index');
        }
    }


    public function edit($id){

        $country = Country::where('CountryID','=',$id)->first();
        return view ('country.edit',compact('country'));

    }

    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->except('_token'), [
            'countryname' => ['required', 'string'],
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();

            // dd($errors);
            return Redirect::back()->withInput()->withErrors($errors);
        }else {

            $insertdata = [
                'CountryName' => $request->input('countryname')];

            DB::table('countries')->where('CountryID', $id)->update($insertdata) ;

            Toastr::success('Country has been update.');
            return redirect()->route('country.index');
        }
    }


    // public function view($id){
    //     $industry = Industry::where('ID','=', $id)->first();
    //     return view('industry.view',compact('industry'));
    // }
     

     public function delete($id){
        Country::where('CountryID','=', $id)->delete();

        Toastr::success('Country has been deleted.');
        return redirect()->route('country.index');
    }

    
}
