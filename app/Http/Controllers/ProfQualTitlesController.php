<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Clients;
use App\Country;
use App\Currency;
use App\Industry;
use App\Location;
use App\JobAd;
use App\SummitStaff;
use App\JobRequirement;
use App\JobDuty;
use App\PreQualTest;
use App\ProfQualTitles;
use App\PreQualAnswers;
use App\CandidateAnswers;
use App\RegistrationDetails;
use App\CvTable;
use App\JobCV;
use App\User;
use App\HardSkill;

use Redirect;
use Toastr;
use Session;
use Validator;
use Auth;
use Notifications;
use DB;

class ProfQualTitlesController extends Controller
{
    //

    public function index(){

        $profqualtitles = ProfQualTitles::all();
        return view ('profqualtitles.index', compact('profqualtitles'));

    }
// hadi hapa ina fanya


    // public function json()
    // {
    //     print_r("Adadasdasd");die;
    //     $query = Industry::orderBy('id','ASC')->get();
    //     print_r($query);die;
    //     return Datatables::of($query)

    //         ->addColumn('full_salary', function ($list) {
    //             return $list->SalCurrency.', '.$list->GrossMonthSal;
    //         })
    //         ->addColumn('client', function ($list) {
    //             return $list->Clients->CompanyName;
    //         })
    //         ->rawColumns(['tools'])
    //         ->make();

    // }

    public function create(){

        // $industries = Industry::all();
        return view ('profqualtitles.create');
    }

    public function store(Request $request){


        $validator = Validator::make($request->except('_token'), [
            'profqualtitlesname' => ['required', 'string'],
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();

            // dd($errors);
            return Redirect::back()->withInput()->withErrors($errors);
        }else {

            $profqualtitle = new ProfQualTitles();
            $profqualtitle->profqualtitle = $request->input('profqualtitlesname');
            $profqualtitle->save();
            Toastr::success('Qualification been created.');
            return redirect()->route('profqualtitles.index');
        }
    }
//hadi hapa inafanya kazi

    public function edit($id){

        $profqualtitle = ProfQualTitles::where('profqualtitleID','=',$id)->first();
        return view ('profqualtitles.edit',compact('profqualtitle'));

    }

    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->except('_token'), [
            'profqualtitlesname' => ['required', 'string'],
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();

            // dd($errors);
            return Redirect::back()->withInput()->withErrors($errors);
        }else {

            $insertdata = [
                'profqualtitle' => $request->input('profqualtitlesname')];

            DB::table('prof_qual_titles')->where('profqualtitleID', $id)->update($insertdata);

            Toastr::success('Qualification has been update.');
            return redirect()->route('profqualtitles.index');
        }
    }


    // public function view($id){
    //     $industry = Industry::where('ID','=', $id)->first();
    //     return view('industry.view',compact('industry'));
    // }
     

     public function delete($id){
       ProfQualTitles::where('profqualtitleID','=', $id)->delete();

        Toastr::success('Qualification has been deleted.');
        return redirect()->route('profqualtitles.index');
    }

    
}
