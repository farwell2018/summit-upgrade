<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Clients;
use App\Country;
use App\Currency;
use App\Industry;
use App\Location;
use App\JobAd;
use App\SummitStaff;
use App\JobRequirement;
use App\JobDuty;
use App\PreQualTest;
use App\PreQualAnswers;
use App\CandidateAnswers;
use App\RegistrationDetails;
use App\CvTable;
use App\JobCV;
use App\User;
use App\HardSkill;

use Redirect;
use Toastr;
use Session;
use Validator;
use Auth;
use Notifications;
use DB;

class HardSkillController extends Controller
{
    //

    public function index(){

        $hardskills = HardSkill::all();
        return view ('hardskill.index', compact('hardskills'));

    }

    // public function json()
    // {
    //     print_r("Adadasdasd");die;
    //     $query = Industry::orderBy('id','ASC')->get();
    //     print_r($query);die;
    //     return Datatables::of($query)

    //         ->addColumn('full_salary', function ($list) {
    //             return $list->SalCurrency.', '.$list->GrossMonthSal;
    //         })
    //         ->addColumn('client', function ($list) {
    //             return $list->Clients->CompanyName;
    //         })
    //         ->rawColumns(['tools'])
    //         ->make();

    // }

    public function create(){

         $hardskills = HardSkill::all();
        // $industries = Industry::all();
        return view ('hardskill.create');
    }

    public function store(Request $request){


        $validator = Validator::make($request->except('_token'), [
            'hardskillname' => ['required', 'string'],
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();

            // dd($errors);
            return Redirect::back()->withInput()->withErrors($errors);
        }else {

            $hardskill = new HardSkill();
            $hardskill->Name = $request->input('hardskillname');
            $hardskill->save();
            Toastr::success('Hardskill has been created.');
            return redirect()->route('hardskill.index');
        }
    }


    public function edit($id){

        $hardskill = HardSkill::where('ID','=',$id)->first();
        return view ('hardskill.edit',compact('hardskill'));

    }

    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->except('_token'), [
            'hardskillname' => ['required', 'string'],
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();

            // dd($errors);
            return Redirect::back()->withInput()->withErrors($errors);
        }else {

            $insertdata = [
                'Name' => $request->input('hardskillname')];

            DB::table('hardskills')->where('ID', $id)->update($insertdata);

            Toastr::success('Hardskill has been updated.');
            return redirect()->route('hardskill.index');
        }
    }
    //  public function show($id)
    //  {

    // $hardskill = HardSkill::where('ID','=', $id)->first();
    // $hardskill = HardSkill::where('Name','=',$hardskillname)->get();
    // // $duties = JobDuty::where('JobID','=',$id)->get();
    // $hardskill = HardSkill::where('ID','=',$id)->paginate(10);

    // return view('hardskill.view',compact('ID','Name'));
    //  }


 
    public function view($id){
        $hardskill = HardSkill::where('ID','=', $id)->first();
        $hardskill = HardSkill::where('Name','=',$hardskillname)->get();
        $hardskill = HardSkill::where('ID','=', $id)->paginate(20);

        
        return view('hardskill.view',compact('HardSkill'));
    }
     

     public function delete($id)

     {
        Hardskill::where('ID','=', $id)->delete();

        Toastr::success('Hardskill has been deleted.');
        return redirect()->route('hardskill.index');
    }

    
}
