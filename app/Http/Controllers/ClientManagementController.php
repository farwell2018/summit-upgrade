<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\Eloquent\Collection;

use App\Clients;
use App\Country;
use App\Currency;
use App\Industry;
use App\Location;
use App\JobAd;
use App\SummitStaff;
use App\JobRequirement;
use App\JobDuty;
use App\PreQualTest;
use App\PreQualAnswers;
use App\CandidateAnswers;
use App\RegistrationDetails;
use App\CvTable;
use App\JobCV;
use App\User;
use App\Interview;
use App\JobTests;

use Redirect;
use Toastr;
use Session;
use Validator;
use Auth;
use Notifications;
use DB;

class ClientManagementController extends Controller
{

    public function index()
    {
      // $clients = Clients::orderBy('ClientID','asc')->get();
      $clients = Clients::orderBy('ContactPerson')->paginate(10);
      $users = User::all();
      $jobs = JobAd::all();
      $interview = Interview::all();
      $staff = SummitStaff::orderBy('Firstname')->get();
      $industries = Industry::orderBy('Name')->get();
      $clientsss = Clients::all();
      $cold = Clients::where('status','=',1)->get();
      $warm = Clients::where('status','=',2)->get();
      $hot= Clients::where('status','=',3)->get();
      $active= Clients::where('status','=',4)->get();
      $activenotloyal= Clients::where('status','=',5)->get();
      $inactiveloyal= Clients::where('status','=',6)->get();
      $inactivenotloyal= Clients::where('status','=',7)->get();

      return view ('clientmanagement.index', compact('clients','users','jobs','interview','staff','industries','clientsss','cold','warm','hot','active','activenotloyal','inactiveloyal','inactivenotloyal'));
    }
     public function json(Request $request)
  {

        $collections = Clients::orderBy('ClientID','Desc')->get();

        foreach ($collections as $key =>$value){
           
          $company = $request->input('company');
          $status = $request->input('status');
          $industry = $request->input('industry');
          $staff = $request->input('staff');


            if($staff)
          {
            
           if($value->StaffID != $staff)
           {
             unset($collections[$key]);
           }
          }
          if($industry)
          {
            if($value->IndustrySector != $industry)
            {
              unset($collections[$key]);
            }
          }

          if($company){
             $searchTerm = strtolower($value->CompanyName);
             $str = strtolower($company);
             $pos = strstr($searchTerm, $str);
            if($pos === false){
              unset($collections[$key]);
            }
          }

          if($status){
            if($value->status != $status){
              unset($collections[$key]);
            }
          }
        }
    $query = new Collection($collections);

      return Datatables::of($query)

      	->addColumn('status', function($list){

        $value = '#';

          if ($list->status == 1) {
          $value = '<span class="badge badge-primary">Cold Lead</span>';
        } elseif ($list->status == 2) {
          $value = '<span class="badge badge-warning">Warm Lead</span>';
        } elseif ($list->status == 3) {
          $value = '<span class="badge badge-danger">Hot Lead</span>';
        } elseif ($list->status == 4) {
          $value = '<span class="badge badge-success">Active Loyal</span>';
        } elseif ($list->status == 5) {
          $value = '<span class="badge badge-success">Active Not Loyal</span>';
        } elseif ($list->status == 6) {
          $value = '<span class="badge badge-success">Inactive Loyal</span>';
        } elseif ($list->status == 7) {
          $value = '<span class="badge badge-primary">Inactive Not Loyal</span>';
        }

         return $value;

       })

        ->addColumn('tools', function ($list) {
          if (Auth::user()->role == 0)
          {
        		 return
            '
        <span style="overflow: visible; position: relative; width: 110px;">
        <a title="View Client" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="'.route('clientmanagement.show',$list->ClientID).'">
           <i class="fas fa-eye" style="color:#80CFFF"></i>
          </a>
          <a title="Edit Client Status" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="' . route('clientmanagement.edit', $list->ClientID) . '">
            <i class="fas fa-edit" style="color: #3069AB"></i>
          </a>
          <a
            title="Delete"
            class="btn btn-sm btn-clean btn-icon btn-icon-sm"
            data-href="'.route('clientmanagement.delete',['id'=>$list->ClientID]).'"
            data-toggle="modal"
            data-target="#deleteConfirmModal"
            >
            <i class="far fa-trash-alt" style="color:#FA0000"></i>
          </a>

      </span>';
        ;}
        else{

             return '
        <span style="overflow: visible; position: relative; width: 100px;">
        <a title="View details" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="'.route('clientmanagement.show',$list->ClientID).'">
           <i class="fas fa-eye" style="color:#80CFFF"></i>
          </a>
          <a title="Edit details" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="' . route('clientmanagement.edit', $list->ClientID) . '">
            <i class="fas fa-edit" style="color: #3069AB"></i>
          </a>
      </span>';
    }
        })
        ->rawColumns(['status','tools'])
        ->make();

  }
  public function create(){
  	$SummitStaff = SummitStaff::orderBy('Firstname')->get();
  	$industries = Industry::orderBy('name')->get();


    return view ('clientmanagement.create',compact('industries','SummitStaff'));
  }
  public function store(Request $request){

    $validator = Validator::make($request->except('_token'), [
    'company_name' => ['required'],
    'industry' => ['required'],
    'physical_address' => ['required'],
    'contact_person' => ['required'],
    'contact_title'=>['required'],
    'contact_email'=>['required'],
    'phone_number'=>['required'],
    'consultant' =>['required'],
    'comment' =>['required'],
    'date' =>['required'],

      ]);
    if ($validator->fails()) {
        $errors = $validator->errors();

      return Redirect::back()->withInput()->withErrors($errors);
      }else{
        $client = new Clients();
        $client->CompanyName = $request->input('company_name');
        $client->IndustrySector =  $request->input('industry');
        $client->PhysAdd = $request->input('physical_address');
        $client->postalAdd = $request->input('postal_address');
        $client->ContactPerson = $request->input('contact_person');
        $client->ContactTitle =  $request->input('contact_title');
        $client->EmailAdd = $request->input('contact_email');
        $client->PhoneNumber = $request->input('phone_number');
        $client->AContactPerson = $request->input('second_contact_person');
        $client->AContactTitle =  $request->input('second_contact_title');
        $client->AEmailAdd = $request->input('second_contact_email');
        $client->APhoneNumber = $request->input('second_phone_number');
        $client->AAContactPerson = $request->input('third_contact_person');
        $client->AAContactTitle= $request->input('third_contact_title');
        $client->AAEmailAdd= $request->input('third_contact_email');
        $client->AAPhoneNumber= $request->input('third_phone_number');
        $client->Website = $request->input('website');
        $client->StaffID = $request->input('consultant');
        $client->source = $request->input('sourceSelect1');
        $client->lastcommunication = $request->input('datelastcomm'); 	
        $client->comment = $request->input('comment');
        $client->client_regon = $request->input('date');
        $client->save();

        Toastr::success('Client has been created.');
        return redirect()->route('clientmanagement.index');
      }

  }
  public function edit($id){

    $client = Clients::where('ClientID','=',$id)->first();

    if ($client->status == 1) {
      $client->status = 'Cold Lead';
    } elseif ($client->status == 2) {
      $client->status = 'Warm Lead';
    } elseif ($client->status == 3) {
      $client->status = 'Hot Lead';
    } elseif ($client->status == 4) {
      $client->status = 'Active Loyal';
    } elseif ($client->status == 5) {
      $client->status = 'Active Not Loyal';
    } elseif ($client->status == 6) {
      $client->status = 'Inactive Loyal';
    } elseif ($client->status == 7) {
      $client->status = 'Inactive Not Loyal';
    }
    // Client source variables
    if ($client->source == 1) {
      $client->source = 'Website';
    } elseif ($client->source == 2) {
      $client->source = 'Recomendation';
    } elseif ($client->source == 3) {
      $client->source = 'Refferal';
    } elseif ($client->source == 4) {
      $client->source = 'Mainline';
    } elseif ($client->source == 5) {
      $client->source = 'Cold Approach';
    } elseif ($client->source == 6) {
      $client->source = 'LPO';
    }
    // Initial Communication variables
    if ($client->initial_comm == 1) {
      $client->initial_comm = 'Email';
    } elseif ($client->initial_comm== 2) {
      $client->initial_comm = 'Phone Call';
    } elseif ($client->initial_comm == 3) {
      $client->initial_comm = 'Skype Call';
    } elseif ($client->initial_comm == 4) {
      $client->initial_comm = 'Face to Face Meeting';
    }

  	$industries = Industry::orderBy('name')->get();
    return view ('clientmanagement.edit',compact('client','industries'));
  }
  public function update(Request $request, $id)
  {
    $validator = Validator::make($request->except('_token'), [
    'company_name' => ['required'],
    'industry' => ['required'],
    'physical_address' => ['required'],
    'contact_person' => ['required'],
    'contact_title'=>['required'],
    'contact_email'=>['required'],
    'phone_number'=>['required'],
        ]);
    $data = $request->all();
    
   

     if(isset($data['client_meet_option']) && $data['client_meet_option'] == 1){
        $validator = Validator::make($request->except('_token'), [
          'client_meet' => ['required'],
        ]);

     }
     if (isset($data['client_proposal_option']) && $data['client_proposal_option'] == 1) {
        $validator = Validator::make($request->except('_token'), [
          'proposal_request' => ['required'],
        ]);

      }

    if(isset($data['sla_shared_option']) && $data['sla_shared_option'] == 1){

        $validator = Validator::make($request->except('_token'), [
          'sla_shared' => ['required'],
        ]);
     }
    if(isset($data['sla_signed_option']) && $data['sla_signed_option']==1){
        $validator = Validator::make($request->except('_token'), [
          'sla_signed' => ['required'],
        ]);

    }
    if(isset($data['status_update_option']) && $data['status_update_option'] == 1){
        $validator = Validator::make($request->except('_token'), [
          'status_update' => ['required'],
        ]);
     }
    if ($validator->fails()) {
      $errors = $validator->errors();

      return Redirect::back()->withInput()->withErrors($errors);
    }else{

       $data = $request->all();
       $client = Clients::where('ClientID','=',$id)->first();

     if (isset($data['company_name'])) {
        // Update Status -> warm lead
       if ($data['company_name'] && $data['contact_person'] && $data['contact_email']){
         $status = 1;
       }
      }

      if (isset($data['sourceSelect1'])) {
          // Update Status -> warm lead
         if ($data['sourceSelect1'] && $data['sourceSelect1'] && $data['sourceSelect1']){
           $status = 2;
         }
      }
      if (isset($data['client_meet'])) {
         // Update Status -> hot lead
       if ($data['client_meet']){
         $status = 3;
        }
      }
      if (isset($data['sla_shared'])) {
        if ($data['sla_shared']) {
          $status = 4; // Update Status -> active loyal
        }
      }
      if (isset($data['status_update'])) {
        if ($data['status_update']) {
          $status = $data['status_update'];// Update Status -> specified
        }
      }
        if (isset($data['proposal_request'])) {

        $file = $request->file('proposal_request');

        // check file type
        $extensions = ['doc', 'docx', 'pdf', 'png', 'jpg', 'jpeg','csv'];
        $data['proposal_request'] = $file->getClientOriginalName();
        if (!in_array($file->getClientOriginalExtension(), $extensions)) {
            //return 'file type not supported';
        }
          
        // } elseif ($file->getSize() > 482000) {
        //      dd($file->getSize());
        //   //return 'file size is larger that 5MB.';
        // } 
        else {
            
          $data['proposal_request'] = str_replace('/tmp/', '', $data['proposal_request']);
          //Move Uploaded File
          $destinationPath = '/clients/bdm_proposal';
           
          $file->move(public_path($destinationPath), $data['proposal_request']);
          //dd($file->move($destinationPath, $data['proposal_request']));
        }
      } else {
        $data['proposal_request'] = $client->bdm_proposal;

      }
      
      
             if (isset($data['sla_request'])) {

        $file = $request->file('sla_request');

        // check file type
        $extensions = ['doc', 'docx', 'pdf', 'png', 'jpg', 'jpeg','csv'];
        $data['sla_request'] = $file->getClientOriginalName();
        if (!in_array($file->getClientOriginalExtension(), $extensions)) {
            //return 'file type not supported';
        }
          
        // } elseif ($file->getSize() > 482000) {
        //      dd($file->getSize());
        //   //return 'file size is larger that 5MB.';
        // } 
        else {
            
          $data['sla_request'] = str_replace('/tmp/', '', $data['sla_request']);
          //Move Uploaded File
          $destinationPath = '/clients/sla';
           
          $file->move(public_path($destinationPath), $data['sla_request']);
          //dd($file->move($destinationPath, $data['proposal_request']));
        }
      } else {
        $data['sla_request'] = $client->sla_proposal;

      }
      

         DB::table('client_info')
        ->where('ClientID', $id)
        ->update([
        'CompanyName' => $data['company_name'],
        'IndustrySector' =>  @$data['industry'],
        'PhysAdd' => @$data['physical_address'],
        'postalAdd' => @$data['postal_address'],
        'ContactPerson' => @$data['contact_person'],
        'ContactTitle' =>  @$data['contact_title'],
        'EmailAdd' => @$data['contact_email'],
        'PhoneNumber' => @$data['phone_number'],
        'AContactPerson' => @$data['second_contact_person'],
        'AContactTitle' => @$data['second_contact_title'],
        'AEmailAdd' => @$data['second_contact_email'],
        'APhoneNumber' => @$data['second_phone_number'],
        'AAContactPerson' => @$data['third_contact_person'],
        'AAContactTitle' => @$data['third_contact_title'],
        'AAEmailAdd' => @$data['third_contact_email'],
        'AAPhoneNumber' => @$data['third_phone_number'],
        'Website' => @$data['website'],
        'kra_pin' => @$data['input_kra_pin'],
        'staff' => @$data['no_Staff'],
        'offices' => @$data['no_offices'],
        'hq' => @$data['hq'],
        'source' => @$data['sourceSelect1'],
        'd_o_c' => @$data['1stcommunication'],
        'initial_comm' => @$data['initial_communication'],
        'lastcommunication'=>@$data['dateoflastcommunication'],
        'sla_expirationdate'=>@$data['slaexpirationdate'],
        'status' => @$status,
        'bdm_date' => @$data['client_meet'],
        'bdm_proposal' => @$data['proposal_request'],
        'sla_proposal' => @$data['sla_request'],
        'sla_date' => @$data['sla_shared'],
        'comment'=>@$data['commentconsultant'],
        'sla_signed' => @$data['sla_signed'],
        'client_bdm_proposal_action'=>@data['client_proposal_option'],
        ]);
      return redirect()->route('clientmanagement.index');
        }
    }
      public function show($id){

        $client = Clients::where('ClientID',$id)->first();
         $staffassigned=SummitStaff::where('StaffID',$client->StaffID)->first();
        $jobAds = JobAd::where('ClientID',$id)->orderBy('ID','Desc')->paginate(10);
        return view('clientmanagement.view',compact('client','jobAds','staffassigned'));
    }
    
    public function delete(Request $request,$id)
    {
      $job = JobAd::where('ClientID',$id)->first();
      if(!empty($job)){
        $test = JobTests::where('ID',$job->ID)->get();
        $duties = JobDuty::where('JobID',$job->ID)->get();
        $requirements = JobRequirement::where('JobID',$job->ID)->get();
       
       if($test){
         foreach($test as $tests){
             $r= PreQualAnswers::where('PreQualTestID',$tests->PreQualTestID)->get();
          
          if($r){
            CandidateAnswers::where('ID',$job->ID)->delete();
            foreach($r as $f){
            PreQualAnswers::where('AnswersID', $f->AnswersID)->delete();
            }
          }
         }
      }
      if($duties){
          foreach($duties as $duty){
             JobDuty::where('ID',$duty->ID)->delete(); 
          }
      }
        if($requirements){
          foreach($requirements as $requirement){
             JobRequirement::where('ID',$requirement->ID)->delete(); 
          }
      }
        JobTests::where('ID',$job->ID)->delete();
        JobCV::where('Job_adID',$job->ID)->delete();
        JobAd::where('ClientID',$id)->delete();
       }

      Clients::where('ClientID',$id)->delete();

        return redirect()->route('clientmanagement.index')->with('status','Client has been Deleted');
    }
    
}
