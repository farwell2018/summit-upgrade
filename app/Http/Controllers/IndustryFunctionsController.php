<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Clients;
use App\Country;
use App\Currency;
use App\Industry;
use App\IndustryFunctions;
use App\Location;
use App\JobAd;
use App\JobRequirement;
use App\JobDuty;
use App\SummitStaff;
use App\PreQualTest;
use App\PreQualAnswers;
use App\CandidateAnswers;
use App\RegistrationDetails;
use App\JobCV;
use App\CvTable;
use App\User;

use DB;
use Redirect;
use Toastr;
use Session;
use Validator;
use Auth;
use Notifications;
//to start from here


class IndustryFunctionsController extends Controller
{
    //

    public function index(){

        $industryfunctions = IndustryFunctions::orderBy('name')->get();
        return view ('industryfunctions.index', compact('industryfunctions'));

    }

    // public function json()
    // {
    //     print_r("Adadasdasd");die;
    //     $query = Industry::orderBy('id','ASC')->get();
    //     print_r($query);die;
    //     return Datatables::of($query)

    //         ->addColumn('full_salary', function ($list) {
    //             return $list->SalCurrency.', '.$list->GrossMonthSal;
    //         })
    //         ->addColumn('client', function ($list) {
    //             return $list->Clients->CompanyName;
    //         })
    //         ->rawColumns(['tools'])
    //         ->make();

    // }

    public function create(){

         $industryfunctions = IndustryFunctions::orderBy('name')->get();
        // $industries = Industry::all();
        return view ('industryfunctions.create');
    }

    public function store(Request $request){

        $validator = Validator::make($request->except('_token'), [
            'industryfunctionsname' => ['required', 'string'],
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();

            // dd($errors);
            return Redirect::back()->withInput()->withErrors($errors);
        }else {

            $industryfunctions = new IndustryFunctions();                       
            $industryfunctions->Name = $request->input('industryfunctionsname');
            $industryfunctions->save();
            Toastr::success('Industry function has been created.');
            return redirect()->route('industryfunctions.index');
        }
    }

     public function edit($id){

        $industryfunction = IndustryFunctions::where('ID','=',$id)->first();
        return view ('industryfunctions.edit',compact('industryfunctions'));

    }
    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->except('_token'), [
            'industryfunctionsname' => ['required', 'string'],
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();

            // dd($errors);
            return Redirect::back()->withInput()->withErrors($errors);
        }else {

            $insertdata = [
                'Name' => $request->input('industryfunctionsname')];

            DB::table('industryfunctions')->where('ID', $id)->update($insertdata);

            Toastr::success('Industry function has been update.');
            return redirect()->route('industryfunctions.index');
        }
    }
//wacha tuone kama inafanya

    // public function view($id){
    //     $industry = Industry::where('ID','=', $id)->first();
    //     return view('industry.view',compact('industry'));
    // }
     

     public function delete($id){
        IndustryFunctions::where('ID','=', $id)->delete();
        Toastr::success('Industry function has been deleted.');
        return redirect()->route('industryfunctions.index');
    }
    
}
