<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;

use App\Clients;
use App\Country;
use App\Currency;
use App\Industry;
use App\Location;
use App\JobAd;
use App\SummitStaff;
use App\JobRequirement;
use App\JobDuty;
use App\PreQualTest;
use App\PreQualAnswers;
use App\CandidateAnswers;
use App\RegistrationDetails;
use App\CvTable;
use App\JobCV;
use App\User;
use App\Interview;
use App\JobTests;
use App\CreateCampaigns;
use App\HardSkill;
use App\ProfQualTitles;
use App\IndustryFunctions;
use App\PersonalSummary;
use App\competencies;
use App\JobCompetencies;

use Mail;
use Redirect;
use Toastr;
use Session;
use Validator;
use Auth;
use Notifications;
use Carbon\Carbon;
use DB;

class JobAdController extends Controller
{
    public function index()
    {
      $countries = Country::all();
    //   $results = Project::latest('created_at')->get();
      $jobs = JobAd::orderBy('ID','DESC')->paginate(10);
      $clients = Clients::all();
      $staff = SummitStaff::all();
      $ads = JobAd::all();
      $industries = Industry::orderBy('Name')->get();

    	return view ('jobAds.index',compact('countries','jobs','clients','staff','ads','industries'));
    }
       public function json2(Request $request, $id)
    {

        $collections = JobCV::where('Job_adID','=',$id)->orderBy('created_at','DESC')->get();
        $value = '#';

        foreach ($collections as $key =>$value)
        {
            
            

          $firstname = $request->input('firstname');
          $lastname = $request->input('lastname');
          $phonenumber = $request->input('phonenumber');
          $gender  = $request->input('gender');
          $nationality = $request->input('nationality');
          $skills = $request->input('skills');
          $education = $request->input('education');
          $hardskills = $request->input('hardskills');
          $marks = $request->input('marks');


            if($firstname)
           {
              $searchTerm = strtolower($value->candidates->RegDetails->Firstname);
              $str = strtolower($firstname);
              $pos = strstr($searchTerm, $str);

              if($pos === false){
               unset($collections[$key]);
             }
           }

            if($lastname)
           {
              $searchTerm = strtolower($value->candidates->RegDetails->Lastname);
              $str = strtolower($lastname);
              $pos = strstr($searchTerm, $str);
              if($pos === false){
               unset($collections[$key]);
             }
           }

            if($phonenumber)
           {
              if($value->candidates->PhoneNumber != $phonenumber){
               unset($collections[$key]);
             }
           }

            if($gender)
          {

           if ( $value->candidates->Gender != $gender)
           {
              unset($collections[$key]);
           }
          }

           if($nationality)
          {
           if($value->candidates->Nationality != $nationality)
           {
             unset($collections[$key]);
           }
          }

            if($skills)
         {
           $s = array();
            foreach($value->candidates->PersonalSum as $sum){
             $s[]= $sum->Skills;
            }

           if(!in_array($skills,$s))
           {
             unset($collections[$key]);
           }
         }


         if($education)
         {
           $all = array();
            foreach($value->candidates->FurtherEducation as $edu){
             $all[]= $edu->FurtherEducation;
            }

           if(!in_array($education,$all))
           {
             unset($collections[$key]);
           }
         }

           if($hardskills)
         {
           $hs = array();
            foreach($value->candidates->PersonalSum as $hard){
             $hs[]= $hard->HardSkills;
            }
           if(!in_array($hardskills, $hs))
           {
             unset($collections[$key]);
           }
         }
         if($marks)
         {
            if($marks === '40'){

              if($value->CandidateMarks > 49)
              {
                  unset($collections[$key]);
              }
            }else if($marks === '50'){

                if(!($value->CandidateMarks > 49 && $value->CandidateMarks <= 69))
                {
                    unset($collections[$key]);
                }
            } else if($marks === '70'){

                  if(!($value->CandidateMarks > 69 && $value->CandidateMarks <= 89))
                  {
                      unset($collections[$key]);
                  }
            }else if($marks === '90'){

                    if(($value->CandidateMarks < 90))
                    {
                        unset($collections[$key]);
                    }
            }
         }
        }

        $query = new Collection($collections);
        return Datatables::of($query)
           ->addColumn('full_name', function ($list) {
               if($list->candidates->RegDetails){
                return $list->candidates->RegDetails->Firstname.', '.$list->candidates->RegDetails->Lastname;
               }
            })
          ->addColumn('Gender', function ($list) {
                return $list->candidates->Gender;
            })
          ->addColumn('Status', function ($list) {
               $status = Interview::where('CV_ID','=',$list->CV_ID)->where('JobAD_ID','=',$list->Job_adID)->first();
               if(!empty($status)){
                return $status->Status;
               }else{
                return 'Applied';
               }
            })
          ->addColumn('CandidateMarks', function ($list) {

                return $list->CandidateMarks;
            })
          ->addColumn('PhoneNumber', function ($list) {
                return $list->candidates->PhoneNumber;
            })
                 ->addColumn('Actions', function ($list) {
            $inter = Interview::where('CV_ID',$list->CV_ID)->first();        
          if (Auth::user()->role == 0){
              
             $a ='
        <span style="overflow: visible; position: relative; width: 110px;">
        <a title="View details" class="btn btn-sm btn-clean btn-icon btn-icon-sm" target="_blank" href=" '.route('candidateCV',[$list->CV_ID,$list->Job_adID]).'">
           <i class="fas fa-eye" style="color:#80CFFF"></i>
          </a>

          <a title="Pre Qual Questions" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="'.route('jobtests.view',[$list->CV_ID,$list->Job_adID]).'">
           <i class="fas fa-tasks" style="color: #000"></i>
          </a>';
          if($inter){
            
            $b ='  <a title="Set Actions" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="'.route('interviewmanagement.edit',[$inter->InterviewID]).'">
             <i class="fas fa-calendar" style="color: #5cb85c"></i></a>';
          }else{
           $b = ' <a title="Set Actions" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="'.route('interviewmanagement.noCreate',[$list->CV_ID,$list->Job_adID]).'">
             <i class="fas fa-calendar" style="color: #FFCB00"></i></a>';
          }

          if($list->MarkRead == 0){          
           $c = '<a title="Mark as Read" class="btn btn-sm btn-clean btn-icon btn-icon-sm mark-reading" href="#" data-url="'.$list->JobCVID.'">
             <i class="fa fa-check" style="color: #3069AB"></i></a>';
          }else{
          $c =' <a class="btn btn-sm btn-clean btn-icon btn-icon-sm mark-unreading"  title="Marked as Read" href="#" data-url="'.$list->JobCVID.'">
             <i class="fa fa-times" aria-hidden="true" style="color:#FA0000"></i></a>';
          }
            if($list->candidates->Blacklisted == 1){
               $d ='  <a title="Remove Blacklist" class="btn btn-sm btn-clean btn-icon btn-icon-sm mark-unblacklisting" href="#"  data-url="'.$list->CV_ID.'">
             <i class="fa fa-user-times" style="color: #FA0000"></i></a>';
          }else{
               $d ='  <a title="Blacklist" class="btn btn-sm btn-clean btn-icon btn-icon-sm mark-blacklisting" href="#" data-url="'.$list->CV_ID.'">
             <i class="fa fa-user" style="color: #5cb85c"></i></a></span>';
          }
          
          
          return $a.$c.$d.$b;


      }else{
        $a ='
        <span style="overflow: visible; position: relative; width: 110px;">
        <a title="View details" class="btn btn-sm btn-clean btn-icon btn-icon-sm" target="_blank" href="'.route('candidateCV',[$list->CV_ID,$list->Job_adID]).'">
           <i class="fas fa-eye" style="color:#80CFFF"></i>
          </a>
          <a title="Pre Qual Questions" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="'.route('jobtests.view',[$list->CV_ID,$list->Job_adID]).'">
           <i class="fas fa-tasks" style="color: #000"></i>
          </a>';
         if($inter){
            $b ='  <a title="Set Actions" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="'.route('interviewmanagement.edit',[$inter->InterviewID]).'">
             <i class="fas fa-calendar" style="color: #5cb85c"></i></a>';
          }else{
           $b =' <a title="Set Actions" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="'.route('interviewmanagement.noCreate',[$list->CV_ID,$list->Job_adID]).'">
             <i class="fas fa-calendar" style="color: #FFCB00"></i></a>';
          }

          if($list->MarkRead == 0){          
           $c ='<a title="Mark as Read" class="btn btn-sm btn-clean btn-icon btn-icon-sm mark-reading" href="#" data-url="'.$list->JobCVID.'">
             <i class="fa fa-check" style="color: #3069AB"></i></a>';
          }else{
        $c = ' <a class="btn btn-sm btn-clean btn-icon btn-icon-sm mark-unreading"  title="Marked as Read" href="#" data-url="'.$list->JobCVID.'">
             <i class="fa fa-times" aria-hidden="true" style="color:#FA0000"></i></a></span>';
          }
          
          return $a.$c.$b;
       }
        })
        ->rawColumns(['Actions'])
        ->make();
    }
     public function json(Request $request)
    {
     $collections = JobAd::orderBy('ID','Desc')->get();

     foreach ($collections as $key =>$value)
     {


         $jobtype = $request->input('type');
         $clients = $request->input('clients');
         $education = $request->input('education');
         $category  = $request->input('category');
         $staff = $request->input('staff');
         $careerlevel = $request->input('careerlevel');
         $jobtitle = $request->input('jobtitle');
         $closed = $request->input('closed_jobs');
         $all = $request->input('all_jobs');

         if($staff)
         {
          if($value->StaffID != $staff)
          {
            unset($collections[$key]);
          }
         }
         //search whole string using just a subsuffix
          if($jobtitle)
          {
             $searchTerm = strtolower($value->JobTitle);
             $str = strtolower($jobtitle);
             $pos = strstr($searchTerm, $str);
             if($pos === false){
              unset($collections[$key]);
            }
          }

         // if ($jobtitle)
         // {
         //   if ($value->JobTitle != $jobtitle)
         //   {
         //       unset($collections[$key]);
         //   }
         // }

         if($clients)
         {
          if ($value->ClientID != $clients)
          {
             unset($collections[$key]);
          }
         }
         //
         if($category)
         {
          if($value->Category != $category)
          {
            unset($collections[$key]);
          }
         }

        if($careerlevel)
        {
          if($value->CareerLevel !=$careerlevel)
          {
            unset($collections[$key]);
          }
        }

        if($education)
        {

          if($value->MinEduReq != $education)
          {
            unset($collections[$key]);
          }
        }

        if($jobtype)
        {
          if($value->JobType != $jobtype)
          {
            unset($collections[$key]);
          }
        }

        if($closed){
            if($closed === 'no'){
                if($value->CandidatePlaced == 0 || $value->Deadline >= Carbon::now()){
                    unset($collections[$key]);
                }
            }else{
                if($value->CandidatePlaced == 1 || $value->Deadline < Carbon::now()){
                    unset($collections[$key]);
                }
            }

        }
        if($all){
            if($closed === 'no'){
                
            unset($collections[$key]);
                
            }else{
                
            }

        }
      }
      $query = new Collection($collections);
      return Datatables::of($query)

          ->addColumn('full_salary', function ($list) {
          	    return $list->SalCurrency.', '.$list->GrossMonthSal;
            })
          ->addColumn('client', function ($list) {
              if($list->Clients){
                   return $list->Clients->CompanyName;
              }else{
                  return '';
              }
               
            })
        ->addColumn('tools', function ($list) {
        	if (Auth::user()->role == 0){
        		 return '
        <span style="overflow: visible; position: relative; width: 110px;">
        <a title="View details" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="'.route('jobAds.show',$list->ID).'">
           <i class="fas fa-eye" style="color:#80CFFF"></i>
          </a>
          <a title="View Applications" class="btn btn-sm btn-clean btn-icon btn-icon-sm" target="_blank" href="'.route('jobAds.viewApplicants',$list->ID).'">
           <i class="fas fa-users" style="color: #000"></i>
          </a>
          <a title="Edit details" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="' . route('jobAds.edit', $list->ID) . '">
             <i class="fas fa-edit" style="color: #3069AB"></i>
          </a>

          <a
            title="Delete"
            class="btn btn-sm btn-clean btn-icon btn-icon-sm"
            data-href="'.route('jobAds.destroy',$list->ID).'"
            data-toggle="modal"
            data-target="#deleteConfirmModal"
            >
            <i class="far fa-trash-alt" style="color:#FA0000"></i>
          </a>


      </span>';
      }else{
      	 return '
        <span style="overflow: visible; position: relative; width: 110px;">
        <a title="View details" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="'.route('jobAds.show',$list->ID).'">
           <i class="fas fa-eye" style="color:#80CFFF"></i>
          </a>
          <a title="View Applicaticants" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="'.route('jobAds.viewApplicants',$list->ID).'">
           <i class="fas fa-users" style="color: #000"></i>
          </a>
          <a title="Edit details" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="' . route('jobAds.edit', $list->ID) . '">
            <i class="fas fa-edit" style="color: #3069AB"></i>
          </a>
      </span>';
       }
        })
        ->rawColumns(['tools'])
        ->make();
  }
  public function create(){
  	$clients = Clients::where('status','=',4)->orWhere('status','=',5)->orWhere('status','=',6)->orWhere('status','=',7)->orderBy('CompanyName')->get();
  	$industries = Industry::orderBy('name')->get();
  	$currencies = Currency::all();
  	$countries = Country::all();
  	$locations = Location::orderBy('LocationName')->get();
    $staff = SummitStaff::all();
    
    $competencies = competencies::all();
   

    // Toastr::success('Job Ad Created.');
    return view ('jobAds.create',compact('clients','industries','currencies','countries','locations','staff','competencies'));
  }
  
  public function clientCreate($id){

  $clients = Clients::all();
  $industries = Industry::orderBy('name')->get();
  $currencies = Currency::all();
  $countries = Country::all();
  $locations = Location::all();
  $staff = SummitStaff::all();
  $competencies = competencies::all();
  $c = Clients::where('ClientID',$id)->first();

  return view ('jobAds.client_create',compact('clients','industries','currencies','countries','locations','staff','competencies','id','c'));

}
  public function store(Request $request)
  {
    $validator = Validator::make($request->except('_token'), [
    'client_name' => ['required'],
    'competencies' => ['required'],
    'job_title' => ['required', 'string'],
    'job_category' => ['required'],
    'summary' => ['required'],
    'education_level'=>['required'],
    'job_type'=>['required'],
    'salary_currency'=>['required'],
    'monthly_salary'=>['required'],
    'show_salary'=>['required'],
    'career_level'=>['required'],
    'deadline'=>['required'],
    'candidate_placed'=>['required'],
    'staff_assigned'=>['required'],
   
      ]);
    if ($validator->fails()) {
        $errors = $validator->errors();

         //dd($errors);
        return Redirect::back()->withInput()->withErrors($errors);
      }else{
        $jobAd = new JobAd();
        $jobAd->ClientID = $request->input('client_name');
        $jobAd->JobTitle = $request->input('job_title');
        $jobAd->Category = $request->input('job_category');
        $jobAd->Summary = $request->input('summary');
        $jobAd->MinEduReq = $request->input('education_level');
        $jobAd->JobType = $request->input('job_type');
        $jobAd->LocationCity = $request->input('location_city');
        $jobAd->LocationCountry = $request->input('location_country');
        $jobAd->SalCurrency = $request->input('salary_currency');
        $jobAd->GrossMonthSal = $request->input('monthly_salary');
        $jobAd->ShowSal = $request->input('show_salary');
        $jobAd->CareerLevel = $request->input('career_level');
        $jobAd->Deadline = $request->input('deadline');
        $jobAd->revenue = $request->input('revenue');
        $jobAd->CandidatePlaced = $request->input('candidate_placed');
        $jobAd->StaffID = $request->input('staff_assigned');
        $jobAd->created_by = Auth::user()->id;
        $jobAd->AnnualPercentageRevenue = $request->input('annualpercentagerevenue');

        $jobAd->save();
        $requirement= $_POST['JobFields']['requirements'];
        foreach ($requirement as $req){
        	$require = new JobRequirement();
        	$require->JobID = $jobAd->id;
        	$require->Requirement = $req;
        	$require->save();
        }
        $duties = $_POST['JobFields']['duties'];
          foreach ($duties as $dut)
          {
            $jobDutiesModel = new JobDuty;
            $jobDutiesModel->JobID = $jobAd->id;
            $jobDutiesModel->Duty  = $dut;
            $jobDutiesModel->save();
           }
           
           
         $competencies=   $_POST['competencies'];
         foreach ($competencies as $comp)
          {
            $jobComp = new JobCompetencies;
            $jobComp->JobID = $jobAd->id;
            $jobComp->Competencies  = $comp;
            $jobComp->save();
          }
           $details = [
                    'taskid' => $jobAd->ID,
                    'title' => 'You have been assigned an new Job',
                    'body' => 'A new job titled'. $jobAd->JobTitle. 'has been created and assigned to you.',
                    'type' => 'job',
            ];
            $staff = SummitStaff::where('StaffID','=',$jobAd->StaffID)->first();
            $user = User::where('id','=',$staff->AccUserID)->first();
            $user->notify(new \App\Notifications\StaffNotification($details));

            Toastr::success('Job Application has been created.');

           return redirect()->route('jobtests.create', ['id'=> $jobAd->id])->with('status','Job Ad Successfully Created. Please add a Job Test');
      }
  }
  public function edit($id)
  {
    $job = JobAd::where('ID','=',$id)->first();
  	$clients = Clients::all();
  	$industries = Industry::orderBy('Name')->get();
  	$currencies = Currency::all();
  	$countries = Country::all();
  	$locations = Location::all();
    $staff = SummitStaff::all();
    $competencies = competencies::all();
    return view ('jobAds.edit',compact('job','clients','industries','currencies','countries','locations','staff','competencies'));
  }
  public function update(Request $request, $id)
  {

    $validator = Validator::make($request->except('_token'), [
    'competencies'=>['required'],
    'education_level'=>['required'],
    'job_type'=>['required'],
    'location_country'=>['required'],
    'salary_currency'=>['required'],
    'monthly_salary'=>['required'],
    'show_salary'=>['required'],
    'career_level'=>['required'],
    'deadline'=>['required'],
    'candidate_placed'=>['required'],
    'staff_assigned'=>['required'],
        ]);
    if ($validator->fails())
      {
        $errors = $validator->errors();
        return Redirect::back()->withInput()->withErrors($errors);
      }else{
        $jobAd = JobAd::where('ID',$id)->update([
        'ClientID' => $request->input('client_name'),
        'Category' => $request->input('job_category'),
        'JobTitle' => $request->input('job_title'),
        'Summary' => $request->input('summary'),
        'MinEduReq' => $request->input('education_level'),
        'JobType' => $request->input('job_type'),
        'LocationCity' => $request->input('location_city'),
        'LocationCountry' => $request->input('location_country'),
        'SalCurrency' =>$request->input('salary_currency'),
        'GrossMonthSal' => $request->input('monthly_salary'),
        'ShowSal' => $request->input('show_salary'),
        'CareerLevel' => $request->input('career_level'),
        'Deadline' => $request->input('deadline'),
        'CandidatePlaced' => $request->input('candidate_placed'),
        'StaffID' => $request->input('staff_assigned'),
        'created_by' => Auth::user()->id,
       'AnnualPercentageRevenue' => $request->input('revenue')
        ]);


           $jobAd = JobAd::find($id);
        JobRequirement::where('JobID','=', $id)->delete();
        $requirement= $_POST['JobFields']['requirements'];
        foreach ($requirement as $req){
        	$require = new JobRequirement();
        	$require->JobID = $id;
        	$require->Requirement = $req;
        	$require->save();
        }
       JobDuty::where('JobID','=', $id)->delete();
        $duties = $_POST['JobFields']['duties'];
          foreach ($duties as $dut)
          {
            $jobDutiesModel = new JobDuty;
            $jobDutiesModel->JobID = $id;
            $jobDutiesModel->Duty  = $dut;
            $jobDutiesModel->save();
           }
     JobCompetencies::where('JobID','=', $id)->delete();
      $competencies=   $_POST['competencies'];
         foreach ($competencies as $comp)
          {
            $jobComp = new JobCompetencies;
            $jobComp->JobID = $id;
            $jobComp->Competencies  = $comp;
            $jobComp->save();
          }
            $details = [
                    'taskid' => $jobAd->ID,
                    'title' => 'The job titled '. $jobAd->JobTitle. ' has been edited',
                    'body' => 'Changes have been made to the job you are assigned too.',
                    'type' => 'job',
            ];
            $staff = SummitStaff::where('StaffID','=',$jobAd->StaffID)->first();
            $user = User::where('id','=',$staff->AccUserID)->first();
            $user->notify(new \App\Notifications\StaffNotification($details));
           Toastr::success('Job Application has been Updated.');
          return redirect()->route('jobAds.show',$jobAd->ID);
        }
    }
    //view applied applicants for particular job
  public function viewApplicants(Request $request,$id)
  {
    $job = JobAd::where('ID','=', $id)->first();
    $requirements = JobRequirement::where('JobID','=',$id)->get();
    $duties = JobDuty::where('JobID','=',$id)->get();
    $applications = JobCV::where('Job_adID','=',$id)->paginate(10);
    $staffassigned=SummitStaff::where('StaffID',$job->StaffID)->first();
    $jobads = JobAd::all();
    $hardskills = HardSkill::all();
    $industry=Industry::all();
    $industryfunctions=IndustryFunctions::all();
    $profqualtitles = ProfQualTitles::all();
    $candidatemarks = JobCV::all();

    return view('jobAds.viewapplicants',compact('job','requirements','duties','staffassigned','applications','jobads','candidatemarks','profqualtitles','hardskills','industry','industryfunctions'));
  }
  public function show(Request $request,$id)
  {
       $inter = array();
    $short = array();
    $place = array();
    $job = JobAd::where('ID','=', $id)->first();
    $requirements = JobRequirement::where('JobID','=',$id)->get();
    $duties = JobDuty::where('JobID','=',$id)->get();
    
    $staffassigned=SummitStaff::where('StaffID',$job->StaffID)->first();
      $applications = JobCV::where('Job_adID','=',$id)->latest()->get();
 
    $interviews = Interview::where('JobAD_ID','=',$job->ID)->get(); 
  
    foreach($interviews as $intered){
      if($intered->Status === 'Interviewed'){

        $inter[] = $intered->CV_ID;

      }elseif($intered->Status === 'Shortlisted'){

          $short[] = $intered->CV_ID;

      }elseif($intered->Status === 'Placed'){

        $place[] = $intered->CV_ID;
     }
    }   

    $application_all =[];
    $intervieweds = [];
    $shortlisteds = [];
    $placeds = [];

    foreach($applications as $application){
      if (in_array($application->CV_ID, $inter))
      {
        $intervieweds[] = $application;

      }elseif (in_array($application->CV_ID, $short)) {

        $shortlisteds[] = $application;

      }elseif (in_array($application->CV_ID, $place)) {

        $placeds[] = $application;

      }else{
        $application_all[] = $application;
      }
    }

    $applications_all = $this->paginate($application_all);

    $applications_all->setPath($request->url());

    $interviewed = $this->paginate($intervieweds);

    $interviewed->setPath($request->url());

    $shortlisted = $this->paginate($shortlisteds);

    $shortlisted->setPath($request->url());
  
    $placed = $this->paginate($placeds);

    $placed->setPath($request->url());
    if($request->has('candidate'))
   {
        //Checkbox checked
   }else
   {
        //Checkbox not checked
   }
    return view('jobAds.view',compact('job','requirements','duties','staffassigned','applications','applications_all','placed','interviewed','shortlisted'));
  }
  //kuweka probably the mail send function after the show function
   public function apply(Request $request, $id)
  {

      $jobs = JobAd::take(10)->get();
      $jobAd = JobAd::where('id','=',$id)->first();
      $questions = JobTests::where('ID','=', $id)->get();
       $details = RegistrationDetails::where('AccUserID', Auth::user()->id)->where('EmailAddress',Auth::user()->email)->first();
      
    $cvdets = CvTable::where('CandidateRegID', $details->CanditateRegID)->first();
    //  dd($cvdets->CV_ID);
       $applied = CandidateAnswers::where('CV_ID','=',$cvdets->CV_ID)->where('ID','=',$id)->get();
     if(count($applied) > 0){

      // Mail::send('emails.job_application',['name' =>  $details->Firstname.' '.$details->Lastname, 'job_title'=> $job->JobTitle],
      //       function ($message) use ($details) {
      //         $message->from('support@farwell-consultants.com', 'Summit Recruitment');
      //         $message->to($details->EmailAddress,$details->first_name);
      //         $message->subject('Job Application');
      // });
    $redirectError = [
        'title' => 'Application Submitted!',
        'content' => 'You had earlier applied for this job. only shortlisted candidates will be contacted.',
    ];
    Session::flash('redirectError', $redirectError);
    return redirect()->route('jobs.my-applications');
    }

      //dd($questions);
      $answers = array();
      foreach ($questions as $key => $value)
      {
        $answers[] = PreQualAnswers::where('PreQualTestID','=', $value->PreQualTestID)->get();
      }
      return view ('jobApplications.prequaltest',compact('jobAd','questions','answers','jobs'));
  }
   public function sendtest(Request $request)
  {
        if (count($request->input()) <= 2)
      {
        $redirectError = [
        'title' => 'Sorry!!',
        'content' => 'Please answer the following pre-qualification question(s) to complete your job application',
    ];
    Session::flash('redirectError', $redirectError);
    return redirect()->back();

    }else{
    $details = RegistrationDetails::where('AccUserID', Auth::user()->id)->where('EmailAddress',Auth::user()->email)->first();
    $cvdets = CvTable::where('CandidateRegID', $details->CanditateRegID)->first();
    $applied = CandidateAnswers::where('CV_ID','=',$cvdets->CV_ID)->where('ID','=',$request->jobid)->get();
    $job = JobAd::where('ID','=',$request->jobid)->first();

    if(count($applied) > 0){

       Mail::send('emails.job_application',['name' =>  $details->Firstname.' '.$details->Lastname, 'job_title'=> $job->JobTitle],
             function ($message) use ($details) {
               $message->from('application@summitrecruitment-search.com','Summit Recruitment');
               $message->to($details->EmailAddress,$details->first_name);
               $message->subject('Job Application');
       });
    $redirectError = [
        'title' => 'Application Successfully Sent!',
        'content' => 'You have completed the pre-qualification questions and successfully sent your application. Please check your email for a confirmation email',
    ];
    Session::flash('redirectError', $redirectError);
    return redirect()->back();
    }
    
    $questions = JobTests::where('ID','=', $request->jobid)->get();
    
 

   $requirement= $_POST['Question'];
   
    $questions = array();

   $answers = array();


   foreach($requirement as $r){

   	$questions[] = $r['Question'];

   	if(!empty($r['Answer'])){
   		$answers[] = $r['Answer'];
   	}
   	
   }

   
   if(count($questions) != count($answers)){

   	 $redirectError = [
        'title' => 'Application not successfull!',
        'content' => 'You have not answered all questions, Please make sure to answer all questions',
    ];
    Session::flash('redirectError', $redirectError);
    return redirect()->back();
   }else{

   //dd($requirement);
  $a = array();
   $total = 0;
   foreach($requirement as $req){
      
   $an = PreQualAnswers::where('PreQualTestID','=',$req['Question'])->where('Answers',$req['Answer'])->first();

    $answers = new CandidateAnswers;
    $answers->ID = $request->jobid;
    $answers->CV_ID = $cvdets->CV_ID;
    $answers->AnswerID = $an->AnswersID;
    $answers->Answer = $req['Answer'];
    $answers->save();

   array_push($a, $an->Marks);
      }
    $tot = array_sum($a)/count($a);
    //dd($tot);
    $total = $this->convert($tot);



      $jobcv = new JobCV;
      $jobcv->CV_ID = $cvdets->CV_ID;
      $jobcv->Job_adID = $request->jobid;
      $jobcv->CandidateMarks = $total;
      $jobcv->MarkRead = 0;
      $jobcv->save();

       Mail::send('emails.job_application',['name' =>  $details->Firstname.' '.$details->Lastname, 'job_title'=> $job->JobTitle],
             function ($message) use ($details) {
               $message->from('application@summitrecruitment-search.com','Summit Recruitment');
               $message->to($details->EmailAddress,$details->first_name);
               $message->subject('Job Application');
       });


    if($answers){
      $redirectMessage = [
          'title' => 'Application Sent!',
          'content' => 'You have successfully applied for the job. An email will be sent to your email address as confirmation. Only shortlisted candidates will be contacted.',
      ];
      Session::flash('redirectMessage', $redirectMessage);
    }else{
      $redirectError = [
          'title' => 'Application Incomplete!',
          'content' => 'Something went wrong wth your application. Please refersh the page and complete all the questions before submitting an application',
      ];
      Session::flash('redirectError', $redirectError);
    }
    return redirect()->route('jobs.my-applications')->with('status','Thank You For Applying For The Below Jobs.');
  }
    }
  }
  public function applications(Request $request)
  {
    $details = RegistrationDetails::where('AccUserID', Auth::user()->id)->where('EmailAddress',Auth::user()->email)->first();
    $cvdets = CvTable::where('CandidateRegID', $details->CanditateRegID)->first();
    $jobcvs = JobCV::where('CV_ID','=', $cvdets->CV_ID)->orderBy('JobCVID', 'DESC')->get();
    $jobCV = [];
    foreach ($jobcvs as $key => $value) {
      # code...
      $cv = JobAd::where('ID','=',$value->Job_adID)->first();
      array_push($jobCV, $cv);
      $jobCV[$key]['status'] = $value->MarkRead;

     if(!empty($request->input('job_category'))){
       $indu = Industry::where('ID','=',$request->input('job_category'))->first();
       if($cv->Category != $indu->Name){
         unset($jobCV[$key]);
       }
     }
     if(!empty($request->input('edulevels'))){
       if($cv->MinEduReq != $request->input('edulevels')){
         unset($jobCV[$key]);
       }
     }
     if(!empty($request->input('clevels'))){
       if($request->input('clevels') === 'Mid Level'){
         if($cv->CareerLevel != 'Mid level'){
           unset($jobCV[$key]);
         }
       }else{
         if($cv->CareerLevel != $request->input('clevels')){
           unset($jobCV[$key]);
         }
       }
     }
     if(!empty($request->input('job_type'))){
       if($cv->JobType != $request->input('job_type')){
         unset($jobCV[$key]);
       }
     }
    }
    $jobs = JobAd::orderBy('ID','DESC')->take(5)->get();
    $categories = Industry::all();

      return view ('jobApplications.MyApplications',compact('jobCV','jobs','categories'));
  }
  public function applied(Request $request, $id)
  {
    $details = RegistrationDetails::where('AccUserID', Auth::user()->id)->where('EmailAddress',Auth::user()->email)->first();
    $cvdets = CvTable::where('CandidateRegID', $details->CanditateRegID)->first();
    $jobdets = JobAd::where('ID','=', $id)->first();
    $jobreqs = JobRequirement::where('JobID','=', $id)->get();
    $jobdutys = JobDuty::where('JobID','=', $id)->get();
    $jobcvs = JobCV::where('CV_ID','=', $cvdets->CV_ID)->get();
    $jobCV = [];
    foreach ($jobcvs as $key => $value) {
      # code...
    $CV = JobAd::where('ID','=',$value->Job_adID)->first();
      array_push($jobCV, $CV);
    }
    $jobs = JobAd::orderBy('ID','DESC')->take(5)->get();
      return view ('jobApplications.SingleApplication',compact('jobCV','jobs','jobdets','jobreqs','jobdutys'));
  }
  public function jobsapplied(Request $request, $id)
  {
     $jobcvs = JobCV::where('CV_ID','=', $id)->get();

     $jobCV = [];
    foreach ($jobcvs as $key => $value) {
      # code...
    $CV = JobAd::where('ID','=',$value->Job_adID)->first();
      array_push($jobCV, $CV);
    }
     return view ('jobAds.jobsapplied',compact('jobcvs','jobCV'));
  }



  public function sendmail(Request $request){
    $jobid = $request->input('jobid');
    $group = $request->input('group');
    $applications = JobCV::where('Job_adID','=',$jobid)->get();
    $job = JobAd::where('ID','=',$jobid)->first();
   
   
    $regrets = array();
    $interviewed = array();
    $shortlisted = array();
    $placed = array();

    foreach ($applications as $application){
    $status = Interview::where('CV_ID','=',$application->CV_ID)->where('JobAD_ID','=',$application->Job_adID)->first();
    if(empty($status)){
      $regrets [] = $application->candidates->RegDetails->EmailAddress;

    }
    elseif(!empty($status) && $status->Status === 'Interviewed'){
        $interviewed [] = $application->candidates->RegDetails->EmailAddress;

    }
    elseif(!empty($status) && $status->Status === 'Shortlisted'){
        $shortlisted [] = $application->candidates->RegDetails->EmailAddress;

    }
    elseif(!empty($status) && $status->Status === 'Placed'){
        $placed[] = $application->candidates->RegDetails->EmailAddress;

    }
    }

    if(!empty($regrets) && $group === 'applicants'){
     $message = CreateCampaigns::where('target','=',6)->first();
     $title = $message->c_name;

     $replace = array('{Job_position_title}');
     $new = array($job->JobTitle );
     $content = str_replace($replace, $new, $message->campaign_info);

    foreach($regrets as $regret){

     $details = RegistrationDetails::where('EmailAddress', $regret)->first();


      Mail::send('emails.send_email',['name' => $details->Firstname.' '.$details->Lastname,'content'=> $content, 'title'=>$title],
            function ($message) use ($details) {
              $message->from('support@farwell-consultants.com', 'Summit Recruitment');
              $message->to($details->EmailAddress,$details->Firstname);
              $message->subject('Job Application');
      });
       $msg = "The Mail has been sent.";
      return response()->json(array('msg' => $msg), 200);
    }
  }elseif(!empty($interviewed) && $group === 'interviewed'){
    $message = CreateCampaigns::where('target','=',1)->first();
    $title = $message->c_name;

    $replace = array('{Job_position_title}');
    $new = array($job->JobTitle );
    $content = str_replace($replace, $new, $message->campaign_info);

   foreach($interviewed as $regret){
     $details = RegistrationDetails::where('EmailAddress', $regret)->first();

      Mail::send('emails.send_email',['name' => $details->Firstname.' '.$details->Lastname,'content'=> $content, 'title'=>$title],
            function ($message) use ($details) {
              $message->from('support@farwell-consultants.com', 'Summit Recruitment');
              $message->to($details->EmailAddress,$details->Firstname);
              $message->subject('Job Application');
      });
     $msg = "The Mail has been sent.";
     return response()->json(array('msg' => $msg), 200);
   }
  }elseif(!empty($shortlisted) && $group === 'shortlisted'){
    $message = CreateCampaigns::where('target','=',2)->first();
    $title = $message->c_name;

    $replace = array('{Job_position_title}');
    $new = array($job->JobTitle );
    $content = str_replace($replace, $new, $message->campaign_info);

   foreach($shortlisted as $regret){
     $details = RegistrationDetails::where('EmailAddress', $regret)->first();

      Mail::send('emails.send_email',['name' => $details->Firstname.' '.$details->Lastname,'content'=> $content, 'title'=>$title],
            function ($message) use ($details) {
              $message->from('support@farwell-consultants.com', 'Summit Recruitment');
              $message->to($details->EmailAddress,$details->Firstname);
              $message->subject('Job Application');
      });
     $msg = "The Mail has been sent.";
     return response()->json(array('msg' => $msg), 200);
   }
  }elseif(!empty($placed) && $group === 'placed'){
    $message = CreateCampaigns::where('target','=',3)->first();
   $title = $message->c_name;

    $replace = array('{Job_position_title}');
    $new = array($job->JobTitle );
    $content = str_replace($replace, $new, $message->campaign_info);


   foreach($placed as $regret){
     $details = RegistrationDetails::where('EmailAddress', $regret)->first();

      Mail::send('emails.send_email',['name' => $details->Firstname.' '.$details->Lastname,'content'=> $content, 'title'=>$title],
            function ($message) use ($details) {
              $message->from('support@farwell-consultants.com', 'Summit Recruitment');
              $message->to($details->EmailAddress,$details->Firstname);
              $message->subject('Job Application');
      });
     $msg = "The Mail has been sent.";
     return response()->json(array('msg' => $msg), 200);
   }
  }


  }
  
  public function destroy(Request $request, $id)
  {
        $job = JobAd::where('ID',$id)->first();

       $test = JobTests::where('ID',$job->ID)->get();
        $duties = JobDuty::where('JobID',$job->ID)->get();
        $requirements = JobRequirement::where('JobID',$job->ID)->get();
       
       if($test){
         foreach($test as $tests){
             $r= PreQualAnswers::where('PreQualTestID',$tests->PreQualTestID)->get();
          
          if($r){
            CandidateAnswers::where('ID',$job->ID)->delete();
            foreach($r as $f){
            PreQualAnswers::where('AnswersID', $f->AnswersID)->delete();
            }
          }
         }
      }
      if($duties){
          foreach($duties as $duty){
             JobDuty::where('ID',$duty->ID)->delete(); 
          }
      }
        if($requirements){
          foreach($requirements as $requirement){
             JobRequirement::where('ID',$requirement->ID)->delete(); 
          }
      }
      CandidateAnswers::where('ID',$job->ID)->delete();
      JobTests::where('ID',$job->ID)->delete();
      JobCV::where('Job_adID',$job->ID)->delete();
      JobAd::where('ID',$id)->delete();
      
      
      return redirect()->route('jobAds.index')->with('status','Job Ad has been Deleted');
  }


  protected function generateRandomString($length = 10)
  {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[ rand(0, $charactersLength - 1) ];
    }
    return $randomString;
  }
  
    public function convert($mark){
        
    $marks = (int)$mark;

  if ($marks == 10){
    $c = 100;

  }else if ($marks == 9){

    $c = 90;

  }else if ($marks == 8){

    $c = 80;

  }else if ($marks == 7){

    $c = 70;

  }else if ($marks == 6){

    $c = 60;

  }else if ($marks == 5){

    $c = 50;

  }else if ($marks == 4){

    $c = 40;

  }else if ($marks == 3){

    $c = 30;

  }else if ($marks == 2){

    $c = 20;
  }else if ($marks == 1){

    $c = 10;
  }

  return $c;
}
 
 public function Blacklist(Request $request){
     
     
         $date = Carbon::now();
         $val = $request->input('val');
         $id =  $request->input('id');
         
         
             $details = DB::table('cv_table')
	      ->where('CV_ID', $id)
	      ->update([
		     "Blacklisted" => $val,
		     "updated_at" => $date,

	      ]);
	      $msg = 'ok';

      return response()->json(array('msg' => $msg), 200);
	      
         
         
     }
      public function paginate($items, $perPage = 25, $page = null, $options = [])
     {
       
         $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
         $items = $items instanceof Collection ? $items : Collection::make($items);
         return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
     }

}
