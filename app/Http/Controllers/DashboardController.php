<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\Eloquent\Collection;

use App\JobAd;
use Charts;
use DB;
use App\SummitStaff;
use App\Ticket;
use App\Country;
use App\Clients;
use App\Interview;
use App\RegistrationDetails;
use App\User;
use Carbon\Carbon;
use App\Industry;
use Auth;

class DashboardController extends Controller
{
  public function  dashboard2()
  {
    $jobs = JobAd::orderBy('ID','Desc')->paginate(5);
    $countries = Country::all();
    $clients = Clients::all();
    $users = User::all();
    $staff = SummitStaff::orderBy('Firstname')->get();
    $interview = Interview::paginate(5);
    $industries = Industry::orderBy('Name')->get();

    return view('dashboardconsultant',compact('jobs','clients','users','staff','interview','countries','industries'));
  }
    public function  dashboard(Request $request)
    {
        if(Auth::user()->role == 1){
              $clients = Clients::all();
      $cold = Clients::where('status','=',1)->get();
      $warm = Clients::where('status','=',2)->get();
      $hot= Clients::where('status','=',3)->get();
      $active= Clients::where('status','=',4)->get();
      $activenotloyal= Clients::where('status','=',5)->get();
      $inactiveloyal= Clients::where('status','=',6)->get();
      $inactivenotloyal= Clients::where('status','=',7)->get();
      //end of client status
      //candidatestatus
      $users = User::all();
      $jobs = JobAd::all();
      //candidate status
      $interview = Interview::all();
      $interviewed = Interview::where('Status','=','Interviewed')->get();
      $scheduled = Interview::where('Status','=','scheduled')->get();
      $shortlisted = Interview::where('Status','=','Shortlisted')->get();
      $placed = Interview::where('Status','=','Placed')->get();
      //end of candidate status
      //staff status
      $staff = SummitStaff::all();
      $stafffs = User::where('role','=',0)->orWhere('role','=',1)->orderBy('id')->paginate(20);
      //endofstaff status
      $tickets = Ticket::all();
      $opentickets = Ticket::where('status','=',0)->get();
      $closedtickets = Ticket::where('status','=',1)->get();
      $industries = Industry::orderBy('Name')->get();

    	return view ('dashboard2', compact('clients','users','jobs','interview','staff','tickets','cold','warm','hot','active','activenotloyal','inactiveloyal','inactivenotloyal','placed','shortlisted','scheduled','interviewed','opentickets','closedtickets','stafffs','industries'));
            
        }else{
            
        if($request->input('ytd')){
            $range = $request->input('ytd');
            
            $start_date = Carbon::parse($range)->toDateTimeString();
            
            $clients = Clients::where('created_at','>', $start_date)->get();
            $users = User::where('created_at','>', $start_date)->get();
            $jobs = JobAd::where('created_at','>', $start_date)->get();
            $interview = Interview::where('created_at','>', $start_date)->get();
            $staff = SummitStaff::where('created_at','>', $start_date)->get();
            $cold = Clients::where('status','=',1)->where('created_at','>', $start_date)->get();
      $warm = Clients::where('status','=',2)->where('created_at','>', $start_date)->get();
      $hot= Clients::where('status','=',3)->where('created_at','>', $start_date)->get();
      $active= Clients::where('status','=',4)->where('created_at','>', $start_date)->get();
      $activenotloyal= Clients::where('status','=',5)->where('created_at','>', $start_date)->get();
      $inactiveloyal= Clients::where('status','=',6)->where('created_at','>', $start_date)->get();
      $inactivenotloyal= Clients::where('status','=',7)->where('created_at','>', $start_date)->get();
      $interviewed = Interview::where('Status','=','Interviewed')->where('created_at','>', $start_date)->get();
      $scheduled = Interview::where('Status','=','scheduled')->where('created_at','>', $start_date)->get();
      $shortlisted = Interview::where('Status','=','Shortlisted')->where('created_at','>', $start_date)->get();
      $placed = Interview::where('Status','=','Placed')->where('created_at','>', $start_date)->get();
       $tickets = Ticket::where('created_at','>', $start_date)->get();
      $opentickets = Ticket::where('status','=',0)->where('created_at','>', $start_date)->get();
      $closedtickets = Ticket::where('status','=',1)->where('created_at','>', $start_date)->get();

   
                
         }else{
             $clients = Clients::all();
            $users = User::all();
            $jobs = JobAd::all();
            $interview = Interview::all();
            $staff = SummitStaff::all();
              $cold = Clients::where('status','=',1)->get();
      $warm = Clients::where('status','=',2)->get();
      $hot= Clients::where('status','=',3)->get();
      $active= Clients::where('status','=',4)->get();
      $activenotloyal= Clients::where('status','=',5)->get();
      $inactiveloyal= Clients::where('status','=',6)->get();
      $inactivenotloyal= Clients::where('status','=',7)->get();
             $interviewed = Interview::where('Status','=','Interviewed')->get();
      $scheduled = Interview::where('Status','=','scheduled')->get();
      $shortlisted = Interview::where('Status','=','Shortlisted')->get();
      $placed = Interview::where('Status','=','Placed')->get();
             $tickets = Ticket::all();
      $opentickets = Ticket::where('status','=',0)->get();
      $closedtickets = Ticket::where('status','=',1)->get();
         }
            
        
        
        if($request->input('daterange')){
            $range = $request->input('daterange');
            $range = explode(' - ', $range);
            
            $start_date = Carbon::parse($range[0])->toDateTimeString();

       $end_date = Carbon::parse($range[1])->toDateTimeString();
            
            $clients = Clients::whereBetween('created_at',[$start_date,$end_date])->get();
            $users = User::whereBetween('created_at',[$start_date,$end_date])->get();
            $jobs = JobAd::whereBetween('created_at',[$start_date,$end_date])->get();
            $interview = Interview::whereBetween('created_at',[$start_date,$end_date])->get();
            $staff = SummitStaff::whereBetween('created_at',[$start_date,$end_date])->get();

        }else{
            $clients = Clients::all();
            $users = User::all();
            $jobs = JobAd::all();
            $interview = Interview::all();
            $staff = SummitStaff::all();
        }
        
          //client status
      
        
         if($request->input('clientrange')){
            $range = $request->input('clientrange');
            $range = explode(' - ', $range);
            
            $start_date = Carbon::parse($range[0])->toDateTimeString();

       $end_date = Carbon::parse($range[1])->toDateTimeString();
       
          $cold = Clients::where('status','=',1)->whereBetween('created_at',[$start_date,$end_date])->get();
      $warm = Clients::where('status','=',2)->whereBetween('created_at',[$start_date,$end_date])->get();
      $hot= Clients::where('status','=',3)->whereBetween('created_at',[$start_date,$end_date])->get();
      $active= Clients::where('status','=',4)->whereBetween('created_at',[$start_date,$end_date])->get();
      $activenotloyal= Clients::where('status','=',5)->whereBetween('created_at',[$start_date,$end_date])->get();
      $inactiveloyal= Clients::where('status','=',6)->whereBetween('created_at',[$start_date,$end_date])->get();
      $inactivenotloyal= Clients::where('status','=',7)->whereBetween('created_at',[$start_date,$end_date])->get();
            
        }else{
            $cold = Clients::where('status','=',1)->get();
      $warm = Clients::where('status','=',2)->get();
      $hot= Clients::where('status','=',3)->get();
      $active= Clients::where('status','=',4)->get();
      $activenotloyal= Clients::where('status','=',5)->get();
      $inactiveloyal= Clients::where('status','=',6)->get();
      $inactivenotloyal= Clients::where('status','=',7)->get();
        }
        
       if($request->input('interviewrange')){
            $range = $request->input('interviewrange');
            $range = explode(' - ', $range);
            
            $start_date = Carbon::parse($range[0])->toDateTimeString();

       $end_date = Carbon::parse($range[1])->toDateTimeString();
       
       $interviewed = Interview::where('Status','=','Interviewed')->whereBetween('created_at',[$start_date,$end_date])->get();
      $scheduled = Interview::where('Status','=','scheduled')->whereBetween('created_at',[$start_date,$end_date])->get();
      $shortlisted = Interview::where('Status','=','Shortlisted')->whereBetween('created_at',[$start_date,$end_date])->get();
      $placed = Interview::where('Status','=','Placed')->whereBetween('created_at',[$start_date,$end_date])->get();
        }else{
           $interviewed = Interview::where('Status','=','Interviewed')->get();
      $scheduled = Interview::where('Status','=','scheduled')->get();
      $shortlisted = Interview::where('Status','=','Shortlisted')->get();
      $placed = Interview::where('Status','=','Placed')->get();
        }
      //end of candidate status
      //staff status
      
       if($request->input('ticketrange')){
            $range = $request->input('ticketrange');
            $range = explode(' - ', $range);
            
            $start_date = Carbon::parse($range[0])->toDateTimeString();

       $end_date = Carbon::parse($range[1])->toDateTimeString();
       
       $tickets = Ticket::whereBetween('created_at',[$start_date,$end_date])->get();
      $opentickets = Ticket::where('status','=',0)->whereBetween('created_at',[$start_date,$end_date])->get();
      $closedtickets = Ticket::where('status','=',1)->whereBetween('created_at',[$start_date,$end_date])->get();
      
        }else{
          $tickets = Ticket::all();
      $opentickets = Ticket::where('status','=',0)->get();
      $closedtickets = Ticket::where('status','=',1)->get();
        }
        //dd(count($clients));
        
      $stafffs = User::where('role','=',0)->orWhere('role','=',1)->get();
      //endofstaff status
      
       $industries = Industry::orderBy('Name')->get();

    	return view ('dashboard', compact('clients','users','jobs','interview','staff','tickets','cold','warm','hot','active','activenotloyal','inactiveloyal','inactivenotloyal','placed','shortlisted','scheduled','interviewed','opentickets','closedtickets','stafffs','industries'));
        }
    }
     public function json(Request $request)
    {
      $collections=JobAd::where('StaffID','=', Auth::user()->SummitStaff->StaffID)->where('Deadline','>=', Carbon::now())->orderBy('ID','ASC')->get();
      $value='#';

      foreach ($collections as $key => $value)
      {
        $jobtype = $request->input('type');
        $clients = $request->input('clients');
        $education = $request->input('education');
        $category  = $request->input('category');
        $careerlevel = $request->input('careerlevel');
        $jobtitle = $request->input('jobtitle');



        //search whole string using just a subsuffix
         if($jobtitle)
         {
            $searchTerm = strtolower($value->JobTitle);
            $str = strtolower($jobtitle);
            $pos = strstr($searchTerm, $str);
            if($pos === false){
             unset($collections[$key]);
           }
         }

        // if ($jobtitle)
        // {
        //   if ($value->JobTitle != $jobtitle)
        //   {
        //       unset($collections[$key]);
        //   }
        // }

        if($clients)
        {
         if ($value->ClientID != $clients)
         {
            unset($collections[$key]);
         }
        }
        //
        if($category)
        {
         if($value->Category != $category)
         {
           unset($collections[$key]);
         }
        }

       if($careerlevel)
       {
         if($value->CareerLevel !=$careerlevel)
         {
           unset($collections[$key]);
         }
       }

       if($education)
       {

         if($value->MinEduReq != $education)
         {
           unset($collections[$key]);
         }
       }

       if($jobtype)
       {
         if($value->JobType != $jobtype)
         {
           unset($collections[$key]);
         }
       }
      }
     $query = new Collection($collections);
      return Datatables::of($query)

         ->addColumn('full_salary', function ($list) {
                return $list->SalCurrency.', '.$list->GrossMonthSal;
            })
          ->addColumn('client', function ($list) {
                return $list->Clients->CompanyName;
            })
        ->addColumn('Actions', function ($list) {
             return '
        <span style="overflow: visible; position: relative; width: 110px;">
        <a title="View details" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="'.route('jobAds.show',$list->ID).'">
           <i class="fas fa-eye" style="color:#80CFFF"></i>
          </a>
            <a title="View Applicants" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="'.route('jobAds.viewApplicants',$list->ID).'">
            <i class="fas fa-users" style="color: #3069AB"></i>
          </a>
          <a title="Edit details" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="' . route('jobAds.edit', $list->ID) . '">
            <i class="fas fa-edit" style="color: #3069AB"></i>
          </a>
      </span>';
        })
        ->rawColumns(['Actions'])
        ->make();
  }
  public function json2(Request $request)
  {
      $collections = Interview::all();
      $value = '#';

      foreach ($collections as $key => $value)
      {
        $jobad = $request->input('jobad');
        $status = $request->input('status');
        $interviewdate = $request->input('interviewdate');
        $firstname = $request->input('firstname');
        $lastname = $request->input('lastname');

        if($firstname)
        {
            if($value->candidates->RegDetails){
          $searchTerm = strtolower($value->candidates->RegDetails->Firstname);
          $str = strtolower($firstname);
          $pos = strstr($searchTerm, $str);

          if($pos === false){
           unset($collections[$key]);
         }
         }else{
             unset($collections[$key]);
         }
        }
        if($lastname)
        {
            if($value->candidates->RegDetails){
           $searchTerm = strtolower($value->candidates->RegDetails->Lastname);
           $str = strtolower($lastname);
           $pos = strstr($searchTerm, $str);
           if($pos === false){
            unset($collections[$key]);
          }
        }else{
               unset($collections[$key]);
          }
        }
        if($interviewdate)
        {
          
          if(date('Y-m-d',strtotime($value->InterviewDate)) != $interviewdate)
          {
            unset($collections[$key]);
          }
        }
        if($jobad)
        {
          if($value->JobAD_ID != $jobad)
          {
            unset($collections[$key]);
          }
        }

        if($status)
        {
          if($value->Status != $status)
          {
            unset($collections[$key]);
          }
        }

        if(Auth::user()->SummitStaff->StaffID != $value->StaffID){
              unset($collections[$key]);
          }
        # code...
      }
      $query = new Collection($collections);

      return Datatables::of($query)
      ->addColumn('CV_ID', function($list){

        /*The candidates object is referenced from the interview's model*/
        if($list->candidates->RegDetails){
         return $list->candidates->RegDetails->Firstname.' '.$list->candidates->RegDetails->Lastname;
        }
       })
       ->addColumn('JobAD_ID', function($list){
         return $list->jobs->JobTitle;
       })
       ->addColumn('StaffID', function($list){
         if(empty($list->staffs)){
           return '';
         }else{
           return $list->staffs->Firstname.' '.$list->staffs->Lastname;
         }
       })
        ->addColumn('InterviewDate',function($list){
         return date("d-m-Y", strtotime($list->InterviewDate));
       })
       ->addColumn('Actions', function ($list) {
              return '
        <span style="overflow: visible; position: relative; width: 100px;">
        <a title="View details" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="'.route('candidateCV',[$list->CV_ID, $list->JobAD_ID]).'">
           <i class="fas fa-eye" style="color:#80CFFF"></i>
          </a>
          <a title="Edit details" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="' . route('interviewmanagement.edit', $list->InterviewID) . '">
            <i class="fas fa-edit" style="color: #3069AB"></i>
          </a>
      </span>';


        })
        ->rawColumns(['Actions'])
        ->make();
  }
  public function show($id){
    return redirect()->action('UserController@index',['id'=>$id]);
  }
  
  
        public function  dashboardFilter(Request $request)
    {
    
            
        if($request->input('ytd')){
            $range = $request->input('ytd');
            
            $start_date = Carbon::parse($range)->toDateTimeString();
            
            $clients = Clients::where('created_at','>', $start_date)->get();
            $users = User::where('created_at','>', $start_date)->get();
            $jobs = JobAd::where('created_at','>', $start_date)->get();
            $interview = Interview::where('created_at','>', $start_date)->get();
            $staff = SummitStaff::where('created_at','>', $start_date)->get();
            $cold = Clients::where('status','=',1)->where('created_at','>', $start_date)->get();
      $warm = Clients::where('status','=',2)->where('created_at','>', $start_date)->get();
      $hot= Clients::where('status','=',3)->where('created_at','>', $start_date)->get();
      $active= Clients::where('status','=',4)->where('created_at','>', $start_date)->get();
      $activenotloyal= Clients::where('status','=',5)->where('created_at','>', $start_date)->get();
      $inactiveloyal= Clients::where('status','=',6)->where('created_at','>', $start_date)->get();
      $inactivenotloyal= Clients::where('status','=',7)->where('created_at','>', $start_date)->get();
      $interviewed = Interview::where('Status','=','Interviewed')->where('created_at','>', $start_date)->get();
      $scheduled = Interview::where('Status','=','scheduled')->where('created_at','>', $start_date)->get();
      $shortlisted = Interview::where('Status','=','Shortlisted')->where('created_at','>', $start_date)->get();
      $placed = Interview::where('Status','=','Placed')->where('created_at','>', $start_date)->get();
       $tickets = Ticket::where('created_at','>', $start_date)->get();
      $opentickets = Ticket::where('status','=',0)->where('created_at','>', $start_date)->get();
      $closedtickets = Ticket::where('status','=',1)->where('created_at','>', $start_date)->get();

   
                
         }else{
             $clients = Clients::all();
            $users = User::all();
            $jobs = JobAd::all();
            $interview = Interview::all();
            $staff = SummitStaff::all();
              $cold = Clients::where('status','=',1)->get();
      $warm = Clients::where('status','=',2)->get();
      $hot= Clients::where('status','=',3)->get();
      $active= Clients::where('status','=',4)->get();
      $activenotloyal= Clients::where('status','=',5)->get();
      $inactiveloyal= Clients::where('status','=',6)->get();
      $inactivenotloyal= Clients::where('status','=',7)->get();
             $interviewed = Interview::where('Status','=','Interviewed')->get();
      $scheduled = Interview::where('Status','=','scheduled')->get();
      $shortlisted = Interview::where('Status','=','Shortlisted')->get();
      $placed = Interview::where('Status','=','Placed')->get();
             $tickets = Ticket::all();
      $opentickets = Ticket::where('status','=',0)->get();
      $closedtickets = Ticket::where('status','=',1)->get();
         }
            
        
      $stafffs = User::where('role','=',0)->orWhere('role','=',1)->orderBy('id')->paginate(20);
      //endofstaff status
      
       $industries = Industry::orderBy('Name')->get();

    	return view ('dashboard', compact('clients','users','jobs','interview','staff','tickets','cold','warm','hot','active','activenotloyal','inactiveloyal','inactivenotloyal','placed','shortlisted','scheduled','interviewed','opentickets','closedtickets','stafffs','industries'));
        }
    
  
}
