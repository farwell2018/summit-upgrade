<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Clients;
use App\Country;
use App\Currency;
use App\Industry;
use App\Location;
use App\JobAd;
use App\SummitStaff;
use App\JobRequirement;
use App\JobDuty;
use App\PreQualTest;
use App\PreQualAnswers;
use App\CandidateAnswers;
use App\RegistrationDetails;
use App\CvTable;
use App\JobCV;
use App\User;
use App\HardSkill;
use App\Subject;

use Redirect;
use Toastr;
use Session;
use Validator;
use Auth;
use Notifications;
use DB;

class SubjectController extends Controller
{
    //

    public function index(){

        $subjects = Subject::all();
        return view ('subject.index', compact('subjects'));

    }

    // public function json()
    // {
    //     print_r("Adadasdasd");die;
    //     $query = Industry::orderBy('id','ASC')->get();
    //     print_r($query);die;
    //     return Datatables::of($query)

    //         ->addColumn('full_salary', function ($list) {
    //             return $list->SalCurrency.', '.$list->GrossMonthSal;
    //         })
    //         ->addColumn('client', function ($list) {
    //             return $list->Clients->CompanyName;
    //         })
    //         ->rawColumns(['tools'])
    //         ->make();

    // }

    public function create(){

        // $industries = Industry::all();
        return view ('subject.create');
    }

    public function store(Request $request){


        $validator = Validator::make($request->except('_token'), [
            'subjectname' => ['required', 'string'],
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();

            // dd($errors);
            return Redirect::back()->withInput()->withErrors($errors);
        }else {

            $subject = new Subject();
            $subject->SubjectTitle = $request->input('subjectname');
            $subject->save();
            Toastr::success('Subject has been created.');
            return redirect()->route('subject.index');
        }
    }


    public function edit($id){

        $subject = Subject::where('SubjectID','=',$id)->first();
        return view ('subject.edit',compact('subject'));

    }

    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->except('_token'), [
            'subjectname' => ['required', 'string'],
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();

            // dd($errors);
            return Redirect::back()->withInput()->withErrors($errors);
        }else {

            $insertdata = [
                'SubjectTitle' => $request->input('subjectname')];

            DB::table('subjects')->where('SubjectID', $id)->update($insertdata);

            Toastr::success('Subject has been updated.');
            return redirect()->route('subject.index');
        }
    }


    // public function view($id){
    //     $industry = Industry::where('ID','=', $id)->first();
    //     return view('industry.view',compact('industry'));
    // }
     

     public function delete($id){
        Subject::where('SubjectID','=', $id)->delete();

        Toastr::success('Subject has been deleted.');
        return redirect()->route('subject.index');
    }

    
}
