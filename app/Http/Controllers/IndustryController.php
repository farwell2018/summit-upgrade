<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Clients;
use App\Country;
use App\Currency;
use App\Industry;
use App\Location;
use App\JobAd;
use App\SummitStaff;
use App\JobRequirement;
use App\JobDuty;
use App\PreQualTest;
use App\PreQualAnswers;
use App\CandidateAnswers;
use App\RegistrationDetails;
use App\CvTable;
use App\JobCV;
use App\User;

use Redirect;
use Toastr;
use Session;
use Validator;
use Auth;
use Notifications;

class IndustryController extends Controller
{
    //

    public function index(){

        $industries = Industry::orderBy('Name')->get();
        return view ('industry.index', compact('industries'));

    }

    public function json()
    {
        print_r("Adadasdasd");die;
        $query = Industry::orderBy('id','ASC')->get();
        print_r($query);die;
        return Datatables::of($query)

            ->addColumn('full_salary', function ($list) {
                return $list->SalCurrency.', '.$list->GrossMonthSal;
            })
            ->addColumn('client', function ($list) {
                return $list->Clients->CompanyName;
            })
            ->rawColumns(['tools'])
            ->make();

    }

    public function create(){

        $industries = Industry::all();
        return view ('industry.create', compact('industries'));
    }

    public function store(Request $request){


        $validator = Validator::make($request->except('_token'), [
            'name' => ['required', 'string'],
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();

            // dd($errors);
            return Redirect::back()->withInput()->withErrors($errors);
        }else {

            $jobAd = new Industry();
            $jobAd->Name = $request->input('name');
            $jobAd->Parent = ($request->input('parent')) ? $request->input('parent') : null;
            $jobAd->Details = ($request->input('details')) ? $request->input('details') : null;
            $jobAd->save();
            Toastr::success('Industry has been created.');
            return redirect()->route('industry.index');
        }
    }


    public function edit($id){

        $industrydata = Industry::where('ID','=',$id)->first();
        $industries = Industry::all();
        return view ('industry.edit',compact('industrydata','industries'));

    }

    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->except('_token'), [
            'name' => ['required', 'string'],
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();

            // dd($errors);
            return Redirect::back()->withInput()->withErrors($errors);
        }else {

            $insertdata = [
                'Name' => $request->input('name'),
                'Parent' => ($request->input('parent')) ? $request->input('parent') : null,
                'Details' => ($request->input('details')) ? $request->input('details') : null
            ];
            \DB::table('industry')->where('id', $id)->update($insertdata);
            Toastr::success('Industry has been update.');
            return redirect()->route('industry.index');
        }
    }


    public function view($id){
        $industry = Industry::where('ID','=', $id)->first();
        return view('industry.view',compact('industry'));
    }
     

     public function delete($id){
        Industry::where('ID','=', $id)->delete();

        Toastr::success('Industry has been delete.');
        return redirect()->route('industry.index');
    }

    
}
