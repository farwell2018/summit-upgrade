<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\Eloquent\Collection;

use App\Clients;
use App\Country;
use App\Currency;
use App\Industry;
use App\Location;
use App\JobAd;
use App\SummitStaff;
use App\JobRequirement;
use App\IndustryFunctions;
use App\JobDuty;
use App\PreQualTest;
use App\PreQualAnswers;
use App\CandidateAnswers;
use App\RegistrationDetails;
use App\CvTable;
use App\JobCV;
use App\User;
use App\Interview;
use App\HardSkill;
use App\ProfQualTitles;
use App\competencies;
use App\InterviewCompetency;
use App\InterviewHardSkill;
use App\InterviewCandidateTest;
use App\InterviewReferee;

use Redirect;
use DB;
use Toastr;
use Session;
use Validator;
use File;
use Auth;
use Notifications;

use Carbon\Carbon;

class InterviewManagementController extends Controller
{
    public function index(){
      $clients = Clients::all();
      $users = User::all();
      $jobs = JobAd::orderBy('JobTitle')->get();
      $interview = Interview::orderBy('InterviewID')->paginate(10);
      $staff = SummitStaff::orderBy('Firstname')->get();
      $industryfunctionss=IndustryFunctions::all();
      $hardskillls = HardSkill::all();
      $currency=Currency::all();
        $industries = Industry::orderBy('Name')->get();

      return view('interviewmanagement.index', compact('clients','users','jobs','interview','staff','industryfunctionss','hardskillls','currency','industries'));
    }
       public function json(Request $request)
  {
      $collections = Interview::where('InterviewDate','>=','2020-02-01 00:00:00')->orderBy('InterviewDate','DESC')->get();
      $value = '#';

        foreach ($collections as $key =>$value)
        {
            
          $jobad = $request->input('jobad');
          $consultant = $request->input('consultant');
          $status = $request->input('status');
          $interviewdate = $request->input('interviewdate');
          $firstname = $request->input('firstname');
          $lastname = $request->input('lastname');
          $category = $request->input('category');
          $functions = $request->input('functions');
          $hardskills = $request->input('hardskills');

          if($firstname)
          {
              if($value->candidates->RegDetails){
            $searchTerm = strtolower($value->candidates->RegDetails->Firstname);
            $str = strtolower($firstname);
            $pos = strstr($searchTerm, $str);

            if($pos === false){
             unset($collections[$key]);
           }
           
              }else{
                  unset($collections[$key]);
              }
          }
          if($lastname)
          {
              if($value->candidates->RegDetails){
             $searchTerm = strtolower($value->candidates->RegDetails->Lastname);
             $str = strtolower($lastname);
             $pos = strstr($searchTerm, $str);
             if($pos === false){
              unset($collections[$key]);
            }
              }else{
                 unset($collections[$key]); 
                  
              }
          }
          if($interviewdate)
          {
            if(date('Y-m-d',strtotime($value->InterviewDate)) != $interviewdate)
            {
              unset($collections[$key]);
            }
          }
          if($jobad)
          {
            if($value->JobAD_ID != $jobad)
            {
              unset($collections[$key]);
            }
          }
          if($consultant)
          {
            if($value->StaffID != $consultant)
            {
              unset($collections[$key]);
            }
          }
          if($status)
          {
            if($value->Status != $status)
            {
              unset($collections[$key]);
            }
          }
          if($category)
          {
            if($value->Jobs->Category != $category)
            {
              unset($collections[$key]);
            }
          }
          if($functions)
          {
            $IFunction= array();
            foreach($value->candidates->WorkExp as $if){
              $IFunction[]= $if->Title;
            }

            if(!in_array($functions, $IFunction))
            {
                unset($collections[$key]);
            }
          }

          if($hardskills)
        {
           $hards = InterviewHardSkill::where('InterviewID',$value->InterviewID)->get();
           if($hards){
              $hs = array();
           foreach($hards as $hard){
            $hs[]= $hard->HardSkill;
           }
           
          if(!in_array($hardskills, $hs))
          {
            unset($collections[$key]);
          } 
           }else{
               unset($collections[$key]);
           }
        }
        }
    $query = new Collection($collections);

      return Datatables::of($query)

       ->addColumn('CV_ID', function($list){
         if($list->candidates->RegDetails){
             return $list->candidates->RegDetails->Firstname.' '.$list->candidates->RegDetails->Lastname;
         }
         
       })
       ->addColumn('JobAD_ID', function($list){
         return $list->jobs->JobTitle;
       })
       ->addColumn('StaffID', function($list){
         if(empty($list->staffs)){
           return '';
         }else{
           return $list->staffs->Firstname.' '.$list->staffs->Lastname;
         }
       })
       ->addColumn('InterviewDate',function($list){
         return date("d-m-Y", strtotime($list->InterviewDate));
       })
      ->addColumn('tools', function ($list)
      {
          if (Auth::user()->role == 0)
          {
             
          $a = '<span style="overflow: visible; position: relative; width: 100px;">
        <a title="View candidate details" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="'.route('candidateCV',[$list->CV_ID, $list->JobAD_ID]).'">
           <i class="fas fa-eye" style="color:#80CFFF"></i>
          </a>
          <a title="Edit Client Status" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="' . route('interviewmanagement.edit', $list->InterviewID) . '">
            <i class="fas fa-edit" style="color: #3069AB"></i>
          </a>';
          
          if($list->Status == "Blacklist"){
               $b ='  <a title="Remove Blacklist" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="'.route('interviewmanagement.blacklist',[$list->CV_ID, 0]).'">
             <i class="fa fa-user-times" style="color: #FA0000"></i></a>';
          }else{
               $b ='  <a title="Blacklist" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="'.route('interviewmanagement.blacklist',[$list->CV_ID, 1]).'">
             <i class="fa fa-user" style="color: #5cb85c"></i></a>';
          }
          
          $c = '<a
            title="Delete candidate details"
            class="btn btn-sm btn-clean btn-icon btn-icon-sm"
            data-href="'.route('interviewmanagement.delete',$list->InterviewID).'"
            data-toggle="modal"
            data-target="#deleteConfirmModal"
            >
            <i class="far fa-trash-alt" style="color:#FA0000"></i>
          </a>
      </span>';
        
        return $a.$b.$c;
      
         }
         else{
          $a = '<span style="overflow: visible; position: relative; width: 100px;">
        <a title="View details" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="'.route('candidateCV',[$list->CV_ID, $list->JobAD_ID]).'">
           <i class="fas fa-eye" style="color:#80CFFF"></i>
          </a>
          <a title="Edit details" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="'.route('interviewmanagement.edit', $list->InterviewID) .'">
            <i class="fas fa-edit" style="color: #3069AB"></i>
          </a>';
           if($list->Status == "Blacklist"){
               $b ='  <a title="Remove Blacklist" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="'.route('interviewmanagement.blacklist',[$list->CV_ID, 0]).'">
             <i class="fa fa-user-times" style="color: #FA0000"></i></a>';
          }else{
               $b ='  <a title="Blacklist" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="'.route('interviewmanagement.blacklist',[$list->CV_ID, 1]).'">
             <i class="fa fa-user" style="color: #5cb85c"></i></a>';
          }
          
      $c = '</span>';
      
        return $a.$b.$c;
              }
        })
        ->rawColumns(['tools'])
        ->make();

  }
   public function create($id)
   {
    $candidates = Interview::where('InterviewID',$id)->first();
    $candidatecvs=CvTable::where('CV_ID',$candidates->CV_ID)->first();
    $regdetails = RegistrationDetails::where('CanditateRegID',$candidatecvs->CandidateRegID)->first();
    $jobAds = JobAd::orderBy('JobTitle')->get();
    $industry=Industry::all();
    $competencies = competencies::all();
    $staff = SummitStaff::all();
    foreach ($candidates as $key => $value) {
      
    array_push($hskills, $value->HardSkills);
}
    $hardskills=HardSkill::all();
    $titles=ProfQualTitles::all();
    $candidate = Interview::all();
    return view ('interviewmanagement.create',compact('candidatecvs','candidates','regdetails','jobAds','industry','titles','candidate','hskills','competencies','staff'));
   }
   public function noCreate($id)
  {
     // $candidatereg= Interview::where('InterviewID',$id)->first();
     $cvs = CvTable::where('CV_ID',$id)->first();
     $regdets = RegistrationDetails::where('CanditateRegID',$cvs->CandidateRegID)->first();
     $jobdets = JobAd::orderBy('JobTitle')->get();
     $hardskills=HardSkill::all();
     $industry = Industry::all();
     $candidate = Interview::all();
     $currency = Currency::all();
     $competencies = competencies::all();
     $staff = SummitStaff::all();

      return view ('interviewmanagement.create',compact('cvs','regdets','jobdets','industry','candidate','currency','hardskills','competencies','staff'));
  }


   public function store(Request $request)
  {
    $validator = Validator::make($request->except('_token'), [
    'interview_date' => ['required'],
    'internalcomments'=>['required'],

      ]);
      if ($validator->fails()) {
        $errors = $validator->errors();
      return Redirect::back()->withInput()->withErrors($errors);
      }else
      {
          
        $candidates = new Interview();
        $candidates->InterviewDate = $request->input('interview_date');
        $candidates->CV_ID = $request->input('cv_id');
        $candidates->ExpYrs =  $request->input('years_experience');
        $candidates->ManagementExp = $request->input('management_experience');
        $candidates->Management = $request->input('management');
        $candidates->Status = $request->input('status');
        $candidates->TestResult_1 = $request->input('test_details');
        $candidates->TestResult_2 = $request->input('word_drafting');
        $candidates->TestResult_3 = $request->input('excel_test');
        $candidates->TestResult_4 = $request->input('situation_test');
        $candidates->TestResult_5 = $request->input('time_manage');
        $candidates->BenefitsReceived = $request->input('benefitsreceived');
        $candidates->lumina = $request->input('lumina');
        $candidates->SalaryExpectation = $request->input('salaryexpectation');
        $candidates->CurrentSalary = $request->input('currentsalary');
        $candidates->NoticePeriod = $request->input('notice_period');
        $candidates->Industry =  $request->input('industry');
        $candidates->JobAD_ID =  $request->input('job');
        $candidates->DateofReference = $request->input('dateofreference');
        $candidates->RefereeName = $request->input('refereename');
        $candidates->CurrentContacts = $request->input('currentcontact');
        $candidates->CurrentCompany = $request->input('currentcompany');
        $candidates->PositionCandidate = $request->input('positionattime');
        $candidates->InterviewSummary = $request->input('interview_summary');
        $candidates->InternalComments = $request->input('internalcomments');
        
        $candidates->GrossSalCurrency = $request->input('salarycurrency');
        $candidates->GrossCurrency = $request->input('grosssalcurr');
         $candidates->StaffID = $request->input('staff_assigned');
         
         if (!is_null($request->file('kra')))
        {
        $candidates->KRA = $this->Fileupload2($request,'kra','kra');
        }
         if (!is_null($request->file('identification')))
        {
        $candidates->ID = $this->Fileupload2($request,'identification','ID');
        }
        
         if (!is_null($request->file('nhif')))
        {
        $candidates->NHIF = $this->Fileupload2($request,'nhif','NHIF');
        }
         if (!is_null($request->file('nssf')))
        {
        $candidates->NSSF = $this->Fileupload2($request,'nssf','NSSF');
        }
        
         if (!is_null($request->file('cogc')))
        {
        $candidates->COG = $this->Fileupload2($request,'COG','cog/');
        }
        
         if (!is_null($request->file('brc')))
        {
        $candidates->PQC = $this->Fileupload2($request,'brc','crb/');
        }
        
         if (!is_null($request->input('eacc')))
        {
        $candidates->EC = $this->Fileupload2($request,'eacc','eacc/');
        }
        
        if (!is_null($request->file('reffeeeree')))
        {
        $candidates->Refpdf3 = $this->Fileupload2($request,'reffeeeree','refereedetails/referee/');
        }
        
        
        
        $candidates->save();
        
        if($_POST['InterviewFields']){
            
        $requirement= $_POST['InterviewFields'];
        
        
        
        foreach ($requirement as $req){
            
            
          $quest = new InterviewCompetency();
          $quest->InterviewID = $candidates->id;
          $quest->compitency = $req['competency'];
          $quest->rating = $req['rating'];
          $quest->comment = $req['dropdowncomments'];
          $quest->save();

        }
        }
        Toastr::success('Interview Notes Have Been Created.');
        return redirect()->route('interviewmanagement.index')->with('status','Interview Notes Successfully Created');
      }
  }
  public function edit($id){
    $candidates = Interview::where('InterviewID',$id)->first();
    if ($candidates->status == 1) {
      $client->status = 'Scheduled';
    } elseif ($candidates->status == 2) {
      $client->status = 'Shortlisted';
    } elseif ($candidates->status == 3) {
      $client->status = 'Interviewed';
    } elseif ($candidates->status == 4) {
      $client->status = 'Recommended';
    } elseif ($candidates->status == 5) {
      $client->status = 'Placed';
    } elseif ($candidates->status == 6) {
      $client->status = 'Discard';
    } elseif ($candidates->status == 7) {
      $client->status = 'Blacklisted';
    }
    if ($candidates->TestResult_2 == 1) {
      $candidates->TestResult_2 = 'Excellent';
    } elseif ($candidates->TestResult_2== 2) {
      $candidates->TestResult_2 = 'V.Good';
    } elseif ($candidates->TestResult_2 == 3) {
      $candidates->TestResult_2 = 'Good';
    } elseif ($candidates->TestResult_2 == 4) {
      $candidates->TestResult_2 = 'Fair';
    } elseif ($candidates->TestResult_2 == 5){
      $candidates->TestResult_2 = 'Unsatisfactory';
    }
    if ($candidates->TestResult_3 == 1) {
      $candidates->TestResult_3 = 'Excellent';
    } elseif ($candidates->TestResult_3== 2) {
      $candidates->TestResult_3 = 'V.Good';
    } elseif ($candidates->TestResult_3 == 3) {
      $candidates->TestResult_3 = 'Good';
    } elseif ($candidates->TestResult_2 == 4) {
      $candidates->TestResult_3 = 'Fair';
    } elseif ($candidates->TestResult_2 == 5){
      $candidates->TestResult_3 = 'Unsatisfactory';
    }
    if ($candidates->TestResult_4 == 1) {
      $candidates->TestResult_4 = 'Excellent';
    } elseif ($candidates->TestResult_4== 2) {
      $candidates->TestResult_4 = 'V.Good';
    } elseif ($candidates->TestResult_4 == 3) {
      $candidates->TestResult_4 = 'Good';
    } elseif ($candidates->TestResult_4 == 4) {
      $candidates->TestResult_4 = 'Fair';
    } elseif ($candidates->TestResult_4 == 5){
      $candidates->TestResult_4 = 'Unsatisfactory';
    }
   if ($candidates->TestResult_5 == 1) {
      $candidates->TestResult_5 = 'Excellent';
    } elseif ($candidates->TestResult_5== 2) {
      $candidates->TestResult_5 = 'V.Good';
    } elseif ($candidates->TestResult_5 == 3) {
      $candidates->TestResult_5 = 'Good';
    } elseif ($candidates->TestResult_5 == 4) {
      $candidates->TestResult_5 = 'Fair';
    } elseif ($candidates->TestResult_5 == 5){
      $candidates->TestResult_5 = 'Unsatisfactory';
    }
    $candidatecvs=CvTable::where('CV_ID',$candidates->CV_ID)->first();
    $regdetails = RegistrationDetails::where('CanditateRegID',$candidatecvs->CandidateRegID)->first();
    $jobAds = JobAd::orderBy('JobTitle')->get();
    $industry=Industry::all();
    $titles=ProfQualTitles::all();
    $hardskills=HardSkill::all();
    $candidate=Interview::all();
    $currency=Currency::all();
    $jobcv = JobCV::where('CV_ID',$candidates->CV_ID)->first();
    $competencies = competencies::all();
    $interviewCompetency = InterviewCompetency::where('InterviewID',$id)->get();
    $staff = SummitStaff::all();

    return view('interviewmanagement.edit',compact('candidates','jobAds','industry','candidatecvs','regdetails','titles','candidatecvs','candidate','currency','hardskills','jobcv','competencies','interviewCompetency','staff'))->with('status','Candidate Notes Successfully Updated');
  }

  public function update(Request $request, $id)
  {
    
    
   $validator = Validator::make($request->except('_token'), [
      'interview_date' => ['required'],
      'internalcomments'=>['required'],
      ]);
      
     
      
       $data = $request->all();
       
       $interview = DB::table('interview')->where('InterviewID', $id)->first();
   
       
        if (isset($data['kra'])) {

        $file = $request->file('kra');

        // check file type
        $extensions = ['doc', 'docx', 'pdf', 'png', 'jpg', 'jpeg','csv'];
        $data['kra'] = $file->getClientOriginalName();
        if (!in_array($file->getClientOriginalExtension(), $extensions)) {
            //return 'file type not supported';
        }
        else {
          $data['kra'] = str_replace('/tmp/', '', $data['kra']);
          //Move Uploaded File
          $destinationPath = '/interview/kra';
           
          $file->move(public_path($destinationPath), $data['kra']);
          //dd($file->move($destinationPath, $data['proposal_request']));
        }
      } else {
        $data['kra'] = $interview->KRA;

      }
      
      if (isset($data['nhif'])) {

        $file = $request->file('nhif');

        // check file type
        $extensions = ['doc', 'docx', 'pdf', 'png', 'jpg', 'jpeg','csv'];
        $data['nhif'] = $file->getClientOriginalName();
        if (!in_array($file->getClientOriginalExtension(), $extensions)) {
            //return 'file type not supported';
        }
        else {
          $data['nhif'] = str_replace('/tmp/', '', $data['nhif']);
          //Move Uploaded File
          $destinationPath = '/interview/NHIF';
           
          $file->move(public_path($destinationPath), $data['nhif']);
          //dd($file->move($destinationPath, $data['proposal_request']));
        }
      } else {
        $data['nhif'] = $interview->NHIF;

      }
      
      
      if (isset($data['identification'])) {

        $file = $request->file('identification');

        // check file type
        $extensions = ['doc', 'docx', 'pdf', 'png', 'jpg', 'jpeg','csv'];
        $data['identification'] = $file->getClientOriginalName();
        if (!in_array($file->getClientOriginalExtension(), $extensions)) {
            //return 'file type not supported';
        }
        else {
          $data['identification'] = str_replace('/tmp/', '', $data['identification']);
          //Move Uploaded File
          $destinationPath = '/interview/ID';
           
          $file->move(public_path($destinationPath), $data['identification']);
          //dd($file->move($destinationPath, $data['proposal_request']));
        }
      } else {
        $data['identification'] = $interview->ID;

      }
        
    
       if (isset($data['nssf'])) {

        $file = $request->file('nssf');

        // check file type
        $extensions = ['doc', 'docx', 'pdf', 'png', 'jpg', 'jpeg','csv'];
        $data['nssf'] = $file->getClientOriginalName();
        if (!in_array($file->getClientOriginalExtension(), $extensions)) {
            //return 'file type not supported';
        }
        else {
          $data['nssf'] = str_replace('/tmp/', '', $data['nssf']);
          //Move Uploaded File
          $destinationPath = '/interview/NSSF';
           
          $file->move(public_path($destinationPath), $data['nssf']);
          //dd($file->move($destinationPath, $data['proposal_request']));
        }
      } else {
        $data['nssf'] = $interview->NSSF;

      }
    
    if (isset($data['cogc'])) {

        $file = $request->file('cogc');

        // check file type
        $extensions = ['doc', 'docx', 'pdf', 'png', 'jpg', 'jpeg','csv'];
        $data['cogc'] = $file->getClientOriginalName();
        if (!in_array($file->getClientOriginalExtension(), $extensions)) {
            //return 'file type not supported';
        }
        else {
          $data['cogc'] = str_replace('/tmp/', '', $data['cogc']);
          //Move Uploaded File
          $destinationPath = '/interview/COG';
           
          $file->move(public_path($destinationPath), $data['cogc']);
          //dd($file->move($destinationPath, $data['proposal_request']));
        }
      } else {
        $data['cogc'] = $interview->COG;

      }
  
       if (isset($data['brc'])) {

        $file = $request->file('brc');

        // check file type
        $extensions = ['doc', 'docx', 'pdf', 'png', 'jpg', 'jpeg','csv'];
        $data['brc'] = $file->getClientOriginalName();
        if (!in_array($file->getClientOriginalExtension(), $extensions)) {
            //return 'file type not supported';
        }
        else {
          $data['brc'] = str_replace('/tmp/', '', $data['brc']);
          //Move Uploaded File
          $destinationPath = '/interview/CRB';
           
          $file->move(public_path($destinationPath), $data['brc']);
          //dd($file->move($destinationPath, $data['proposal_request']));
        }
      } else {
        $data['brc'] = $interview->PQC;

      }
     
    if (isset($data['eacc'])) {

        $file = $request->file('eacc');

        // check file type
        $extensions = ['doc', 'docx', 'pdf', 'png', 'jpg', 'jpeg','csv'];
        $data['eacc'] = $file->getClientOriginalName();
        if (!in_array($file->getClientOriginalExtension(), $extensions)) {
            //return 'file type not supported';
        }
        else {
          $data['eacc'] = str_replace('/tmp/', '', $data['eacc']);
          //Move Uploaded File
          $destinationPath = '/interview/EACC';
           
          $file->move(public_path($destinationPath), $data['eacc']);
          //dd($file->move($destinationPath, $data['proposal_request']));
        }
      } else {
        $data['eacc'] = $interview->EC;

      }
     
        
       
   DB::table('interview')
    ->where('InterviewID', $id)
    ->update([
    'InterviewDate' => $data['interview_date'],
    'Industry' =>  @$data['industry'],
    'JobAD_ID' =>@$data['title'],
    'SalaryExpectation' =>@$data['salaryexpectation'],
    'CurrentSalary' =>@$data['currentsalary'],
    'InterviewSummary' =>  @$data['interviewsummary'],
    'InternalComments' => @$data['internalcomments'],
    'ManagementExp' => @$data['management_experience'],
    'Management' => @$data['management'],
    'StartDate'=>@$data['startdate'],
    'GrossSalCurrency'=>@$data['salarycurrency'],
    'GrossCurrency'=>@$data['grosssalcurr'],
    'GrossSalary'=>@$data['grosssalary'],
    'ClosureComments'=>@$data['comments'],
    'lumina'=>@$data['lumina'],
    'NoticePeriod'=>@$data['notice_period'],
    'CurrentContacts'=>@$data['currentcontact'],
    'DateofReference'=>@$data['dateofreference'],
    'RefereeName'=>@$data['refereename'],
    'PositionCandidate'=>@$data['positionattime'],
    'CurrentCompany'=>@$data['currentcompany'],
    'whyrecom' => @$data['Recommended'],
    'placedon' =>@$data['Placed'],
    'whyblack' =>@$data['Blacklist'],
    'ExpYrs' => @$data['years_experience'],
    'Status' => @$data['status'],
    'TestResult_2' => @$data['word_drafting'],
    'TestResult_3' =>@$data['excel_test'],
    'TestResult_4' =>@$data['situation_test'],
    'TestResult_5' =>@$data['time_manage'],
    'PreQualTestResults' =>@$data['pre_qual'],
    'TestResult_1'=>  @$data['test_details'],
    'KRA' => @$data['kra'],
    'NHIF' =>@$data['nhif'] ,
    'ID' => @$data['identification'] ,
    'NSSF' =>@$data['nssf'],
    'EC' => @$data['eacc'] ,
    'PQC' => @$data['brc']  ,
    'COG'=>  @$data['cogc'],
    'StaffID'=> @$data['staff_assigned'],
     ]);
     
    
     
     if($_POST['InterviewFields']){
     InterviewCompetency::where('InterviewID',$id)->delete();
     
   
     $requirement= $_POST['InterviewFields'];
        
        foreach ($requirement as $req){
          $quest = new InterviewCompetency();
          $quest->InterviewID = $id;
          $quest->compitency = $req['competency'];
          $quest->rating = $req['rating'];
          $quest->comment = $req['dropdowncomments'];
          $quest->save();

        }
     }
     
     if($_POST['hardskills']){
     InterviewHardSkill::where('InterviewID',$id)->delete();
     $hards= $_POST['hardskills'];
     
    
        foreach ($hards as $req){
          $quests = new InterviewHardSkill();
          $quests->InterviewID = $id;
          $quests->HardSkill = $req;
          $quests->save();

        }
     }
     
     
     
      if(isset($data['InterviewReferee'])){
          
     InterviewReferee::where('InterviewID',$id)->delete();
     
     $referee= $data['InterviewReferee']['referee'];
     
     //dd($referee);
        // 
        foreach ($referee as $req){
         if (isset($req)) {
        // check file type
        $extensions = ['doc', 'docx', 'pdf', 'png', 'jpg', 'jpeg','csv'];
        $doc = $req->getClientOriginalName();
        if (!in_array($req->getClientOriginalExtension(), $extensions)) {
            //return 'file type not supported';
        }
        else {
          $doc = str_replace('/tmp/', '', $doc);
          //Move Uploaded File
          $destinationPath = '/interview/Referee';
           
          $req->move(public_path($destinationPath), $doc);
          
        }
      } 
          $ref = new InterviewReferee();
          $ref->InterviewID = $id;
          $ref->referee = $doc;
          $ref->save();

        }
     }
     
    
     
     if(isset($data['InterviewTest'])){
     InterviewCandidateTest::where('InterviewID',$id)->delete();
     
     //dd($data['InterviewTest']);
     
     $tests= $data['InterviewTest']['canTest'];
        
        foreach ($tests as $test){
            
     if (isset($test)) {
        // check file type
        $extensions = ['doc', 'docx', 'pdf', 'png', 'jpg', 'jpeg','csv'];
        $tes = $test->getClientOriginalName();
        if (!in_array($test->getClientOriginalExtension(), $extensions)) {
            //return 'file type not supported';
        }
        else {
          $tes = str_replace('/tmp/', '', $tes);
          //Move Uploaded File
          $destinationPath = '/interview/CandidateTests';
           
          $test->move(public_path($destinationPath), $tes);
          
        }
      } 
            
          $can = new InterviewCandidateTest();
          $can->InterviewID = $id;
          $can->candidate_test = $tes;
          $can->save();

        }
     }
     
     
     
     Toastr::success('Candidate details have been Updated.');
       $list = Interview::where('interviewID',$id)->first();

      return redirect()->route('candidateCV',[$list->CV_ID, $list->JobAD_ID])->with('status','Candidate Notes Successfully Updated');
  }
  
  public function delete(Request $request, $id){
  Interview::where('InterviewID',$id)->delete();

 return redirect()->route('interviewmanagement.index')->with('status','Candidate Notes Successfully Deleted');


}
  public function Fileupload ($request,$file = 'file', $folder)
    {
            $upload = null;
            //create folder if none
            if (!File::exists($folder)) {
                    File::makeDirectory($folder);
            }
            //chec fo file
            if (!is_null($request->file($file))) {
                    $extension = $request->file($file)->getClientOriginalExtension();
                    $fileName = rand(11111, 99999).uniqid().'.' . $extension;
                    $upload = $request->file($file)->move($folder, $fileName);
                    if ($upload) {
                            $upload = $folder.$fileName;
                    } else {
                            $upload = null;
                    }
            }
            return $upload;
    }
    
    
    
  public function Fileupload2 ($request,$file = 'file', $folder)
    {
            $upload = null;
            //create folder if none
            //chec fo file
            if (!is_null($request->file($file))) {
                    $extension = $request->file($file)->getClientOriginalExtension();
                    $fileName = rand(11111, 99999).uniqid().'.' . $extension;
                    $upload = $request->file($file)->move('public/interview/'.$folder, $fileName);
                    if ($upload) {
                            $upload = $fileName;
                    } else {
                            $upload = null;
                    }
            }
            return $upload;
    }
    
    public function Blacklist($id,$val){
         $date = Carbon::now();
         
          if($val == 1){
        $details = DB::table('interview')
	      ->where('CV_ID', $id)
	      ->update([
		     "Status" => 'Blacklist',
		     "updated_at" => $date,

	      ]); 
          }else{
           $details = DB::table('interview')
	      ->where('CV_ID', $id)
	      ->update([
		     "Status" => 'Scheduled',
		     "updated_at" => $date,

	      ]);    
              
          }
	     
	       return redirect()->route('interviewmanagement.index');
	      
         
         
     }
}
