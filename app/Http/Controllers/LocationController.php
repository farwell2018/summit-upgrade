<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Clients;
use App\Country;
use App\Currency;
use App\Industry;
use App\Location;
use App\JobAd;
use App\SummitStaff;
use App\JobRequirement;
use App\JobDuty;
use App\PreQualTest;
use App\PreQualAnswers;
use App\CandidateAnswers;
use App\RegistrationDetails;
use App\CvTable;
use App\JobCV;
use App\User;

use Redirect;
use Toastr;
use Session;
use Validator;
use Auth;
use Notifications;
use DB;

class LocationController extends Controller
{
    //

    public function index(){

        $locations = Location::all();
        return view ('location.index', compact('locations'));

    }

    // public function json()
    // {
    //     print_r("Adadasdasd");die;
    //     $query = Industry::orderBy('id','ASC')->get();
    //     print_r($query);die;
    //     return Datatables::of($query)

    //         ->addColumn('full_salary', function ($list) {
    //             return $list->SalCurrency.', '.$list->GrossMonthSal;
    //         })
    //         ->addColumn('client', function ($list) {
    //             return $list->Clients->CompanyName;
    //         })
    //         ->rawColumns(['tools'])
    //         ->make();

    // }

    public function create(){

        // $industries = Industry::all();
        return view ('location.create');
    }

    public function store(Request $request){


        $validator = Validator::make($request->except('_token'), [
            'locationname' => ['required', 'string'],
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();

            // dd($errors);
            return Redirect::back()->withInput()->withErrors($errors);
        }else {

            $location = new location();
            $location->LocationName = $request->input('locationname');
            $location->save();
            Toastr::success('Location has been created.');
            return redirect()->route('location.index');
        }
    }


    public function edit($id){

        $location = Location::where('LocationID','=',$id)->first();
        return view ('location.edit',compact('location'));

    }

    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->except('_token'), [
            'locationname' => ['required', 'string'],
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();

            // dd($errors);
            return Redirect::back()->withInput()->withErrors($errors);
        }else {

            $insertdata = [
                'LocationName' => $request->input('locationname')];

            DB::table('locations')->where('LocationID', $id)->update($insertdata);

            Toastr::success('Location has been update.');
            return redirect()->route('location.index');
        }
    }


    // public function view($id){
    //     $industry = Industry::where('ID','=', $id)->first();
    //     return view('industry.view',compact('industry'));
    // }
     

     public function delete($id){
        Location::where('LocationID','=', $id)->delete();

        Toastr::success('Location has been delete.');
        return redirect()->route('location.index');
    }

    
}
