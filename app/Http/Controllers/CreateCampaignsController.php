<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\CreateCampaigns;
use App\Clients;
use App\Country;
use App\Currency;
use App\Industry;
use App\Location;
use App\JobAd;
use App\SummitStaff;
use App\JobRequirement;
use App\JobDuty;
use App\PreQualTest;
use App\PreQualAnswers;
use App\CandidateAnswers;
use App\RegistrationDetails;
use App\CvTable;
use App\JobCV;
use App\User;
use App\Interview;

use Redirect;
use Toastr;
use Session;
use Validator;
use Auth;
use Notifications;
use DB;


class CreateCampaignsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('communication.index');
    }

    public function json(Request $request)
   {


     $query = CreateCampaigns::all();
     return Datatables::of($query)
     
         ->addColumn('target_group', function ($list) {
               if($list->target == 1){
                  return 'Interviewed';
               }elseif($list->target == 2){
                 return 'Scheduled';
               }elseif($list->target == 3){
                 return 'Recommended';
               }elseif($list->target == 4){
                 return 'Staff';
               }elseif($list->target == 5){
                 return 'All';
               }
           })
       ->addColumn('tools', function ($list) {
        return '
       <span style="overflow: visible; position: relative; width: 110px;">
       <a title="View details" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="'.route('communication.show',$list->id).'">
          <i class="fas fa-eye" style="color:#80CFFF"></i>
         </a>
         <a title="Edit details" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="' . route('communication.edit', $list->id) . '">
           <i class="fas fa-edit" style="color: #3069AB"></i>
         </a>
     </span>';

       })
       ->rawColumns(['tools'])
       ->make();
 }


    public function create()
    {

      return view ('communication.create');
    }

    public function store(Request $request){
     $validator = Validator::make($request->except('_token'), [
    'communication' => ['required'],
    'target' => ['required'],
    'message'=>['required'],
     ]);

      if ($validator->fails()) {
        $errors = $validator->errors();

      return Redirect::back()->withInput()->withErrors($errors);
      }else{
         $communication = new CreateCampaigns();
         $communication->c_name = $request->input('communication');
         $communication->campaign_info = $request->input('message');
         $communication->target = $request->input('target');

         $communication->save();

      return redirect()->route('communication.index')->withSuccess(['Data saved successfully.']);

    }
  }

    public function show(Request $id ){

      $createcampaigns = CreateCampaigns::where('id',$id)->first();

      return view('createcampaigns.view',compact('createcampaigns'));

     }




      public function edit()
    {
        //
    }

     public function destroy()
    {
        //
    }
}
