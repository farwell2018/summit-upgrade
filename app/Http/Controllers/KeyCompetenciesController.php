<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\competencies;

use Redirect;
use Toastr;
use Session;
use Validator;
use Auth;
use Notifications;
use DB;

class KeyCompetenciesController extends Controller
{
    public function index()
    {
    	$competencies = competencies::orderBy('Name')->get();
    	return view ('competency.index',compact('competencies'));
    }

    public function create()
    {

    	return view('competency.create');
    }
    
    public function store(Request $request)
    {

    	 $validator = Validator::make($request->except('_token'), [
            'competencyname' => ['required', 'string'],
        ]);
    	 if ($validator->fails()) {
            $errors = $validator->errors();

            return Redirect::back()->withInput()->withErrors($errors);

         }
         else {

            $competencies = new competencies();
            $competencies->Name = $request->input('competencyname');
            $competencies->save();
            Toastr::success('competency has been created.');
            return redirect()->route('competencies.index');

         }
    }
    public function edit($id)
    {

    	$competencies = competencies::where('ID','=',$id)->first();

    	return view ('competency.edit',compact('competencies'));
    }

    public function update(Request $request,$id)
    {
    	 $validator = Validator::make($request->except('_token'), [
            'competencyname' => ['required', 'string'],
        ]);

    	  if ($validator->fails()) {
            $errors = $validator->errors();

            // dd($errors);
            return Redirect::back()->withInput()->withErrors($errors);
        }else {

        	 $insertdata = [
                'Name' => $request->input('competencyname')];
                DB::table('keycompetencies')->where('ID', $id)->update($insertdata);

            Toastr::success('Key Competency has been updated.');
            return redirect()->route('competencies.index');
        }

    }

    public function delete($id)
    {
    	competencies::where('ID','=', $id)->delete();

        Toastr::success('Key Competency has been deleted.');
        return redirect()->route('competencies.index');

    }
}
