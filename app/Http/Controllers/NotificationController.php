<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ticket;
use App\Notify;
use Auth;
use App\JobAd;
use DB;
use App\User;
use App\RegistrationDetails;
use Redirect;
use Session;
use Notification;
use Carbon\Carbon;

class NotificationController extends Controller
{
    //
        public function tickets()
    {
        
        $jobs = JobAd::take(5)->get(); 
        $tickets = Ticket::where('user_id', Auth::user()->id)->get();

        auth()->user()->unreadNotifications->markAsRead();
        
        return view('tickets.ticket', compact('jobs','tickets'));
    }

    public function send(Request $request)
    {
        $details = RegistrationDetails::where('AccUserID', Auth::user()->id)->first();
    	Ticket::create([
    		'type' => $request->type,
    		'user_id' => Auth::user()->id,
    		'description' => $request->description,
            'raised_by' => $details->Firstname.' '.$details->Lastname,
    		'status' => 0
    	]);


        $redirectMessage = [
            'title' => 'Ticket Sent!',
            'content' => 'Your ticket has been sent successfully. Kindly click on raised tickets to check on your ticket status and response from summit.',
        ];
        Session::flash('redirectMessage', $redirectMessage);

      return redirect()->back();
    }

       public function admintickets()//kuguzia hapa kutoka kesho
    {
        
        $tickets = Ticket::all();

        return view('tickets.adminticket', compact('tickets'));
    }

    public function response(Request $request)
    {
        $tickets = Ticket::find($request->ticket_id);
        //dd($tickets);
        $user = User::find($tickets->user_id);
            # code...
            $details = [
                    'title' => $tickets->type,
                    'body' => 'You have received a response for your ticket. Please check on the ticket page',
            ];

        Ticket::where('id', $request->ticket_id)->update([
            'response' => $request->response,
            'status' => $request->status
        ]);

        $user->notify(new \App\Notifications\Tickets($details));


      return redirect()->back();
    }

    public function staffNotification($id){
       
       $notification = Notify::where('id','=',$id)->first();
        
       $ans = json_decode($notification->data,true);
        if(is_array($ans)){
            $type=$ans['type'];
            $task = $ans['taskid']; 
        }
        
        if ($type === "job"){
        $notification->read_at = Carbon::now();
        $notification->update();

        return redirect()->route('jobAds.show',$task);
        }

    }
}
