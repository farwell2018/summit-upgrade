<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Admin
{
    public function handle($request, Closure $next)
    {

         if (Auth::check()) {
            
            $user = Auth::user();
            if($user->role == 0 || $user->role == 1){
                return $next($request);
            }
        }
        return redirect('/');
    }
}
