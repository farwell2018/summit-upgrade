<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InterviewReferee extends Model
{
    //
     //
     //Set table for model
  protected $table = 'interview_referee';

   /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['ID', 'InterviewID', 'referee'];


   public function Interview()
    {
        return $this->belongsTo('App\Interview', 'ID','InterviewID');
    }
}
