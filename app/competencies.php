<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class competencies extends Model
{
    protected $table ='keycompetencies';
    protected $fillable =
     [
            'ID', 'Name'
     ];
     public $timestamps = false;
     
     public function jobad()
    {
    	return $this->belongsTo(JobAd::class);
    }


}
