<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InterviewHardSkill extends Model
{
    //
     //
     //Set table for model
  protected $table = 'interview_hardskills';

   /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['ID', 'InterviewID', 'Competency','rating','comment'];


   public function Interview()
    {
        return $this->belongsTo('App\Interview', 'ID','InterviewID');
    }
}
