<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobTests extends Model
{
    //Set table for model
  protected $table = 'pre_qual_test';

   /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'PreQualTestID', 'ID', 'Question'
  ];
  public $timestamps = false;
  public function jobs()
  {
    return $this->belongsTo(JobAd::class,'ID','ID');
  }
  public function answers()
  {
    return $this->hasMany(PreQualAnswers::class);
  }

  public function cvs()
  {
    return $this->belongsTo(JobCv::class);
  }

    
}
