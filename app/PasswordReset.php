<?php

/**
 * Description of PasswordReset
 *
 * @author Ansel Melly <ansel@anselmelly.com>
 * @date Jul 31, 2015
 * @link http://www.anselmelly.com
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class PasswordReset extends Model {

    protected $table = 'password_resets';
    protected $fillable = ['email', 'token'];

      /*
      * The events map for the model
      *
      * @var array
      */
      protected $dispachesEvents = [
        'created'=> PasswordResetCreated::class,
        'updated'=> PasswordResetUpdated::class
      ];
}
