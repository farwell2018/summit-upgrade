<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CreateCampaigns extends Model
{
    //
    protected $table = 'campaigns';

 protected $fillable = [
        'c_name','campaign_info','target'
    ];
}
