<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Interview extends Model
{
    /**
    belongs to simply establishes relationships whereby argument 1 is the target table
    argument 2 is the foreign key in current classes model reference. 
    argument 3 is the target table being referenced by argument 2

     e.g "interview" model has a foreign key called {argument 2 of method "belongsTo"} that
    refers to {argument 3 of method "belongsTo"} found in table {argument 1 of method "belongsTo"}
    NOTE: THIS EXTENDS TO ALL CLASSES THAT EXTEND THE MODEL CLASS. 

    */
    

    protected $table = 'interview';


    public function Candidates(){

       return $this->belongsTo('App\CvTable', 'CV_ID','CV_ID');
     }

     public function Jobs()
     {

        return $this->belongsTo('App\JobAd', 'JobAD_ID','ID');
      }

      
      public function Staffs()
      {

        return $this->belongsTo('App\SummitStaff', 'StaffID','StaffID');
     }
       
     public function competency()
    {

      return $this->hasMany(competencies::class);
    }
    
     public function hardsk()
    {

      return $this->hasMany(competencies::class);
    }
}
