<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Notifications\ResetPassword;
use App\Notifications\VerifyEmail;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;   
    protected $fillable = [
        'email', 'password', 'confirmpassword', 'role', 'status'
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];
  public function sendPasswordResetNotification($token)
  {
    $this->notify(new ResetPassword($token, $this->email));
  } 
  public function resendVerification()
  {
    $this->notify(new VerifyEmail($this));
  }  
  public function sendEmailVerificationNotification()
  {
    $this->notify(new VerifyEmail($this->email));
  }  
  public function verifyUser()
  {
    return $this->hasOne('App\VerifyUser');
  }
  public function SummitStaff()
  {
    return $this->hasOne('App\SummitStaff', 'AccUserID');
  }
}
