<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InterviewCandidateTest extends Model
{
    //
     //
     //Set table for model
  protected $table = 'interview_candidate_test';

   /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['ID', 'InterviewID', 'candidate_test'];


   public function Interview()
    {
        return $this->belongsTo('App\Interview', 'ID','InterviewID');
    }
}
