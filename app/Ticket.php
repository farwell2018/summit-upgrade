<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Ticket extends Model
{
    use Notifiable;
    //
    protected $table = 'tickets';

    protected $fillable = [
	  	"id",
	    "type",
	    "user_id",
	    "description",
	    "status",
	    "response",
	    "reponder_id",
	    "created_at",
	    "updated_at",
  	];


    public function users()
    {
        return $this->belongsTo('App\User', 'user_id','id');
    }

}
