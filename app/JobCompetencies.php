<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobCompetencies extends Model
{
    //
     //Set table for model
  protected $table = 'job_competencies';

   /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['ID', 'JobID', 'Competencies'];


   public function Clients()
    {
        return $this->belongsTo('App\JobAd', 'ID','JobID');
    }
}
