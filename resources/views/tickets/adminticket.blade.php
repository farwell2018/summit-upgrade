@extends('layouts.admin')
@section('title','Tickets')
@section('content')
<div class="row " style="margin-left:8px">
      <a href="{{route('home')}}" style="color:#858796" >
         <span class="text">Dashboard</span> </a> &nbsp; | &nbsp;
         <span class="text">Ticket Management Board</span> </a>&nbsp;       
</div>
    <div class="row">
        <div class="col-md-12">
        <div class="card mb-3">
            <div class="card-header tab-form-header text-white" style="background-color: #1C2E5C">
              Raised tickets
            </div>
            <div class="card-body">
              <table class="table table-bordered table-striped table-hover dt-responsive">
                <thead style="background-color: #FFCB00;color:#000">
                  <th>#</th>
                  <th>Ticket Type</th>
                  <th>Ticket Description</th>
                  <th>Response</th>
                  <th>Email</th>
                  <th>Created On</th>                 
                  <th>Updated On</th>                  
                  <th>Status</th>
                  <th>Actions</th>
                </thead>
                <tbody>
                @foreach($tickets as $ticket)
                <tr>
                  <td>{{$ticket->id}}</td>>
                 <td>{{$ticket->type}}</td>
                 <td>{{$ticket->description}}</td>
                 <td>@if($ticket->response == null) Not Responded @else {{$ticket->response}} @endif</td>
                 <td>{{$ticket->users->email}}</td>
                 <td>{{$ticket->created_at}}</td>
                 <td>{{$ticket->updated_at}}</td>             
                 <td>@if($ticket->status == 0)
                  <span class="badge badge-danger"> Open</span>                 
                   @else
                   <span class="badge badge-success"> Closed</span>
                    @endif</td>
                 <td>
                  <span style="overflow: visible; position: relative; width: 110px;"  onclick="responde({{$ticket->id}})">
                    <i class="fas fa-edit" style="color: #3069AB"></i>
                  </span>              
                 </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
      </div>
  </div>
@include('includes.administrator.tickets.edit-ticket')
@endsection
<script type="text/javascript">
function responde(id)
  {
    $('#editTicketModal'+ id).modal('show');
  }
</script>  