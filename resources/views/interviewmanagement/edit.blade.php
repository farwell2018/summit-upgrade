@extends('layouts.admin')

@section('title','Interview Management - Edit Pane')

     @php
              $tests = array(
              array("id" => "Significantly exceeds expectation ", "name" => "Significantly exceeds expectation"),
              array("id" => "Exceeds Expectation", "name" => "Exceeds Expectation"),
              array("id" => "Meets Expectation", "name" => "Meets Expectation"),
              array("id" => "Needs Development", "name" => "Needs Development"),
              array("id" => "Unsatisfactory", "name" => "Unsatisfactory"),
              array("id" => "Not Applicable", "name" => "Not Applicable"),
              array("id" => "To be emailed", "name" => "To be emailed"),
              );
              
    $currreny = array(
                       array("id" =>"KES", "name" => "KES"),
                       array("id" =>"EURO", "name" => "EURO"),
                       array("id" =>"UK Pound", "name" => "UK Pound"),
                       array("id" =>"USD", "name" => "USD"),
                       array("id" =>"RAND", "name" => "RAND"),
                       array("id" =>"UGX", "name" => "UGX"),
                       array("id" =>"TZS", "name" => "TZS"),
                       array("id" =>"ZMK", "name" => "Zambian Kwacha"),
    );
    $management = array(
                       array("id" =>"1", "name" => "Yes"),
                       array("id" =>"0", "name" => "No"),
                       
    );
    
    $retains = array(
     array("id" =>"0 - 20 - Unsatisfactory", "name" => "0 – 20 Unsatisfactory The candidate has demonstrated very little skill in solving problems/ initiative/responsibility/honesty."),
      array("id" =>"21 - 30 - Unsatisfactory", "name" => "21 – 30 	Unsatisfactory	The candidate has demonstrated very little skill in solving problems/ initiative/responsibility/honesty."),
      array("id" =>"31 - 40 - Unsatisfactory", "name" => "31 – 40 	Unsatisfactory	The candidate has demonstrated very little skill in solving problems/ initiative/responsibility/honesty."),
     array("id" =>"41 - 50 - Unsatisfactory", "name" => "41 – 50	Unsatisfactory	The candidate has demonstrated very little skill in solving problems/ initiative/responsibility/honesty."),
     array("id" =>"51 - 60 - Needs Development", "name" => "51 - 60	Needs Development	The candidate has demonstrated limited skill in solving problems/ initiative/responsibility/honesty."),
      array("id" =>"61 - 70 - Needs Development", "name" => "61 - 70	Needs Development 	The candidate has demonstrated limited skill in solving problems/ initiative/responsibility/honesty."),
       array("id" =>"71 - 80 - Meets Expectations", "name" => "71 - 80	Meets Expectations	The candidate has demonstrated adequate skill in solving problems/ initiative/responsibility/honesty."),
        array("id" =>"81 - 90 - Meets Expectations", "name" => "81 - 90	Meets Expectations	The candidate has demonstrated adequate skill in solving problems/ initiative/responsibility/honesty."),
        array("id" =>"91 - 98 - Exceeds Expectations", "name" => "91 - 98	Exceeds Expectations	The candidate has demonstrated all of the skill in solving problems/ initiative/responsibility/honesty."),
        array("id" =>"99 - 100 - Significantly Exceeds Expectations", "name" => "99 - 100	Significantly Exceeds Expectations 	The candidate has demonstrated outstanding skill in solving problems/ initiative/responsibility/honesty."),
         array("id" => "Not Applicable", "name" => "Not Applicable"),
              array("id" => "To be emailed", "name" => "To be emailed"),
    );
    
    $time = array(
     array("id" =>"0 - 20 - Unsatisfactory", "name" => "0 – 20 Unsatisfactory The candidate has demonstrated very little skill in prioritization & logical explanation"),
                array("id" =>"21 - 30 - Unsatisfactory", "name" => "21 – 30 Unsatisfactory The candidate has demonstrated very little skill in prioritization & logical explanation"),
                array("id" =>"31 - 40 - Unsatisfactory", "name" => "31 – 40 Unsatisfactory The candidate has demonstrated very little skill in prioritization & logical explanation"),
                array("id" =>"41 - 50 - Unsatisfactory", "name" => "41 – 50 Unsatisfactory The candidate has demonstrated very little skill in prioritization & logical explanation"),
                array("id" =>"51 - 60 - Needs Development", "name" => "51 - 60 Needs Development The candidate has demonstrated a limited amount of skill in prioritization & logical explanation"),
                array("id" =>"61 - 70 - Needs Development", "name" => "61 - 70 Needs Development The candidate has demonstrated a limited amount of skill in prioritization & logical explanation"),
                array("id" =>"71 - 80 -  Meets Expectations", "name" => "71 - 80 Meets Expectations The candidate has demonstrated adequate skill in prioritization & logical explanation"),
                array("id" =>"81 - 90 - Meets Expectations", "name" => "81 - 90 Meets Expectations The candidate has demonstrated adequate skill in prioritization & logical explanation"),
                array("id" =>"91 - 98 - Exceeds Expectations", "name" => "91 - 98 Exceeds Expectations The candidate has demonstrated very good skill in prioritization & logical explanation"),
                array("id" =>"99 - 100 - Significantly Exceeds Expectations", "name" => "99 - 100 Significantly Exceeds Expectations The candidate has demonstrated outstanding skill in prioritization & logical explanation"),
                 array("id" => "Not Applicable", "name" => "Not Applicable"),
              array("id" => "To be emailed", "name" => "To be emailed"),
    
    
    );
    $managementExp = array(
      array("id" =>"Executive-Director", "name" => "Executive/Director Level"),
       array("id" =>"Senior-Level", "name" => "Senior Level"),
       array("id" =>"Mid-level", "name" => "Mid Level"),
        array("id" =>"Lower-Level", "name" => "Lower Level"),
    
    
    );
    
    
    $statuses = array(
    array("id" =>"Scheduled", "name" => "Scheduled"),
             array("id" =>"Interviewed", "name" => "Interviewed"),
              array("id" =>"Shortlisted", "name" => "Shortlisted"),
             array("id" =>"Recommended", "name" => "Recommended"),
             array("id" =>"Placed", "name" => "Placed"),
             array("id" =>"Discard", "name" => "Discard"),
            array("id" =>"Blacklist" , "name" => "Blacklist"),
    
    
    );
    
    
              @endphp
@section('css')
<style>
 .bootstrap-select.selectdropdown.bs3{
      border: 1px solid #d1d3e2 !important;
  }
    .dropdown-menu .open{
        max-width: 350px !important;
        min-width: 350px !important;
    }
    
</style>
@endsection
@section('content')

    <div class="row " style="margin-left:8px">
        <a href="{{route('home')}}" style="color:#858796" >
            <span class="text">Home</span> </a> &nbsp; | &nbsp;
        <a href="{{route('interviewmanagement.index')}}" style="color:#858796" >
            <span class="text">Interview Management Board</span> </a>  &nbsp; | &nbsp;
            <span class="text"></span><a>{{$regdetails->Firstname}} {{$regdetails->Lastname}}</a>

    </div>
    <br>
       @if (session('status'))
          <div class="alert alert-success" role="alert">
              {{ session('status') }}
          </div>
      @endif
<div class="kt-portlet">
  <div class="kt-portlet__head">
     <div class="kt-portlet__head-label">
        <h3 class="kt-portlet__head-title">
          <!--  Edit Candidate:&nbsp <b>{{$regdetails->Firstname}} {{$regdetails->Lastname}}</b> -->
      </div>
  </div>
  <form class="kt-form" method="POST" action="{{route('interviewmanagement.update', $candidates->InterviewID) }}"  enctype="multipart/form-data">
    <div class="kt-portlet__body">
      @csrf
      <div class="row">
        <div class="col">
          <div class="form-group">
             <label for="interview_date">Interview Date:&nbsp<b>{{Carbon\carbon::parse($candidates->InterviewDate)->format('M jS Y')}}</b><span class="asterick" style="color:red">*</span></label>
             <input type="date" id="interview_date"  name="interview_date" class=" form-control"  value="{{Carbon\carbon::parse($candidates->InterviewDate)->format('M jS Y')}}" required max="{{ now()->toDateString('Y-m-d') }}">
          </div>
        </div>
        <div class="col">
          <div class="form-group">
             <label for="years_experience">{{ __('Years of Experience') }}<span class="asterick" style="color:red">*</span></label>
             <input type="text" name="years_experience" class="form-control" value="{{$candidates->ExpYrs}}" required>
             @if ($errors->has('years_experience'))
             <span class="invalid-feedback" role="alert">
             <strong>{{ $errors->first('years_experience') }}</strong>
             </span>
             @endif
          </div>
        </div>

        <div class="col">
          <div class="form-group">
              <label for="management_experience">Management Experience:&nbsp<b></b><span class="asterick" style="color:red">*</span></label>
                <select class="form-control" id="management" name="management" required>
                    @foreach($management as $manage)
                    @if($manage['id'] == $candidates->Management)
                    <option selected value="{{$manage['id']}}">{{$manage['name']}}</option>
                    
                    @else
                    <option value="{{$manage['id']}}">{{$manage['name']}}</option>
                    @endif
                @endforeach
                </select>
          </div>
          @if($candidates->Management == 1)
          <div id="ma_experience" class="form-group" >
              <label>{{ __('Management Experience') }}</label>
              <select class="form-control" name="management_experience" id="management_experience">
                 @foreach($managementExp as $manage)
                    @if($manage['id'] == $candidates->ManagementExp)
                    <option selected value="{{$manage['id']}}">{{$manage['name']}}</option>
                    
                    @else
                    <option value="{{$manage['id']}}">{{$manage['name']}}</option>
                    @endif
                @endforeach
                
              </select>
              <!--<input type="numeric" class="form-control" id="management_experience" name="management_experience" value=""hidden/><br/>-->
          </div>  
          @else 
           <div id="ma_experience" class="form-group" style="display:none">
              <label>{{ __('Management Experience') }}</label>
              <select class="form-control" name="management_experience" id="management_experience">
                <option value="">Select Management Experience</option>
                <option value="Executive-Director">Executive/Director Level</option>
                <option value="Senior-Level">Senior Level</option>
                <option value="Mid-level">Mid Level</option>
                <option value="Lower-Level">Lower Level</option>
              </select>
              <!--<input type="numeric" class="form-control" id="management_experience" name="management_experience" value=""hidden/><br/>-->
          </div>  
          
          
          @endif
        </div>
            
      </div>
      <div class="row">

        <div class="col">
            <div class="form-group">
            <label for="management_experience">Salary Currency:&nbsp<b></b><span class="asterick" style="color:red">*</span></label>
            <select name="salarycurrency" class="form-control" required>
              @foreach($currency as $currencies)
              @if($currencies->CurrencyID == $candidates->GrossSalCurrency)
              <option value="{{$currencies->CurrencyID}}" selected>{{$currencies->CurrencyName}}</option>
              @else
              <option value="{{$currencies->CurrencyID}}">{{$currencies->CurrencyName}}</option>
              @endif
              @endforeach
            </select>
          </div>
        </div>
        <div class="col">
          <div class="form-group">
            <label for="salaryexpectation">Salary Expectation<span class="asterick" style="color:red">*</span></label>
            <input type="numeric" name="salaryexpectation" class="form-control" value="{{$candidates->SalaryExpectation}}" required/>
          </div>
        </div>
        <div class="col">
            <div class="form-group ">
                 @php $comp= array();
                   
                   $hards = App\InterviewHardSkill::where('InterviewID','=', $candidates->InterviewID)->get();
                   
                   @endphp
                   @foreach($hards as $c)
                    
                    @php $comp[] = $c->HardSkill; @endphp
                   
                   @endforeach
                
              <label >Hard Skills<span class="asterick" style="color:red">*</span></label>
              <select class="form-control selectpicker selectdropdown" name="hardskills[]" id="hardskills"  data-width="100px;" data-live-search="true" data-size="10" multiple="multiple" required>
                <option value="">Select Hard Skill</option>
                @foreach($hardskills as $hardskill)
                @if(in_array($hardskill['Name'],$comp))
                <option value="{{$hardskill->Name}}" selected="selected">{{$hardskill->Name}}</option>
                @else
                <option value="{{$hardskill->Name}}">{{$hardskill->Name}}</option>
                @endif
                @endforeach
              </select>
            </div>
        </div>
      </div>
      
        <div class="row">
            <div class="col">
          <div class="form-group">
            <label>Current Salary<span class="asterick" style="color:red">*</span></label>
            <input type="text" name="currentsalary" class="form-control" value="{{$candidates->CurrentSalary}}" required>
          </div>
        </div>
           <div class="col">
               <div class="form-group">
            <label for="management_experience">Salary Currency:&nbsp<b></b><span class="asterick" style="color:red">*</span></label>
             <select id="grosssalcurr" name="grosssalcurr" class="form-control" required>
                     
              @foreach($currency as $currencies)
              @if($currencies->CurrencyID == $candidates->GrossCurrency)
              <option value="{{$currencies->CurrencyID}}" selected>{{$currencies->CurrencyName}}</option>
              @else
              <option value="{{$currencies->CurrencyID}}">{{$currencies->CurrencyName}}</option>
              @endif
              @endforeach
          
                    </select>
          </div>
        </div>
        <div class="col">
          <div class="form-group">
            <label>Benefits Received<span class="asterick" style="color:red">*</span></label>
            <input type="text " name="benefitsreceived" class="form-control" value="{{$candidates->BenefitsReceived}}" required>
          </div>
        </div>
        <div class="col">
          <div class="form-group">
            <label>Notice Period<span class="asterick" style="color:red">*</span></label>
            <input type="text" name="notice_period" class="form-control" value="{{$candidates->NoticePeriod}}" required>
          </div>
        </div>
      </div>
       <div class="row">
        <div class="col">
          <div class="form-group">
              <label for="title">{{ __('Job Title') }}<span class="asterick" style="color:red">*</span></label>
                <select id="title" name="title" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" required>
                  @foreach( $jobAds as $title)
                   @if($candidates->JobAD_ID == $title->ID)
                    <option value="{{$title->ID}}" selected>{{$title->JobTitle}}</option>
                   @else
                    <option value="{{$title->ID}}">{{$title->JobTitle}}</option>
                    @endif
                  @endforeach
                </select>
                  @if ($errors->has('jobAds'))
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('jobAds') }}</strong>
                      </span>
                  @endif
          </div>
        </div>
        <div class="col">
          <div class="form-group">
             <label for="industry">{{ __('Industry / Sector') }}<span class="asterick" style="color:red">*</span></label>
                <select id="industry" name="industry" class="form-control{{ $errors->has('industry') ? ' is-invalid' : '' }}" required>
                  @foreach($industry as $industry)
                  @if($industry->Name === $candidates->Industry)
                  <option value="{{$candidates->Industry}}" selected>{{$candidates->Industry}}</option>
                  @else
                  <option value="{{$industry->Name}}">{{$industry->Name}}</option>
                  @endif
                  @endforeach
                </select>
                @if ($errors->has('industry'))
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('industry') }}</strong>
                </span>
                @endif
          </div>
        </div>
        <div class="col">
          <div class="form-group">
            <label for="status">Status:<span class="asterick" style="color:red">*</span></label>
            <select id="status" name="status" class="form-control{{ $errors->has('status') ? ' is-invalid' : '' }}" required>
              @if(!empty($candidates->Status))
                @foreach($statuses as $status)
                    @if($status['id'] == $candidates->Status)
                    <option selected value="{{$status['id']}}">{{$status['name']}}</option>
                    
                    @else
                    <option value="{{$status['id']}}">{{$status['name']}}</option>
                    @endif
                @endforeach
               @else
               <option value="Scheduled">Scheduled</option>
               <option value="Interviewed">Interviewed</option>
               <option value="Shortlisted">Shortlisted</option>
               <option value="Recommended">Recommended</option>
               <option value="Placed">Placed</option>
               <option value="Discard">Discard</option>
               <option value="Blacklist">Blacklist</option>
             @endif
            </select>
          </div>
                  <div id="status_placed" class="form-group" style="display: none;">
              <label>{{ __('Placed') }}</label>
              <div class="form-group form-spacing">
                    <label for="status">Start Date:</label>
                    <input type="date" name="startdate" value="{{$candidates->StartDate}}" class="form-control">
              </div>
                  <div class="form-group form-spacing">
                    <label for="status">GrossSalCurrency</label>
                    <select id="grosssalcurr" name="grosssalcurr" class="form-control">
                      @if(!empty($candidates->GrossSalCurrency))
                      <option value="{{$candidates->GrossSalCurrency}}">{{$candidates->GrossSalCurrency}}</option>
                      <option value="KES">KES</option>
                      <option value="EURO">EURO</option>
                      <option value="UK Pound">UK Pound</option>
                      <option value="USD">USD</option>
                      <option value="RAND">RAND</option>
                      <option value="UGX">UGX</option>
                      <option value="TZS">TZS</option>
                      <option value="ZMK">Zambian Kwacha</option>
                      @else
                      <option value="{{$candidates->GrossSalCurrency}}">{{$candidates->GrossSalCurrency}}</option>
                      <option value="KES">KES</option>
                      <option value="EURO">EURO</option>
                      <option value="UK Pound">UK Pound</option>
                      <option value="USD">USD</option>
                      <option value="RAND">RAND</option>
                      <option value="UGX">UGX</option>
                      <option value="TZS">TZS</option>
                      <option value="ZMK">Zambian Kwacha</option>
                      @endif
                    </select>
                  </div>
                   <div class="form-group form-spacing">
                    <label for="status">Gross Salary</label>
                    <input type="text" name="grosssalary" value="{{$candidates->GrossSalary}}" class="form-control">
                  </div>
                  <div class="form-group form-spacing">
                    <label for="status">Comments</label>
                    <textarea class="form-control" name="comments">{{$candidates->ClosureComments}}</textarea>
                  </div>
          </div>         
        </div>
      </div>
      <div class="row">
                   
        <div class="col">
          <div class="form-group">
            <label for="excel_test">Excel Test: <b>{{$candidates->TestResult_3}}</b></label>
            <select class="form-control" name="excel_test" id="excel_test" value="">
            
              @foreach($tests as $test)
              
              @if($candidates->TestResult_3 === $test['id'])
              <option value="{{$test['id']}}" selected="selected">{{$test['name']}}</option>
              @else
              <option value="{{$test['id']}}">{{$test['name']}}</option>
              @endif
              @endforeach
                
            </select>
          </div>
        </div>
        <div class="col">
          <div class="form-group">
            <label for="word_drafting">Written Test: <b>{{$candidates->TestResult_2}}</b> </label>
            <select class="form-control" name="word_drafting" id="word_drafting" value="" >
              
                @foreach($tests as $test)
              
              @if($candidates->TestResult_2 === $test['id'])
              <option value="{{$test['id']}}" selected="selected">{{$test['name']}}</option>
              @else
              <option value="{{$test['id']}}">{{$test['name']}}</option>
              @endif
              @endforeach
            </select>
          </div>
        </div>
        <div class="col">
          <label for="situation_test">Situation Test:{{$candidates->TestResult_4}}</label>
          <div class="form-group">
            <select class="form-control" name="situation_test" id="situation_test" value="" >
                
                   @foreach($retains as $test)
              
              @if($candidates->TestResult_4 === $test['id'])
              <option value="{{$test['id']}}" selected="selected">{{$test['name']}}</option>
              @else
              <option value="{{$test['id']}}">{{$test['name']}}</option>
              @endif
              @endforeach
                <!--<option value="">Select Rating</option>-->
                <!--<option value="0 - 20 - Unsatisfactory"> 0 – 20 Unsatisfactory The candidate has demonstrated very little skill in solving problems/ initiative/responsibility/honesty.</option>-->
                <!--<option value="21 - 30 - Unsatisfactory">21 – 30 	Unsatisfactory	The candidate has demonstrated very little skill in solving problems/ initiative/responsibility/honesty.</option>-->
                <!--<option value="31 - 40 - Unsatisfactory">31 – 40 	Unsatisfactory	The candidate has demonstrated very little skill in solving problems/ initiative/responsibility/honesty.</option>-->
                <!--<option value="41 - 50 - Unsatisfactory">41 – 50	Unsatisfactory	The candidate has demonstrated very little skill in solving problems/ initiative/responsibility/honesty.</option>-->
                <!--<option value="51 - 60 - Needs Development">51 - 60	Needs Development	The candidate has demonstrated limited skill in solving problems/ initiative/responsibility/honesty.</option>-->
                <!--<option value="61 - 70 - Needs Development">61 - 70	Needs Development 	The candidate has demonstrated limited skill in solving problems/ initiative/responsibility/honesty.</option>-->
                <!--<option value="71 - 80 - Needs Development">71 - 80	Meets Expectations	The candidate has demonstrated adequate skill in solving problems/ initiative/responsibility/honesty.</option>-->
                <!--<option value="81 - 90 - Meets Expectations">81 - 90	Meets Expectations	The candidate has demonstrated adequate skill in solving problems/ initiative/responsibility/honesty.</option>-->
                <!--<option value="91 - 98 - Exceeds Expectations">91 - 98	Exceeds Expectations	The candidate has demonstrated all of the skill in solving problems/ initiative/responsibility/honesty.</option>-->
                <!--<option value="99 - 100 - Significantly Exceeds Expectations">99 - 100	Significantly Exceeds Expectations 	The candidate has demonstrated outstanding skill in solving problems/ initiative/responsibility/honesty.</option>-->
            </select>
          </div>
        </div>
        <!--<div class="col">-->
        <!--  <div class="form-group">-->
        <!--    <label>Lumina:</label>-->
        <!--    <input type="text" name="lumina" class="form-control" value="{{$candidates->lumina}}">-->
        <!--  </div>-->
        <!--</div>        -->
      </div>
      <div class="row">
        <div class="col">          
          <div class="form-group">
            <label for="timemanagement">Time Management:</label>
            <select class="form-control" name="time_manage" id="time_manage" >
                 @foreach($time as $test)
              
              @if($candidates->TestResult_5 === $test['id'])
              <option value="{{$test['id']}}" selected="selected">{{$test['name']}}</option>
              @else
              <option value="{{$test['id']}}">{{$test['name']}}</option>
              @endif
              @endforeach
                <!-- <option value="">Select Rating</option>-->
                <!--<option value="0 - 20 - Unsatisfactory">0 – 20 Unsatisfactory The candidate has demonstrated very little skill in prioritization & logical explanation</option>-->
                <!--<option value="21 - 30 - Unsatisfactory">21 – 30 Unsatisfactory The candidate has demonstrated very little skill in prioritization & logical explanation</option>-->
                <!--<option value="31 - 40 - Unsatisfactory">31 – 40 Unsatisfactory The candidate has demonstrated very little skill in prioritization & logical explanation</option>-->
                <!--<option value="41 - 50 - Unsatisfactory">41 – 50 Unsatisfactory The candidate has demonstrated very little skill in prioritization & logical explanation</option>-->
                <!--<option value="51 - 60 - Needs Development">51 - 60 Needs Development The candidate has demonstrated a limited amount of skill in prioritization & logical explanation</option>-->
                <!--<option value="61 - 70 - Needs Development">61 - 70 Needs Development The candidate has demonstrated a limited amount of skill in prioritization & logical explanation</option>-->
                <!--<option value="71 - 80 -  Meets Expectations">71 - 80 Meets Expectations The candidate has demonstrated adequate skill in prioritization & logical explanation</option>-->
                <!--<option value="81 - 90 - Meets Expectations">81 - 90 Meets Expectations The candidate has demonstrated adequate skill in prioritization & logical explanation</option>-->
                <!--<option value="91 - 98 - Exceeds Expectations">91 - 98 Exceeds Expectations The candidate has demonstrated very good skill in prioritization & logical explanation</option>-->
                <!--<option value="99 - 100 - Significantly Exceeds Expectations">99 - 100 Significantly Exceeds Expectations The candidate has demonstrated outstanding skill in prioritization & logical explanation</option>-->
            </select>
          </div>
        </div>
        <div class="col">
           <div class="form-group">
            <label for="test_details">Ravens IQ:</label>
            <input type="numeric" name="test_details" class="form-control" value="{{$candidates->TestResult_1}}">
          </div>
        </div>
        <div class="col">
          <div class="form-group">
            <label>Other Tests</label>
            <input type="text" name="lumina" class="form-control">
          </div>
        </div>
        
      </div> 
      <div id="allCompetecies">
      <div class="row">
          <div class="col">
              <label for="Interview_competency_1">Competencies 1:</label>
              <div class="form-group">
                  <select class="form-control" id="Interview_competency_1" name="InterviewFields[1][competency]">
                      <option>Select Competency</option>
                      @php $jComp = array(); @endphp
                      @foreach($competencies as $competency)
                      
                      @php array_push($jComp,[$competency->ID => $competency->Name]);@endphp
                      
                      <option value="{{$competency->ID}}">{{$competency->Name}}</option>
                      @endforeach
                  </select> 
                  
              </div>
          </div>
          <div class="col">
              <label for="Interview_rating_1">Rating Scale 1:</label>
              <div class="form-group">
                  <select class="form-control" name="InterviewFields[1][rating]" id="Interview_rating_1">
                          <option>Select Rating Scale</option>
                          <option value="Significantly exceeds expectation">Significantly exceeds expectation</option>
                          <option value="Exceeds expectation">Exceeds expectation</option>
                          <option value="Meets expectation">Meets expectation</option>
                          <option value="Needs Development">Needs Development</option>
                          <option value="Unsatisfactory">Unsatisfactory</option>
                  </select>
              </div>
          </div>
          <div class="col">
              <label for="Interview_dropdowncomments_1">Comments 1:</label>
              <div class="form-group">
                  <select class="form-control" name="InterviewFields[1][dropdowncomments]" id="Interview_dropdowncomments_1">
                  <option value="">Select Comment</option>
                  <option value="The candidate has demonstrated outstanding  skill / knowledge / experience / behaviour">The Candidate has demonstrated outstanding  skill / knowledge / experience / behaviour</option>
                  <option value="The candidate has demonstrated very good skill / knowledge / experience / behaviour">The Candidate has demonstrated very good skill / knowledge / experience / behaviour </option>
                  <option value="The candidate has demonstrated adequate skill / knowledge / experience / behaviour">The Candidate has demonstrated adequate skill / knowledge / experience / behaviour </option>
                  <option value="The candidate has demonstrated limited skill / knowledge / experience / behaviour">The candidate has demonstrated limited skill / knowledge / experience / behaviour </option>
                  <option value="The candidate has demonstrated very little skill / knowledge / experience / behaviour">The candidate has demonstrated very little skill / knowledge / experience / behaviour </option>
                  </select>
              </div>
          </div>
      </div>
      </div>
      <div class="row">
          <div class="col">
      <div class="form-group form-spacing">
             <input type="button" value="+Add Competencies" class="btn btn-primary pull-right" id="addcompetencies" />
           </div>
           </div>
           </div>
      <div class="row">
        <div class="col">
          <label for="interviewdocuments">Confirm Availability Of Interview Documents</label>
          <div class="form-group form">
            <label>KRA</label>
              <input type="checkbox" name="KRA"  id="interviewkra" value="1">  @if($candidates->KRA) <a href="/summitre/public/interview/kra/{{$candidates->KRA}}" target="_blank" style="float:right"><b>{{ $candidates->KRA }}</b></a> @endif
              <input type="file" name="kra" class="form-control"><br>          
              <label>ID</label>
              <input type="checkbox" name="IDENTIFICATION"  id="interviewid" value="1"> @if($candidates->ID) <a href="/summitre/public/interview/ID/{{$candidates->ID}}" target="_blank" style="float:right"><b>{{ $candidates->ID }}</b></a> @endif
              <input type="file" name="identification" class="form-control"><br>             
              <label>NHIF</label>
              <input type="checkbox" name="NHIF"  id="interviewnhif" value="1"> @if($candidates->NHIF) <a href="/summitre/public/interview/NHIF/{{$candidates->NHIF}}" target="_blank" style="float:right"><b>{{ $candidates->NHIF }}</b></a> @endif
              <input type="file" name="nhif" class="form-control"><br>             
              <label>NSSF</label>
              <input type="checkbox" name="NSSF"  id="interviewnssf" value="1"> @if($candidates->NSSF) <a href="/summitre/public/interview/NSSF/{{$candidates->NSSF}}" target="_blank" style="float:right"><b>{{ $candidates->NSSF }}</b></a> @endif
              <input type="file" name="nssf" class="form-control"><br>              
              <label>Certificate Of Good Conduct</label>
              <input type="checkbox" name="CERTIFICATE"  id="interviewcertificate" value="1"> @if($candidates->COG) <a href="/summitre/public/interview/COG/{{$candidates->COG}}" target="_blank" style="float:right"><b>{{ $candidates->COG }}</b></a> @endif
              <input type="file" name="cogc" class="form-control"><br>            
              <label>Credit Reference Bureau</label>
             <input type="checkbox" name="crb"  id="interviewcrb" value="1"> @if($candidates->PQC) <a href="/summitre/public/interview/CRB/{{$candidates->PQC}}" target="_blank" style="float:right"><b>{{ $candidates->PQC }}</b></a> @endif
             <input type="file" name="brc" class="form-control"><br>
             <label>EACC Clearance </label>
             <input type="checkbox" name="ethics"  id="interviewethics" value="1"> @if($candidates->EC) <a href="/summitre/public/interview/EACC/{{$candidates->EC}}" target="_blank" style="float:right"><b>{{ $candidates->EC }}</b></a> @endif
             <input type="file" name="eacc" class="form-control"><br>
           </div>
          <div id="referees">
               @php
                 $r = App\InterviewReferee::where('InterviewID','=', $candidates->InterviewID)->get();
                  $phpRefereeId=1; @endphp
            @if(count($r) > 0)
                  @foreach($r as $referee)
                    <div class="form-group" >
                <label for="Interview_referee_{{$phpRefereeId }}" class="required">
                  Upload Referee {{$phpRefereeId }}</label>  @if($referee->referee) <a href="/summitre/public/interview/Referee/{{$referee->referee}}" target="_blank" style="float:right"><b>{{ $referee->referee }}</b></a> @endif
              <input  type='file' class="form-control" name="InterviewReferee[referee][{{$phpRefereeId }}]" id="Interview_referee_{{$phpRefereeId }}" />
           </div>
                @php
                   $phpRefereeId++;
                @endphp
            
                @endforeach
              @else
            <div class="form-group" >
                <label for="Interview_referee_1" class="required">
                  Upload Referee 1</label>
              <input  type='file' class="form-control" name="InterviewReferee[referee][1]" id="Interview_referee_1" />
           </div>
           @endif
           </div>
           <div class="form-group form-spacing">
             <input type="button" value="+Add Referee" class="btn btn-primary pull-right" id="addreferee" />
           </div><br><br>
           <div id="cantest">
               
              @php
                 $cans = App\InterviewCandidateTest::where('InterviewID','=', $candidates->InterviewID)->get();
                  $phpTestId=1; @endphp
            @if(count($cans) > 0)
                  @foreach($cans as $test)
                   
                    <div class="form-group" >
                <label for="Interview_cantest_{{$phpTestId }}" class="required">
                 Candidate Test {{$phpTestId }}</label>  @if($test->candidate_test) <a href="/summitre/public/interview/CandidateTests/{{$test->candidate_test}}" target="_blank" style="float:right"><b>{{ $test->candidate_test }}</b></a> @endif
              <input  type='file' class="form-control" name="InterviewTest[canTest][{{$phpTestId }}]" id="Interview_cantest_{{$phpTestId }}" />
           </div>
                @php
                   $phpTestId++;
                @endphp
            
                @endforeach
              @else
            <div class="form-group" >
                <label for="Interview_cantest_1" class="required">
                  Candidate Test 1</label>
              <input  type='file' class="form-control" name="InterviewTest[canTest][1]" id="Interview_cantest_1" />
           </div>
           @endif
           </div>
           <div class="form-group form-spacing">
             <input type="button" value="+ Add Candidate Test" class="btn btn-primary pull-right" id="addcandidate" />
           </div>
        </div>
        <div class="col">
         <div class="form-group">
           <label for="interviewdocuments">Interview Summary</label>
          <div class="form-group">
            <textarea id="editor" name="interviewsummary" class="form-control" style="height: 380px">{{$candidates->InterviewSummary}}</textarea>
          </div>
        </div>
        <div class="form-group">
          <div class="form-group">
            <label for="internalcomments">Internal Comments</label>
            <div class="form-group">
              <textarea id="editor1" name="internalcomments" class="form-control" style="height: 380px">{{$candidates->InternalComments}}</textarea>
            </div>
          </div>
        </div>
      </div>
      
    </div>
    <div class="col">
      <div class="form-group">
          <label for="staff_assigned">{{ __('Assign to Staff') }}<span class="asterick" style="color:red">*</span></label>
          <select id="staff_assigned" name="staff_assigned" class="form-control{{ $errors->has('staff_assigned') ? ' is-invalid' : '' }}" required>
            <option disabled selected>Select Staff</option>
           @foreach($staff as $staf)
           @if($staf['StaffID'] == $candidates->StaffID)
             <option value="{{$staf->StaffID}}" selected="selected"> {{$staf->Firstname.' '.$staf->Lastname}}</option>
              @else
           <option value="{{$staf->StaffID}}">{{$staf->Firstname.' '.$staf->Lastname}} </option>
           @endif
           @endforeach
          </select>
            @if ($errors->has('staff_assigned'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('staff_assigned') }}</strong>
                </span>
                @endif
    </div>
       </div>
    <div class="kt-portlet__foot text-center">
      <div class="kt-form__actions">
        <button type="submit" class="btn btn-primary">Submit</button>
        <!--<button type="reset" class="btn btn-secondary">Cancel</button>-->
        <a href="javascript:history.back()" class="btn btn-light">Cancel</a>
      </div>
    </div>
  </form>
  <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
  <script>
    document.getElementById('buttonid').addEventListener('click', openDialog);
    function openDialog()
    {
    document.getElementById('fileid').click();
    }
  </script>
  <script>
    document.getElementById('buttonssid').addEventListener('click', openDialog);
    function openDialog()
    {
    document.getElementById('filidss').click();
    }
  </script>
  <script>
    jQuery(document).ready(function($) {
  $('.refereeee') .hide()
$('a[href^="#"]').on('click', function(event) {
$('.refereeee') .hide()
    var target = $(this).attr('href');

    $('.refereeee'+target).toggle();

});
});
  </script>
<script>
  tinymce.init({
    selector: 'textarea#editor',
    menubar: false
  });
</script>
<script>
  tinymce.init({
    selector: 'textarea#editor1',
    menubar: false
  });
</script>
<script>
  $('select[name=status]').on('change', function()
   {
    if (this.value == 'Placed') {
      $("#status_placed").show();
    } else {
      $('#status').val(this.value);
      $("#status_placed").hide();
    }
  });
</script>
<script>
  $('select[name=management]').on('change', function()
   {
    if (this.value == '1') {
      $("#ma_experience").show();
    } else {
      $('#management_experience').val('0');
      $("#ma_experience").hide();
    }
  });
</script>
<script>
 $( document ).ready(function() {
     
if( typeof phpCompId !== 'undefined' ) {
	var compId =phpCompId;
}
else{
var compId =2;
}

$('#addcompetencies').click( function(j) {
     
     var comps = '<?php echo json_encode($jComp); ?>';
     
             var link=  '<div class="row">'+
                     '<div class="col">'+
			        '<div class="form-group ">'+
			        '<label for="Interview_competency_'+compId+'">Competencies '+compId+'</label>'+
			        '<select class="form-control" id="Interview_competency_'+compId+'" name="InterviewFields['+compId+'][competency]">'+
			        '<option>Select Competency</option>';
			        $.each(JSON.parse(comps), function(index, item) {
			            
			         $.each(item, function(ind, it) {

                    link +=  '<option value="'+ind+'">'+it+'</option>';
			         });
                        });
			       link += '</select>'+
			        '</div>'+
			        '</div>'+
			        
			 '<div class="col">'+
              '<label for="Interview_rating_'+compId+'">Rating Scale '+compId+':</label>'+
              '<div class="form-group">'+
                   '<select class="form-control" name="InterviewFields['+compId+'][rating]" id="Interview_rating_'+compId+'">'+
                         ' <option>Select Rating Scale</option>'+
                          '<option value="Significantly exceeds expectation">Significantly exceeds expectation</option>'+
                          '<option value="Exceeds Expectation">Exceeds Expectation</option>'+
                          '<option value="Meets Expectation">Meets Expectation</option>'+
                          '<option value="Needs Development">Needs Development</option>'+
                          '<option value="Unsatisfactory">Unsatisfactory</option>'+
                  '</select>'+
              '</div>'+
          '</div>'+
          '<div class="col">'+
              '<label for="Interview_dropdowncomments_'+compId+'">Comments '+compId+':</label>'+
              '<div class="form-group">'+
                    '<select class="form-control" name="InterviewFields['+compId+'][dropdowncomments]" id="Interview_dropdowncomments_'+compId+'">'+
                  '<option value="">Select Comment</option>'+
                  '<option value="The candidate has demonstrated outstanding  skill / knowledge / experience / behaviour">The candidate has demonstrated outstanding  skill / knowledge / experience / behaviour</option>'+
                  '<option value="The candidate has demonstrated very good skill / knowledge / experience / behaviour">The candidate has demonstrated very good skill / knowledge / experience / behaviour </option>'+
                  '<option value="The candidate has demonstrated adequate skill / knowledge / experience / behaviour">The candidate has demonstrated adequate skill / knowledge / experience / behaviour </option>'+
                  '<option value="The candidate has demonstrated limited skill / knowledge / experience / behaviour">The candidate has demonstrated limited skill / knowledge / experience / behaviour </option>'+
                  '<option value="The candidate has demonstrated very little skill / knowledge / experience / behaviour">The candidate has demonstrated very little skill / knowledge / experience / behaviour </option>'+
                  '</select>'+
              '</div>'+
          '</div>'+
		'</div>';
		
 	$("#allCompetecies").append(link);
 	compId++;
 	});
    
 }); 
 
 
// $('#addreferee').click( function(j) {
//     var phpRefereeId = <?php echo $phpRefereeId;?>;
//       if( typeof phpRefereeId !== 'undefined' ) {
// 	   var refId = phpRefereeId;
//     }
//     else{
//     var refId =2;
//     }
//       var max = 3;
       
//       console.log(refId);
//       if(refId <= max){
  
//      $("#referees").append(
  
//                 '<div class="form-group ">'+
//                 '<label for="Interview_referee_'+refId+'">Upload Referee '+refId+'</label>'+
//                 '<input  type="file" class="form-control" name="InterviewReferee[referee]['+refId+']" id="Interview_referee_'+refId+'" />'+
//                 '</div>'
//      );
//       }
//       else{
//          $("#referees").append('<p style="font-size:12;color:red">Max Number of uploads has been reached</p>'
//      );  
//       }
//      refId++;
//      });
     
     
// $('#addcandidate').click( function(j) {
//     var phpTestId = <?php echo $phpTestId;?>;
    
//   if( typeof phpTestId !== 'undefined' ) {
// 	var testId = phpTestId;
//   }
//     else{
//         var testId =2;
//         } 
//       var max = 5;
       
//       if(testId <= max){
  
//      $("#cantest").append(
  
//                 '<div class="form-group ">'+
//                 '<label for="Interview_cantest_'+testId+'">Candidate Test '+testId+'</label>'+
//                 '<input  type="file" class="form-control" name="InterviewTest[canTest]['+testId+']" id="Interview_cantest_'+testId+'" />'+
//                 '</div>'
//      );
//       }
//       else{
//          $("#cantest").append('<p style="font-size:12;color:red">Max Number of uploads has been reached</p>'
//      );  
//       }
//      testId++;
//      });
</script>
<script>
jQuery(document).ready(function($) {
if ( $('#interview_date')[0].type != 'date' ) $('#interview_date').datepicker();

});
</script>

<script>

  $( document ).ready(function() {
    var phpReqId = {{$phpRefereeId}};
    var phpDutyId = {{$phpTestId}};
    console.log(phpReqId);
    console.log(phpDutyId);
  if( typeof phpDutyId !== 'undefined' ) {
   var testId =phpDutyId;
  }
  else{
  var testId =2;
  }
  if( typeof phpReqId !== 'undefined' ) {
   var refId =phpReqId;
  }
  else{
  var refId =2;
  }
  $('#addreferee').click( function(j) {
   
     $("#referees").append(
  
                '<div class="form-group ">'+
                '<label for="Interview_referee_'+refId+'">Upload Referee '+refId+'</label>'+
                '<input  type="file" class="form-control" name="InterviewReferee[referee]['+refId+']" id="Interview_referee_'+refId+'" />'+
                '</div>'
     );
      
     refId++;
     });
     $('#addcandidate').click( function(j) {
   
     $("#cantest").append(
  
                '<div class="form-group ">'+
                '<label for="Interview_cantest_'+testId+'">Candidate Test '+testId+'</label>'+
                '<input  type="file" class="form-control" name="InterviewTest[canTest]['+testId+']" id="Interview_cantest_'+testId+'" />'+
                '</div>'
     );
      
     testId++;
     });
  });
  </script>
@endsection
