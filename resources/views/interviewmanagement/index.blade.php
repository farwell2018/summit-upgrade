@extends('layouts.admin')
@section('title','Interview Management')
@section('content')
<div class="row">
  <div class="col">
<div class="row" style="margin-left:8px">
    <a href="{{route('home')}}" style="color:#858796" >
        <span class="text">Dashboard</span> </a> &nbsp; | &nbsp;
        <span class="text">Interview Management Board</span> </a>&nbsp;
</div>
</div>
</div>
<br><br>
<div class="card" style="background-color: #1C2E5C;align-content: center;">
  <div id="filterForm7">
  <div class="card-body">
    <label style="color: #80CFFF">INTERVIEW MANAGEMENT</label>
  <div class="row">
    <div class="col">
      <div class="form-group form-spacing">
        <label style="color: #80CFFF">First Name</label>
        <input type="text" name="firstname" class="form-control" placeholder="First Name">
      </div>
    </div>
    <div class="col">
      <div class="form-group form-spacing">
        <label style="color: #80CFFF">Last Name</label>
        <input type="text" name="lastname" class="form-control" placeholder="Last Name">
      </div>
    </div>
    <div class="col">
      <div class="form-group form-spacing">
        <label style="color: #80CFFF">Job Title</label>
        <select id="jobad" name="jobad" class="form-control">
          <option value="">Select Job Title</option>
          @foreach($jobs as $job)
          <option value="{{$job->ID}}">{{$job->JobTitle}}</option>
          @endforeach
        </select>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col">
          <div class="form-group form-spacing">
          <label style="color: #80CFFF">Consultant</label>
            <select id="consultant" name="consultant" class="form-control" >
                 <option value="">Select Consultant</option>
                  @foreach($staff as $staffs)
                 <option value="{{$staffs->StaffID}}">{{$staffs->Firstname}} {{$staffs->Lastname}}</option>
                  @endforeach
            </select>
                <span class="invalid-feedback" role="alert">
                  <strong></strong>
                </span>
          </div>
    </div>
    <div class="col">
      <div class="form-group form-spacing">
        <label style="color: #80CFFF">Interview Date</label>
        <input type="date" name="interviewdate" class="form-control">
      </div>
    </div>
    <div class="col">
           <div class="form-group form-spacing">
              <label style="color: #80CFFF">Status</label>
                <select class="form-control" name="status" id="status" value="">
                  <option value="">Filter By Status</option>
                  <option value="Scheduled">Scheduled</option>
                  <option value="Interviewed">Interviewed</option>
                  <option value="Shortlisted">Shortlisted</option>
                  <option value="Recommended">Recommended</option>
                  <option value="Placed">Placed</option>
                  <option value="Discard">Discard</option>
                  <option value="Blacklist">Blacklist</option>
                </select>
          </div>
    </div>
 </div>

 <div class="row">
   <div class="col">
     <div class="form-group form-spacing">
      <label style="color: #80CFFF">Industry</label>
      <select name="industry" class="form-control" value="">
          <option  value="">Select Job Industry</option>
         @foreach($industries as $industry)
        <option value="{{$industry->Name}}">{{$industry->Name}}</option>
        @endforeach
        </select>
     </div>
   </div>
   <div class="col">
     <div class="form-group form-spacing">
      <label style="color: #80CFFF">Industry Functions</label>
       <select class="form-control" name="functions" id="functions" value="">
        <option value="">Select Industry Function</option>
        @foreach($industryfunctionss as $industryfunctions)
         <option value="{{$industryfunctions->Name}}">{{$industryfunctions->Name}}</option>
         @endforeach
       </select>
     </div>
   </div>
   <div class="col">
     <div class="form-group form-spacing">
      <label style="color: #80CFFF">Hard Skills</label>
       <select class="form-control" name="hardskills" id="hardskills" value="">
         <option value="">Select Hardskill</option>
         @foreach($hardskillls as $hardskill)
         <option value="{{$hardskill->Name}}">{{$hardskill->Name}}</option>
         @endforeach
       </select>
     </div>
   </div>
 </div>
<div class="col text-right">
    <button class="btn  pull-right" onclick="filterInterview()" style="background-color:  #FFCB00;color:#3069AB;margin-left: 15px;margin-bottom: 15px;">SEARCH</button>
    <button class="btn pull-right"  onClick="window.location.reload();" style="background-color: #FFCB00;color: #000" >RESET</button>
</div>
</div>
</div>
</div><br>
<div class="row">
  <div class="col">
    <div class="buttonfilter">
      <div class="row">
        <div class="col-12">          
          <p>
            <a href="javascript:;" class="btn" onmousedown="toggleDiv('mydiv');"style="background-color: #FFCB00;color: #000" >Switch Orientation</a>
           <!--  <button>Switch Orientation</button> -->
           <!--  <button class="btn pull-right" style="background-color: #FFCB00;color: #000" >TABULAR VIEW</button>
            <button class="btn pull-right" onclick="myFunction()" style="background-color: #FFCB00;color: #000" >LIST VIEW</button> -->
          </p>
          <p>

         </p>
        </div>
        <div class="col-md-2">

        </div>
      </div>
    </div>
  </div>
</div>
<div class="card mb-5">
  <div class="card-header tab-form-header text-white" style="background-color: #1C2E5C">
   INTERVIEW MANAGEMENT
 </div>
   <div id="mydivon" style="display:block">
      <div class="card-body">
        <table class="table table table-bordered table-striped table-hover dt-responsive wrap" id="interviews" width="100%">
          <thead style="background-color: #FFCB00">
            <tr>
              <th style="color: #000">#</th>
              <th style="color: #000">Candidate:</th>
              <th style="color: #000">Job Ad:</th>
              <th style="color: #000">Consultant:</th>
              <th style="color: #000">Status:</th>
              <th style="color: #000">Interview Date:</th>
              <th style="color: #000">Actions:</th>
            </tr>
          </thead>
        </table>
      </div>
 </div>
  <div id="mydivoff" style="display:none">
    <div class="row">
      @foreach($interview as $interviews)
        <div class="col-6">
          <div class="card">
            <div class="card-header tab-form-header text-white " style="background-color: #1C2E5C ">
              {{$interviews->candidates->RegDetails->Firstname.' '.$interviews->candidates->RegDetails->Lastname}}
               @if(Auth::user()->role == 0)
              <div class="row">
                <div class="col">
                  <a href="{{route('candidateCV',[$interviews->CV_ID, $interviews->JobAD_ID])}}"><span class="text-primary edit-contact"><i class="far fa-eye" style="color: #fff"></i></span></a><br>
                </div>
                <div class="col">
                  <a href="{{route('interviewmanagement.edit', $interviews->InterviewID)}}"><span class="text-primary edit-contact"><i class="far fa-edit" style="color: #fff"></i></span></a><br>
                </div>
                <div class="col">
                  <a href="#"><span class="text-primary edit-contact" ><i class="fas fa-trash-alt" style="color:#fff"></i></span></a>
                </div>
              </div>
              @else
                <div class="row">
                <div class="col">
                  <a href="{{route('candidateCV',[$interviews->CV_ID, $interviews->JobAD_ID])}}"><span class="text-primary edit-contact"><i class="far fa-eye" style="color: #fff"></i></span></a><br>
                </div>
                <div class="col">
                  <a href="{{route('interviewmanagement.edit', $interviews->InterviewID)}}"><span class="text-primary edit-contact"><i class="far fa-edit" style="color: #fff"></i></span></a><br>
                </div>
              </div>
              @endif
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col">
                  <div class="card">
                    <div class="card-body">
                       <div class="container text-center">
                          <img src="@if(isset(Auth::user()->SummitStaff->profilePhoto)){{ asset('uploads/staff/'. Auth::user()->SummitStaff->profilePhoto.'')}}@else {{asset('admin-assets/img/default.jpg')}} @endif" class="rounded" alt="Profile Photo" align="text-center" width="180" height="180">
                       </div>
                    </div>
                  </div><br><br>
                  <div class="text-center">
                         <a href="{{route('candidateCV',[$interviews->CV_ID,$interviews->JobAD_ID])}}" class="btn" style="background-color: #0EA616;color: #fff">View Candidate</a>
                    </div>
                </div>
                 <div class="col">
                       <div class="card">
                         <div class="card-body">
                           <p><strong>Job Title:</strong>&nbsp{{$interviews->jobs->JobTitle}}</p>
                           <p><strong>Interview Date:</strong>&nbsp{{$interviews->InterviewDate}}</p>
                           <p><strong>Current Status:</strong>&nbsp{{$interviews->Status}}</p>
                           <p><strong>Interview Details:</strong>&nbsp{!!$interviews->CV_ID,$interviews->JobAD_ID,$interviews->InterviewDetails!!}</p>
                           <p><strong>Consultant:</strong></p>
                         </div>
                       </div>
                     </div>
              </div>
            </div>
          </div>
        </div>
      @endforeach
    </div>
    <div class="row pagination-row">
             {{$interview->links()}}
      </div>
    </div>
</div>  `
@endsection
@section('footer-js')
<style>
    .bs-example{
        margin: 20px;
    }
    .accordion .fa{
        margin-right: 0.5rem;
    }
</style>
<script>
  function toggleDiv(divid)
  {
 
    varon = divid + 'on';
    varoff = divid + 'off';
 
    if(document.getElementById(varon).style.display == 'block')
    {
    document.getElementById(varon).style.display = 'none';
    document.getElementById(varoff).style.display = 'block';
    }
   
    else
    {  
    document.getElementById(varoff).style.display = 'none';
    document.getElementById(varon).style.display = 'block'
    }
} 
</script>
<script>
function myFunction() {
  var x = document.getElementById("listview");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
</script>
<script>
    $(document).ready(function(){
        // Add minus icon for collapse element which is open by default
        $(".collapse.show").each(function(){
          $(this).prev(".card-header").find(".fa").addClass("fa-minus").removeClass("fa-plus");
        });
        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function(){
          $(this).prev(".card-header").find(".fa").removeClass("fa-plus").addClass("fa-minus");
        }).on('hide.bs.collapse', function(){
          $(this).prev(".card-header").find(".fa").removeClass("fa-minus").addClass("fa-plus");
        });
    });
</script>
<script>
var dataTable;

let baseUrl = "{!! route('interviewmanagement.json') !!}?";

function encodeQueryData(data)
{
 const ret = [];
 for (let d in data)
   ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
 return ret.join('&');
}

//Filter method
function filterInterview(){
    //Get company value

  const interviewdate = document.querySelector('#filterForm7 input[name=interviewdate]').value
  const firstname = document.querySelector('#filterForm7 input[name=firstname]').value
  const lastname = document.querySelector('#filterForm7 input[name=lastname]').value
  const category = document.querySelector('#filterForm7 select[name=industry]').value
  const jobad = document.querySelector('#filterForm7 select[name=jobad]').value
  const consultant = document.querySelector('#filterForm7 select[name=consultant]').value
  const status = document.querySelector('#filterForm7 select[name=status]').value
  const hardskills = document.querySelector('#filterForm7 select[name=hardskills]').value
  const functions = document.querySelector('#filterForm7 select[name=functions]').value

console.log(functions);
  //Add to URL
    const url = baseUrl  + "&" + encodeQueryData({jobad,interviewdate,consultant,status,firstname,lastname,category,hardskills,functions})

  //console.log(consultant);
  dataTable.ajax.url( url ).load();
}
$(document).ready(function(){
 dataTable= $('#interviews').DataTable({
    responsive: true,
    "deferRender": true,
    "processing": true,
    "serverSide": true,
    "ordering": false, //disable column ordering
    "lengthMenu": [
      [25, 50, 100,  -1],
      [25, 50, 100,  "All"] // change per page values here
    ],
    "pageLength": 25,
    "ajax":  {
      url: baseUrl,
      method: 'GET'
    },
    // dom: '<"html5buttons"B>lTfgitp',
    "dom": "<'row' <'col-md-12'>><'row'<'col-md-8 col-sm-12'lB><'col-md-4 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
    buttons: [
      { extend: 'copy',exportOptions: {columns: [0, 1, 2, 3,4,5,6]}},
      {extend: 'csv',exportOptions: {columns: [0, 1, 2, 3,4,5,6]}},
      {extend: 'excel', title: '{{ config('app.name', 'Summit') }} - List of all Users',exportOptions: {columns: [0, 1, 2, 3,4,5]}},
      {extend: 'pdf', title: '{{ config('app.name', 'Summit') }} - List of all Users',exportOptions: {columns: [0, 1, 2, 3,4,5]}},
      {extend: 'print',
      customize: function (win){
        $(win.document.body).addClass('white-bg');
        $(win.document.body).css('font-size', '10px');
        $(win.document.body).find('table')
        .addClass('compact')
        .css('font-size', 'inherit');
      }
    }
  ],
  columns: [
    {data: 'InterviewID', name: 'InterviewID', orderable: true, searchable: true},
    {data: 'CV_ID', name: 'CV_ID', orderable: true, searchable: true},
    {data: 'JobAD_ID', name: 'JobAD_ID', orderable: true, searchable: true},
    {data: 'StaffID', name: 'StaffID', orderable: false, searchable: true},
    {data: 'Status', name: 'Status', orderable: true, searchable: false},
    {data: 'InterviewDate', name: 'InterviewDate', orderable: true, searchable: false},
    {data: 'tools', name: 'tools', orderable: false, searchable: false,"defaultContent": "<i>Not set</i>"},
  ],
});
});
</script>
@endsection
