@extends('layouts.admin')

@section('title','Interview Management - Edit Pane')

@section('css')
<style>
  .bootstrap-select.selectdropdown.bs3{
      border: 1px solid #d1d3e2 !important;
  }
    .dropdown-menu .open{
        max-width: 350px !important;
        min-width: 350px !important;
    }
    
</style>
@endsection
@section('content')

    <div class="row " style="margin-left:8px">
        <a href="{{route('home')}}" style="color:#858796" >
            <span class="text">Home</span> </a> &nbsp; | &nbsp;
        <a href="{{route('interviewmanagement.index')}}" style="color:#858796" >
            <span class="text">Interview Management Board</span> </a>  &nbsp; | &nbsp;
            <span class="text"></span><a></a>
     Interview Notes for  {{$regdets->Firstname}} {{$regdets->Lastname}}
    </div>
    <br>
       @if (session('status'))
          <div class="alert alert-success" role="alert">
              {{ session('status') }}
          </div>
      @endif
<div class="kt-portlet">
  <div class="kt-portlet__head">
     <div class="kt-portlet__head-label">
        <h6 class="kt-portlet__head-title">
        </h6>
      </div>
  </div>
  <form class="kt-form" method="POST" action="{{route('interviewmanagement.store')}}"  enctype="multipart/form-data">
    <div class="kt-portlet__body">
      @csrf
       <input type="hidden" name="cv_id" class="form-control" value="{{$cvs->CV_ID}}" >
      <div class="row">
        <div class="col">
          <div class="form-group">
             <label for="interview_date">Interview Date:&nbsp<b></b><span class="asterick" style="color:red">*</span></label>
             <input  type="date" id="interview_date" name="interview_date" class=" form-control"  value="" max="{{ now()->toDateString('Y-m-d') }}" required>
          </div>
        </div>
        <div class="col">
          <div class="form-group">
             <label for="years_experience">{{ __('Years of Experience') }}<span class="asterick" style="color:red">*</span></label>
             <input type="text" name="years_experience" class="form-control" value="" required>
          </div>
        </div>
        <div class="col">
          <div class="form-group">
              <label for="management_experience">Management Experience:&nbsp<b></b><span class="asterick" style="color:red">*</span></label>
                <select class="form-control" id="management" name="management" required>
                  <option  value="">Select Management Experience</option>
                  <option value="1">Yes</option>
                  <option value="0">No</option>
                </select>
          </div>
          <div id="ma_experience" class="form-group" style="display: none;">
              <label>{{ __('Management Experience') }}</label>
               <select class="form-control" name="management_experience" id="management_experience">
                <option value="">Select Management Experience</option>
                <option value="Executive-Director">Executive/Director Level</option>
                <option value="Senior-Level">Senior Level</option>
                <option value="Mid-level">Mid Level</option>
                <option value="Lower-Level">Lower Level</option>
              </select>
              <!-- <input type="numeric" class="form-control" id="management_experience" name="management_experience" value="" hidden/><br/> -->
          </div>       
        </div> 
      </div>
      <div class="row">
        <div class="col">
             <div class="form-group">
            <label for="management_experience">Salary Currency:&nbsp<b></b><span class="asterick" style="color:red">*</span></label>
             <select id="grosssalcurr" name="grosssalcurr" class="form-control" required>
                      <option value="">Select Currency</option>
                      <option value="KES">KES</option>
                      <option value="EURO">EURO</option>
                      <option value="UK Pound">UK Pound</option>
                      <option value="USD">USD</option>
                      <option value="RAND">RAND</option>
                      <option value="UGX">UGX</option>
                      <option value="TZS">TZS</option>
                      <option value="ZMK">Zambian Kwacha</option>
                    </select>
          </div>
        </div>
        <div class="col">
           <div class="form-group">
            <label for="management_experience">Salary Expectation:&nbsp<b></b><span class="asterick" style="color:red">*</span></label>
            <input type="text" name="salaryexpectation" value="" class="form-control" required>
          </div>
        </div>
        <div class="col">
          <div class="form-group">
            <label>Hardskills<span class="asterick" style="color:red">*</span></label>
            <select class="form-control selectpicker selectdropdown" name="hardskills" id="hardskills"  data-width="100px;" data-live-search="true" data-size="10" multiple="multiple" required>
              @foreach($hardskills as $hardskill)
             <option value="{{$hardskill->ID}}">{{$hardskill->Name}}</option>
              @endforeach
            </select>
          </div>
        </div>
      </div>
      <div class="row">
            <div class="col">
          <div class="form-group">
            <label>Current Salary<span class="asterick" style="color:red">*</span></label>
            <input type="text" name="currentsalary" class="form-control" required>
          </div>
        </div>
           <div class="col">
               <div class="form-group">
            <label for="management_experience">Salary Currency:&nbsp<b></b><span class="asterick" style="color:red">*</span></label>
             <select id="grosssalcurr" name="grosssalcurr" class="form-control" required>
                      <option value="">Select Currency</option>
                      <option value="KES">KES</option>
                      <option value="EURO">EURO</option>
                      <option value="UK Pound">UK Pound</option>
                      <option value="USD">USD</option>
                      <option value="RAND">RAND</option>
                      <option value="UGX">UGX</option>
                      <option value="TZS">TZS</option>
                      <option value="ZMK">Zambian Kwacha</option>
                    </select>
          </div>
        </div>
        <div class="col">
          <div class="form-group">
            <label>Benefits Received<span class="asterick" style="color:red">*</span></label>
            <input type="text " name="benefitsreceived" class="form-control" required>
          </div>
        </div>
        <div class="col">
          <div class="form-group">
            <label>Notice Period<span class="asterick" style="color:red">*</span></label>
            <input type="text" name="notice_period" class="form-control" required>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <div class="form-group">
              <label for="title">{{ __('Job Title') }}<span class="asterick" style="color:red">*</span></label>
                <select id="title" name="job" class="form-control" required>
                    <option value="">Select Job Title</option>
                @foreach($jobdets as $jobs)
                    <option value="{{$jobs->ID}}">{{$jobs->JobTitle}}</option>
                 @endforeach
                </select>
          </div>
        </div>
        <div class="col">
          <div class="form-group">
             <label for="industry">{{ __('Industry / Sector ') }}<span class="asterick" style="color:red">*</span></label>
                <select id="industry" name="industry" class="form-control" required>
                  <option value="">Select Industry</option>
                @foreach($industry as $industries)
                  <option value="{{$industries->Name}}">{{$industries->Name}}</option>
                  @endforeach
                </select>

          </div>
        </div>
        <div class="col">
          <div class="form-group">
            <label for="status">Status:<span class="asterick" style="color:red">*</span></label>
            <select id="status" name="status" class="form-control" required>
              <option value="">Select Status</option>
              <option value="Scheduled">Scheduled</option>
              <option value="Interviewed">Interviewed</option>
              <option value="Shortlisted">Shortlisted</option>
              <option value="Recommended">Recommended</option>
              <option value="Placed">Placed</option>
              <option value="Discard">Discard</option>
              <option value="Blacklist" data-toggle="modal" data-target="#mymodal3">Blacklist</option>
            </select>
          </div>
           <div id="status_placed" class="form-group" style="display: none;">
              <label>{{ __('Placed') }}</label>
              <div class="form-group form-spacing">
                    <label for="status">Start Date:</label>
                    <input type="date" name="startdate" value="" class="form-control">
              </div>
                  <div class="form-group form-spacing">
                    <label for="status">GrossSalCurrency</label>
                    <select id="grosssalcurr" name="grosssalcurr" class="form-control">
                      <option value="KES">KES</option>
                      <option value="EURO">EURO</option>
                      <option value="UK Pound">UK Pound</option>
                      <option value="USD">USD</option>
                      <option value="RAND">RAND</option>
                      <option value="UGX">UGX</option>
                      <option value="TZS">TZS</option>
                      <option value="ZMK">Zambian Kwacha</option>
                    </select>
                  </div>
                   <div class="form-group form-spacing">
                    <label for="status">Gross Salary</label>
                    <input type="text" name="grosssalary" value="" class="form-control">
                  </div>
                  <div class="form-group form-spacing">
                    <label for="status">Comments</label>
                    <textarea class="form-control" name="comments"></textarea>
                  </div>
          </div>
        </div>      
      </div>
      <div class="row">
        <div class="col">
          <div class="form-group">
            <label for="excel_test">Excel Test:</label>
            <select class="form-control" name="excel_test" id="excel_test" value="" >
                <option value="">Select Rating</option>
                <option value="Significantly exceeds expectation">Significantly exceeds expectation</option>
                <option value="Exceeds Expectation">Exceeds Expectation</option>
                <option value="Meets Expectation">Meets Expectation</option>
                <option value="Needs Development">Needs Development </option>
                <option value="Unsatisfactory">Unsatisfactory</option>
                <option value="Not Applicable">Not Applicable</option>
                <option value="To be emailed">To be emailed</option>
            </select>
          </div>
        </div>
        <div class="col">
          <div class="form-group">
            <label for="word_drafting">Written Test</label>
            <select class="form-control" name="word_drafting" id="word_drafting" value="" >
                <option value="">Select Rating</option>
                <option value="Significantly exceeds expectation">Significantly exceeds expectation</option>
                <option value="Exceeds Expectation">Exceeds Expectation</option>
                <option value="Meets Expectation">Meets Expectation</option>
                <option value="Needs Development">Needs Development </option>
                <option value="Unsatisfactory">Unsatisfactory</option>
                  <option value="Not Applicable">Not Applicable</option>
                <option value="To be emailed">To be emailed</option>
            </select>
          </div>
        </div>
        <div class="col">
          <label for="situation_test">Situation Test</label>
          <div class="form-group">
            <select class="form-control" name="situation_test" id="situation_test" value="" >
                <option value="">Select Rating</option>
                <option value="0 - 20 - Unsatisfactory"> 0 – 20 Unsatisfactory The candidate has demonstrated very little skill in solving problems/ initiative/responsibility/honesty.</option>
                <option value="21 - 30 - Unsatisfactory">21 – 30 	Unsatisfactory	The candidate has demonstrated very little skill in solving problems/ initiative/responsibility/honesty.</option>
                <option value="31 - 40 - Unsatisfactory">31 – 40 	Unsatisfactory	The candidate has demonstrated very little skill in solving problems/ initiative/responsibility/honesty.</option>
                <option value="41 - 50 - Unsatisfactory">41 – 50	Unsatisfactory	The candidate has demonstrated very little skill in solving problems/ initiative/responsibility/honesty.</option>
                <option value="51 - 60 - Needs Development">51 - 60	Needs Development	The candidate has demonstrated limited skill in solving problems/ initiative/responsibility/honesty.</option>
                <option value="61 - 70 - Needs Development">61 - 70	Needs Development 	The candidate has demonstrated limited skill in solving problems/ initiative/responsibility/honesty.</option>
                <option value="71 - 80 - Meets Expectations">71 - 80	Meets Expectations	The candidate has demonstrated adequate skill in solving problems/ initiative/responsibility/honesty.</option>
                <option value="81 - 90 - Meets Expectations">81 - 90	Meets Expectations	The candidate has demonstrated adequate skill in solving problems/ initiative/responsibility/honesty.</option>
                <option value="91 - 98 - Exceeds Expectations">91 - 98	Exceeds Expectations	The candidate has demonstrated all of the skill in solving problems/ initiative/responsibility/honesty.</option>
                <option value="99 - 100 - Significantly Exceeds Expectations">99 - 100	Significantly Exceeds Expectations 	The candidate has demonstrated outstanding skill in solving problems/ initiative/responsibility/honesty.</option>
                 <option value="Not Applicable">Not Applicable</option>
                <option value="To be emailed">To be emailed</option>
            </select>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <label for="timemanagement">Time Management</label>
          <div class="form-group">
            <select class="form-control" name="time_manage" id="time_manage" >
                <option value="0 - 20 - Unsatisfactory">0 – 20 Unsatisfactory The candidate has demonstrated very little skill in prioritization & logical explanation</option>
                <option value="21 - 30 - Unsatisfactory">21 – 30 Unsatisfactory The candidate has demonstrated very little skill in prioritization & logical explanation</option>
                <option value="31 - 40 - Unsatisfactory">31 – 40 Unsatisfactory The candidate has demonstrated very little skill in prioritization & logical explanation</option>
                <option value="41 - 50 - Unsatisfactory">41 – 50 Unsatisfactory The candidate has demonstrated very little skill in prioritization & logical explanation</option>
                <option value="51 - 60 - Needs Development">51 - 60 Needs Development The candidate has demonstrated a limited amount of skill in prioritization & logical explanation</option>
                <option value="61 - 70 - Needs Development">61 - 70 Needs Development The candidate has demonstrated a limited amount of skill in prioritization & logical explanation</option>
                <option value="71 - 80 -  Meets Expectations">71 - 80 Meets Expectations The candidate has demonstrated adequate skill in prioritization & logical explanation</option>
                <option value="81 - 90 - Meets Expectations">81 - 90 Meets Expectations The candidate has demonstrated adequate skill in prioritization & logical explanation</option>
                <option value="91 - 98 - Exceeds Expectations">91 - 98 Exceeds Expectations The candidate has demonstrated very good skill in prioritization & logical explanation</option>
                <option value="99 - 100 - Significantly Exceeds Expectations">99 - 100 Significantly Exceeds Expectations The candidate has demonstrated outstanding skill in prioritization & logical explanation</option>
                  <option value="Not Applicable">Not Applicable</option>
                <option value="To be emailed">To be emailed</option>
            </select>
          </div>
        </div>
        <div class="col">
          <label for="test_details">Ravens IQ</label>
          <div class="form-group">
            <input type="numeric" name="test_details" class="form-control{{ $errors->has('test_details') ? ' is-invalid' : '' }}" value="" >
            @if ($errors->has('test_details'))
            <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('test_details') }}</strong>
            </span>
            @endif
          </div>
        </div>
          <div class="col">
          <div class="form-group">
            <label>Other Tests</label>
            <input type="text" name="lumina" class="form-control">
          </div>
        </div>
        <!--<div class="col">-->
        <!--  <label for="tests">Pre Qual Test</label>-->
        <!--  <div class="form-group">-->
        <!--    <input type="numeric" name="pre_qual" class="form-control" value="" disabled>-->
        <!--  </div>-->
        <!--</div>-->
      </div>
      <div id="allCompetecies">
      <div class="row">
          <div class="col">
              <label for="Interview_competency_1">Competencies 1:</label>
              <div class="form-group">
                  <select class="form-control" id="Interview_competency_1" name="InterviewFields[1][competency]">
                      <option>Select Competency</option>
                      @php $jComp = array(); @endphp
                      @foreach($competencies as $competency)
                      
                      @php array_push($jComp,[$competency->ID => $competency->Name]);@endphp
                      
                      <option value="{{$competency->ID}}">{{$competency->Name}}</option>
                      @endforeach
                  </select> 
                  
              </div>
          </div>
          <div class="col">
              <label for="Interview_rating_1">Rating Scale 1:</label>
              <div class="form-group">
                  <select class="form-control" name="InterviewFields[1][rating]" id="Interview_rating_1">
                          <option>Select Rating Scale</option>
                           <option value="Significantly exceeds expectation">Significantly exceeds expectation</option>
                        <option value="Exceeds Expectation">Exceeds Expectation</option>
                             
                            <option value="Meets Expectation">Meets Expectation</option>
                             <option value="Needs Development">Needs Development</option>
                          <option value="Unsatisfactory">Unsatisfactory</option>
                          
                         
                         
                         
                  </select>
              </div>
          </div>
          <div class="col">
              <label for="Interview_dropdowncomments_1">Comments 1:</label>
              <div class="form-group">
                  <select class="form-control" name="InterviewFields[1][dropdowncomments]" id="Interview_dropdowncomments_1">
                  <option value="">Select Comment</option>
                  <option value="The candidate has demonstrated outstanding  skill / knowledge / experience / behaviour">The Candidate has demonstrated outstanding  skill / knowledge / experience / behaviour</option>
                  <option value="The candidate has demonstrated very good skill / knowledge / experience / behaviour">The Candidate has demonstrated very good skill / knowledge / experience / behaviour </option>
                  <option value="The candidate has demonstrated adequate skill / knowledge / experience / behaviour">The Candidate has demonstrated adequate skill / knowledge / experience / behaviour </option>
                  <option value="The candidate has demonstrated limited skill / knowledge / experience / behaviour">The candidate has demonstrated limited skill / knowledge / experience / behaviour </option>
                  <option value="The candidate has demonstrated very little skill / knowledge / experience / behaviour">The candidate has demonstrated very little skill / knowledge / experience / behaviour </option>
                  </select>
              </div>
          </div>
      </div>
      </div>
      <div class="row">
          <div class="col">
      <div class="form-group form-spacing">
             <input type="button" value="+Add Competencies" class="btn btn-primary pull-right" id="addcompetencies" />
           </div>
           </div>
           </div>
      <div class="row">
        <div class="col">
         <label for="interviewdocuments">Confirm Availability Of Interview Documents</label>
          <div class="form-group form">
            <label>KRA</label>
              <input type="checkbox" name="KRA"  id="interviewkra" value="1">
              <input type="file" name="kra" class="form-control"><br>          
              <label>ID</label>
              <input type="checkbox" name="ID"  id="interviewid" value="1">
              <input type="file" name="id" class="form-control"><br>             
              <label>NHIF</label>
              <input type="checkbox" name="NHIF"id="interviewnhif" value="1">
              <input type="file" name="nhif" class="form-control"><br>             
              <label>NSSF</label>
              <input type="checkbox" name="NSSF"  id="interviewnssf" value="1">
              <input type="file" name="nssf" class="form-control"><br>              
              <label>Certificate Of Good Conduct</label>
              <input type="checkbox" name="CERTIFICATE"  id="interviewcertificate" value="1">
              <input type="file" name="cogc" class="form-control"><br>            
              <label>Credit Reference Bureau</label>
             <input type="checkbox" name="crb"  id="interviewcrb" value="1">
             <input type="file" name="brc" class="form-control"><br>
             <label>EACC Clearance </label>
             <input type="checkbox" name="ethics"  id="interviewethics" value="1">
             <input type="file" name="eacc" class="form-control"><br>
             
             <label>Proffessional Qualifications </label>
             <input type="checkbox" name="prof"  id="proffessional" value="1">
             <input type="file" name="proff" class="form-control"><br>
             
             <label>Education Certificates </label>
             <input type="checkbox" name="educationcertificates"  id="educationcertificates" value="1">
             <input type="file" name="education" class="form-control"><br>
           </div>
          
           <div id="referees">
            <div class="form-group" >
                <label for="Interview_referee_1" >
                  Upload Referee 1</label>
              <input  type='file' class="form-control" name="InterviewFields[referee][1]" id="Interview_referee_1" />
           </div>
           </div>
           <div class="form-group form-spacing">
             <input type="button" value="+Add Referee" class="btn btn-primary pull-right" id="addreferee" />
           </div><br><br>
           <div id="cantest">
            <div class="form-group" >
                <label for="Interview_cantest_1" class="required">
                  Candidate Test 1</label>
              <input  type='file' class="form-control" name="InterviewTest[cantest][1]" id="Interview_cantest_1" />
           </div>
           </div>
           <div class="form-group form-spacing">
             <input type="button" value="+ Add Candidate Test" class="btn btn-primary pull-right" id="addcandidate" />
           </div>
        </div>
        <div class="col">
          <div class="row">
          <div class="col">
              <div class="form-group">
             <label for="interviewdocuments">Interview Summary</label>
            <textarea id="editor" name="interview_summary" style="height: 480px"class="form-control"></textarea>
          </div>
            <div class="form-group">
              <label for="internalcomments">Internal Comments<span class="asterick" style="color:red">*</span></label>
              <textarea id="editor1" name="internalcomments" style="height: 480px" class="form-control" ></textarea>
            </div>
          </div>         
        
          
          </div>
        </div>
       
      </div>
      <div class="col">
       <div class="form-group">
          <label for="staff_assigned">{{ __('Assign to Staff') }}<span class="asterick" style="color:red">*</span></label>
          <select name="staff_assigned" class="form-control" id="staff_assigned">
         <option value="{{Auth::user()->SummitStaff->StaffID}}" selected>{{Auth::user()->SummitStaff->Firstname.' '. Auth::user()->SummitStaff->Lastname}} </option>
           @foreach($staff as $staf)
           <option value="{{$staf->StaffID}}">{{$staf->Firstname.' '.$staf->Lastname}} </option>
           @endforeach
          </select>
            
    </div>
    </div>
       <div class="kt-portlet__foot text-center">
      <div class="kt-form__actions">
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-secondary">Cancel</button>
      </div>
    </div>
    </div>
  </form>
  <script>
document.getElementById('from').max = new Date(new Date().getTime() - new Date().getTimezoneOffset() * 60000).toISOString().split("T")[0];

</script>
   <script>
    document.getElementById('buttonidd').addEventListener('click', openDialog);
    function openDialog()
    {
    document.getElementById('fileidd').click();
    }
  </script>
    <script>
    document.getElementById('buttonid').addEventListener('click', openDialog);
    function openDialog()
    {
    document.getElementById('fileid').click();
    }
  </script>
  <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
  tinymce.init({
    selector: 'textarea#editor',
    menubar: false
  });
</script>
<script>
  tinymce.init({
    selector: 'textarea#editor1',
    menubar: false
  });
</script>

<script>
  $('select[name=management]').on('change', function()
   {
    if (this.value == '1') {
      $("#ma_experience").show();
    } else {
      $('#management_experience').val('0');
      $("#ma_experience").hide();
    }
  });
</script>
<script>
  $('select[name=status]').on('change', function()
   {
    if (this.value == 'Placed') {
      $("#status_placed").show();
    } else {
      $('#status').val(this.value);
      $("#status_placed").hide();
    }
  });
</script>
<script>
  var el = document.getElementById("interview_date");
      el.onchange = onChangeDate;
       function onChangeDate(e)
       {
         if(new Date(el.value) < new Date())
         {
         alert("invalid date");
         }
       }
</script>
<script>
 $( document ).ready(function() {
     
if( typeof phpCompId !== 'undefined' ) {
	var compId =phpCompId;
}
else{
var compId =2;
}

$('#addcompetencies').click( function(j) {
     
     var comps = '<?php echo json_encode($jComp); ?>';
     
             var link=  '<div class="row">'+
                     '<div class="col">'+
			        '<div class="form-group ">'+
			        '<label for="Interview_competency_'+compId+'">Competencies '+compId+'</label>'+
			        '<select class="form-control" id="Interview_competency_'+compId+'" name="InterviewFields['+compId+'][competency]">'+
			        '<option>Select Competency</option>';
			        $.each(JSON.parse(comps), function(index, item) {
			            
			         $.each(item, function(ind, it) {

                    link +=  '<option value="'+ind+'">'+it+'</option>';
			         });
                        });
			       link += '</select>'+
			        '</div>'+
			        '</div>'+
			        
			 '<div class="col">'+
			   
                          
              '<label for="Interview_rating_'+compId+'">Rating Scale '+compId+':</label>'+
              '<div class="form-group">'+
                  '<select class="form-control" name="InterviewFields['+compId+'][rating]" id="Interview_rating_'+compId+'">'+
                         ' <option>Select Rating Scale</option>'+
                          '<option value="Significantly exceeds expectation">Significantly exceeds expectation</option>'+
                          '<option value="Exceeds Expectation">Exceeds Expectation</option>'+
                          '<option value="Meets Expectation">Meets Expectation</option>'+
                          '<option value="Needs Development">Needs Development</option>'+
                          '<option value="Unsatisfactory">Unsatisfactory</option>'+
                  '</select>'+
              '</div>'+
          '</div>'+
          '<div class="col">'+
              '<label for="Interview_dropdowncomments_'+compId+'">Comments '+compId+':</label>'+
              '<div class="form-group">'+
                  '<select class="form-control" name="InterviewFields['+compId+'][dropdowncomments]" id="Interview_dropdowncomments_'+compId+'">'+
                  '<option value="">Select Comment</option>'+
                  '<option value="The candidate has demonstrated outstanding  skill / knowledge / experience / behaviour">The candidate has demonstrated outstanding  skill / knowledge / experience / behaviour</option>'+
                  '<option value="The candidate has demonstrated very good skill / knowledge / experience / behaviour">The candidate has demonstrated very good skill / knowledge / experience / behaviour </option>'+
                  '<option value="The candidate has demonstrated adequate skill / knowledge / experience / behaviour">The candidate has demonstrated adequate skill / knowledge / experience / behaviour </option>'+
                  '<option value="The candidate has demonstrated limited skill / knowledge / experience / behaviour">The candidate has demonstrated limited skill / knowledge / experience / behaviour </option>'+
                  '<option value="The candidate has demonstrated very little skill / knowledge / experience / behaviour">The candidate has demonstrated very little skill / knowledge / experience / behaviour </option>'+
                  '</select>'+
              '</div>'+
          '</div>'+
		'</div>';
		
 	$("#allCompetecies").append(link);
 	compId++;
 	});
    
 }); 
 
    $('#addreferee').click( function(j) {
     
     $("#referees").append(
  
                '<div class="form-group ">'+
                '<label for="Interview_referee_'+refId+'">Upload Referee '+refId+'</label>'+
                '<input  type="file" class="form-control" name="InterviewReferee[referee]['+refId+']" id="Interview_referee_'+refId+'" />'+
                '</div>'
     );
      
     refId++;
     });
     
     
     $('#addcandidate').click( function(j) {
       
     
  
     $("#cantest").append(
  
                '<div class="form-group ">'+
                '<label for="Interview_cantest_'+testId+'">Candidate Test '+testId+'</label>'+
                '<input  type="file" class="form-control" name="InterviewTest[canTest]['+testId+']" id="Interview_cantest_'+testId+'" />'+
                '</div>'
     );
     
     testId++;
     });
</script>
<script>
jQuery(document).ready(function($) {
if ( $('#interview_date')[0].type != 'date' ) $('#interview_date').datepicker();

});
</script>
@endsection
