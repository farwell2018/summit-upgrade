@extends('layouts.candidate')

@section('doc-title','Terms and Conditions')

@section('content')

<div class="container">
  <div class="row">
 <div class="col-12">
   <h1 class="text-secondary" style="font-size: 32px;">Summit Recruitment And Search Privacy Policy</h1>

<hr />
 </div>

    <div class="col-4">

     <ul class="terms-sidebar">
    <li><a href="#Policy">The Privacy Policy</a></li>
    <li><a href="#Personal">Types of Personal Data Collected</a></li>
    <li><a href="#Retention">Retention of Your Personal Data</a></li>
    <li><a href="#Disclosure">Disclosure of Your Personal Data</a></li>
    <li><a href="#Breach">Data Breach</a></li>
    <li><a href="#Acceptance">Acceptance of this policy</a></li>
    <li><a href="#Contact">Contact Us</a></li>


     </ul>
    </div>
<div class="col-8 terms-content">

    <h3 id="Policy">The Privacy Policy </h3>
  @php $date = \Carbon\Carbon::now(); @endphp
    <p>Last updated: {{$date->toFormattedDateString()}}</p>
            <p>
              This Privacy Policy describes our policies and procedures on the collection, use and disclosure of your information. It also helps you understand your privacy rights and how the law protects you.
            </p>
            <p>
              We use your Personal data to provide and improve our service to you. By using this service, you agree to the collection and use of information in accordance with this Privacy Policy.

    </p>

    <h3 id="Personal">Types of Personal Data Collected</h3>

    <p>
              While using our service, we may ask you to provide us with certain personal information that can be used to contact or identify you. This information may include, but is not limited to:</p>
            <ul>
                <li>Email address</li>
                <li>First name and last name</li>
                <li>Phone number</li>
                <li>Address, State, Province, ZIP/Postal code, City</li></ul>

              <p>The Company may use Personal data for the following purposes:</p>
                  <ul>
                  <li>To provide and maintain our service</li>
                  <li>To manage your account</li>
                  <li>To contact you</li>
                  <li>To manage your requests and feedback</li>
                  <li>To respond to legal requests and prevent harm</li>

            </li></ul>



    <h3 id="Retention">Retention of Your Personal Data</h3>

    <p>

                Summit Recruitment & Search will retain your Personal data only for as long as is necessary for the purposes set out in this Privacy Policy. We will retain and use your Personal data to the extent necessary to comply with our legal obligations, resolve disputes, and enforce our legal agreements and policies.
            </p>
            <p>
                The Company will also retain usage data for internal analysis purposes. Usage data is generally retained for a shorter period of time, except when this data is used to strengthen the security or to improve the functionality of our service, or we are legally obligated to retain this data for longer periods of time.

    </p>


    <h3 id="Disclosure">Disclosure of Your Personal Data</h3>

    <p>Under certain circumstances, Summit Recruitment & Search may be required to disclose your Personal data by law or in response to valid requests by public authorities (e.g. a court or a government agency). <p>
      <p>  The Company may disclose your Personal data to:</p>
        <ul>

            <li>Comply with a legal obligation</li>
              <li>	Protect and defend the rights or property of the Company</li>
            <li>	Prevent or investigate possible wrongdoing in connection with the service</li>
            <li>	Protect the personal safety of Users of the service or the public</li>
            <li>	Protect against legal liability</li>
        </ul>
    

    <h3 id="Breach">Data Breach</h3>

  <p>In the event that the Website is compromised or User’s Personal information has been disclosed to unrelated third parties as a result of external activity, including but not limited to, security attacks or fraud, we reserve the right to take reasonable appropriate action, including but not limited to, investigation and reporting, as well as notifying law enforcement authorities. In the event of a data breach, we will make every reasonable effort to notify affected individuals if we believe that there is a risk of harm to the User as a result of the breach or if notice is otherwise required by law. In this situation, we will post a notice on the Website and send you an email.</p>

    <h3 id="Acceptance"> Acceptance of this policy</h3>

    <p>By continuing with this service, you accept that you have read this Policy and agree to all its’ Terms and Conditions. By using the Website or its’ services you agree to be bound by this Policy.</p>
    <p>Accordingly, Summit Recruitment & Search is hereby authorized to introduce all Users of the service to appropriate companies and to act on behalf of all Users in matters relating to finding a job placement,  including but not limited to, taking all relevant references from previous employers, checking credentials with the Ethics & Anti-corruption Commission, Police Clearance Certification and acquiring personal credentials from the Credit Rating Bureau.</p>


    <h3 id="Contact">Contact Us</h3>

    <p>If you would like to contact us to understand more about this Policy or wish to contact us concerning any matter relating to individual rights and your Personal information, you may do so via the contact form, send an email to <a mailto="info@summitrecruitment-search.com">info@summitrecruitment-search.com</a> or call our direct lines 0713 461 279 |0738 555 933.

</div>
</div>

</div>

@endsection

@section('js')
<script type="text/javascript">
$(document).ready(function() {
  $(".terms-sidebar").find("a").click (e) ->
      e.preventDefault()
      $(this).addClass('active');
      section = $(this).attr "href"
      $("html, body").animate
          scrollTop: $(section).offset().top
});
</script>

@endsection
