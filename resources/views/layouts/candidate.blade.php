<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="shortcut icon" type="image/png" href="{{ asset('public/images/Mobile-Website-App-150x150.png') }}"/>

    <!-- Scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://kit.fontawesome.com/8f04f65c87.js" crossorigin="anonymous"></script>
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script type="text/javascript" src="{{ asset('public/build/js/intlTelInput.js')}}"></script>
    <script type="text/javascript" src="{{ asset('public/build/js/utils.js')}}"></script>
    <script src="{{ asset('public/js/app.js') }}" defer></script>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Questrial&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous"></script>

    <link href="{{ asset('public/css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/main.css') }}" rel="stylesheet">
    <link href="{{ asset('public/build/css/intlTelInput.css') }}" rel="stylesheet">
    @yield('css')
    
    <style>
    .tox .tox-notification--warning{
    display:none !important;
    }
   </style>
</head>
<body>
    <div id="app">


        @include('includes.general.app-header')

        <main class="py-4">
            @yield('content')
        </main>


<!-- Back to top button -->
@guest
@else
<a id="support" href="https://www.summitrecruitment-search.com/summitre/tickets"><span>Support</span></a>
<!-- <a id="support" href="{{ route('tickets')}}"><span>Support</span></a> -->

@endguest
<a id="back-top"><span>Back Up</span></a>

        @include('includes.general.app-footer')
        @include('includes.modals.notification')
        @include('includes.modals.errors')
        @include('includes.modals.popup')

    </div>
@if(Session::has('redirectError'))
<script type="text/javascript">
$(document).ready(function() {
    //console.log("got it");
 $("#errorModal").modal('show');
});
</script>
@endif

<script type="text/javascript">
    tinymce.init({
        selector:'textarea.description',
        width: 700,
        height: 300
    });
</script>


@yield('js')

<script type="text/javascript">
//<![CDATA[
var owa_baseUrl = 'https://analyse.farwell-consultants.com/';
var owa_cmds = owa_cmds || [];
owa_cmds.push(['setSiteId', '3dfa64831fbb0ae74972204e8c73c074']);
owa_cmds.push(['trackPageView']);
owa_cmds.push(['trackClicks']);

(function() {
	var _owa = document.createElement('script'); _owa.type = 'text/javascript'; _owa.async = true;
	owa_baseUrl = ('https:' == document.location.protocol ? window.owa_baseSecUrl || owa_baseUrl.replace(/http:/, 'https:') : owa_baseUrl );
	_owa.src = owa_baseUrl + 'modules/base/js/owa.tracker-combined-min.js';
	var _owa_s = document.getElementsByTagName('script')[0]; _owa_s.parentNode.insertBefore(_owa, _owa_s);
}());
//]]>
</script><!-- End Open Web Analytics Code -->
<script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
 <script>
  tinymce.init({
    selector: 'textarea#editor',
    menubar: false,
    plugins : "paste",
    paste_text_sticky : true,
    
  });
</script>
<script>
 tinymce.init({
   selector: 'textarea.editor1',
   menubar: false,
   toolbar1: 'formatselect |  bold italic |  numlist bullist | removeformat',
   plugins: ' advlist lists paste',
   paste_text_sticky : true,
 });
</script>
<script>
 tinymce.init({
   selector: 'textarea.editor3',
   menubar: false,
   toolbar1: 'formatselect |  bold italic  | numlist bullist | removeformat',
   plugins: ' advlist lists paste',
   paste_text_sticky : true,
 });
</script>
<script>
 tinymce.init({
   selector: 'textarea.editor4',
   menubar: false,
   toolbar1: 'formatselect | bold italic  |  numlist bullist | removeformat',
   plugins: ' advlist lists paste',
   paste_text_sticky : true,
 });
</script>
<script>
 tinymce.init({
   selector: 'textarea.editor5',
   menubar: false,
    toolbar1: 'formatselect | bold italic |  numlist bullist | removeformat',
   plugins: ' advlist lists paste',
   paste_text_sticky : true,
 });
</script>
<script>
jQuery(document).ready(function($) {

if ( $('#edstart_date')[0].type != 'date' ) $('#edstart_date').datepicker();

if ( $('.edend_date')[0].type != 'date' ) $('.edend_date').datepicker();

});
</script>
</body>
</html>

