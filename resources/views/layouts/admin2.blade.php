<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>{{ config('app.name', 'Summit') }} - @yield('title')</title>

  <!-- Custom fonts for this template-->
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
  <link href="{{asset('public/admin-assets/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">


   @foreach(config('admin-constants.styles') as $style)
  <link href="{{asset($style)}}" rel="stylesheet" type="text/css" />
  @endforeach
  <!-- Custom styles for this template>
  <link href="{{asset('sbadmin/css/sb-admin-2.min.css')}}" rel="stylesheet"-->
  <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">


  <script src="{{asset('public/admin-assets/vendor/jquery/jquery.min.js')}}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
 
  <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
  <script src="{{asset('public/js/admin.js?t='.time())}}"></script>
  <script type="{{asset('public/admin-assets/vendor/chart.js/Chart.js')}}"></script>
  <script type="{{asset('public/admin-assets/vendor/chart.js/Chart.min.js')}}"></script>
  


<!-- fullCalendar -->
  <link rel="stylesheet" href="{{asset('public/adminlte/bower_components/fullcalendar/dist/fullcalendar.min.css')}}">
  <link rel="stylesheet" href="{{asset('public/adminlte/bower_components/fullcalendar/dist/fullcalendar.print.min.css')}}" media="print">
<style>
html {
 font-size: unset;
}
.tox .tox-notification--warning{
display:none !important;
}
    
</style>
</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
   
    <!-- End of Sidebar -->
     @include('includes.administrator.sidebar')
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">
        <!-- Topbar -->
         @include('includes.administrator.header')
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          @yield('content')

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      @include('includes.administrator.footer')
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  

  <!-- Bootstrap core JavaScript-->
 @foreach(config('admin-constants.js') as $js)
  <script src="{{asset($js)}}" type="text/javascript"></script>
  @endforeach
  
  
    <!-- Calendar JavaScript-->
  <script src="{{asset('public/adminlte/bower_components/fullcalendar/dist/fullcalendar.min.js')}}"></script>

  <!-- Core plugin JavaScript-->
  <script src="{{asset('public/sbadmin/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

  <!-- Custom scripts for all pages-->
  <script src="{{asset('public/sbadmin/js/sb-admin-2.min.js')}}"></script>

  <!-- Page level plugins -->
  <script src="{{asset('public/sbadmin/vendor/chart.js/Chart.min.js')}}"></script>

  <!-- Page level custom scripts -->
  <script src="{{asset('public/sbadmin/js/demo/chart-area-demo.js')}}"></script>
  <script src="{{asset('public/sbadmin/js/demo/chart-pie-demo.js')}}"></script>
  <script src="{{asset('public/sbadmin/js/demo/chart-bar-demo.js')}}"></script>
  
<script>

$( document ).ready(function() {
  var phpReqId = {{$reqid}};
  var phpDutyId = {{$dutyid}};
  console.log(phpReqId);
  console.log(phpDutyId);
if( typeof phpDutyId !== 'undefined' ) {
 var dutyId =phpDutyId;
}
else{
var dutyId =2;
}
if( typeof phpReqId !== 'undefined' ) {
 var reqId =phpReqId;
}
else{
var reqId =2;
}
$('#addduties').click( function(j) {

 $("#duties").append(

             '<div class="form-group">'+
             '<label for="JobFields_duties_'+dutyId+'" class="required">Duty '+dutyId+' <span class="required">*</span></label>'+
             '<input size="60"  name="JobFields[duties]['+dutyId+']" id="JobFields_duties_'+dutyId+'" type="text" value="" class="form-control">'+
             '</div>'
 );
 dutyId++;
 });
$('#addrequirementes').click( function(j) {

 $("#requirements").append(

             '<div class="form-group">'+
             '<label for="JobFields_requirements_'+reqId+'" class="required">Requirement '+reqId+' <span class="required">*</span></label>'+
             '<input size="60"  name="JobFields[requirements]['+reqId+']" id="JobFields_requirements_'+reqId+'" type="text" value="" class="form-control">'+
             '</div>'
 );
 reqId++;
 });
});
</script>

{!! Toastr::render() !!}
@yield('footer-js')
</body>

</html>
