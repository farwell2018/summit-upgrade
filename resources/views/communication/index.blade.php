@extends('layouts.admin')

@section('title','Communication Management')


@section('content')

<div class="row " style="margin-left:8px">
        <a href="{{route('home')}}" style="color:#858796" >
            <span class="text">Dashboard</span> </a> &nbsp; | &nbsp;
            <span class="text">Communication Management Board</span> </a>&nbsp;

    </div>
<br>
 <div class="row user-add-button">
  <a href="{{route('communication.create')}}" class="btn btn-primary btn-icon-split" style="margin-right: 15px;">
  <span class="icon"><i class="fas fa-plus"></i></span>
  <span class="text">New Communication</span> </a>
</div>


<div class="card mb-5">
  <div class="card-header tab-form-header">
    Communications Board
  </div>
  <div class="card-body">
		    <table class="table table-bordered table-striped table-hover dt-responsive nowrap" id="dataTable" >
		      <thead>
		        <tr>
		       
		          <th>Title</th>
		          <th>Target Group</th>
		          <th>Tool</th>

		        </tr>
		      </thead>
		    </table>
  </div>
</div>
    @endsection
    @section('footer-js')
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />


    <script>
    var dataTable;
    let baseUrl = "{!! route('communications.json') !!}?";

    function encodeQueryData(data) {
     const ret = [];
     for (let d in data)
       ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
     return ret.join('&');
    }
    //Filter method
    function filterJobs(){
      const type = document.querySelector('#filterForm select[name=jobtype]').value

      const careerlevel = document.querySelector('#filterForm select[name=careerlevel]').value

      const education = document.querySelector('#filterForm select[name=education]').value

      const jobtitle = document.querySelector('#filterForm input[name=jobtitle]').value

      const category = document.querySelector('#filterForm select[name=category]').value

      const clients = document.querySelector('#filterForm select[name=clients]').value

      const staff = document.querySelector('#filterForm select[name=staff]').value

      const url = baseUrl  + "&" + encodeQueryData({type,careerlevel,education,jobtitle,category,clients,staff})

      console.log(url);
      dataTable.ajax.url( url ).load();
    }
    $(document).ready(function(){
        dataTable= $('#dataTable').DataTable({
        responsive: true,
        "deferRender": true,
        "processing": true,
        "serverSide": true,
        "ordering": true, //disable column ordering
        "lengthMenu": [
          [25, 50, 100,  -1],
          [25, 50, 100, "All"] // change per page values here
        ],
        "pageLength": 25,
        "ajax": {
           url: baseUrl,
          method: 'GET'
        },
        // dom: '<"html5buttons"B>lTfgitp',
        "dom": "<'row' <'col-md-12'>><'row'<'col-md-8 col-sm-12'lB><'col-md-4 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
        buttons: [
          { extend: 'copy',exportOptions: {columns: [0, 1, 2, 3,4,5,6]}},
          {extend: 'csv',exportOptions: {columns: [0, 1, 2, 3,4,5,6]}},
          {extend: 'excel', title: '{{ config('app.name', 'Summit') }} - List of all Users',exportOptions: {columns: [0, 1, 2, 3,4,5]}},
          {extend: 'pdf', title: '{{ config('app.name', 'Summit') }} - List of all Users',exportOptions: {columns: [0, 1, 2, 3,4,5]}},
          {extend: 'print',
          customize: function (win){
            $(win.document.body).addClass('white-bg');
            $(win.document.body).css('font-size', '10px');
            $(win.document.body).find('table')
            .addClass('compact')
            .css('font-size', 'inherit');
          }
        }
      ],
      columns: [
        {data: 'c_name', name: 'c_name', orderable: true, searchable: true},
        {data: 'target', name: 'target', orderable: true, searchable: true},
        {data: 'tools', name: 'tools', orderable: false, searchable: false,"defaultContent": "<i>Not set</i>"},
      ],
    });
    });
    </script>
    @endsection
