@extends('layouts.admin')

@section('title','Communication Management')

@section('content')
<div class="row " style="margin-left:8px">
      <a href="" style="color:#858796" >
          <span class="text">Home</span> </a> &nbsp; | &nbsp;
      <a href="">
          <span class="text">Communication Create</span></a>
</div>
<br>
<form class="kt-form" method="POST" action="{{route('communication.store')}}"  enctype="multipart/form-data">
  @csrf
    <div class="kt-portlet__body">
          <div class="form-group">
	            <label for="communication">{{ __('Communication Title') }}</label>
	            <input id="communication" type="test" class="form-control{{ $errors->has('communication') ? ' is-invalid' : '' }}" name="communication" value="{{ old('communication') }}" placeholder="Title Of This Communication" required>
	            @if ($errors->has('communication'))
	            <span class="invalid-feedback" role="alert">
	              <strong>{{ $errors->first('communication') }}</strong>
	            </span>
	            @endif
          </div>
          <div class="form-group">
              <label>Target</label>
              <select name="target" id="target" class="form-control">
                <option value="">Select Email Target</option>
                <option value="1">Interviewed</option>
                <option value="2">Shortlisted</option>
                <option value="3">Placed</option>
                <option value="4">Staff</option>
                <option value="5">All</option>
                <option value="6">Job Applicants</option>
              </select>
          </div>
           <!-- <div class="form-group">
               <label for="que_date">Queue Date</label>
                <input id="que_date" type="date" name="que_date" class=" form-control {{ $errors->has('que_date') ? ' is-invalid' : '' }}"  value="" required>
                @if ($errors->has('que_date'))
                <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('que_date') }}</strong>
                @endif
           </div> -->
            <div class="form-group">
               <label for="que_date">Email Body</label>
            <textarea id="editor" name="message" class="form-control" style="height: 380px"></textarea>
          </div>
    </div>
    <div class="form-group text-center">
        <button type="submit" class="btn btn-primary">Create</button>
        <button type="reset" class="btn btn-secondary">Cancel</button>
    </div>
</form>
<script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
  tinymce.init({
    selector: 'textarea#editor',
    menubar: false
  });
</script>
@endsection
