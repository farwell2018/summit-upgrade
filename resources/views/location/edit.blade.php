@extends('layouts.admin')

@section('title','Location')


@section('content')

     <div class="row " style="margin-left:8px">
        <a href="{{route('home')}}" style="color:#858796" >
            <span class="text">Home</span> </a> &nbsp; | &nbsp;
        <a href="{{route('location.index')}}" >
            <span class="text">Location List</span> </a>
    </div>
    <br>

    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Edit a Location
                </h3>
            </div>
        </div>
        <!--begin::Form-->
        <form class="kt-form" method="POST" action="{{ route('location.update',$location->LocationID) }}"
              enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="locationname">Location Name</label>
                <input id="locationname" type="text" class="form-control{{ $errors->has('locationname') ? ' is-invalid' : '' }}" name="locationname" value="{{ $location->LocationName  }}" required>

                @if ($errors->has('locationname'))
                    <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('locationname') }}</strong>
            </span>
                @endif
            </div>

            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="reset" class="btn btn-secondary">Cancel</button>
                </div>
            </div>
        </form>
        <!--end::Form-->
@endsection
