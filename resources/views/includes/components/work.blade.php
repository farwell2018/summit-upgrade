	<div class="tab-form mb-5">
		<div class="tab-form-header text-white">
			<h4>Work Experience<span class="asterick">*</span></h4>
		</div>
		<div class="form-group" style="padding: 2%">
			@if(!empty($work))
			@foreach($work as $work)
				@if($work->Title != null)
				<div class="row education">
				<div class="col-md-10">
				<h4 class="text-primary">{{$work->Title}}</h4>
				<p>Company: {{$work->Company}}</p>
				<p><span>From - @php echo date("m-Y", strtotime($work->StartDate)); @endphp - to - @if($work->CurrentDate == 'Yes')  Date  @else @php echo date("m-Y", strtotime($work->EndDate)) @endphp @endif </span></p>
			    </div>
				<div class="col-md-2">
		   		<span class="text-primary" onclick="removeWork({{$work->WorkExpID}})" style="cursor: pointer;"><i class="fas fa-trash"></i> Delete</span>
		   		<span class="text-primary float-right" onclick="editWork({{$work->WorkExpID}})" style="cursor: pointer;"><i class="fas fa-edit"></i> Edit</span>
		   		</div></div>
				@else
			    <div class="row education">
				<div class="col-md-10">
				<h4 class="text-primary">@if($work->OtherWork){{$work->OtherWork}}@else{{$work->Title}}@endif</h4>
				<p>Company: {{$work->Company}}</p>
				<p><span>From - @php echo date("m-Y", strtotime($work->StartDate)); @endphp - to - @if($work->CurrentDate == 'Yes')  Date  @else @php echo date("m-Y", strtotime($work->EndDate)) @endphp @endif </span></p>
					</div>
				<div class="col-md-2">
		   		<span class="text-primary" onclick="removeWork({{$work->WorkExpID}})" style="cursor: pointer;"><i class="fas fa-trash"></i> Delete</span>
		   		<span class="text-primary float-right" onclick="editWork({{$work->WorkExpID}})" style="cursor: pointer;"><i class="fas fa-edit"></i> Edit</span>
		   		</div></div>
				@endif
				<hr/>
			@endforeach	

			@else
		   <span>Add your work experiences</span>
		   @endif	
		   <span class="text-primary float-right mb-3 add-experience" style="cursor: pointer;"><i class="fas fa-plus"></i> Add</span>
		   <br/>
		 </div>
	</div>


