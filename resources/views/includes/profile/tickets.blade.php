          
          <div class="row">
            @if(count($tickets) > 0)
            <div class="col-md-12">
              <div class="row" style="margin-bottom: 10px">
                <div class="col-md-2"><h4 class="text-center">Ticket</h4></div>
                <div class="col-md-3"><h4 class="text-center">Description</h4></div>
                <div class="col-md-3"><h4 class="text-center">Response</h4></div>
                <div class="col-md-2"><h4 class="text-center">Updated at</h4></div>
                <div class="col-md-2"><h4 class="text-center">Status</h4></div>
                </div>                
              @foreach($tickets as $ticket)
              <div class="applied row no-gutters">
                <div class="col-md-2 border-right"><span class="text-secondary">@if($ticket->id < 10) T00{{$ticket->id}} @else T0{{$ticket->id}} @endif: {{$ticket->type}}</span></div>
                <div class="col-md-3 border-right"><span>{{$ticket->description}}</span></div>
                <div class="col-md-3 border-right"><span>@if($ticket->response == null) Not Responded @else {{$ticket->response}} @endif</span></div>
                <div class="col-md-2 border-right"><span>{{$ticket->updated_at}}</span></div>
                <div class="col-md-2 border-right"><span class="text-info">@if($ticket->status == 0)
                <span class="badge badge-danger"> Open</span>                 
                 @else 
                 <span class="badge badge-success"> Closed</span>                
                  @endif 
                 </span>
               </div>
                </div>
              @endforeach
            </div>
              @else
              <div class="text-center col-12">
           <br><br><br>
            <p>
                Your list of raised tickets is currently empty.
            </p>
            <br><br><br>
            <br><br><br>
        </div>
              @endif
          </div>