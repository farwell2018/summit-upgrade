	<div class="tab-form mb-5">
		<div class="tab-form-header text-white">
			<h4>Work Experience</h4>
		</div>
		<div class="form-group" style="padding: 2%">
			@if(count($work) > 0)
			@foreach($work as $work)
				<div class="row education">
				<div class="col-md-10">
				<h4 class="text-primary">{{$work->Title}}</h4>
				<p>Company: {{$work->Company}}</p>
				<p><span>From - @php echo date("m-Y", strtotime($work->StartDate)); @endphp To - @if($work->CurrentDate == 'Yes')  now  @else @php echo date("m-Y", strtotime($work->EndDate)) @endphp @endif </span></p>
			
			    <h4 class="text-primary">Responsibilites:</h4>
			    	
				@foreach($workresps as $res)
				@foreach($res as $resp)
				@if($resp->WorkExpID == $work->WorkExpID)
			     @if(empty($resp->Responsibility))
			     @else
				{!! $resp->Responsibility !!}
			
				@endif
				@endif
				@endforeach
				@endforeach
				
				<h4 class="text-primary">Achievements:</h4>
				
				@foreach($workresps as $res)
				@foreach($res as $resp)
				@if($resp->WorkExpID == $work->WorkExpID)
				 @if(empty($resp->Achievement))
			     @else
				{!! $resp->Achievement !!}
				
				@endif
				@endif
				@endforeach
				@endforeach
				
					</div>
				<div class="col-md-2">
		   		<span class="text-primary" onclick="removeWork({{$work->WorkExpID}})" style="cursor: pointer;"><i class="fas fa-trash"></i> Delete</span>
		   		<span class="text-primary float-right" onclick="editWork({{$work->WorkExpID}})" style="cursor: pointer;"><i class="fas fa-edit"></i> Edit</span>
		   		</div></div>
				<hr/>
			@endforeach

			@else
		   <span>Add your work experiences</span>
		   @endif
		   <span class="text-primary float-right mb-3 more-experience" style="cursor: pointer;"><i class="fas fa-plus"></i> Add</span>
		   <br/>
		 </div>
	</div>
