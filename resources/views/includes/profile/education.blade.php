	<div class="tab-form mb-5">
		<div class="tab-form-header text-white">
			<h4>Education</h4>
		</div>
		<div class="form-group" style="padding: 4% 2%">
			@if(!empty($education))
			@foreach($education as $edu)
					@if($edu->FurtherEducation != null)
					<div class="row education">
					<div class="col-md-10">
					<h4 class="text-primary">{{$edu->FurtherEducation}} {{$edu->Subjects}}</h4>
					<p>@if(empty($edu->Specialization)) @else Specialization: {{$edu->Specialization}}@endif</p>
					<p><span>{{$edu->Institution}} |</span> <span>From @php echo date("m-Y", strtotime($edu->QualStartGradDate)) @endphp - To @php echo date("m-Y", strtotime($edu->QualEndGradDate)) @endphp</span></p>
					</div>
					<div class="col-md-2">
			   		<span class="text-primary" onclick="removeEdu({{$edu->FurtherEducationID}})" style="cursor: pointer;"><i class="fas fa-trash"></i> Delete</span>
			   		<span class="text-primary float-right" onclick="editEdu({{$edu->FurtherEducationID}})" style="cursor: pointer;"><i class="fas fa-edit"></i> Edit</span>
			   		</div></div>
					@else
					<div class="row education">
					<div class="col-md-10">
					<h4 class="text-primary">{{$edu->Institution}}</h4>
					<p><span>From @php echo date("m-Y", strtotime($edu->QualStartGradDate)) @endphp - To @php echo date("m-Y", strtotime($edu->QualEndGradDate)) @endphp</span></p>
					</div>
					<div class="col-md-2">
			   		<span class="text-primary" onclick="removeEdu({{$edu->FurtherEducationID}})" style="cursor: pointer;"><i class="fas fa-trash"></i> Delete</span>
			   		<span class="text-primary float-right" onclick="editEdu({{$edu->FurtherEducationID}})" style="cursor: pointer;"><i class="fas fa-edit"></i> Edit</span>
			   		</div></div>
					@endif

				<hr/>
			@endforeach

			@else
		   <span>Add your education level</span>
		   @endif
		   <span class="text-primary float-right mb-3 add-education" style="cursor: pointer;"><i class="fas fa-plus"></i> Add</span>
		 </div>
	</div>

	<div class="tab-form mb-5">
		<div class="tab-form-header text-white">
			<h4>Professional Qualifications</h4>
		</div>

		<div class="form-group" style="padding: 2%">
			@if(!empty($qualifications))
			@foreach($qualifications as $value => $qual)
				<div class="row">
				<div class="col-md-10">
				<h4 class="text-primary">{{$qual->ProfessionalQualifications}}</h4>
				<p>Title: {{$qual->ProfQualTitle}}</p>
				<p> <span>From @php echo date("Y", strtotime($qual->StartDate)) @endphp - To @php echo date("Y", strtotime($qual->EndDate)) @endphp</span></p>
				</div>
				<div class="col-md-2">
			   	<span class="text-primary" onclick="removeProf({{$qual->ProfQualID}})" style="cursor: pointer;"><i class="fas fa-trash"></i> Delete</span>
			   	<span class="text-primary float-right" onclick="editProf({{$qual->ProfQualID}})"  style="cursor: pointer;"><i class="fas fa-edit"></i> Edit</span>
			   	</div>
				</div>
				<hr/>
			@endforeach

			@else
		   <span>Add your Professional qualifications and Professional Bodies</span>
		   @endif
		   <span class="text-primary float-right mb-3 add-profession" style="cursor: pointer;"><i class="fas fa-plus"></i> Add</span>
		   <br/>
		</div>
	</div>
	<div class="tab-form mb-5">
		<div class="tab-form-header text-white">
			<h4>Professional Bodies</h4>
		</div>
		<div class="form-group" style="padding: 2%">
			@if(!empty($profbodies))
			@foreach($profbodies as $value => $qual)
			  @foreach($profBodiesTitles as $key => $prof)
			  @if($qual->ProfessionalBody == $prof->id)
				<div class="row">
				<div class="col-md-10">
				<h4 class="text-primary">{{$prof->name}}</h4>
				<p>Title: {{$prof->name}}</p>
				<p>Membership No/Certificate No:{{$profbodies[$value]->Membership_number}}</p>
				<!-- <p>Membership No/Certificate No:<span class="badge badge-primary">{{$profbodies[$value]->Membership_number}}</span></p> -->
				</div>
				<div class="col-md-2">
			   	<span class="text-primary" onclick="removeBodies({{$qual->ProfBodyID}})" style="cursor: pointer;"><i class="fas fa-trash"></i> Delete</span>
			   	<span class="text-primary float-right" onclick="editBodies({{$qual->ProfBodyID}})"  style="cursor: pointer;"><i class="fas fa-edit"></i> Edit</span>
			   	</div>
				</div>
				<hr/>
				@else
				@endif
				@endforeach
			@endforeach

			@else
		   <span>Add your Professional Bodies</span>
		   @endif
		   <span class="text-primary float-right mb-3 add-bodies" style="cursor: pointer;"><i class="fas fa-plus"></i> Add</span>
		   <br/>
		</div>

	</div>
