
<div class="modal" id="interviewModal" tabindex="-1" role="dialog" aria-labelledby="interviewModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        	<h4 class="modal-title text-primary text-center" style="color:#fff !important">Please select if your want to export personal contacts</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form method="POST" action="{{ URL::route('interview.export')}}">
                      @csrf
                      <input type="hidden" name="cvdets" value="{{$cvdets->CV_ID}}" id="rate_course">
                      <div class="form-group " style="margin-top:40px;margin-left:20px;margin-right:20px">
                          <select id="future_bundles" class="form-control" name="selection" required>
                            <option value=" "> Select Option </option>
                           <option value="yes"> Yes </option>
                           <option value="no"> No </option>
                         </select>
                      </div>
                      <div class="modal-footer justify-content-center">
                         
                               <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-success">Submit</button>
                         
                      </div>
                  </form>
    </div>
  </div>
</div>

