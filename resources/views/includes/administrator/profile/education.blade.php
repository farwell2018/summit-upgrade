	<div class="mb-5">
	<div class="tab-form ">
		<div class="tab-form-header text-white" style="background-color: #1C2E5C">
			<h4>Education</h4>
			 <span title="Add Comment" class="text-white edit-education" style="cursor: pointer;"><i class="fas fa-comment"></i> <!--wacha tuangalie hapa-->
		</div>
		<div class="form-group" style="padding: 4% 2%">
			@if(!empty($education))
			@foreach($education as $edu)
					@if($edu->FurtherEducation != null)
				    <div class="row">
				    	<div class="column" style="color: #1C2E5C">
				    		<h6 class="text" style="color: #1C2E5C"><b>Specialization:</b></h6>
				    		<h6 class="text" style="color: #1C2E5C"><b>Institution:</b></h6>
				    	</div>
				    	<div class="column" style="color: #1C2E5C;padding-left: 70px">
				    		<p> {{$edu->FurtherEducation}} {{$edu->Subjects}}</p>
				    		<p>{{$edu->Institution}}</p>
				    		<p>From @php echo date("d-m-Y", strtotime($edu->QualStartGradDate)) @endphp - To @php echo date("d-m-Y", strtotime($edu->QualEndGradDate)) @endphp</p>
				    	</div>				    
				    </div>
				    @else
				    <div class="row">
				    	<div class="column">				    		
				    	</div>
				    	<div class="column" style="color: #1C2E5C;padding-left: 70px">
				    		<p>{{$edu->Institution}}</p>
				    		<p>From @php echo date("d-m-Y", strtotime($edu->QualStartGradDate)) @endphp - To @php echo date("d-m-Y", strtotime($edu->QualEndGradDate)) @endphp</p>
				    	</div>
				    </div> 
					@endif
				<hr/>
			@endforeach	
		    @elseif(empty($education))
		    <p>No Education Details</p>
		    @endif
		 </div>
	</div>
	@php $x = 0; @endphp
	@foreach($managements as $management)
	@if(!empty($management->section))
   @if($management->section === "education")
    @php $x++; @endphp
    <div class="card comment-card">
    	<div class="card-body comment-body">
    		<p>
    			 <span class="details-span-first">{{'Comment'}} {{$x}}</span>
    			 | <span class="details-span">@if($management->StaffDetails) {{$management->StaffDetails->Firstname}} {{$management->StaffDetails->Lastname}} @endif</span>
    			 | <span class="details-span">{{Carbon\Carbon::parse($management->updated_at)->format('M jS Y')}}</span>
                 
    			 <span class="details-span" style="float:right">
    			 	@if($management->staffID == Auth::user()->SummitStaff->id)
                      <a href="#" class="edit-education" data-id="{{ $management->id }}" data-comment="{{ $management->comment }}"> Edit </a>
                 
    			 	@else
                      <a href="#" class="view-education" data-view="{{ $management->comment }}"> View </a>
                       
                    @endif
    			 </span>
    		</p>
    	</div>
    </div>
   @endif
   @endif
   @endforeach
</div>
  <div class="mb-5">
	<div class="tab-form">
		<div class="tab-form-header text-white" style="background-color: #1C2E5C ">
			<h4>Professional Qualifications</h4>
			 <span title="Add Comment" class="text-white edit-proffesional" style="cursor: pointer;"><i class="fas fa-comment"></i></span>
		</div>

		<div class="form-group" style="padding: 2%;">
			@if(!empty($qualifications))
			@foreach($qualifications as $value => $qual)
				<div class="row" >
				<div class="col-md-10" style="color: #1C2E5C">
				<h6 class="text" style="color: #1C2E5C"><b>Proffessional Qualification:</b> {{$qual->ProfessionalQualifications}}</h6>
				<h6><b>Title:</b> {{$qual->ProfQualTitle}}</h6>
				<p > <span style="color: #1C2E5C">From @php echo date("d-m-Y", strtotime($qual->StartDate)) @endphp - To @php echo date("d-m-Y", strtotime($qual->EndDate)) @endphp</span></p>
				</div>
				</div>
				<hr/>
			@endforeach	
		   @endif		   
		   <br/>
		</div>
	</div>
	@php $x = 0; @endphp
	@foreach($managements as $management)
	@if(!empty($management->section))
   @if($management->section === "proffesional")
    @php $x++; @endphp
    <div class="card comment-card">
    	<div class="card-body comment-body">
    		<p>
    			 <span class="details-span-first">{{'Comment'}} {{$x}}</span>
    			 | <span class="details-span">@if($management->StaffDetails){{$management->StaffDetails->Firstname}} {{$management->StaffDetails->Lastname}} @endif</span>
    			 | <span class="details-span">{{Carbon\Carbon::parse($management->updated_at)->format('M jS Y')}}</span>
                 
    			 <span class="details-span" style="float:right">
    			 	@if($management->staffID == Auth::user()->SummitStaff->id)
                      <a href="#" class="edit-proffesional" data-id="{{ $management->id }}" data-comment="{{ $management->comment }}"> Edit </a>
                 
    			 	@else
                      <a href="#" class="view-proffesional" data-view="{{ $management->comment }}"> View </a>
                       
                    @endif
    			 </span>
    		</p>
    	</div>
    </div>
   @endif
   @endif
   @endforeach
	</div>

<div class="mb-5">
	<div class="tab-form">
		<div class="tab-form-header text-white" style="background-color: #1C2E5C ">
			<h4>Professional Bodies</h4>
			 <!--<span title="Add Comment" class="text-white edit-proffesional" style="cursor: pointer;"><i class="fas fa-comment"></i></span>-->
		</div>

		<div class="form-group" style="padding: 2%;">
			@if(!empty($qualifications))
			@foreach($profbodies as $value => $qual)
			  @foreach($profBodiesTitles as $key => $prof)
			  @if($qual->ProfessionalBody == $prof->id)
				<div class="row" >
				<div class="col-md-10" style="color: #1C2E5C">
				<h6 class="text" style="color: #1C2E5C">{{$prof->name}}</h6>
				<h6><b>Title:</b> {{$prof->name}}</h6>
				<p>Membership No/Certificate No:{{$profbodies[$value]->Membership_number}}</p>
				</div>
				</div>
				<hr/>
		        @else
				@endif
				@endforeach
			@endforeach
		   @endif		   
		   <br/>
		</div>
	</div>

	</div>

	

	 