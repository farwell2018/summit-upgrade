	<div class="mb-5">
	<div class="tab-form">
		<div class="tab-form-header text-white" style="background-color: #1C2E5C">
			<h4>Work Experience</h4>
			 <span title="Add comment" class="text-white edit-workexperience" style="cursor: pointer;"><i class="fas fa-comment"></i></span>
		</div>
		<div class="form-group" style="padding: 2%" style="color: #1C2E5C">
			@if(count($work) > 0)
			@foreach($work as $work)
				<div class="row education" style="color: #1C2E5C">
				<div class="col-md-10" style="color: #1C2E5C">
				<h5 class="text" style="color: #1C2E5C">{{$work->Title}}</h5>
				<p>Company: {{$work->Company}}</p><p>Currency Name: @if($work->CurrencyID == 1)KES @elseif($work->CurrencyID == 2)EURO @elseif($work->CurrencyID == 3)UK POUND @elseif($work->CurrencyID == 4)USD @elseif($work->CurrencyID == 5)RAND @elseif($work->CurrencyID == 6)UGX 
				@elseif($work->CurrencyID == 7)TZS @elseif($work->CurrencyID == 8)Zambian Kwacha (ZMK) @endif </p><p>Monthly Salary: {{$work->MonthlyGrossSalary}}</p>
				<p><span>From - @php echo date("m-Y", strtotime($work->StartDate)); @endphp To - @if($work->CurrentDate == 'Yes')  now  @else @php echo date("m-Y", strtotime($work->EndDate)) @endphp @endif </span></p>
				<h6 class="text" style="color: #1C2E5C"><b>Responsibilites:</b></h6>
			   @php $workrespz = \App\WorkExperienceResponsibility::where('WorkExpID', $work->WorkExpID)->get(); @endphp
				@foreach($workrespz as $res)
				@if($res->WorkExpID == $work->WorkExpID)
				 @if(empty($res->Responsibility))
			     @else
				    
				{!! $res->Responsibility !!}
				
				@endif
				@endif
				@endforeach
				
				<h6 class="text" style="color: #1C2E5C"><b>Achievements:</b></h6>
				
				@foreach($workrespz as $res)
				@if($res->WorkExpID == $work->WorkExpID)
				 @if(empty($res->Achievement))
			     @else
				{!! $res->Achievement !!}
				
				@endif
				@endif
				@endforeach
				
					</div>
				</div>
				<hr/>
			@endforeach	
		   @endif	
		   
		   <br/>
		 </div>
	</div>
	@php $x = 0; @endphp
	@foreach($managements as $management)
	@if(!empty($management->section))
   @if($management->section === "workexperience")
    @php $x++; @endphp
    <div class="card comment-card">
    	<div class="card-body comment-body">
    		<p>
    			 <span class="details-span-first">{{'Comment'}} {{$x}}</span>
    			 | <span class="details-span">@if($management->StaffDetails) {{$management->StaffDetails->Firstname}} {{$management->StaffDetails->Lastname}} @endif</span>
    			 | <span class="details-span">{{Carbon\Carbon::parse($management->updated_at)->format('M jS Y')}}</span>
                 
    			 <span class="details-span" style="float:right">
    			 	@if($management->staffID == Auth::user()->SummitStaff->id)
                      <a href="#" class="edit-workexperience" data-id="{{ $management->id }}" data-comment="{{ $management->comment }}"> Edit </a>
                 
    			 	@else
                      <a href="#" class="view-workexperience" data-view="{{ $management->comment }}"> View </a>
                       
                    @endif
    			 </span>
    		</p>
    	</div>
    </div>
   @endif
   @endif
   @endforeach
</div>
    <div class="mb-5">
	<div class="tab-form ">
		<!--<div class="tab-form-header text-white" style="background-color: #1C2E5C">-->
		<!--	<h4 >Other Interests</h4>-->
  <!--          <span title="Add Comment" class="text-white edit-interests" style="cursor: pointer;"><i class="fas fa-comment"></i></span>-->
		<!--</div>-->

		 <!--<div class="form-group row"  style="padding:1% 2%;color: #1C2E5C"">-->
		 <!--	<ul>-->
	  <!--  	@if(count($interests) > 0 && $interests[0]->Interests != null)-->
		 <!--   <li>{{$interests[0]->Interests}}</li>-->
	  <!--		@endif-->
	  <!--  	@if(count($interests) > 0 && $interests[0]->Interest2 != null)-->
		 <!--   <li>{{$interests[0]->Interest2}}</li>-->
	  <!--		@endif-->
	  <!--  	@if(count($interests) > 0 && $interests[0]->Interest3 != null)-->
		 <!--   <li>{{$interests[0]->Interest3}}</li>-->
	  <!--		@endif-->
	  <!--  	@if(count($interests) > 0 && $interests[0]->Interest4 != null)-->
		 <!--   <li>{{$interests[0]->Interest4}}</li>-->
	  <!--		@endif-->
	  <!--  	@if(count($interests) > 0 && $interests[0]->Interest5 != null)-->
		 <!--   <li>{{$interests[0]->Interest5}}</li>-->
	  <!--		@endif-->
	  <!--		</ul>-->
	  <!--	</div>-->
	</div>
	
	@php $x = 0; @endphp
	@foreach($managements as $management)
	@if(!empty($management->section))
   @if($management->section === "interests")
    @php $x++; @endphp
    <div class="card comment-card">
    	<div class="card-body comment-body">
    		<p>
    			 <span class="details-span-first">{{'Comment'}} {{$x}}</span>
    			 | <span class="details-span">@if($management->StaffDetails){{$management->StaffDetails->Firstname}} {{$management->StaffDetails->Lastname}}@endif</span>
    			 | <span class="details-span">{{Carbon\Carbon::parse($management->updated_at)->format('M jS Y')}}</span>
                 
    			 <span class="details-span" style="float:right">
    			 	@if($management->staffID == Auth::user()->SummitStaff->id)
                      <a href="#" class="edit-interests" data-id="{{ $management->id }}" data-comment="{{ $management->comment }}"> Edit </a>
                 
    			 	@else
                      <a href="#" class="view-interests" data-view="{{ $management->comment }}"> View </a>
                       
                    @endif
    			 </span>
    		</p>
    	</div>
    </div>
   @endif
   @endif
   @endforeach
</div>
