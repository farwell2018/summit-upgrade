
 <div class="topnav" width="100%" style="background-color: #1C2E5C">
     @php $staff = \App\SummitStaff::where('StaffID',$candidates->StaffID)->first(); @endphp
    @if ($staff)
  @if(!empty($candidates->StaffID))<a  class="active"href="#" style="background-color: #1C2E5C;font-size:15px"><b>Consultant :</b>&nbsp&nbsp&nbsp&nbsp&nbsp<span class="text">{{$staff->Firstname}} {{$staff->Lastname}}</span></a>@endif
  
  @endif
  <a  class="active"href="#" style="background-color: #1C2E5C;font-size:15px"><b>Candidate Mail :</b>&nbsp&nbsp&nbsp&nbsp&nbsp<span class="text">{{$details->EmailAddress}}</span></a>
  <a  class="active"href="#" style="background-color: #1C2E5C;font-size:15px"><b>Interview Date :</b>&nbsp&nbsp&nbsp&nbsp&nbsp<span class="text"> @if(empty($candidates->InterviewDate)) @else{{date("d-m-Y", strtotime($candidates->InterviewDate))}}@endif</span></a>
  <div class="topnav-right">
    <a class="active" href="{{route('interviewmanagement.edit', $candidates->InterviewID)}}" style="background-color: #FFCB00;color: #000;border-radius: 3px">Edit Notes&nbsp<span class="text edit-currentdetails" style="cursor: pointer;"><i class="fas fa-edit"></i></span></a>   
  </div>
</div>

<!-- <span class="text-white edit-newinterviewnotes" style="float:right;"><i class="fas fa-edit"></i></span> -->
<br>
  <div class="mb-5">
  <div class="tab-form">
    <div class="tab-form-header text-white" style="background-color: #1C2E5C">
      <h4>Interview Summary</h4>
        <!--  <span class="text-white edit-currentdetails" style="cursor: pointer;"><i class="fas fa-edit"></i></span> -->
    </div>
   <div class="form-group" style="padding: 2%">

        <div class="row">
        <div class="col-md-10">
        <h4 class="text-primary"></h4>
        <div class="interviewsummary" style="color:#1C2E5C">
          @if(!empty($candidates->InterviewSummary))
          <p > {!!$candidates->InterviewSummary!!}</p>
          @elseif(empty($candidates->InterviewSummary))
          <p>No Interview Summary Details</p>
          @endif
        </div>    
        </div>
        </div>
        <hr/>
    </div>

  </div>
  @php $x = 0; @endphp
  @foreach($managements as $management)
  @if(!empty($management->section))
   @if($management->section === "interviewsummary")
    @php $x++; @endphp
    <!-- <div class="card comment-card">
      <div class="card-body comment-body">
        <p>
           <span class="details-span-first">{{'Comment'}} {{$x}}</span>
           | <span class="details-span">{{$management->StaffDetails->Firstname}} {{$management->StaffDetails->Lastname}}</span>
           | <span class="details-span">{{Carbon\Carbon::parse($management->updated_at)->format('M jS Y')}}</span>

           <span class="details-span" style="float:right">
            @if($management->staffID == Auth::user()->SummitStaff->id)
                      <a href="#" class="edit-newinterviewsummary" data-id="{{ $management->id }}" data-comment="{{ $management->comment }}"> Edit </a>

            @else
                      <a href="#" class="view-newinterviewsummary" data-view="{{ $management->comment }}"> View </a>

                    @endif
           </span>
        </p>
      </div>
    </div> -->
   @endif
   @endif
   @endforeach
  </div>


<div class="row">
  <div class="col-md-6">
    <div class="mb-5">
      <div class="tab-form ">
      <div class="tab-form-header text-white" style="background-color: #1C2E5C">
        <h4>Current Employment Details</h4>
            <!-- <span class="text-white edit-currentdetails" style="cursor: pointer;"><i class="fas fa-edit"></i></span> -->

      </div>


<div class="form-group" style="padding: 2%">

        <div class="row">
        <div class="col-md-10">
        <h4 class="text-primary"></h4>
        <div class="row">
          <div class="column"style="float: left;width: 50%;padding: 10px;color: #1C2E5C">
              <p>Industries:</p>
              <p>Years of Experience:</p>
              <p>Current Salary:</p>
              <p>Benefits Received:</p>              
          </div>
          <div class="column" style="float: left;width: 50%;padding: 10px;color: #1C2E5C">
            @if(!empty($candidates->Industry))
            <p>{{$candidates->Industry}}</p>
            @elseif(empty($candidates->Industry))
            <p>No Industry Details</p>
            @endif
            @if(!empty($candidates->ExpYrs))
            <p>{{$candidates->ExpYrs}}</p>
            @elseif(empty($candidates->ExpYrs))
            <p>No Years of Experience Details</p>
            @endif
            @if(!empty($candidates->CurrentSalary))
            <p>{{$candidates->CurrentSalary}}</p>
            @elseif(empty($candidates->CurrentSalary))
            <p>No Current Salary Details</p>
            @endif
            @if(!empty($candidates->BenefitsReceived))
            <p>{{$candidates->BenefitsReceived}}</p>
            @elseif(empty($candidates->BenefitsReceived))
            <p>No Benefits Received Details</p>
            @endif
          </div>
        </div>
        </div>
        </div>
        <hr/>
    </div>
    </div>
      @php $x = 0; @endphp
  @foreach($managements as $management)
  @if(!empty($management->section))
   @if($management->section === "currentdetails")
    @php $x++; @endphp
   @endif
   @endif
   @endforeach
    </div>

  </div>
    <div class="col-md-6">
      <div class="mb-5">
    <div class="tab-form" style="border-color: #1C2E5C">
      <div class="tab-form-header text-white" style="background-color: #1C2E5C">
        <h4>Status to New Employment</h4>
              <!-- <span class="text-white edit-currentdetails" style="cursor: pointer;"><i class="fas fa-edit"></i></span> -->
      </div>

<div class="form-group" style="padding: 2%">

        <div class="row">
        <div class="col-md-10">
        <h4 class="text-primary"></h4>
       <div class="row">
            <div class="column" style="float: left;width: 50%;padding: 10px;color: #1C2E5C">              
                <p>Notice Period:</p>
                <p>Salary Expectation:</p>
                <p>Current Salary:</p>
                <p>Interview Date:</p>
                <p>Start Date:</p>
             </div>
         <div class="column"style="float: left;width: 50%;padding: 10px;color: #1C2E5C">
              @if(!empty($candidates->NoticePeriod))           
              <p>{{$candidates->NoticePeriod}}</p>
              @elseif(empty($candidates->NoticePeriod))
              <p>No Notice Period Details</p>
              @endif
              @if(!empty($candidates->SalaryExpectation))
              <p>{{$candidates->SalaryExpectation}}</p>
              @elseif(empty($candidates->SalaryExpectation))
              <p>No Salary Expectation Details</p>
              @endif
              @if(!empty($candidates->CurrentSalary))
              <p>{{$candidates->CurrentSalary}}</p>
              @elseif(empty($candidates->CurrentSalary))
              <p>No Current Salary Details</p>
              @endif
              @if(!empty($candidates->InterviewDate))
              <p>{{$candidates->InterviewDate}}</p>
              @elseif(empty($candidates->InterviewDate))
              <p>No Interview Date Details</p>
              @endif
              @if(!empty($candidates->StartDate))
              <p>{{Carbon\carbon::parse($candidates->StartDate)->format('M jS Y')}}</p>
              @elseif(empty($candidates->StartDate))
              <p>No Start Date Details</p>
              @endif
          </div>
        </div>

        </div>
        </div>

    </div>
    </div>
    @php $x = 0; @endphp
  @foreach($managements as $management)
  @if(!empty($management->section))
   @if($management->section === "newemploymentdetails")
    @php $x++; @endphp
  <!--   <div class="card comment-card">
      <div class="card-body comment-body">
        <p>
           <span class="details-span-first">{{'Comment'}} {{$x}}</span>
           | <span class="details-span">{{$management->StaffDetails->Firstname}} {{$management->StaffDetails->Lastname}}</span>
           | <span class="details-span">{{Carbon\Carbon::parse($management->updated_at)->format('M jS Y')}}</span>

           <span class="details-span" style="float:right">
            @if($management->staffID == Auth::user()->SummitStaff->id)
                      <a href="#" class="edit-newemploymentdetails" data-id="{{ $management->id }}" data-comment="{{ $management->comment }}"> Edit </a>

            @else
                      <a href="#" class="view-newemploymentdetails" data-view="{{ $management->comment }}"> View </a>

                    @endif
           </span>
        </p>
      </div>
    </div> -->
   @endif
   @endif
   @endforeach
  </div>
    </div>
  </div>


  <div class="row">
    <div class="col-md-6">
      <div class="mb-5">
    <div class="tab-form ">
      <div class="tab-form-header text-white" style="background-color: #1C2E5C">
        <h4>Candidate Test Results</h4>
           <!--  <span class="text-white edit-currentdetails" style="cursor: pointer;"><i class="fas fa-edit"></i></span> -->
      </div>
     <div class="form-group" style="padding: 2%", style="overflow:auto;height:100vh;">

        <div class="row">
        <div class="col-md-10">
        <h4 class="text-primary"></h4>
          <div class="row">
            <div class="column" style="float: left;width: 50%;padding: 10px;color: #1C2E5C">
              <p>Ravens IQ:</p>
              <p>Word Letter:</p>
              <p>Excel Test:</p>
              <p>Situation Test:</p>
              <p>Time Management:</p>              
              <p>Other Tests:</p>
              <p>Pre Qual Test Management:</p>
            </div>
            <div class="column" style="float: left;width: 50%;padding: 10px;color: #1C2E5C">
                
               @if(!empty($candidates->TestResult_1))
              <p>{{$candidates->TestResult_1}}</p>
              @elseif(empty($candidates->TestResult_1))
              <p>No Word Letter Test Details</p>
              @endif
              @if(!empty($candidates->TestResult_2))
              <p>{{$candidates->TestResult_2}}</p>
              @elseif(empty($candidates->TestResult_2))
              <p>No Word Letter Test Details</p>
              @endif
              @if(!empty($candidates->TestResult_3))
              <p>{{$candidates->TestResult_3}}</p>
              @elseif(empty($candidates->TestResult_3))
              <p>No Excel Test Details</p>
              @endif
              @if(!empty($candidates->TestResult_4))
              <p>{{$candidates->TestResult_4}}</p>
              @elseif(empty($candidates->TestResult_4))
              <p>No Situation Test Details</p>
              @endif
              @if(!empty($candidates->TestResult_5))
              <p>{{$candidates->TestResult_5}}</p>
              @elseif(empty($candidates->TestResult_3))
              <p>No Time Management Test Details</p>
              @endif
              @if(!empty($candidates->lumina))         
              <p>{{$candidates->lumina}}</p>
              @elseif(empty($candidates->lumina))
              <p>No Lumina Test Details</p>
              @endif
              @if(!empty($candidates->PreQualTestResults)) 
              <p>{{$candidates->PreQualTestResults}}</p>
              @elseif(empty($candidates->PreQualTestResults))
              <p>No Pre-Qual Test Details</p>
              @endif             
            </div>
          </div>
        </div>
        </div>
        <hr/>
    </div>
    </div>
    @php $x = 0; @endphp
  @foreach($managements as $management)
  @if(!empty($management->section))
   @if($management->section === "hardskills")
    @php $x++; @endphp
    <!-- <div class="card comment-card">
      <div class="card-body comment-body">
        <p>
           <span class="details-span-first">{{'Comment'}} {{$x}}</span>
           | <span class="details-span">{{$management->StaffDetails->Firstname}} {{$management->StaffDetails->Lastname}}</span>
           | <span class="details-span">{{Carbon\Carbon::parse($management->updated_at)->format('M jS Y')}}</span>

           <span class="details-span" style="float:right">
            @if($management->staffID == Auth::user()->SummitStaff->id)
                      <a href="#" class="edit-hardskills" data-id="{{ $management->id }}" data-comment="{{ $management->comment }}"> Edit </a>

            @else
                      <a href="#" class="view-hardskills" data-view="{{ $management->comment }}"> View </a>

                    @endif
           </span>
        </p>
      </div>
    </div> -->
   @endif
   @endif
   @endforeach
  </div>
    </div>
    <div class="col-md-6">
      <div class="mb-5">
    <div class="tab-form ">
      <div class="tab-form-header text-white" style="background-color: #1C2E5C;">
        <h4>Internal Comments</h4>
            <!--  <span class="text-white edit-currentdetails" style="cursor: pointer;"><i class="fas fa-edit"></i></span> -->
      </div>
      <div class="form-group" style="padding: 2%">
        <div class="row">
           <div class="col-md-10" style="color: #1C2E5C">
              <h4 class="text-primary"></h4>
              @if(!empty($candidates->InternalComments))
              <p class="details-span-first" ><b></b>{!!$candidates->InternalComments!!}</p>
              @elseif(empty($candidates->InternalComments))
              <p class="details-span-first" ><b></b>No Internal Comments</p>
              @endif
          </div>
        </div>
        <hr/>
    </div>

    </div>
    @php $x = 0; @endphp
  @foreach($managements as $management)
  @if(!empty($management->section))
   @if($management->section === "newopencomments")
    @php $x++; @endphp
    <!-- <div class="card comment-card">
      <div class="card-body comment-body">
        <p>
           <span class="details-span-first">{{'Comment'}} {{$x}}</span>
           | <span class="details-span">{{$management->StaffDetails->Firstname}} {{$management->StaffDetails->Lastname}}</span>
           | <span class="details-span">{{Carbon\Carbon::parse($management->updated_at)->format('M jS Y')}}</span>

           <span class="details-span" style="float:right">
            @if($management->staffID == Auth::user()->SummitStaff->id)
                      <a href="#" class="edit-newopencomments" data-id="{{ $management->id }}" data-comment="{{ $management->comment }}"> Edit </a>

            @else
                      <a href="#" class="view-newopencomments" data-view="{{ $management->comment }}"> View </a>

                    @endif
           </span>
        </p>
      </div>
    </div> -->
   @endif
   @endif
   @endforeach
  </div>
    </div>
    
        <div class="col-md-6">
      <div class="mb-5">
    <div class="tab-form ">
      <div class="tab-form-header text-white" style="background-color: #1C2E5C;">
        <h4>Hard Skills</h4>
            <!--  <span class="text-white edit-currentdetails" style="cursor: pointer;"><i class="fas fa-edit"></i></span> -->
      </div>
      <div class="form-group" style="padding: 2%">
        <div class="row">
           <div class="col-md-10" style="color: #1C2E5C">
               @php $hardskills = \App\InterviewHardSkill::where('InterviewID',$candidates->InterviewID)->get();@endphp
             @if($hardskills)
              @foreach($hardskills as $hard)
               <span class="badge" style="background-color: #1C2E5C">{{$hard->HardSkill}}</span>
             @endforeach
             @endif
          </div>
        </div>
        <hr/>
    </div>

    </div>

  </div>
    </div>
    
    <div class="col-md-12">
      <div class="mb-5">
    <div class="tab-form ">
      <div class="tab-form-header text-white" style="background-color: #1C2E5C;">
        <h4>Competencies</h4>
            <!--  <span class="text-white edit-currentdetails" style="cursor: pointer;"><i class="fas fa-edit"></i></span> -->
      </div>
      <div class="form-group" style="padding: 2%">
        <div class="row">
           <div class="col-md-10" style="color: #1C2E5C">
           
               @php $competencies = \App\InterviewCompetency::where('InterviewID',$candidates->InterviewID)->get();@endphp
             @if($competencies)
              @foreach($competencies as $comp)
               @php $com = \App\competencies::where('ID',$comp->compitency)->first();@endphp
               @if($com)
               <div class="row">
              <div class="col-4"><p>{{$com->Name}}</p></div>
               <div class="col-4"><p>{{$comp->rating}}</p></div>
               <div class="col-4"><p>{{$comp->comment}}</p></div></div>
             @endif
             @endforeach
             @endif
          </div>
        </div>
        <hr/>
    </div>

    </div>
  </div>
    </div>
</div>
