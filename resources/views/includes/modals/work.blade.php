<div class="modal" id="workModal" tabindex="-1" role="dialog" aria-labelledby="workModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">

        	<h4 class="modal-title text-primary text-center">Add Your Work Experience</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form method="POST" action="{{route('add.profile.work')}}">
        	 @csrf
      <div class="modal-body">
			<div class="container">

	  		<div class="form-group row" >
		    <label for="name" class="col-md-4 text-center text-white col-form-label label label-primary">Position Title<span class="asterick" style="color:red">*</span></label>
		    <div class="col-md-8">
		        <select class="selectpicker" data-live-search="true" data-size="10" data-width="100%;" name="industryfunc" id="industryfunc" required>
		    		@foreach($functions as $key => $function)
		    		<option value="{{$functions[$key]->Name}}">{{$functions[$key]->Name}}</option>
		    		@endforeach
		    	</select>
		       <!--<input type="text" class="form-control" id="title" name="title" placeholder="Your Job Title" required>-->
		       <!--@error('title')-->
		       <!--     <span class="invalid-feedback" role="alert">-->
		       <!--         <strong>{{ $message }}</strong>-->
		       <!--     </span>-->
	        <!--   @enderror-->
		  	  </div>
		      </div>
		    
		    <div class="form-group row otherPosition" style="display:none;">
              <label for="name" class="col-md-4 text-center text-white col-form-label label label-primary">Other Position Title</label>
                 <div class="col-md-8">
                   <input type="text" name="otherposition" class="form-control" id="colFormLabel"  value="">
                </div>
           </div>
        

	  		<div class="form-group row">
		    <label for="name" class="col-md-4 text-center text-white col-form-label label label-primary">Company Name<span class="asterick" style="color:red">*</span></label>
		    <div class="col-md-8">
		      <input type="text" class="form-control" id="company" name="company" placeholder="Company Name"required>
		       @error('company')
		            <span class="invalid-feedback" role="alert">
		                <strong>{{ $message }}</strong>
		            </span>
	           @enderror
		  	</div>
		    </div>

	  		<div class="form-group row">
		    <label for="name" class="col-md-4 text-center text-white col-form-label label label-primary">Industry Sector<span class="asterick" style="color:red">*</span></label>
		    <div class="col-md-8">
		      <select class="selectpicker" data-live-search="true" data-size="10" data-width="100%;" name="sector" required>
			      <option value="null" selected>Select Industry Sector</option>
			      @foreach($industry as $key => $sector)
			      <option value="{{$industry[$key]->Name}}">{{$industry[$key]->Name}}</option>
			      @endforeach
			  </select>
		  	</div>
		    </div>
		    
		      <div class="form-group row otherSector" style="display:none;">
            <label for="name" class="col-md-4 text-center text-white col-form-label label label-primary">Other Industry Sector</label>
             <div class="col-md-8">
               <input type="text" name="othersector" class="form-control" id="colFormLabel"  value="">
            </div>
           </div>

	  	<!-- 	<div class="form-group row">
		    <label for="name" class="col-md-4 text-center text-white col-form-label label label-primary">Position Title</label>
		    <div class="col-md-8">
		      <select  class="selectpicker" data-live-search="true" data-size="10" data-width="100%;" name="industry">
			      <option value="null" selected>Select Position Title</option>
			      @foreach($functions as $industry)
			      <option value="{{$industry->ID}}">{{$industry->Name}}</option>
			      @endforeach
			  </select>
		  	</div>
		    </div> -->

	  		<div class="form-group row">
		    <label for="name" class="col-md-4 text-center text-white col-form-label label label-primary">Job Type<span class="asterick" style="color:red">*</span></label>
		    <div class="col-md-8">
			  <select class="selectpicker"  name="jobtype" id="jobtype" required>
				<option value=""></option>
				<option value="Full Time">Full Time</option>
				<option value="Part Time">Part Time</option>
				<option value="Internship">Internship</option>
				<option value="Interim">Interim</option>
				<option value="Consulting">Consulting</option>
				<option value="Contract">Contract</option>
				<option value="Others">Others</option>
			  </select>
		  	</div>
		    </div>
		    
		    <div id="rev3" class="form-group row"style="display:none;" >
	    	<label for="name" class="col-md-4 text-center text-white col-form-label label label-primary">Other Job Type</label>
	    	<div class="col-md-8">
	      	<input type="text" class="form-control" placeholder="Other Job Type" id="other-jobtype" name="other-jobtype">
	      	</div>
		    </div>

	  		<div class="form-group row">
		    <label for="colFormLabel" class="col-md-4 text-center text-white col-form-label label label-primary">Start Date<span class="asterick" style="color:red">*</span></label>
		    <div  class="col-md-4">
	        	<input type="date" class="form-control" id="wstart_date" name="start_date" placeholder="Y-M-D" data-date-format="yyyy-mm-dd" required>

		  	</div>
		    </div>

	  		<div class="form-group row">
		    <div class="col-md-4"></div>
		    <div class="col-md-4">
			<label class="check-inline">Current Job
			  <input type="checkbox" name="current" value="Yes">
			  <span class="checkmark"></span>
			</label>
		  	</div>
		    </div>

	  		<div class="form-group row" id="checkcurrent">
		    <label for="colFormLabel" class="col-md-4 text-center text-white col-form-label label label-primary">End Date</label>
		    <div class="col-md-4">
	        	<input  type="date" id="wend_date" name="end_date" placeholder="Y-M-D" class="form-control" data-date-format="yyyy-mm-dd"/>
		  	</div>
		    </div>

	  		<div class="form-group row">
		    <label for="name" class="col-md-4 text-center text-white col-form-label label label-primary">Monthly Gross<span class="asterick" style="color:red">*</span></label>
		    <div class="col-md-4" >
		      <select id="inputState" class="selectpicker" name="currency"required>
			      <option value="null" selected>Select Currency</option>
			      @foreach($currency as $currency)
			      <option value="{{$currency->	CurrencyID}}">{{$currency->CurrencyName}}</option>
			      @endforeach
			  </select>
		  	</div>
		    <div class="col-md-4">
	      	<input type="text" class="form-control" placeholder="Monthly Salary" name="salary" required>
		    </div>
		    </div>

		    <div class="form-group row">
		    <label for="name" class="col-md-4 text-center text-white col-form-label label label-primary">Benefits</label>
		    <div class="col-md-4">
		      <select class="selectpicker benefits"  name="benefits" id="benefits" multiple>
				<option value=""></option>
				<option value="Medical Insurance">Medical Insurance</option>
				<option value="Car Loan">Car Loan</option>
				<option value="Housing">Housing</option>
				<option value="Others">Others</option>
			  </select>
		  	</div>
		  	<div class="col-md-4 otherBenefits">
	      	<input type="text" class="form-control" placeholder="Other Benefits" name="other-benefits">
		    </div>
		    </div>

		    <div class="form-group row">
		    <label for="name" class="col-md-4 text-center text-white col-form-label label label-primary">Allowances</label>
		    <div class="col-md-4">
	      	<input type="text" class="form-control" placeholder="Allowances" name="allowances">
		    </div>
		    </div>


	  		<div class="form-group row">
		    <label for="name" class="col-md-4 text-center text-white col-form-label label label-primary">Direct Supervisor <span class="asterick" style="color:red">*</span></label>
		    <div class="col-md-4">
	      		<input type="text" class="form-control" placeholder="Name" name="line_name" required>
		  	</div>
		    <div class="col-md-4">
	      	<input type="text" class="form-control" placeholder="Designation" name="line_designation" required>
		    </div>
		    </div>
		    <div class="form-group row">
		    <div class="col-md-4"></div>
             <div class="col-md-4">
	      	<input type="email" class="form-control" placeholder="Email" name="linemanageremail" value="" required><!--links-->
		    </div>
		      <div class="col-md-4">
	      	<input type="tel" class="form-control" placeholder="0716123456" name="linemanagercontact" value="" required><!--links-->
		    </div>
		    </div>


	  		<div class="form-group row">
		    <div class="col-md-4"></div>
		    <div class="col-md-4">
			<label class="check-inline">Contact Referee
			  <input type="checkbox" name="contactref1" value="Yes">
			  <span class="checkmark"></span>
			</label>
		  	</div>
		    <div class="col-md-4">
		    </div>
		    </div>
		    <!--Responsibilites adding-->
            <small style="color:red;">*You are about to add responsilities & achievements for the role, kindly note, special characters i.e.</small>
            <small style="color:red;">*Bullets, asterick, numbers aren’t accepted.*</small>
	        <div class="form-group row">
	           <label for="colFormLabel" class="col-md-4 text-center text-white col-form-label label label-primary" style="max-height: 34px;">Responsibilities<span class="asterick" style="color:red">*</span></label> 
	            <div class="col-md-8" >
	                <textarea id="editors" name="responsibility" class="form-control editor5" style="height: 180px"></textarea>
	             </div>
	        </div>
	        <!--Achievements adding-->
	         <div class="form-group row">
	             <label for="colFormLabel" class="col-md-4 text-center text-white col-form-label label label-primary" style="max-height: 34px;">Achievements<span class="asterick" style="color:red">*</span></label>
	                <div class="col-md-8" >
                  <textarea id="editors1" name="achivements" class="form-control editor1" style="height: 180px"></textarea>
               </div>
	         </div>
		   <div class="form-group row">
		    <label for="reasonsleaving" class="col-md-4 text-center text-white col-form-label label label-primary">Reason for leaving<span class="asterick" style="color:red">*</span></label>
		    <div class="col-md-8">
		      <select class="selectpicker" id="reasons"  name="reasonsleaving" required>
			      <option value="" selected></option>
			      <option value="1">End of Contract</option>
			      <option value="2">Resignation</option>
			      <option value="3">Sabatical</option>
			      <option value="4">Greener Pastures</option>
			      <option value="6">Redundancy</option>
			      <option value="5">Career Growth</option>
			      <option value="others">Others</option>
			  </select>
		  	</div>
		    </div>
		    <div id="rev1" class="form-group row"style="display:none;" >
	    	<label for="name" class="col-md-4 text-center text-white col-form-label label label-primary">Other Reason</label>
	    	<div class="col-md-8">
	      	<input type="text" class="form-control" placeholder="Other Reason For Leaving" id="other-reason" name="other-reason">
	      	</div>
		    </div>
            <div class="modal-footer justify-content-center">
              <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-success">Save changes</button>
            </div>
  	  </form>
    </div>
  </div>
</div>
<script type="text/javascript">
document.getElementById('from').max = new Date(new Date().getTime() - new Date().getTimezoneOffset() * 60000).toISOString().split("T")[0];
document.getElementById('to').max = new Date(new Date().getTime() - new Date().getTimezoneOffset() * 60000).toISOString().split("T")[0];
</script>
