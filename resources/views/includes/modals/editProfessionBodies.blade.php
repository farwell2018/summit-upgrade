@foreach($profbodies as $value => $qual)
<div class="modal" id="professionBodiesModal{{$qual->ProfBodyID}}" tabindex="-1" role="dialog" aria-labelledby="professionBodiesModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        	<h2 class="modal-title text-primary text-center">Edit Your Professional Bodies</h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form method="POST" action="{{route('edit.profile.proffesionBodies')}}">
        	 @csrf

      <div class="modal-body">
        	<div class="container">
        	 <input type="hidden" name="bodyid" value="{{$qual->ProfBodyID}}">

        	<br/>
	  		<div class="form-group row">
		    <label for="name" class="col-md-6 text-center text-white col-form-label-lg label label-primary">Professional bodies</label>
	    	<div class="col-md-6">
    			<select class="selectpicker" data-live-search="true" data-size="10" name="bodies">
					@foreach($profBodiesTitles as $key => $prof)
					@if($profbodies[$value]->ProfessionalBody == $prof->id)
					<option value="{{$prof->id}}" selected>{{$prof->name}}</option>
					@else
					<option value="{{$prof->id}}">{{$prof->name}}</option>
					@endif
					@endforeach
	    		</select>
	    	</div>
			</div>

      <div class="form-group row ">
        <label for="name" class="col-md-6 text-center text-white col-form-label label label-primary">Membership Number/Certificate No</label>
        <div class="col-md-6">
        <input type="text" name="membership-number" class="form-control" id="colFormLabel"  value="{{$profbodies[$value]->Membership_number}}">
        </div>
      </div>


      <div class="form-group row">
      <label for="name" class="col-md-6 text-center text-white col-form-label label label-primary">Validity Dates</label>
      <div class="col-md-3">
          <input type="date" class="form-control" id="start_date" name="start_date" value='<?php echo date("Y-m-d", strtotime($qual->StartDate)); ?>' />
      </div>
      <div class="col-md-3">
          <input type="date" class="form-control" id="end_date" name="end_date" value='<?php echo date("Y-m-d", strtotime($qual->EndDate)); ?>' />
      </div>
    </div>

			</div>

      </div>
      <div class="modal-footer justify-content-center">
        <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-success">Save changes</button>
      </div>
  	  </form>
    </div>
  </div>
</div>

@endforeach
