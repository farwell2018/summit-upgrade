<div class="modal" id="educationModal" tabindex="-1" role="dialog" aria-labelledby="educationModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        	<h2 class="modal-title text-primary text-center">Add your education details</h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form method="POST" action="{{route('add.profile.feducation')}}">
        	 @csrf
        	 <input type="hidden" name="educationid">
      <div class="modal-body">
        	<div class="container">

	  		<div class="form-group row">
		    <label for="name" class="col-md-6 text-center text-white col-form-label-lg label label-primary">Education level<span class="asterick" style="color:red">*</span></label>
	    	<div class="col-md-6">
	    		<select class="selectpicker edLevel" name="level" autocomplete="off"  style="width: 100%" >
	    			<option value="">Choose a level</option>
	    			<option value="Primary_School">Primary school</option>
	    			<option value="Secondary_High_School">Secondary/High school</option>
	    			<option value="High_A_Level">Further education</option>
	    		</select>
	    	</div>
			</div>

	  		<div class="form-group row highLevel">
		    <label for="name" class="col-md-6 text-center text-white col-form-label-lg label label-primary">Certification level<span class="asterick" style="color:red">*</span></label>
	    	<div class="col-md-6">
	    		<select  class="selectpicker" data-width="100%;" name="furthered" id="furthered" >
					<option value=""></option>
					<option value="Certificate">Certificate</option>
					<option value="Degree">Bachelors</option>
					<option value="Masters">Masters</option>
					<option value="PHD">PHD</option>
					<option value="Diploma">Diploma</option>
					<option value="Higher Diploma">Higher diploma</option>
					<option value="PGDip">PGDip</option>
					<option value="PGCert">PGCert</option>

				</select>
	    	</div>
			</div>

	  		<div class="form-group row highLevel">
		    <label for="name" class="col-md-6 text-center text-white col-form-label-lg label label-primary">Subjects / Discipline<span class="asterick" style="color:red">*</span></label>
	    	<div class="col-md-6">
	    		<select  class="selectpicker" data-live-search="true" data-size="10" data-width="100%;" name="subject" id="subject" >
					<option value=""></option>
					@foreach($subjects as $key => $subject)
					<option value="{{$subjects[$key]->SubjectTitle}}">{{$subjects[$key]->SubjectTitle}}</option>
					@endforeach
				</select>
	    	</div>
			</div>
			
			<div class="form-group row otherSubject" style="display: none;">
           <label for="name" class="col-md-6 text-center text-white col-form-label label label-primary">Other Subject Title</label>
          <div class="col-md-6">
          <input type="text" name="othersubjecttitle" class="form-control" id="colFormLabel"  value="">
          </div>
        </div>

	  		<div class="form-group row highLevel">
		    <label for="name" class="col-md-6 text-center text-white col-form-label-lg label label-primary">Specialization</label>
	    	<div class="col-md-6">
	    		<select  class="selectpicker" data-live-search="true" data-size="10" data-width="100%;" name="specialization" id="specialization">
					<option value=""></option>
					@foreach($specializations as $specialization)
					<option value="{{$specialization->SpecializationTitle}}">{{$specialization->SpecializationTitle}}</option>
					@endforeach
				</select>
	    	</div>
			</div>
			
			
				<div class="form-group row otherSpecialization" style="display: none;">
           <label for="name" class="col-md-6 text-center text-white col-form-label label label-primary">Other Specialization Title</label>
          <div class="col-md-6">
          <input type="text" name="otherspecialization" class="form-control" id="colFormLabel"  value="">
          </div>
        </div>
		 

	  		<div class="form-group row">
		    <label for="name" class="col-md-6 text-center text-white col-form-label-lg label label-primary">Institution<span class="asterick" style="color:red">*</span></label>
	    	<div class="col-md-6">
				<input type="text" name="institution" class="form-control" id="colFormLabel" placeholder="Institution" >
	    	</div>
			</div>

	  		<div class="form-group row highLevel">
		    <label for="name" class="col-md-6 text-center text-white col-form-label-lg label label-primary">Class type</label>
	    	<div class="col-md-6">
	    		<select  class="selectpicker" data-width="100%;" name="type" id="type">
					<option value=""></option>
					<option value="First-class honours ">First-class honours </option>
					<option value="Second-class honours - Upper Division">Second-class honours - upper division</option>
					<option value="Second-class honours - Lower Division">Second-class honours - lower division</option>
					<option value="Third-class honours">Third-class honours</option>
					<option value="Distinction ">Distinction </option>
					<option value="Merit ">Merit </option>
					<option value="Pass ">Pass </option>
					<option value="Others">Others</option>
				</select>
	    	</div>
			</div>
			<div id="rev" class="form-group row" style="display:none;">
	    	<label for="name" class="col-md-6 text-center text-white col-form-label label label-primary">Other Class type</label>
	    	<div class="col-md-6">
	      	<input type="text" class="form-control" placeholder="Other Class Type" id="other-classtype" name="other-classtype">
	      	</div>
		    </div>
			
	  		<div class="form-group row">
		    <label for="name" class="col-md-6 text-center text-white col-form-label-lg label label-primary">Attendance dates<span class="asterick" style="color:red">*</span></label>
	    	<div class="col-md-3">
        		<input type="date" class="form-control" id="start_date" name="start_date" placeholder="Y-M-D" data-date-format="yyyy-mm-dd" required/>
	    	</div>
	    	<div class="col-md-3">
        		<input type="date" class="form-control" id="end_date" name="end_date" placeholder="Y-M-D" data-date-format="yyyy-mm-dd" required/>
	    	</div>
			</div>

			</div>
      </div>
      <div class="modal-footer justify-content-center">
        <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-success">Save changes</button>
      </div>
  	  </form>
    </div>
  </div>
</div>