<div class="modal" id="professionBodiesModal" tabindex="-1" role="dialog" aria-labelledby="professionBodiesModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        	<h2 class="modal-title text-primary text-center">Add Your Professional Bodies</h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form method="POST" action="{{route('add.profile.proffesionBodies')}}">
        	 @csrf
      <div class="modal-body">
        	<div class="container">
        	<br/>
	  		<div class="form-group row ">
		    <label for="name" class="col-md-6 text-center text-white col-form-label label label-primary">Professional Bodies<span class="asterick" style="color:red">*</span></label>
	    	<div class="col-md-6">
    			<select class="selectpicker" data-live-search="true" data-size="10" name="bodies">
					@foreach($profBodiesTitles as $key => $prof)
					<option value="{{$prof->id}}">{{$prof->name}}</option>
					@endforeach
	    		</select>
	    	</div>
			</div>
			 <div class="form-group row otherBodies" style="display: none;">
          <label for="name" class="col-md-6 text-center text-white col-form-label label label-primary">Other Body</label>
          <div class="col-md-6">
          <input type="text" name="otherbody" class="form-control" id="colFormLabel"  value="">
          </div>
        </div>
      <div class="form-group row ">
        <label for="name" class="col-md-6 text-center text-white col-form-label label label-primary">Membership Number/Certificate No</label>
        <div class="col-md-6">
        <input type="text" name="membership-number" class="form-control" id="colFormLabel"  value="">
        </div>
      </div>
    <div class="form-group row">
    <label for="name" class="col-md-6 text-center text-white col-form-label label label-primary">Validity Dates<span class="asterick" style="color:red">*</span></label>
    <div class="col-md-3">
        <input type="date" class="form-control" id="bstart_date" name="start_date" placeholder="Y-M-D"  data-date-format="yyyy-mm-dd" required/>
    </div>
    <div class="col-md-3">
        <input type="date"  class="form-control" id="bend_date" name="end_date" placeholder="Y-M-D" data-date-format="yyyy-mm-dd" required/>
    </div>
  </div>

			</div>
      </div>
      <div class="modal-footer justify-content-center">
        <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-success">Save changes</button>
      </div>
  	  </form>
    </div>
  </div>
</div>
