
<div class="modal" id="contactDetailsModal" tabindex="-1" role="dialog" aria-labelledby="contactDetailsModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        	<h2 class="modal-title text-primary text-center">Edit your personal details</h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="POST" action="{{route('edit.contact.details')}}">
        	 @csrf
      <div class="modal-body">
  <div class="row">
	<div class="col-md-12">
		<div class="container">
  		<div class="form-group row">
	    <label for="name" class="col-md-4 text-center text-white col-form-label-lg label label-primary">Name</label>
	    <div class="col-md-4">
	      <input type="text" class="form-control" id="name" name="fname" value="{{$details->Firstname}}">
	       @error('fname')
	            <span class="invalid-feedback" role="alert">
	                <strong>{{ $message }}</strong>
	            </span>
           @enderror
	  	</div>
	    <div class="col-md-4">
	      <input type="text" class="form-control" name="lname" value="{{$details->Lastname}}">
	       @error('lname')
	            <span class="invalid-feedback" role="alert">
	                <strong>{{ $message }}</strong>
	            </span>
           @enderror
	    </div>
	    </div>
  		<div class="form-group row">
	    <label for="colFormLabel" class="col-md-4 text-center text-white col-form-label-lg label label-primary">Mobile number</label>
	    <div class="col-md-4">
	    	@if(!empty($cvdets->PhoneNumber))
	      <input type="tel" class="form-control" name="phone" placeholder="" value="{{$cvdets->PhoneNumber}}" required>
	      	@else
	      <input type="tel" class="form-control" name="phone" placeholder="" required>
	      	@endif
	       @error('phone')
	            <span class="invalid-feedback" role="alert">
	                <strong>{{ $message }}</strong>
	            </span>
           @enderror
	  	</div>
	    <div class="col-md-4">
	    	@if(!empty($cvdets->PhoneNumberOther))
		    	<span class="text-primary add-mobile" style="cursor: pointer;display: none;"><i class="fas fa-plus"></i> Add another</span>
	    	@else
		    	<span class="text-primary add-mobile" style="cursor: pointer;"><i class="fas fa-plus"></i> Add another</span>
	      	@endif
	    </div>
	    </div>
  		<div id="mobile">
	    	@if(!empty($cvdets->PhoneNumberOther))
  			<div class="form-group row">
  				<label for="colFormLabel" class="col-md-4 text-center text-white col-form-label-lg label label-primary">Other number</label>
  				<div class="col-md-4"><input class="form-control" id="full_phone1" type="tel"  value="{{$cvdets->PhoneNumberOther}}" name="phone1"/></div>
  				<div class="col-md-4"><a href="#" class="remove_field"><i class="fa fa-times"></i></a></div>
  			</div>
	      	@endif
  		</div>
  		<div class="form-group row">
	    <label for="colFormLabel" class="col-md-4 text-center text-white col-form-label-lg label label-primary">Email account</label>
	    <div class="col-md-4">
	      <input type="email" class="form-control" name="email" value="{{$details->EmailAddress}}" disabled="disabled">
	       @error('email')
	            <span class="invalid-feedback" role="alert">
	                <strong>{{ $message }}</strong>
	            </span>
           @enderror
	  	</div>
	    <div class="col-md-4">
	    	@if(!empty($cvdets->EmailAddressOther))
	    	<span class="text-primary add-email" style="cursor: pointer;display: none"><i class="fas fa-plus"></i> Add another</span>
	    	@else
	    	<span class="text-primary add-email" style="cursor: pointer;"><i class="fas fa-plus"></i> Add another</span>
	      	@endif
	    </div>
	    </div>	    
  		<div id="email">  			
	    	@if(!empty($cvdets->EmailAddressOther))
  			<div class="form-group row">
  				<label for="colFormLabel" class="col-md-4 text-center text-white col-form-label-lg label label-primary">Other email</label>
  				<div class="col-md-4"><input type="email" class="form-control" name="email1" value="{{$cvdets->EmailAddressOther}}"></div>
  				<div class="col-md-4"><a href="#" class="remove_field"><i class="fa fa-times"></i></a></div>
  			</div>
	      	@endif
  		</div>	    
  		<div class="form-group row">
	    <div class="col-md-4"></div>
	    <div class="col-md-4">
			<label class="check-inline">Can we contact you?
	    	@if(!empty($cvdets->Contactable) && $cvdets->Contactable == 'Yes')
			  <input type="checkbox" name="contactable" value="Yes" checked="checked">
	    	@else
			  <input type="checkbox" name="contactable" value="Yes">
	      	@endif
			  <span class="checkmark"></span>
			</label>	    	
	    </div>
	    <div class="col-md-4"></div>
	    </div>
  		<div class="form-group row">
	    <label for="colFormLabel" class="col-md-4 text-center text-white col-form-label-lg label label-primary">Driving licence</label>
	    <div class="col-md-4">
	    	<select class="selectpicker" data-width="100%;" name="licence" required>
	    	  @if(!empty($cvdets->DL) && $cvdets->DL == 'Yes')		      
		      <option value="Yes" selected>Yes</option>
		      <option value="No">No</option>
	    	  @elseif(!empty($cvdets->DL) && $cvdets->DL == 'No')		      
		      <option value="Yes">Yes</option>
		      <option value="No" selected>No</option>
		      @else		     
		      <option value="Yes">Yes</option>
		      <option value="No">No</option>
		      @endif
		    </select>
	  	</div>
	    <div class="col-md-4"></div>
	    </div>
  		<div class="form-group row">
	    <label for="colFormLabel" class="col-md-4 text-center text-white col-form-label-lg label label-primary">Car owner</label>
	    <div class="col-md-4">
	    	<select class="selectpicker" data-width="100%;" name="owner" required>
	    	  @if(!empty($cvdets->CarOwner) && $cvdets->CarOwner == 'Yes')		     
		      <option value="Yes" selected>Yes</option>
		      <option value="No">No</option>
	    	  @elseif(!empty($cvdets->CarOwner) && $cvdets->CarOwner == 'No')		     
		      <option value="Yes">Yes</option>
		      <option value="No" selected>No</option>
		      @else		     
		      <option value="Yes">Yes</option>
		      <option value="No">No</option>
		      @endif
		    </select>
	  	</div>
	    <div class="col-md-4"></div>
	    </div>
  		<div class="form-group row">
	    <label for="colFormLabel" class="col-md-4 text-center text-white col-form-label-lg label label-primary">Nationality</label>
	    <div class="col-md-4">
	    	<select class="selectpicker" data-width="100%;" name="nationality" required>
	    	  @if(!empty($cvdets->Nationality) && $cvdets->Nationality == 'Kenyan')
		      <option value="null">Nationality</option>
		      <option value="Kenyan" selected>Kenyan</option>
		      <option value="Non-Kenyan">Non-Kenyan</option>
	    	  @elseif(!empty($cvdets->Nationality) && $cvdets->Nationality == 'Non-Kenyan')
		      <option value="null">Nationality</option>
		      <option value="Kenyan">Kenyan</option>
		      <option value="Non-Kenyan" selected>Non-Kenyan</option>
		      @else
		      <option value="null" selected>Nationality</option>
		      <option value="Kenyan">Kenyan</option>
		      <option value="Non-Kenyan">Non-Kenyan</option>
		      @endif
		    </select>
	  	</div>
	    <div class="col-md-4"></div>
	    </div>
  		<div class="form-group row">
	    <label for="colFormLabel" class="col-md-4 text-center text-white col-form-label-lg label label-primary">Identification<span class="asterick">*</span></label>
	    <div class="col-md-4">
	      @if(!empty($cvdets->ID_No) && $cvdets->ID_No != null)
	      <input type="text" class="form-control id" placeholder="ID Number" value="{{$cvdets->ID_No}}" name="id">
	      <input type="hidden" name="identification" value="ID">

	      <input style="margin-top:10px;" type="tel" class="form-control passport3" placeholder="Passport Number" value="{{$cvdets->Passport_No}}" name="passport" >
	      <input type="hidden" name="identification" value="ID">
	      @else
	      <input type="tel" class="form-control id" placeholder="ID Number" name="id">
	      <input type="hidden" name="identification" value="ID">

	      <input type="tel" class="form-control  passport3" placeholder="Passport Number" name="passport" style="margin-top:10px;">
	      <input type="hidden" name="identification" value="ID">
	      @endif
	      @if(!empty($cvdets->Passport_No) && $cvdets->Passport_No != null)
	      <input type="tel" class="form-control  passport" placeholder="Passport Number" value="{{$cvdets->Passport_No}}" name="passport">
	      <input type="hidden" name="identification" value="Passport">
	      @else
	      <input type="tel" class="form-control  passport" placeholder="Passport Number" name="passport">
	      <input type="hidden" name="identification" value="Passport">
	      @endif
	    </div>
	    <div class="col-md-4 passport">
	      <select class="selectpicker" data-width="100%;" data-live-search="true" data-size="10" name="passport_country">
		      <option value="">Passport Country</option>
		      @foreach($countries as $country)
	      		@if(!empty($cvdets->Passport_Country) && $cvdets->Passport_Country == $country->CountryID)
		      <option value="{{$country->CountryID}}" selected>{{$country->CountryName}}</option>
		      @else
		      <option value="{{$country->CountryID}}">{{$country->CountryName}}</option>
		      @endif
		      @endforeach
		  </select>
	  	</div>
         @if(!empty($cvdets->ID_No) && $cvdets->ID_No != null)
         <div class="col-md-4 passport3">
	      <select class="selectpicker" data-width="100%;" data-live-search="true" data-size="10" name="passport_country">
		      <option value="">Passport Country</option>
		      @foreach($countries as $country)
	      		@if(!empty($cvdets->Passport_Country) && $cvdets->Passport_Country == $country->CountryID)
		      <option value="{{$country->CountryID}}" selected>{{$country->CountryName}}</option>
		      @else
		      <option value="{{$country->CountryID}}">{{$country->CountryName}}</option>
		      @endif
		      @endforeach
		  </select>
	  	</div>
         @else
	  	<div class="col-md-4 passport2">
	      <select class="selectpicker" data-width="100%;" data-live-search="true" data-size="10" name="passport_country">
		      <option value="">Passport Country</option>
		      @foreach($countries as $country)
	      		@if(!empty($cvdets->Passport_Country) && $cvdets->Passport_Country == $country->CountryID)
		      <option value="{{$country->CountryID}}" selected>{{$country->CountryName}}</option>
		      @else
		      <option value="{{$country->CountryID}}">{{$country->CountryName}}</option>
		      @endif
		      @endforeach
		  </select>
	  	</div>
	  	 @endif
	    </div>
  		<div class="form-group row">
	    <label for="colFormLabel" class="col-md-4 text-center text-white col-form-label-lg label label-primary">Country Of Residence<span class="asterick">*</span></label>
	    <div class="col-md-4">
	    	<select class="selectpicker" data-width="100%;" data-size="10" data-live-search="true" name="country" required>
		      <option value="">Country</option>
		      @foreach($countries as $country)
	      		@if(!empty($cvdets->ID_Country) && $cvdets->ID_Country == $country->CountryID)
		      <option value="{{$country->CountryID}}" selected>{{$country->CountryName}}</option>
		      @else
		      <option value="{{$country->CountryID}}">{{$country->CountryName}}</option>
		      @endif
		      @endforeach
		    </select>
	  	</div>
	    <div class="col-md-4 loc">
	    	<select class="selectpicker" data-width="100%;" data-size="10" data-live-search="true" name="location" >
		      <option value="">City</option>
		      @foreach($locations as $location)
	      		@if(!empty($cvdets->location) && $cvdets->location == $location->LocationID)
		      <option value="{{$location->LocationID}}" selected>{{$location->LocationName}}</option>
		      @else
		      <option value="{{$location->LocationID}}">{{$location->LocationName}}</option>
		      @endif
		      @endforeach
		    </select>
	  	</div>
	  	<div class="col-md-4 loc-text">
	    	 <input type="text" class="form-control" placeholder="Location" value="{{$cvdets->location}}" name="location">
	  	</div>
	    </div>
  		<div class="form-group row">
	    <div class="col-md-4"></div>
	    <div class="col-md-4">
	      @if(!empty($cvdets->PhysicalAddress))
	      <input type="tel" class="form-control  @error('address') is-invalid @enderror" placeholder="Physical Address" name="address" value="{{$cvdets->PhysicalAddress}}">
	      @else
	      <input type="tel" class="form-control  @error('address') is-invalid @enderror" placeholder="Physical Address" name="address" required>
	      @endif
	       @error('address')
	            <span class="invalid-feedback" role="alert">
	                <strong>{{ $message }}</strong>
	            </span>
           @enderror
	  	</div>
	    <div class="col-md-4">	    	
	      @if(!empty($cvdets->PO_BOX))
	      <input type="tel" class="form-control  @error('pobox') is-invalid @enderror" placeholder="Postal Code" name="pobox" value="{{$cvdets->PO_BOX}}">
	      @else
	      <input type="tel" class="form-control  @error('pobox') is-invalid @enderror" placeholder="Postal Code" name="pobox">
	      @endif

	       @error('pobox')
	            <span class="invalid-feedback" role="alert">
	                <strong>{{ $message }}</strong>
	            </span>
           @enderror
	    </div>
	    </div>
  		<div class="form-group row">
	    <label for="colFormLabel" class="col-md-4 text-center text-white col-form-label-lg label label-primary">Birth details</label>
	    <div class="col-md-4">
	      @if(!empty($cvdets->DOB))
        	<input type="EODdate" class="form-control" id="date" name="date" placeholder="Y-M-D"   value="{{$cvdets->DOB}}" data-date-format="yyyy-mm-dd"/>
	      @else
        	<input type="date" class="form-control" id="DOBdate" name="date" placeholder="Y-M-D" data-date-format="yyyy-mm-dd"/>
	      @endif
	  	</div>
	    <div class="col-md-4">
	    	@php 
	    	$gender = array(
	    		array("name" => "Female"),
	    		array("name" => "Male"),
	    	);
	    	@endphp
	    	<select id="gender" class="selectpicker" data-width="100%;" name="gender" required>
	    		<option></option>
		      @foreach($gender as $value)
	    		@if(!empty($cvdets->Gender) && $cvdets->Gender == $value['name'])
		      <option value="{{$value['name']}}" selected>{{$value['name']}}</option>
		      @else
		      <option value="{{$value['name']}}">{{$value['name']}}</option>
		      @endif
		      @endforeach
		    </select>
	  	</div>
	    </div>

  		</div>
	</div>
  </div>
      </div>
      <div class="modal-footer justify-content-center">
        <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-success">Save changes</button>
      </div>
  	  </form>
    </div>
  </div>
</div>
