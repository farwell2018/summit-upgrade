
@foreach($work as $work)
@php
$types = array(
	array("name" => "Full Time"),
	array("name" => "Part Time"),
	array("name" => "Internship"),
	array("name" => "Contract"),
	array("name" => "Interim"),
	array("name" => "Consulting"),
	array("name" => "Others"),
);
$benefits = array(
	array("names" => "Medical Insurance"),
	array("names" => "Car Loans"),
	array("names" => "Housing"),
	array("names" => "Others"),
);

@endphp
<div class="modal" id="workModal{{$work->WorkExpID}}" tabindex="-1" role="dialog" aria-labelledby="workModalLabel" aria-hidden="true">
  <div class="modal-dialog  modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        	<h2 class="modal-title text-primary text-center">Edit Your Work Experience</h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="POST" action="{{route('edit.profile.work')}}">
        	 @csrf
        	 <input type="hidden" name="workid" value="{{$work->WorkExpID}}">
            <div class="modal-body">
			<div class="container">

	  		<!--<div class="form-group row" hidden>-->
		   <!-- <label for="name" class="col-md-4 text-center text-white col-form-label label label-primary">Position Title<span class="asterick" style="color:red">*</span></label>-->
		   <!-- <div class="col-md-8">-->
		   <!--   <input type="text" class="form-control" id="title" name="title" value="{{$work->Title}}">-->
		  	<!--</div>-->
		   <!-- </div>-->

		    <div class="form-group row">
		    <label for="name" class="col-md-4 text-center text-white col-form-label label label-primary">Position Title<span class="asterick" style="color:red">*</span></label>
		    <div class="col-md-8">
		      <select class="selectpicker" data-live-search="true" data-size="10" data-width="100%;" name="industryfunc">		      	
			      @foreach($functions as $function)
			      @if($work->Title === $function->Name)
			      <option value="{{$function->Name}}" selected>{{$function->Name}}</option>
			      @else
			      <option value="{{$function->Name}}">{{$function->Name}}</option>
			      @endif
			      @endforeach
			  </select>
		  	</div>
		    </div>

	  		<div class="form-group row">
		    <label for="name" class="col-md-4 text-center text-white col-form-label label label-primary">Company Name<span class="asterick" style="color:red">*</span></label>
		    <div class="col-md-8">
		      <input type="text" class="form-control" id="company" name="company" value="{{$work->Company}}">
		       @error('company')
		            <span class="invalid-feedback" role="alert">
		                <strong>{{ $message }}</strong>
		            </span>
	           @enderror
		  	</div>
		    </div>
	 		<div class="form-group row">
		    <label for="name" class="col-md-4 text-center text-white col-form-label label label-primary">Industry Sector</label>
		    <div class="col-md-8">
		      <select class="selectpicker" data-live-search="true" data-size="10" data-width="100%;" name="sector">
			      @foreach($industry as $sectors)
			      @if($work->Sector == $sectors->ID)
			      <option value="{{$sectors->ID}}" selected>{{$sectors->Name}}</option>
			      @else
			      <option value="{{$sectors->ID}}">{{$sectors->Name}}</option>
			      @endif
			      @endforeach
			  </select>
		  	</div>
		    </div>
	  		<!--Position Title-->
	  		<div class="form-group row">
		    <label for="name" class="col-md-4 text-center text-white col-form-label label label-primary">Job Type<span class="asterick" style="color:red">*</span></label>
		    <div class="col-md-8">
			  <select class="selectpicker"  name="jobtype" id="jobtype">
			      @foreach($types as $type)
			      @if($work->JobType == $type['name'])
					<option value="{{$type['name']}}" selected>{{$type['name']}}</option>
			      @else
					<option value="{{$type['name']}}">{{$type['name']}}</option>
			      @endif
			      @endforeach
			  </select>
		  	</div>
		    </div>
	  		<div class="form-group row">
		    <label for="colFormLabel" class="col-md-4 text-center text-white col-form-label label label-primary">Start<span class="asterick" style="color:red">*</span></label>
		    <div class="col-md-4">
	        	<input type="date" class="form-control" id="mydate" name="start_date" placeholder="Y-M-D" value="{{date('Y-m-d', strtotime($work->StartDate))}}"  data-date-format="yyyy-mm-dd"/>
		  	</div>
		    </div>
	  		<div class="form-group row">
		    <div class="col-md-4"></div>
		    <div class="col-md-4">
			 @if($work->CurrentDate == 'Yes')
			<label class="check-inline">Current Job
			  <input type="checkbox" name="current" value="Yes" checked>
			  <span class="checkmark"></span>
			</label>
			@else
			<label class="check-inline">Current Job
			  <input type="checkbox" name="current" value="Yes">
			  <span class="checkmark"></span>
			</label>
			   @endif
		  	</div>
		    </div>
	  		<div class="form-group row" id="checkcurrent">
		    <label for="colFormLabel" class="col-md-4 text-center text-white col-form-label label label-primary">End Date</label>
		    <div class="col-md-4">
			      @if($work->CurrentDate != 'Yes')
	        	<input  type="date" class="form-control" id="edate" name="end_date" placeholder="Y-M-D" value="{{date('Y-m-d', strtotime($work->EndDate))}}" data-date-format="yyyy-mm-dd"/>
	        	@else
	        	<input type="date" class="form-control" id="edate" name="end_date"  value="{{date('Y-m-d', strtotime($work->EndDate))}}" placeholder="Y-M-D" data-date-format="yyyy-mm-dd"/>
	        	@endif
		  	</div>
		    </div>
	  		<div class="form-group row">
		    <label for="name" class="col-md-4 text-center text-white col-form-label label label-primary">Monthly Gross</label>
		    <div class="col-md-4">
		      <select id="inputState" class="form-control" name="currency">
			      @foreach($currency as $curr)
			      @if($work->CurrencyID == $curr->CurrencyID)
			      <option value="{{$curr->CurrencyID}}" selected>{{$curr->CurrencyName}}</option>
			      @else
			      <option value="{{$curr->CurrencyID}}">{{$curr->CurrencyName}}</option>
			      @endif
			      @endforeach
			  </select>
		  	</div>
		    <div class="col-md-4">
	      	<input type="text" class="form-control" placeholder="Monthly Salary" name="salary" value="{{$work->MonthlyGrossSalary}}">
		    </div>
		    </div>
		    <div class="form-group row">
		    <label for="name" class="col-md-4 text-center text-white col-form-label label label-primary">Benefits</label>
		    <div class="col-md-4">
		      <select class="selectpicker benefits"  name="benefits[]" id="benefits" multiple>
                  @foreach($benefits as $benefit)
			      @if($work->Benefits == $benefit['names'])
					<option value="{{$benefit['names']}}" selected>{{$benefit['names']}}</option>
			      @else
					<option value="{{$benefit['names']}}">{{$benefit['names']}}</option>
			      @endif
			      @endforeach
			  </select>
		  	</div>
		  	@if($work->Benefits == 'Others')
		  	<div class="col-md-4 otherBenefit">
	      	<input type="text" class="form-control" placeholder="Other Benefits" name="other-benefits" value="{{$work->OtherBenefits}}">
		    </div>
		    @endif
		    <div class="col-md-4 otherBenefits">
	      	<input type="text" class="form-control" placeholder="Other Benefits" name="other-benefits" value="{{$work->OtherBenefits}}" >
		    </div>
		    </div>
		    <div class="form-group row">
		    <label for="name" class="col-md-4 text-center text-white col-form-label label label-primary">Allowances</label>
		    <div class="col-md-4">
	      	<input type="text" class="form-control" placeholder="Allowances" name="allowances" value="{{$work->Allowances}}">
		    </div>
		    </div>
	  		<div class="form-group row">
		    <label for="name" class="col-md-4 text-center text-white col-form-label label label-primary">Direct Supervisor<span class="asterick" style="color:red">*</span></label>
		    <div class="col-md-4">
	      		<input type="text" class="form-control" placeholder="Name" name="line_name"  value="{{$work->Line_Manager_Name}}" required>
		  	</div>
		    <div class="col-md-4">
	      	<input type="text" class="form-control" placeholder="Designation" name="line_designation" value="{{$work->Line_Manager_Designation}}" required>
		    </div>
		    </div>
		    <div class="form-group row">
		    <div class="col-md-4"></div>
             <div class="col-md-4">
	      	<input type="email" class="form-control" placeholder="Email" name="linemanageremail" value="{{$work->linemanageremail}}" required>
		    </div>
		      <div class="col-md-4">
	      	<input type="text" class="form-control" placeholder="Phone Number" name="linemanagercontact" value="{{$work->linemanagercontact}}" required>
		    </div>
		    </div>
	  		<div class="form-group row">
		    <div class="col-md-4"></div>
		    <div class="col-md-4">
			<label class="check-inline">Contact Manager
			  <input type="checkbox" name="contactref1" value="Yes" checked>
			  <span class="checkmark"></span>
			</label>
		  	</div>
		    <div class="col-md-4">
		    </div>
		    </div>	 
               <div class="form-group row">
                   <div class="col-md-12"><small style="color:red"><b>Please limit the Responsibilities to 5 </b> </small></div>
		    	<label for="colFormLabel" class="col-md-4 text-center text-white col-form-label label label-primary" style="max-height: 34px;">Responsibilities<span class="asterick" style="color:red">*</span> </label> 
		    	  <div class="col-md-8" >
						@php
				      $res = App\WorkExperienceResponsibility::where('WorkExpID', $work->WorkExpID)->first();
				    @endphp

					<textarea id="editors3" name="responsibility" class="form-control editor3" style="height: 180px">
            	@if(!empty($res->Responsibility))
              {!!$res->Responsibility!!}
							@endif
					 </textarea>
				 </div>
		    	  
		    </div>
		    	 <div class="form-group row">
		    	     <div class="col-md-12"><small style="color:red"><b>Please limit the Achievements to 5 </b></small></div>
	             <label for="colFormLabel" class="col-md-4 text-center text-white col-form-label label label-primary" style="max-height: 34px;">Achievements<span class="asterick" style="color:red">*</span></label>
	             <div class="col-md-8">

							  <textarea id="editors4" name="achievements" style="height: 180px"class="form-control editor4">
                 	@if(!empty($res->Achievement))
		              {!! $res->Achievement !!}
									@endif
								</textarea>

           </div>
	         </div>
          <div class="form-group row">
		    <label for="reasonsleaving" class="col-md-4 text-center text-white col-form-label label label-primary">Reason for leaving<span class="asterick" style="color:red">*</span></label>
		    <div class="col-md-8">
		      <select class="selectpicker" id="reasons"data-live-search="true" data-size="10" data-width="100%;" name="reasonsleaving">
			      <option value="">--Select Option--</option>
			      <option value="1">End of Contract</option>
			      <option value="2">Resignation</option>
			      <option value="3">Sabatical</option>
			      <option value="4">Greener Pastures</option>
			      <option value="5">Career Growth</option>
			      <option value="others" >Others</option>
			  </select>
		  	</div>
		    </div>
		     <div id="rev1" class="form-group row"style="display:none;" >
	    	<label for="name" class="col-md-4 text-center text-white col-form-label label label-primary">Other Reason</label>
	    	<div class="col-md-8">
	      	<input type="text" class="form-control" placeholder="Other Reason For Leaving" id="other-reason" name="other-reason">
	      	</div>
		    </div>

			</div>
      </div>
      <div class="modal-footer justify-content-center">
        <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-success">Save changes</button>
      </div>
  	  </form>
    </div>
  </div>
</div>

@endforeach
