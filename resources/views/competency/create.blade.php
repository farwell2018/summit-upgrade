@extends('layouts.admin')

@section('title','Create Competency')

@section('content')
 <div class="row " style="margin-left:8px">
        <a href="{{route('home')}}" style="color:#858796" >
            <span class="text">Home</span> </a> &nbsp; | &nbsp;
        <a href="{{route('competencies.index')}}" >
            <span class="text">Key Competencies List</span></a>
 </div>
 <br>
<div class="kt-portlet">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Add a Competency
            </h3>
        </div>
    </div>
</div>
<form class="kt-form" method="POST" action="{{ route('competencies.store') }}" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="competencyname">Competency Name</label>
            <input id="competencyname" type="text" class="form-control{{ $errors->has('competencyname') ? ' is-invalid' : '' }}" name="competencyname" value="{{ old('competencyname')  }}" required>

            @if ($errors->has('competencyname'))
            <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('competency') }}</strong>
            </span>
            @endif
        </div>   

        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <button type="submit" class="btn btn-primary">Submit</button>
                <button type="reset" class="btn btn-secondary">Cancel</button>
            </div>
        </div>
</form>
@endsection