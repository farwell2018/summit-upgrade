@extends('layouts.admin')

@section('title','Key Competencies Management')


@section('content')
<div class="row " style="margin-left:8px">
    <a href="{{route('home')}}" style="color:#858796" >
        <span class="text">Dashboard</span></a>&nbsp; | &nbsp;
        <span class="text">Competencies Management</span> </a>&nbsp;
    
</div>
<br>
<div class="row user-add-button">
    <a href="{{route('competencies.create')}}" class="btn btn-primary btn-icon-split" style="margin-right: 15px;">
        <span class="icon"><i class="fas fa-plus"></i></span>
        <span class="text">New Competency</span> </a>
</div>
<div class="card mb-5">
    <div class="card-header tab-form-header">
       Key Competencies
    </div>
    <div class="card-body">
        <table class="table table-bordered table-striped table-hover dt-responsive nowrap" id="dataTablenew" width="100%">
            <thead style="background-color: #FFCB00">
            <tr>
                <th style="color: #000">Competency Name</th>
                <th style="color: #000">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($competencies as $k => $competency)
                <tr>
                    <td>{{ $competency->Name }}</td>
                    <td>                    	
                    	<a title="Edit details" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="{{route('competencies.edit', $competency->ID)}}">
                                <i class="fas fa-edit" style="color: #3069AB"></i>
                        </a>

                            <form action="{{route('competencies.delete', $competency->ID)}}" method="POST" style="display:inline">
                            @csrf
                            @method('delete')
                            <button type="submit" class="btn btn-sm btn-clean btn-icon btn-icon-sm"><i class="fa fa-trash" aria-hidden="true" style="color:#FA0000"></i></button>
                        </form>

                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection