<html>
    <head>
        <style>
        @font-face {
        font-family: 'Tahoma';
        src: url('{{ asset('/public/fonts/Tahoma_Regular.ttf') }}') format("truetype");
        font-weight: normal;
        font-style: normal;
        font-variant: normal;
     }

    @font-face {
            font-family: 'Tahoma-Bold';
            src: url('{{ asset('/public/fonts/Tahoma_Bold.ttf') }}') format("truetype");
            font-weight: bold;
            font-style: bold;
            font-variant: bold;
    }


    body {
            font-family: 'Tahoma';
            
    }
            @page{
                margin-top: 100px; /* create space for header */
                margin-bottom: 70px; /* create space for footer */
            }
            header, footer{
                position: fixed;
                left: 0px;
                right: 0px;
            }
            header{
                height: 60px;
                top:-110px
            }
            body{
            	color:#1D2F5D;
            	
            }
            footer{
                height: 50px;
                bottom: -20px;
                margin-left:40px;
                color:#1C2E5C;
                font-size:13px;
              }
              html{
                margin-left:0;
                margin-right:0;

              }
              table { page-break-inside:auto;border:0; font-family:Tahoma;}
              tr    { page-break-inside:avoid; page-break-after:auto;border:0; font-family:Tahoma;}
              th {
                border:0;
              margin:0;
              padding:0;
              text-align: left;
              color:#1D2F5D;
              font-family:Tahoma;
              font-weight:bold;

            }
            td {border:0;
                font:inherit;
              font-size:100%;
              margin:0;
              padding:0;
              color:#1D2F5D;
              }
            ul{
             margin:0;
             padding:0;

            }
            li{
              margin:0;
              padding:0;
            }
            .resp ul{
              margin-left:20px
            }
        </style>
    </head>
    <body>
        <header>
          <div class="row">
            <div class="col-12">
                <div class="logo" >
                  <img src="{{asset('/public/images/icons/summit-logo.png')}}" style="width: 135px;padding:40px;padding-top:10px;">
                </div>
            </div>
          </div>
        </header>
        <footer>
            <p><span style="display:inline-block"> <img src="{{asset('/public/images/icons/Web.png')}}" style="width: 20px;margin-top:2px">  Web: www.summitrecruitment-search.com</span><span style="display:inline-block; margin-left:47px"> <img src="{{asset('/public/images/icons/Email_1.png')}}" style="width: 20px;margin-top:5px"> Email Address: info@summitrecruitment-search.com</span></p>
              <p><span style="display:inline-block"><img src="{{asset('/public/images/icons/Phone.png')}}" style="width: 20px;margin-top:2px"> Tel: +254 713 461 279 / +254 738 555 933 </span><span style="display:inline-block;  margin-left:40px"> <img src="{{asset('/public/images/icons/Location.png')}}" style="width: 20px;margin-top:2px">  Blixen Court, Offices 2,3,4,5 Karen Road, Karen, Nairobi</span></p>
        </footer>
        <main>
          <div class="row">
            <div class="col-12" style="background:#1C2E5C;height:11.5%;margin-top:20px">
              <div class="information" >
              <h2  style="color: #fff;padding:20px;padding-left:50px;font-size:15px;font-weight:bold;">{{$details->Firstname}} {{$details->Lastname}}
                @if(!empty($details->CandidatePhoto))
                 
                 
                  @if($cvdets->CV_ID == 130274)
                  <img  src="{{ asset('public/uploads/'.$details->CandidatePhoto.'')}}" style="width:80px; height: 80px; border-radius:250%; float:right;margin-right:50px;margin-top:-25px; border:1px solid #29ABE2;-ms-transform: rotate(-90deg);transform: rotate(-90deg)
                 ">
                    
                    @else
                 <img  src="{{ asset('public/uploads/'.$details->CandidatePhoto.'')}}" style="width:80px; height: 80px; border-radius:250%; float:right;margin-right:50px;margin-top:-25px; border:1px solid #29ABE2
                 ">
                 @endif
                 @endif

              </h2>

            </div>
          </div>
          </div>

          @if($option === 'yes')

         <div class="row" >
            <div class="col-12" style="margin-top:20px">
              <table border="1px" style="table-layout:fixed;width:100%;margin-left:40px;margin-right:58px;font-size:12px;border:0;" cellspacing="5">
           <tr>
               <th style="width:25%;"><span style="font-weight:bold;"><img src="{{asset('/public/images/icons/Phone.png')}}" style="width: 20px;margin-top:2px"></span> {{$cvdets->PhoneNumber}} </th>
               <th style="width:30%;"><span style="font-weight:bold"><img src="{{asset('/public/images/icons/Email_1.png')}}" style="width: 20px;margin-top:5px"></span> {{$details->EmailAddress}}</th>
               <th style="width:15%;"><span style="font-weight:bold"><img src="{{asset('/public/images/icons/Mailbox.png')}}" style="width: 20px;margin-top:4px"></span> {{$cvdets->PO_BOX}}</th>
               
           </tr>
           <tr >
               	@if($cvdets->SkypeContact)
               <th style="width:25%;"><span style="font-weight:bold"><img src="{{asset('/public/images/icons/Skype.png')}}" style="width: 20px;margin-top:4px"></span> {{$cvdets->SkypeContact}}</th>
              @endif
           	@if($cvdets->LinkedInContact)
               <th style="width:40%;"><span style="font-weight:bold"><img src="{{asset('/public/images/icons/LinkedIn.png')}}" style="width: 20px;margin-top:4px"></span> {{$cvdets->LinkedInContact}}</th>
             @endif
              
           </tr>
       </table>
            </div>

          </div>

          @endif

          <div class="row" >
            <div class="col-12" style="margin-top:20px">
              <div style="margin-left:40px;margin-right:58px;margin-bottom:0;">
              <p style="display: inline-block; background:#29ABE2;color:#fff;padding:20px;padding-top:10px; padding-bottom:10px;border-bottom-left-radius:80%;border-top-right-radius:80%;margin-bottom:0;width:25%;font-weight:bold;font-size:12px">Personal Information</p>
              <p style="width: 68%;line-height: 0em;border: 1px solid #29ABE2;display: inline-block;opacity: 1;margin-top:-20px;margin-bottom:0;">&nbsp;</p>
           </div>
              <table border="1px" style="table-layout:fixed;width:100%;margin-left:40px;margin-right:40px;font-size:12px;border:0;">
           <tr>
               <th style="width:25%;"><span style="font-weight:bold">Date of Birth:</span> {{$cvdets->DOB}}</th>
               <th style="width:25%;"><span style="font-weight:bold">Nationality:</span> {{$cvdets->Nationality}}</th>
               <th style="width:25%;"><span style="font-weight:bold">National ID:</span> @if($cvdets->Identification != null) {{$cvdets->ID_No}} @else {{$cvdets->Passport_No}} @endif</th>
               <th style="width:25%;"><span style="font-weight:bold">Address:</span> {{$cvdets->PO_BOX}}</th>
           </tr>
       </table>
            </div>

          </div>


          <div class="row summary" >
            <div class="col-12" style="margin-top:20px">
              <div style="margin-left:40px;margin-right:42px;margin-bottom:0;">
              <p style="display: inline-block; background:#29ABE2;color:#fff;padding:20px;padding-top:10px; padding-bottom:10px;border-bottom-left-radius:80%;border-top-right-radius:80%;margin-bottom:0;width:25%;font-weight:bold;font-size:12px">Personal Summary</p>
              <p style="width: 66%;line-height: 0em;border: 1px solid #29ABE2;display: inline-block;opacity: 1;margin-top:-20px;margin-bottom:0;">&nbsp;</p>
           </div>
           <div style="  margin-left:40px;margin-right:68px; font-size:12px !important">
              {!! $cvdets->PersonalSummary !!}
            </div>
              @php
              $checks = array(
              array("id" => "Business_Management ", "name" => "Business Management"),
              array("id" => "Computer", "name" => "Computer"),
              array("id" => "Construction", "name" => "Construction"),
              array("id" => "Customer_Service", "name" => "Customer Service"),
              array("id" => "Diplomacy", "name" => "Diplomacy"),
              array("id" => "Effective_Listening", "name" => "Effective Listening"),
              array("id" => "Financial_Management", "name" => "Financial Management"),
              array("id" => "Interpersonal", "name" => "Interpersonal"),
              array("id" => "Multi-tasking", "name" => "Multi-tasking"),
              array("id" => "Negotiating", "name" => "Negotiating"),
              array("id" => "Organisation", "name" => "Organisation"),
              array("id" => "People_Management", "name" => "People Management"),
              array("id" => "Planning", "name" => "Planning"),
              array("id" => "Presentation", "name" => "Presentation"),
              array("id" => "Problem_Solving", "name" => "Problem Solving"),
              array("id" => "Programming", "name" => "Programming"),
              array("id" => "Report_Writing", "name" => "Report Writing"),
              array("id" => "Research", "name" => "Research"),
              array("id" => "Resourcefulness", "name" => "Resourcefulness"),
              array("id" => "Sales_Ability", "name" => "Sales Ability"),
              array("id" => "Technical", "name" => "Technical"),
              array("id" => "Time_Management", "name" => "Time Management"),
              array("id" => "Training", "name" => "Training"),
              array("id" => "Verbal_Communication", "name" => "Verbal Communication"),
              array("id" => "Written_Communication", "name" => "Written Communication"),
              );
              @endphp

          <table border="1px" style="width:100%;font-size:12px;margin-left:40px;margin-right:62px;" >
           <tr>
               <th style="width:25%;font-size:15px;font-family: 'Tahoma-Bold';font-size:12px">Soft Skills</th>
               <th style="width:25%;font-size:15px;font-family: 'Tahoma-Bold';font-size:12px">Key Skills</th>
               <th style="width:25%;font-size:15px;font-family: 'Tahoma-Bold';font-size:12px">Hard Skills</th>
           </tr>

           <tr style="margin-top:10px">
            <td >
              <ul style="list-style:none">
              @foreach($attrs as $attr)
                @if($attr != null)
              <li>{{$attr}}</li>
              @endif
            @endforeach
             <ul>
            </td>

            <td>
              <ul style="list-style:none">
                @foreach($checks as $check)
                @if(in_array($check['id'],array_values($skills)))
                <li>{{$check['name']}}</li>
              @endif
              @endforeach
             <ul>

            </td>

            <td>
              <ul style="list-style:none">
                @foreach($hardskills as $hardskill)
                @if(in_array($hardskill->ID,array_values($hskills)))
                <li>{{$hardskill->Name}}</li>
              @endif
              @endforeach
             <ul>
            </td>

           </tr>
         </table>
        </div>
          </div>



          <div class="row" >
            <div class="col-12" style="margin-top:20px;">
              <div style="margin-left:40px;margin-right:40px;margin-bottom:0;">
              <p style="display: inline-block; background:#29ABE2;color:#fff;padding:20px;padding-top:10px; padding-bottom:10px;border-bottom-left-radius:80%;border-top-right-radius:80%;margin-bottom:0;width:25%;font-weight:bold;font-size:12px">Work Experience</p>
              <p style="width: 66%;line-height: 0em;border: 1px solid #29ABE2;display: inline-block;opacity: 1;margin-top:-20px;margin-bottom:0;">&nbsp;</p>
           </div>
              @foreach($work as $key => $work)
              @if($key == 0)
              <table border="1px" style="table-layout:fixed;width:100%;margin-left:40px;margin-right:58px;font-size:12px;margin-bottom:0">
               @else
               <table border="1px" style="table-layout:fixed;width:100%;margin-left:40px;margin-right:58px;font-size:12px;margin-bottom:0;margin-top:60px">
               @endif
           <tr>
               <th style="width:25%;"><span style="font-family:'Tahoma-Bold';font-size:12px"> {{$work->Company}}</span> </th>
               <th style="width:25%;">{{$work->Title}}</th>
               <th style="width:25%;"><span style="font-weight:bold">Start Date:</span>@php echo date("d-m-Y", strtotime($work->StartDate)); @endphp </th>
               <th style="width:25%;"><span style="font-weight:bold">End Date:</span> @if($work->CurrentDate == 'Yes')  Currently Work here @else @php echo date("d-m-Y", strtotime($work->EndDate)); @endphp  @endif</th>
           </tr>
       </table>
         <table border="1px" style="table-layout:fixed;width:100%;margin-left:40px;margin-right:40px;font-size:12px;margin-top:-20px">
           <tr>
             <td class="resp" width="">
               <h4 style="margin-bottom:0;font-family:'Tahoma-Bold';font-size:12px;margin-bottom:0">Responsibilities</h4>
                @php $workrespz = \App\WorkExperienceResponsibility::where('WorkExpID', $work->WorkExpID)->get(); @endphp
                
          @foreach($workrespz as $res)
          
          {!! $res->Responsibility !!}
          
          
          @endforeach
        
           
         </td>
       </tr>
</table>
       <table border="1px" style="table-layout:fixed;width:100%;margin-left:40px;margin-right:58px;font-size:12px;margin-top:-20px">
           <tr>
             <td class="resp" width="100%">
               <h4 style="margin-bottom:0;font-family:'Tahoma-Bold';font-size:12px">Achievements</h4>
          @foreach($workrespz as $res)
          
          {!! $res->Achievement !!}
          @endforeach
         
           
           
           <td>
       </tr>
   </table>
 @if($comments)
<table border="1px" style="table-layout:fixed;width:100%;margin-left:40px;margin-right:58px;font-size:12px;">
      <tr>
        <td>
        <ol style="font-size:12px; margin:10px;margin-left:20px;">
         @foreach($comments as $comment)
         @if($comment->section === 'workexperience')
        <li>{{$comment->comment}}</li>
         @endif
         @endforeach
       </ol>

      </td></tr>
       </table>   @endif
       
            @endforeach
 
            

            </div>

          </div>



              <div class="row" >
                <div class="col-12" style="margin-top:20px">
                  <div style="margin-left:40px;margin-right:40px;margin-bottom:0;">
                  <p style="display: inline-block; background:#29ABE2;color:#fff;padding:20px;padding-top:10px; padding-bottom:10px;border-bottom-left-radius:80%;border-top-right-radius:80%;margin-bottom:0;width:25%;font-weight:bold;font-size:12px">Education</p>
                  <p style="width: 65%;line-height: 0em;border: 1px solid #29ABE2;display: inline-block;opacity: 1;margin-top:-20px;margin-bottom:0;">&nbsp;</p>
               </div>
                  @foreach($education as $edu)
                  <table border="1px" style="table-layout:fixed;width:100%;margin-left:40px;margin-right:58px;font-size:12px;" cellspacing="10">
                     
               <tr>
                   <th style="width:25%;"><span style="font-weight:bold">{{$edu->Institution}}</span> </th>
                   <th style="width:25%;"> {{$edu->Subjects}}</th>
                   <th style="width:25%;"><span style="font-weight:bold">Graduation Date:</span> @php echo date("d-m-Y", strtotime($edu->QualEndGradDate)) @endphp</th>

               </tr>
           </table>
           @endforeach

                </div>
              </div>


            @if(count($qualifications) > 0)
              <div class="row" >
                <div class="col-12" style="margin-top:20px;">
                  <div style="margin-left:40px;margin-right:40px;margin-bottom:0;">
                  <p style="display: inline-block; background:#29ABE2;color:#fff;padding:20px;padding-top:10px; padding-bottom:10px;border-bottom-left-radius:80%;border-top-right-radius:80%;margin-bottom:0;width:30%;font-weight:bold;font-size:12px">Proffesional Qualifications</p>
                  <p style="width: 60%;line-height: 0em;border: 1px solid #29ABE2;display: inline-block;opacity: 1;margin-top:-20px;margin-bottom:0;">&nbsp;</p>
               </div>
                @foreach($qualifications as $value => $qual)
                  <table border="1px" style="table-layout:fixed;width:100%;margin-left:40px;margin-right:58px;font-size:12px;" cellspacing="10">
               <tr>
                   <th style="width:25%;"><span style="font-weight:bold">{{$qual->ProfQualTitle}}</span> </th>
                   <th style="width:25%;"><span style="font-weight:bold">Graduation Date:</span>  @php echo date("d-m-Y", strtotime($qual->EndDate)) @endphp</th>

               </tr>
           </table>
           @endforeach

                </div>

              </div>
              @endif
              @if(count($profbodies) > 0)
              <div class="row" >
                <div class="col-12" style="margin-top:20px;">
                  <div style="margin-left:40px;margin-right:40px;margin-bottom:0;">
                  <p style="display: inline-block; background:#29ABE2;color:#fff;padding:20px;padding-top:10px; padding-bottom:10px;border-bottom-left-radius:80%;border-top-right-radius:80%;margin-bottom:0;width:25%;font-weight:bold;font-size:12px">Professional Bodies</p>
                  <p style="width: 65%;line-height: 0em;border: 1px solid #29ABE2;display: inline-block;opacity: 1;margin-top:-20px;margin-bottom:0;">&nbsp;</p>
               </div>
                  @foreach($profbodies as $value => $bodies)
                  <table border="1px" style="table-layout:fixed;width:100%;margin-left:40px;margin-right:40px;font-size:12px;" cellspacing="10">
                    <tr>
                  @foreach($profs as $key => $prof)
                  @if($bodies->ProfessionalBody == $profs[$key]->profqualtitleID)
                   <th style="width:25%;"><span style="font-weight:bold">{{$profs[$key]->profqualtitle}}</span> </th>
                   <th style="width:25%;"><span style="font-weight:bold">Membership Number:</span> {{$profs[$key]->Membership_Number}}</th>
           @endif
           @endforeach
         </tr>
      </table>
           @endforeach

                </div>

              </div>
              @endif

              <div class="row" >
                <div class="col-12" style="margin-top:20px;">
                  <div style="margin-left:40px;margin-right:40px;margin-bottom:0;">
                  <p style="display: inline-block; background:#29ABE2;color:#fff;padding:20px;padding-top:10px; padding-bottom:10px;border-bottom-left-radius:80%;border-top-right-radius:80%;margin-bottom:0;width:25%;font-weight:bold;font-size:12px">Languages</p>
                  <p style="width: 65%;line-height: 0em;border: 1px solid #29ABE2;display: inline-block;opacity: 1;margin-top:-20px;margin-bottom:0;">&nbsp;</p>
               </div>
                  <table border="1px" style="table-layout:fixed;width:100%;margin-left:60px;margin-right:40px;font-size:12px;">
               <tr>
                 @if(!empty($lang->Language1))
                 <th style="width:25%;"><span style="font-weight:bold">{{$lang->Language1}}</span> |  {{$lang->Fluency1}}</th>

                 @endif
               @if(!empty($lang->Language2))
               <th style="width:25%;"><span style="font-weight:bold">{{$lang->Language2}}</span> |  {{$lang->Fluency2}}</th>

                 @endif
               @if(!empty($lang->Language3))
                <th style="width:25%;"><span style="font-weight:bold">{{$lang->Language3}}</span> |  {{$lang->Fluency3}}</th>
                 @endif
               @if(!empty($lang->Language4))
                 <th style="width:25%;"><span style="font-weight:bold">{{$lang->Language4}}</span> |  {{$lang->Fluency4}}</th>
                 @endif

               </tr>
           </table>



                </div>

              </div>
        </div>
        </main>
    </body>
</html>
