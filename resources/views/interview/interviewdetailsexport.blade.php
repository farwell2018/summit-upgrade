<html>
    <head>
 <style>
        @font-face {
        font-family: 'Tahoma';
        src: url('{{ asset('/public/fonts/Tahoma_Regular.ttf') }}') format("truetype");
        font-weight: normal;
        font-style: normal;
        font-variant: normal;
}

@font-face {
        font-family: 'Tahoma-bold';
        src: url('{{ asset('/public/fonts/Tahoma_Regular.ttf') }}') format("truetype");
        font-weight: bold;
        font-style: bold;
        font-variant: bold;
}
    body {
            font-family: 'Tahoma';
            
    }
     
            @page{
                margin-top: 100px; /* create space for header */
                margin-bottom: 70px; /* create space for footer */
            }
            header, footer{
                position: fixed;
                left: 0px;
                right: 0px;
            }
            header{
                height: 60px;
                top:-110px
            }
            body{
              color:#1D2F5D;
              
            }
           footer{
                height: 50px;
                bottom: -20px;
                margin-left:40px;
                color:#1C2E5C;
                font-size:13px;
              }
              html{
                margin-left:0;
                margin-right:0;

              }
              table { page-break-inside:auto;border:0;font-family:Tahoma; }
              tr    { page-break-inside:avoid; page-break-after:auto;border:0;font-family:Tahoma; }
              th {
                border:0;
                font:inherit;
              font-size:100%;
              margin:0;
              padding:0;
              text-align: left;
              color:#1D2F5D;
              font-family:Tahoma;
              

            }
            td {border:0;
                font:inherit;
              font-size:100%;
              margin:0;
              padding:0;
              color:#1D2F5D;
              font-family:Tahoma;
              
              }
            ul{
             margin:0;
             padding:0;

            }
            li{
              margin:0;
              padding:0;
            }
            .resp ul{
              margin-left:20px
            }
        </style>
    </head>
    <body>
        <header>
          <div class="row">
            <div class="col-12">
                <div class="logo">
                  <img src="{{asset('/public/images/icons/summit-logo.png')}}" style="width: 150px;padding:40px;padding-top:10px">
                </div>
            </div>
          </div>
        </header>
      <footer>
            <p><span style="display:inline-block"> <img src="{{asset('/public/images/icons/Web.png')}}" style="width: 20px;margin-top:2px">  Web: www.summitrecruitment-search.com</span><span style="display:inline-block; margin-left:47px"> <img src="{{asset('/public/images/icons/Email_1.png')}}" style="width: 20px;margin-top:5px"> Email Address: info@summitrecruitment-search.com</span></p>
              <p><span style="display:inline-block"><img src="{{asset('/public/images/icons/Phone.png')}}" style="width: 20px;margin-top:2px"> Tel: +254 713 461 279 / +254 738 555 933 </span><span style="display:inline-block;  margin-left:40px"> <img src="{{asset('/public/images/icons/Location.png')}}" style="width: 20px;margin-top:2px">  Blixen Court, Offices 2,3,4,5 Karen Road, Karen, Nairobi</span></p>
        </footer>
        <main>
          <div class="row">
            <div class="col-12" style="background:#1C2E5C;height:11.5%;margin-top:20px">
              <div class="information" >
                  @foreach($candidates as $candidate)
                  @php 
                  $title = \App\JobAd::where('ID', $candidate->JobAD_ID)->first();
                  @endphp
                  @endforeach
                  <h2 > 
                    @if(!empty($dets->CandidatePhoto))
                    @if($cvdets->CV_ID == 130274)
                    <img class="img-profile" src="{{ asset('public/uploads/'.$dets->CandidatePhoto.'')}}" style="width:80px; height: 80px; border-radius:250%; float:right;margin-right:43px; border:1px solid #29ABE2;margin-top:-5px;-ms-transform: rotate(-90deg);transform: rotate(-90deg)">
                    @else
                 <img class="img-profile" src="{{ asset('public/uploads/'.$dets->CandidatePhoto.'')}}" style="width:80px; height: 80px; border-radius:250%; float:right;margin-right:43px; border:1px solid #29ABE2;margin-top:-5px">
                 @endif
                 @endif
                 
                  <table style="margin-top: 10px; margin-left:40px; float:left " cellpadding = "5" >
                      <tr>
                      <th style="color: #fff;font-size:12px;font-weight:bold;font-family:'Tahoma';color:#fff;margin-right:20px">Full Name</th>
                      <th style="color: #fff;font-size:12px;font-weight:bold;font-family:'Tahoma';color:#fff">
                          Position Title
                      </th>
                      </tr>
                      
                      <tr>
                          <td>
                              <span style="border:1px solid #29ABE2;padding:10px; padding-top:5px; padding-bottom:5px; display:inline-block; font-size:12px;margin-top:5px;border-radius:50%;font-family: 'Tahoma';color:#fff;margin-right:20px">{{$dets->Firstname}} {{$dets->Lastname}}</span>
                          </td>
                          <td>
                              <span style="border:1px solid #29ABE2;padding:10px; padding-top:5px; padding-bottom:5px; display:inline-block; font-size:12px;margin-top:5px;border-radius:50%;font-family: 'Tahoma';color:#fff">{{$title->JobTitle}}</span>
                          </td>
                      </tr>
                      
                  </table>
              
              

              </h2>

            </div>
          </div>
          </div>

          <div class="row" >
            <div class="col-12" style="margin-top:20px">
              <div style="margin-left:40px;margin-right:40px;margin-bottom:0">
              <p style="display: inline-block; background:#29ABE2;color:#fff;padding:20px;padding-top:10px; padding-bottom:10px;border-bottom-left-radius:80%;border-top-right-radius:80%;margin-bottom:0;width:25%;font-weight:bold;font-size:12px">Interview Summary</p>
              <p style="width: 67%;line-height: 0em;border: 1px solid #29ABE2;display: inline-block;opacity: 1;margin-top:-20px;margin-bottom:0">&nbsp;</p>
           </div>
           <div style="margin-left:40px;margin-right:40px; border:1px solid #1C2E5C;border-radius:50%;padding:10px;margin-top:10px;font-size:12px !important">
               @foreach($candidates as $candidate)
            {!!$candidate->InterviewSummary!!}
            @endforeach
            </div>
            </div>

          </div>


          <div class="row summary" >
            <div class="col-12" style="margin-top:20px">

              <div style="margin-left:40px;margin-right:40px;margin-bottom:0">
              <p style="display: inline-block; background:#29ABE2;color:#fff;padding:20px;padding-top:10px; padding-bottom:10px;border-bottom-left-radius:80%;border-top-right-radius:80%;margin-bottom:0;width:25%;font-weight:bold;font-size:12px">Employment Details</p>
              <p style="width: 68%;line-height: 0em;border: 1px solid #29ABE2;display: inline-block;opacity: 1;margin-top:-20px;margin-bottom:0">&nbsp;</p>
           </div>
          <table border="1px" style="width:100%;font-size:12px;margin-left:30px;margin-right:40px;font-family: 'Tahoma';" cellspacing="15">
           <tr>
               <th style="font-weight:bold;">Notice Period</th>
               <th style="font-weight:bold">Salary Expectation</th>
               <th style="font-weight:bold">Current Salary</th>
               <th style="font-weight:bold">Years of Experience</th>
               <th style="font-weight:bold">Management Experience</th>
           </tr>

           <tr style="margin-top:10px">

             @foreach($candidates as $candidate)

              <td style="border:1px solid #1C2E5C;border-radius:50%;padding:10px;font-family: 'Tahoma';">{{$candidate->NoticePeriod}}</td>
              <td style="border:1px solid #1C2E5C;border-radius:50%;padding:10px;font-family: 'Tahoma';">{{$candidate->SalaryExpectation}}</td>
              <td style="border:1px solid #1C2E5C;border-radius:50%;padding:10px;font-family: 'Tahoma';">{{$candidate->CurrentSalary}}</td>
              <td style="border:1px solid #1C2E5C;border-radius:50%;padding:10px;font-family: 'Tahoma';">{{$candidate->ExpYrs}}</td>
              @if($candidate->Management == 1)
              <td style="border:1px solid #1C2E5C;border-radius:50%;padding:10px;font-family: 'Tahoma';"> {{$candidate->ManagementExp}}</td>
              @else
              <td style="border:1px solid #1C2E5C;border-radius:50%;padding:10px;font-family: 'Tahoma';"> None</td>
              
              @endif
             @endforeach
           </tr>

         </table>

         <table border="1px" style="width:100%;font-size:12px;margin-left:40px;margin-right:40px;;font-family: 'Tahoma';" >

          <tr>
            @foreach($candidates as $candidate)
           <td>Benefits Recieved | {{$candidate->BenefitsReceived}}</td>
            @endforeach
          </tr>
        </table>
        </div>
          </div>



          <div class="row" >
            <div class="col-12" style="margin-top:20px">
              <div style="margin-left:40px;margin-right:40px;margin-bottom:0">
              <p style="display: inline-block; background:#29ABE2;color:#fff;padding:20px;padding-top:10px; padding-bottom:10px;border-bottom-left-radius:80%;border-top-right-radius:80%;margin-bottom:0;width:25%;font-weight:bold;font-size:12px">Candidate Test Results</p>
              <p style="width: 67%;line-height: 0em;border: 1px solid #29ABE2;display: inline-block;opacity: 1;margin-top:-20px;margin-bottom:0">&nbsp;</p>
           </div>


              <table border="1px" style="table-layout:fixed;width:100%;margin-left:30px;margin-right:40px;font-size:12px;font-family: 'Tahoma';" cellspacing="15">
           <tr>
               <th style="width:25%;"><span style="font-weight:bold"> Written Test</span> </th>
               <th style="width:25%;"><span style="font-weight:bold">Excel</th>
               <th style="width:25%;"><span style="font-weight:bold">Others:</span></th>
               <th style="width:25%;"><span style="font-weight:bold">Ravens IQ:</span></th>
           </tr>
            <tr>
                @foreach($candidates as $candidate)
             <td style="border:1px solid #1C2E5C;border-radius:50%;padding:10px;font-family: 'Tahoma';">{{$candidate->TestResult_2}}</td>
             <td style="border:1px solid #1C2E5C;border-radius:50%;padding:10px;font-family: 'Tahoma';">{{$candidate->TestResult_3}}</td>
             <td style="border:1px solid #1C2E5C;border-radius:50%;padding:10px;font-family: 'Tahoma';">{{$candidate->lumina}}</td>
             <td style="border:1px solid #1C2E5C;border-radius:50%;padding:10px;font-family: 'Tahoma';">{{$candidate->TestResult_1}}</td>

             @endforeach
            </tr>
       </table>

       <table border="1px" style="table-layout:fixed;width:100%;margin-left:40px;margin-right:44px;font-size:12px;margin-top:20px;font-family: 'Tahoma';" cellspacing="5">
    <tr style="margin-top:20px">
        <th style="width:25%;"><span style="font-weight:bold"> Time Management</span> </th>
    </tr>
    <tr >
      @foreach($candidates as $candidate)
      
       
      @if ($candidate->TestResult_5 === '0 - 20 - Unsatisfactory')
      <td style="border:1px solid #1C2E5C;border-radius:50%;padding:10px;font-family: 'Tahoma';"> 0 – 20 Unsatisfactory The candidate has demonstrated very little skill in prioritization & logical explanation</td>
      @elseif($candidate->TestResult_5 === '21 - 30 - Unsatisfactory')
       <td style="border:1px solid #1C2E5C;border-radius:50%;padding:10px;font-family: 'Tahoma';"> 21 – 30 Unsatisfactory The candidate has demonstrated very little skill in prioritization & logical explanation</td>
       @elseif($candidate->TestResult_5 === '31 - 40 - Unsatisfactory')
       <td style="border:1px solid #1C2E5C;border-radius:50%;padding:10px;font-family: 'Tahoma';"> 31 – 40 Unsatisfactory The candidate has demonstrated very little skill in prioritization & logical explanation</td>
        @elseif($candidate->TestResult_5 === '41 - 50 - Unsatisfactory')
       <td style="border:1px solid #1C2E5C;border-radius:50%;padding:10px;font-family: 'Tahoma';"> 41 – 50 Unsatisfactory The candidate has demonstrated very little skill in prioritization & logical explanation</td>
        @elseif($candidate->TestResult_5 === '51 - 60 - Needs Development')
       <td style="border:1px solid #1C2E5C;border-radius:50%;padding:10px;font-family: 'Tahoma';"> 51 - 60 Needs Development The candidate has demonstrated a limited amount of skill in prioritization & logical explanation</td>
        @elseif($candidate->TestResult_5 === '61 - 70 - Needs Development')
       <td style="border:1px solid #1C2E5C;border-radius:50%;padding:10px;font-family: 'Tahoma';"> 61 - 70 Needs Development The candidate has demonstrated a limited amount of skill in prioritization & logical explanation</td>
        @elseif($candidate->TestResult_5 === '71 - 80 -  Meets Expectations')
       <td style="border:1px solid #1C2E5C;border-radius:50%;padding:10px;font-family: 'Tahoma';"> 71 - 80 Meets Expectations The candidate has demonstrated adequate skill in prioritization & logical explanation</td>
        @elseif($candidate->TestResult_5 === '81 - 90 - Meets Expectations')
       <td style="border:1px solid #1C2E5C;border-radius:50%;padding:10px;font-family: 'Tahoma';"> 81 - 90 Meets Expectations The candidate has demonstrated adequate skill in prioritization & logical explanation</td>
         @elseif($candidate->TestResult_5 === '91 - 98 - Exceeds Expectations')
       <td style="border:1px solid #1C2E5C;border-radius:50%;padding:10px;font-family: 'Tahoma';"> 91 - 98 Exceeds Expectations The candidate has demonstrated very good skill in prioritization & logical explanation</td>
         @elseif($candidate->TestResult_5 === '99 - 100 - Significantly Exceeds Expectations')
       <td style="border:1px solid #1C2E5C;border-radius:50%;padding:10px;font-family: 'Tahoma';"> 99 - 100 Significantly Exceeds Expectations The candidate has demonstrated outstanding skill in prioritization & logical explanation</td>
         @elseif($candidate->TestResult_5 === 'To be emailed')
       <td style="border:1px solid #1C2E5C;border-radius:50%;padding:10px;font-family: 'Tahoma';"> To be emailed</td>
        @elseif($candidate->TestResult_5 === 'Not Applicable')
       <td style="border:1px solid #1C2E5C;border-radius:50%;padding:10px;font-family: 'Tahoma';"> Not Applicable</td>
       @endif
      
      
      
      @endforeach
    </tr>

      </table>
      <table border="1px" style="table-layout:fixed;width:100%;margin-left:40px;margin-right:45px;font-size:12px;margin-top:20px;;font-family: 'Tahoma';" cellspacing="5">
      <tr>
       <th style="width:25%;"><span style="font-weight:bold"> Situation Test</span> </th>

      </tr>
      <tr>
        @foreach($candidates as $candidate)
        
           
      @if ($candidate->TestResult_4 === '0 - 20 - Unsatisfactory')
      <td style="border:1px solid #1C2E5C;border-radius:50%;padding:10px;font-family: 'Tahoma';">  0 – 20 Unsatisfactory The candidate has demonstrated very little skill in solving problems/ initiative/responsibility/honesty</td>
      @elseif($candidate->TestResult_4 === '21 - 30 - Unsatisfactory')
       <td style="border:1px solid #1C2E5C;border-radius:50%;padding:10px;font-family: 'Tahoma';">21 – 30 	Unsatisfactory	The candidate has demonstrated very little skill in solving problems/ initiative/responsibility/honesty</td>
       @elseif($candidate->TestResult_4 === '31 - 40 - Unsatisfactory')
       <td style="border:1px solid #1C2E5C;border-radius:50%;padding:10px;font-family: 'Tahoma';"> 31 – 40 	Unsatisfactory	The candidate has demonstrated very little skill in solving problems/ initiative/responsibility/honesty</td>
        @elseif($candidate->TestResult_4 === '41 - 50 - Unsatisfactory')
       <td style="border:1px solid #1C2E5C;border-radius:50%;padding:10px;font-family: 'Tahoma';"> 41 – 50	Unsatisfactory	The candidate has demonstrated very little skill in solving problems/ initiative/responsibility/honesty</td>
        @elseif($candidate->TestResult_4 === '51 - 60 - Needs Development')
       <td style="border:1px solid #1C2E5C;border-radius:50%;padding:10px;font-family: 'Tahoma';"> 51 - 60	Needs Development	The candidate has demonstrated limited skill in solving problems/ initiative/responsibility/honesty</td>
        @elseif($candidate->TestResult_4 === '61 - 70 - Needs Development')
       <td style="border:1px solid #1C2E5C;border-radius:50%;padding:10px;font-family: 'Tahoma';"> 61 - 70	Needs Development 	The candidate has demonstrated limited skill in solving problems/ initiative/responsibility/honesty</td>
        @elseif($candidate->TestResult_4 === '71 - 80 - Meets Expectations')
       <td style="border:1px solid #1C2E5C;border-radius:50%;padding:10px;font-family: 'Tahoma';"> 71 - 80	Meets Expectations	The candidate has demonstrated adequate skill in solving problems/ initiative/responsibility/honesty</td>
        @elseif($candidate->TestResult_4 === '81 - 90 - Meets Expectations')
       <td style="border:1px solid #1C2E5C;border-radius:50%;padding:10px;font-family: 'Tahoma';"> 81 - 90	Meets Expectations	The candidate has demonstrated adequate skill in solving problems/ initiative/responsibility/honesty</td>
         @elseif($candidate->TestResult_4 === '91 - 98 - Exceeds Expectations')
       <td style="border:1px solid #1C2E5C;border-radius:50%;padding:10px;font-family: 'Tahoma';"> 91 - 98	Exceeds Expectations	The candidate has demonstrated all of the skill in solving problems/ initiative/responsibility/honesty</td>
         @elseif($candidate->TestResult_4 === '99 - 100 - Significantly Exceeds Expectations')
       <td style="border:1px solid #1C2E5C;border-radius:50%;padding:10px;font-family: 'Tahoma';"> 99 - 100	Significantly Exceeds Expectations 	The candidate has demonstrated outstanding skill in solving problems/ initiative/responsibility/honesty</td>
         @elseif($candidate->TestResult_4 === 'To be emailed')
       <td style="border:1px solid #1C2E5C;border-radius:50%;padding:10px;font-family: 'Tahoma';"> To be emailed</td>
        @elseif($candidate->TestResult_4 === 'Not Applicable')
       <td style="border:1px solid #1C2E5C;border-radius:50%;padding:10px;font-family: 'Tahoma';"> Not Applicable</td>
       @endif
      
        @endforeach
      </tr>

      </table>

      <table border="1px" style="table-layout:fixed;width:100%;margin-left:30px;margin-right:45px;font-size:12px;margin-top:20px;;font-family: 'Tahoma';" cellspacing="15">
      <tr>
       <th style="width:25%;"><span style="font-weight:bold"> Competencies</span> </th>

      </tr>

        @foreach($candidates as $candidate)
        @php $competencies = \App\InterviewCompetency::where('InterviewID',$candidate->InterviewID)->get(); @endphp
        @foreach( $competencies as $comp)
        @php $com = \App\competencies::where('ID',$comp->compitency)->first(); @endphp
        @if($com)
          <tr>
        <td style="border:1px solid #1C2E5C;border-radius:50%;padding:10px;font-family: 'Tahoma';">{{$com->Name}}</td>
          <td style="border:1px solid #1C2E5C;border-radius:50%;padding:10px;font-family: 'Tahoma';">{{$comp->rating}}</td>
            <td style="border:1px solid #1C2E5C;border-radius:50%;padding:10px;font-family: 'Tahoma';">{{$comp->comment}}</td>
            </tr>
            @endif
            @endforeach

        @endforeach


      </table>
            </div>

          </div>

              <div class="row" >
                <div class="col-12" style="background:#1C2E5C;margin-top:10%;border-radius:50%;margin-left:43px;margin-right:45px">
                  <div class="information" style="margin-left:40px;margin-right40px;">
                  <h2  style="color: #fff;padding:10px;font-size:15px;font-weight:bold;font-family: 'Tahoma';">
                    Work References, Certificate of Good Conduct, Credit Reference Bureau and Ethics & Anti-Corruption Commission ( if applicable ) are all in progress

                  </h2>

                </div>
              </div>
              </div>

        </main>
    </body>
</html>
