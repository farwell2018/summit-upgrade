@extends('layouts.admin')
@section('title','Admin-Dashboard')
@section('css')

<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
@endsection
@section('content')

<!--dashboard breadcrumb-->

 <div class="row" style="margin-left:8px">
    <span class="text">Dashboard Panel</span> </a> &nbsp;
 </div>
 
          <a class="btn pull-right" href="{{route('dashboard')}}" style="background-color: #FFCB00;color:#3069AB;border-radius: 5px;font-size: 14px !important;padding:8px;margin-left:20px">REFRESH</a>
          
          <form class = "form-inline pull-right" role = "form" method="GET" action="{{route('dashboard.filter')}}">
         <div class="form-group">
            <!--<label for="daterange">Date Range</label>-->
            <input type="hidden" name="ytd"  value="<?php echo date('Y-m-d', strtotime('first day of january this year')); ?>" />
          <button class="btn  pull-right" type="submit"  style="background-color:  #FFCB00;color:#3069AB">YTD</button>
          </div>
          </form>
 <br><br>
   <div class="card">
      <div class="card-header tab-form-header text-white" style="background-color: #1C2E5C ">
       <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">General Overview</div>
       
       <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 " >
           <form class = "form-inline pull-right" role = "form" method="GET" action="{{route('dashboard')}}">
          <div class="form-group" style="padding-right:30px">
            <!--<label for="daterange">Date Range</label>-->
            <input type="text" name="daterange" id="daterange" class="form-control" value="" placeholder="Click to select date range" style="width:220px"/>
          </div>
          
          <div class="form-group">
          <button class="btn  pull-right" type="submit"  style="background-color:  #FFCB00;color:#3069AB">FILTER</button>
          </div>
          </form>
          
          
        </div>
      </div>
      <div class="card-body">
          <div class="row">
              <div class="col-xl-3 col-lg-6 mb-4">
                 <div class="bg-white rounded-lg p-5 shadow">
                    <h2 class="h6 font-weight-bold text-center mb-4" style="font-size: 15px;">Total Candidates</h2>
                      <div class="progress mx-auto" data-value='{{count($users)}}'>
                        <span class="progress-left">
                              <span class="progress-bar border-primary"></span>
                        </span>
                        <span class="progress-right">
                              <span class="progress-bar border-primary"></span>
                        </span>
                        <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                          <div class="h2 font-weight-bold">{{count($users)}}<sup class="small"></sup></div>
                        </div>
                      </div><br>
                      <div class="text-center">
                <i class="fas fa-arrow-right" ></i>&nbsp<a class="btn" href="{{route('candidatemanagement.index')}}" style="background-color: #FFCB00;color:#3069AB;border-radius: 5px;font-size: 14px !important;padding:5px"><strong>Go To Candidates</strong></a>
              </div>
                </div>                
            </div>
            <div class="col-xl-3 col-lg-6 mb-4">
              <div class="bg-white rounded-lg p-5 shadow">
                <h2 class="h6 font-weight-bold text-center mb-4" style="font-size: 15px;">Total Jobs</h2>
                <!-- Progress bar 2 -->
                <div class="progress mx-auto" data-value='{{count($jobs)}}'>
                  <span class="progress-left">
                                <span class="progress-bar border-danger"></span>
                  </span>
                  <span class="progress-right">
                                <span class="progress-bar border-danger"></span>
                  </span>
                  <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                    <div class="h2 font-weight-bold">{{count($jobs)}}<sup class="small"></sup></div>
                  </div>
                </div><br>
                <div class="text-center">
                <i class="fas fa-arrow-right" ></i>&nbsp<a class="btn" href="{{route('jobAds.index')}}" style="background-color: #FFCB00;color:#3069AB;border-radius: 5px;font-size: 14px !important;padding:5px"><strong>
                Go To Jobs</strong></a>
              </div>
              </div>              
            </div>
            <div class="col-xl-3 col-lg-6 mb-4">
              <div class="bg-white rounded-lg p-5 shadow">
                <h2 class="h6 font-weight-bold text-center mb-4" style="font-size: 15px;">Total Clients</h2>

                <!-- Progress bar 3 -->
                <div class="progress mx-auto" data-value='{{count($clients)}}'>
                  <span class="progress-left">
                                <span class="progress-bar border-success"></span>
                  </span>
                  <span class="progress-right">
                                <span class="progress-bar border-success"></span>
                  </span>
                  <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                    <div class="h2 font-weight-bold">{{count($clients)}}<sup class="small"></sup></div>
                  </div>
                </div><br>
                <div class="text-center">
                <i class="fas fa-arrow-right" >&nbsp</i><a class="btn" href="{{route('clientmanagement.index')}}" style="background-color: #FFCB00;color:#3069AB;border-radius: 5px;font-size: 14px !important;padding:5px"><strong>Go To Clients</strong></a>
              </div>
              </div>              
            </div>
            <div class="col-xl-3 col-lg-6 mb-4">
              <div class="bg-white rounded-lg p-5 shadow">
                <h2 class="h6 font-weight-bold text-center mb-4" style="font-size: 15px;">Total Consultants</h2>

                <!-- Progress bar 4 -->
                <div class="progress mx-auto" data-value='{{count($staff)}}'>
                  <span class="progress-left">
                                <span class="progress-bar border-warning"></span>
                  </span>
                  <span class="progress-right">
                                <span class="progress-bar border-warning"></span>
                  </span>
                  <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                    <div class="h2 font-weight-bold">{{count($staff)}}<sup class="small"></sup></div>
                  </div>
                </div><br>
                <div class="text-center">
                <i class="fas fa-arrow-right" ></i>&nbsp<a class="btn" href="{{route('users.index')}}" style="background-color: #FFCB00;color:#3069AB;border-radius: 5px;font-size: 14px !important;padding:5px"><strong>Go To Consultants</strong></a>
              </div>
              </div>               
            </div>           
          </div>
   </div>
  </div><br><br>
    <div class="card">
  <div class="card-header tab-form-header text-white" style="background-color: #1C2E5C ">
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">Client Overview</div>
    
    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 ">
           <form class = "form-inline pull-right" role = "form" method="GET" action="{{route('dashboard')}}">
          <div class="form-group" style="padding-right:30px">
            <!--<label for="daterange">Date Range</label>-->
            <input type="text" name="clientrange" id="clientrange" class="form-control" value="" placeholder="Click to select date range" style="width:220px"/>
          </div>
          
          <div class="form-group">
          <button class="btn  pull-right" type="submit"  style="background-color:  #FFCB00;color:#3069AB">FILTER</button>
          </div>
          </form>
        </div>
  </div>
  <div class="card-body">
        <div class="row">
            <div class="col-xl-3 col-lg-6 mb-4">
              <div class="bg-white rounded-lg p-5 shadow">
                <h2 class="h6 font-weight-bold text-center mb-4" style="font-size: 15px;">Total Active Clients</h2>

                <!-- Progress bar 1 -->
                <div class="progress mx-auto" data-value='{{(count($active))}}'>
                  <span class="progress-left">
                                <span class="progress-bar border-success"></span>
                  </span>
                  <span class="progress-right">
                                <span class="progress-bar border-success"></span>
                  </span>
                  <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                    <div class="h2 font-weight-bold">{{(count($active))}}<sup class="small"></sup></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-6 mb-4">
              <div class="bg-white rounded-lg p-5 shadow">
                <h2 class="h6 font-weight-bold text-center mb-4" style="font-size: 15px;">Active Not Loyal</h2>
                <!-- Progress bar 2 -->
                <div class="progress mx-auto" data-value='{{(count($activenotloyal))}}'>
                  <span class="progress-left">
                                <span class="progress-bar border-danger"></span>
                  </span>
                  <span class="progress-right">
                                <span class="progress-bar border-danger"></span>
                  </span>
                  <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                    <div class="h2 font-weight-bold">{{(count($activenotloyal))}}<sup class="small"></sup></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-6 mb-4">
              <div class="bg-white rounded-lg p-5 shadow">
                <h2 class="h6 font-weight-bold text-center mb-4" style="font-size: 15px;">Inactive and Loyal</h2>
                <!-- Progress bar 3 -->
                <div class="progress mx-auto" data-value='{{(count($inactiveloyal))}}'>
                  <span class="progress-left">
                                <span class="progress-bar border-success"></span>
                  </span>
                  <span class="progress-right">
                                <span class="progress-bar border-success"></span>
                  </span>
                  <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                    <div class="h2 font-weight-bold">{{(count($inactiveloyal))}}<sup class="small"></sup></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-6 mb-4">
              <div class="bg-white rounded-lg p-5 shadow">
                <h2 class="h6 font-weight-bold text-center mb-4" style="font-size: 15px;">Inactive Not Loyal</h2>
                <!-- Progress bar 4 -->
                <div class="progress mx-auto" data-value='{{(count($inactivenotloyal))}}'>
                  <span class="progress-left">
                                <span class="progress-bar border-primary"></span>
                  </span>
                  <span class="progress-right">
                                <span class="progress-bar border-primary"></span>
                  </span>
                  <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                    <div class="h2 font-weight-bold">{{(count($inactivenotloyal))}}<sup class="small"></sup></div>
                  </div>
                </div>
              </div>
            </div>
      </div>
      <div class="text-center">
          <i class="fas fa-arrow-right" ></i>&nbsp<a class="btn" href="{{route('clientmanagement.index')}}" style="background-color: #FFCB00;color:#3069AB;border-radius: 5px;font-size: 14px !important;padding:5px"><strong>Go To Client Management</strong></a>
      </div>
  </div>
  </div>
  <br><br>
   <div class="row">
  <!--  <div class="col">
      <div class="card">
  <div class="card-header tab-form-header">
    Job Ads Overview
  </div>
  <div class="card-body">
    <div class="row">
      <div class="col">

      </div>
      <div class="col">

    </div>
    </div>
  </div>
  </div>
   </div> -->
   <div class="col md-12">
    <div class="card">
          <div class="card-header tab-form-header text-white" style="background-color: #1C2E5C ">
           <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 ">Interview Management Overview</div>
           
           <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 ">
           <form class = "form-inline pull-right" role = "form" method="GET" action="{{route('dashboard')}}">
          <div class="form-group" style="padding-right:10px">
            <!--<label for="daterange">Date Range</label>-->
            <input type="text" name="interviewrange" id="interviewrange" class="form-control" value="" placeholder="Click to select date range" style="width:212px"/>
          </div>
          
          <div class="form-group">
          <button class="btn  pull-right" type="submit"  style="background-color:  #FFCB00;color:#3069AB">FILTER</button>
          </div>
          </form>
        </div>
           
          </div>
      <div class="card-body">
        <div class="row">
              <div class="col-xl-3 col-lg-6 mb-4">
                  <div class="bg-white rounded-lg p-5 shadow">
                <h2 class="h6 font-weight-bold text-center mb-4" style="font-size: 15px;">Shortlisted Candidates</h2>
                  <div class="progress mx-auto" data-value='{{(count($shortlisted))}}'>
                   <span class="progress-left">
                              <span class="progress-bar border-primary"></span>
                   </span>
                   <span class="progress-right">
                              <span class="progress-bar border-primary"></span>
                   </span>
                   <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                        <div class="h2 font-weight-bold">{{(count($shortlisted))}}<sup class="small"></sup></div>
                   </div>
              </div>
              </div>
              </div>
              <div class="col-xl-3 col-lg-6 mb-4">
                  <div class="bg-white rounded-lg p-5 shadow">
                <h2 class="h6 font-weight-bold text-center mb-4" style="font-size: 15px;">Scheduled Candidates</h2>
                   <div class="progress mx-auto" data-value='{{(count($scheduled))}}'>
                   <span class="progress-left">
                              <span class="progress-bar border-warning"></span>
                   </span>
                   <span class="progress-right">
                              <span class="progress-bar border-warning"></span>
                   </span>
                   <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                        <div class="h2 font-weight-bold">{{(count($scheduled))}}<sup class="small"></sup></div>
                   </div>
               </div>
              </div></div>
              <div class="col-xl-3 col-lg-6 mb-4">
                  <div class="bg-white rounded-lg p-5 shadow">
                     <h2 class="h6 font-weight-bold text-center mb-4" style="font-size: 15px;">Interviewed Candidates</h2>
                   <div class="progress mx-auto" data-value='{{(count($interviewed))}}'>
                   <span class="progress-left">
                              <span class="progress-bar border-danger"></span>
                   </span>
                   <span class="progress-right">
                              <span class="progress-bar border-danger"></span>
                   </span>
                   <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                        <div class="h2 font-weight-bold">{{(count($interviewed))}}<sup class="small"></sup></div>
                   </div>
               </div>
                </div></div>
                
                <div class="col-xl-3 col-lg-6 mb-4">
                    <div class="bg-white rounded-lg p-5 shadow">
                     <h2 class="h6 font-weight-bold text-center mb-4" style="font-size: 15px;">Placed Candidates</h2>
                   <div class="progress mx-auto" data-value='{{(count($placed))}}'>
                   <span class="progress-left">
                      <span class="progress-bar border-success"></span>
                   </span>
                   <span class="progress-right">
                              <span class="progress-bar border-success"></span>
                   </span>
                   <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                        <div class="h2 font-weight-bold">{{(count($placed))}}<sup class="small"></sup></div>
                   </div>
               </div>
                </div></div>
        </div><br>
        <div class="text-center">
                <i class="fas fa-arrow-right" ></i>&nbsp<a class="btn" href="{{route('interviewmanagement.index')}}"style="background-color: #FFCB00;color:#3069AB;border-radius: 5px;font-size: 14px !important;padding:5px"><strong>Go To Interview Management</strong></a>
              </div>
      </div>
  </div>
   </div>
   </div><br><br>
   <div class="row">
   <div class="col md-12">
      <div class="card">
        <div class="card-header tab-form-header text-white" style="background-color: #1C2E5C ">
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 " >Trouble Tickets</div>
          
          <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 ">
           <form class = "form-inline pull-right" role = "form" method="GET" action="{{route('dashboard')}}">
          <div class="form-group" style="padding-right:10px">
            <!--<label for="daterange">Date Range</label>-->
            <input type="text" name="ticketrange" id="ticketrange" class="form-control" value="" placeholder="Click to select date range" style="width:212px"/>
          </div>
          
          <div class="form-group">
          <button class="btn  pull-right" type="submit"  style="background-color:  #FFCB00;color:#3069AB">FILTER</button>
          </div>
          </form>
        </div>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col">
                <div class="bg-white rounded-lg p-5 shadow">
              <h2 class="h6 font-weight-bold text-center mb-4" style="font-size: 15px;">Total Tickets</h2>
              <div class="progress mx-auto" data-value='{{(count($tickets))}}'>
                   <span class="progress-left">
                      <span class="progress-bar border-primary"></span>
                   </span>
                   <span class="progress-right">
                              <span class="progress-bar border-primary"></span>
                   </span>
                   <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                        <div class="h2 font-weight-bold">{{(count($tickets))}}<sup class="small"></sup></div>
                   </div>
               </div>
            </div></div>
             <div class="col">
                 <div class="bg-white rounded-lg p-5 shadow">
              <h2 class="h6 font-weight-bold text-center mb-4" style="font-size: 15px;">Closed Tickets</h2>
              <div class="progress mx-auto" data-value='{{(count($closedtickets))}}'>
                   <span class="progress-left">
                      <span class="progress-bar border-success"></span>
                   </span>
                   <span class="progress-right">
                              <span class="progress-bar border-success"></span>
                   </span>
                   <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                        <div class="h2 font-weight-bold">{{(count($closedtickets))}}<sup class="small"></sup></div>
                   </div>
               </div>
            </div></div>
            <div class="col">
                <div class="bg-white rounded-lg p-5 shadow">
              <h2 class="h6 font-weight-bold text-center mb-4" style="font-size: 15px;">Open Tictets</h2>
              <div class="progress mx-auto" data-value='{{(count($opentickets))}}'>
                   <span class="progress-left">
                      <span class="progress-bar border-danger"></span>
                   </span>
                   <span class="progress-right">
                      <span class="progress-bar border-danger"></span>
                   </span>
                   <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                        <div class="h2 font-weight-bold">{{(count($opentickets))}}<sup class="small"></sup></div>
                   </div>
               </div>
            </div></div>
           
          <!--   <div class="col">
              <h2 class="h6 font-weight-bold text-center mb-4">Go To Tickets</h2>
                <div class="progress mx-auto" data-value=''>
                   <span class="progress-left">
                      <span class="progress-bar border-success"></span>
                   </span>
                   <span class="progress-right">
                              <span class="progress-bar border-success"></span>
                   </span>
                   <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">
                        <i class="fas fa-arrow-right" ></i><a class="btn" href="{{route('tickets')}}"style="background-color:#FFCB00;color:#3069AB "><i class="fas fa-arrow-right" >Go Tickets</i></a>
                   </div>
               </div>
            </div> -->
          </div><br>
          <div class="text-center">
            <i class="fas fa-arrow-right" ></i>&nbsp<a class="btn" href="{{route('tickets')}}"style="background-color:#FFCB00;color:#3069AB;font-size: 14px !important;padding:5px "><strong>Go To Tickets</strong></a>
          </div>          
        </div>
      </div>
   </div>
  </div><br><br>  
  <div class="card">
  <div class="card-header tab-form-header text-white" style="background-color: #1C2E5C ">
    Staff Overview
  </div>
  <div class="card-body">
      <div class="row">
         
      @foreach($stafffs as $stafff)
       @if($stafff->SummitStaff && $stafff->SummitStaff->status == 1)
          <div class="col-xl-3 col-lg-6 mb-4">
            <div class="bg-white rounded-lg p-5 shadow">
              <h2 class="h6 font-weight-bold text-center mb-4"></h2>
                <div class="row">
                 <!--  <div class="col">
                      <img class="img-profile" src="@if(isset($stafff->SummitStaff->profilePhoto)) {{ asset('uploads/staff/'. $stafff->SummitStaff->profilePhoto.'')}} @else {{asset('admin-assets/img/default.jpg')}} @endif" style="width:70px; height:70px">
                  </div> -->
                  <div class="col">
                      <p>  <span>@if(!empty($stafff->SummitStaff)){{$stafff->SummitStaff->Firstname}} {{$stafff->SummitStaff->Lastname}} @endif</span> |  <span>@if(($stafff->role == 0)){{'Administrator'}}@else {{'Staff'}}@endif</span></p>
                                  
                  </div>   
                  </div>
                   @php
                  $interview = App\Interview::where('StaffID','=',$stafff->SummitStaff->StaffID)->get();
                  $jobsassigned = App\JobAd::where('StaffID','=',$stafff->SummitStaff->StaffID)->get();
                  $placedcandidates = App\Interview::where('StaffID','=',$stafff->SummitStaff->StaffID)->get();
                  $onboardedclients = App\Clients::where('StaffID','=',$stafff->SummitStaff->StaffID)->get();
                 @endphp
                 
                 <div class="row">
                  <div class="col">
                      <div class="progress mx-auto" data-value='{{(count($interview))}}' >
                         <span class="progress-left">
                            <span class="progress-bar border-success"></span>
                         </span>
                        <span class="progress-right">
                              <span class="progress-bar border-success"></span>
                        </span>
                        <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">                                               
                          <span><div class="h2 font-weight-bold" style="font-size:1.5rem"> {{(count($interview))}} <br><sup class="small"> Interviewed</sup></div>
                        </div>
                      </div>
                      </div>
                      <div class="col">
                      <div class="progress mx-auto" data-value='{{(count($jobsassigned))}}' >
                         <span class="progress-left">
                            <span class="progress-bar border-primary"></span>
                         </span>
                        <span class="progress-right">
                              <span class="progress-bar border-primary"></span>
                        </span>
                        <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">                                               
                          <span><div class="h2 font-weight-bold" style="font-size:1.5rem">{{(count($jobsassigned))}} <br><sup class="small"> Jobs Assigned</sup></div>
                        </div>
                      </div>
                  </div>
                  <div class="col">
                      <div class="progress mx-auto" data-value='{{(count($onboardedclients))}}' >
                         <span class="progress-left">
                            <span class="progress-bar border-danger"></span>
                         </span>
                        <span class="progress-right">
                              <span class="progress-bar border-danger"></span>
                        </span>
                        <div class="progress-value w-100 h-100 rounded-circle d-flex align-items-center justify-content-center">                                               
                          <span><div class="h2 font-weight-bold" style="font-size:1.5rem">{{(count($onboardedclients))}}<br><sup class="small"> Onboarded Clients</sup></div>
                        </div>
                      </div>
                  </div>
                </div>
            </div>
          </div>
          @endif
           @endforeach
    </div>
  </div>
  </div>

<style>
.progress {
  width: 150px;
  height: 150px;
  background: none;
  position: relative;
  box-shadow:none !important;
}
.progress::after {
  content: "";
  width: 100%;
  height: 100%;
  border-radius: 50%;
  border: 6px solid #eee;
  position: absolute;
  top: 0;
  left: 0;
}
.progress>span {
  width: 50%;
  height: 100%;
  overflow: hidden;
  position: absolute;
  top: 0;
  z-index: 1;
}
.progress .progress-left {
  left: 0;
}
.progress .progress-bar {
  width: 100%;
  height: 100%;
  background: none;
  border-width: 6px;
  border-style: solid;
  position: absolute;
  top: 0;
}
.progress .progress-left .progress-bar {
  left: 100%;
  border-top-right-radius: 80px;
  border-bottom-right-radius: 80px;
  border-left: 0;
  -webkit-transform-origin: center left;
  transform-origin: center left;
}
.progress .progress-right {
  right: 0;
}
.progress .progress-right .progress-bar {
  left: -100%;
  border-top-left-radius: 80px;
  border-bottom-left-radius: 80px;
  border-right: 0;
  -webkit-transform-origin: center right;
  transform-origin: center right;
}
.progress .progress-value {
  position: absolute;
  top: 0;
  left: 0;
}
body {
  background: #ff7e5f;
  background: -webkit-linear-gradient(to right, #ff7e5f, #feb47b);
  background: linear-gradient(to right, #ff7e5f, #feb47b);
  min-height: 100vh;
}
.rounded-lg {
  border-radius: 1rem;
}
.text-gray {
  color: #aaa;
}
div.h4 {
  line-height: 1rem;
}

.daterangepicker.dropdown-menu.ltr.show-calendar.opensright{
    top: 220px;
    left: 547.812px !important;
    right: auto;
    
}
.daterangepicker:before, .daterangepicker:after {
    position: absolute;
    display: inline-block;
    border-bottom-color: rgba(0, 0, 0, 0.2);
    content: none !important;
}

</style>


@endsection

@section('footer-js')
<script>
var dataTable;
let baseUrl = "{!! route('dashboard.json') !!}?";

function encodeQueryData(data) {
 const ret = [];
 for (let d in data)
   ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
 return ret.join('&');
}

function filterJobs(){
  const type = document.querySelector('#filterForm select[name=jobtype]').value

  const careerlevel = document.querySelector('#filterForm select[name=careerlevel]').value

  const education = document.querySelector('#filterForm select[name=education]').value

  const jobtitle = document.querySelector('#filterForm input[name=jobtitle]').value

  const category = document.querySelector('#filterForm select[name=industry]').value

  const clients = document.querySelector('#filterForm select[name=clients]').value


  const url = baseUrl  + "&" + encodeQueryData({type,careerlevel,education,jobtitle,category,clients})

  console.log(url);
  dataTable.ajax.url( url ).load();
}


  $(document).ready(function(){
dataTable= $('#dataTable').DataTable({
    responsive: true,
    "deferRender": true,
    "processing": true,
    "serverSide": true,
    "ordering": true, //disable column ordering
    "lengthMenu": [
      [50, 100, 150, 200, 250, -1],
      [50, 100, 150, 200, 250, "All"] // change per page values here
    ],
    "pageLength": 10,
    "ajax": {
       url: baseUrl,
      method: 'GET'
    },
    // dom: '<"html5buttons"B>lTfgitp',
    "dom": "<'row' <'col-md-12'>><'row'<'col-md-8 col-sm-12'lB><'col-md-4 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
    buttons: [
      { extend: 'copy',exportOptions: {columns: [0, 1, 2, 3,4,5,6]}},
      {extend: 'csv',exportOptions: {columns: [0, 1, 2, 3,4,5,6]}},
      {extend: 'excel', title: '{{ config('app.name', 'Summit') }} - List of all Users',exportOptions: {columns: [0, 1, 2, 3,4,5]}},
      {extend: 'pdf', title: '{{ config('app.name', 'Summit') }} - List of all Users',exportOptions: {columns: [0, 1, 2, 3,4,5]}},
      {extend: 'print',
      customize: function (win){
        $(win.document.body).addClass('white-bg');
        $(win.document.body).css('font-size', '10px');
        $(win.document.body).find('table')
        .addClass('compact')
        .css('font-size', 'inherit');
      }
    }
  ],
  columns: [
    {data: 'ID', name: 'ID', orderable: true, searchable: true},
    {data: 'JobTitle', name: 'JobTitle', orderable: true, searchable: true},
    {data: 'Category', name: 'Category', orderable: true, searchable: true},
    {data: 'client', name: 'client', orderable: true, searchable: false},
    {data: 'JobType', name: 'JobType', orderable: false, searchable: false},
    {data: 'LocationCountry', name: 'LocationCountry', orderable: true, searchable: false},
    {data: 'full_salary', name: 'full_salary', orderable: true, searchable: false},
    {data: 'Deadline', name: 'Deadline', orderable: true, searchable: false},
    {data: 'Actions', name: 'tools', orderable: false, searchable: false,"defaultContent": "<i>Not set</i>"},
  ],
});
});

</script>
<script>
var dataTables;

let baUrl = "{!! route('dashboard2.json') !!}?";

function encodeQueryData(data)
{
 const ret = [];
 for (let d in data)
   ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
 return ret.join('&');
}

//Filter method
function filterInterview(){
    //Get company value

    const interviewdate = document.querySelector('#filterForm7 input[name=interviewdate]').value
    const firstname = document.querySelector('#filterForm7 input[name=firstname]').value
    const lastname = document.querySelector('#filterForm7 input[name=lastname]').value
    const jobad = document.querySelector('#filterForm7 select[name=jobad]').value
    const status = document.querySelector('#filterForm7 select[name=status]').value

    //Add to URL
      const url = baUrl  + "&" + encodeQueryData({jobad,interviewdate,status,firstname,lastname})


  console.log(url);
  dataTables.ajax.url( url ).load();
}
$(document).ready(function(){
 dataTables= $('#interviews').DataTable({
    responsive: true,
    "deferRender": true,
    "processing": true,
    "serverSide": true,
    "ordering": true, //disable column ordering
    "lengthMenu": [
      [5, 10, 15, 20, 25, -1],
      [5, 10, 15, 20, 25, "All"] // change per page values here
    ],
    "pageLength": 10,
    "ajax":  {
      url: baUrl,
      method: 'GET'
    },
    // dom: '<"html5buttons"B>lTfgitp',
    "dom": "<'row' <'col-md-12'>><'row'<'col-md-8 col-sm-12'lB><'col-md-4 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
    buttons: [
      { extend: 'copy',exportOptions: {columns: [0, 1, 2, 3,4,5,6]}},
      {extend: 'csv',exportOptions: {columns: [0, 1, 2, 3,4,5,6]}},
      {extend: 'excel', title: '{{ config('app.name', 'Summit') }} - List of all Users',exportOptions: {columns: [0, 1, 2, 3,4,5]}},
      {extend: 'pdf', title: '{{ config('app.name', 'Summit') }} - List of all Users',exportOptions: {columns: [0, 1, 2, 3,4,5]}},
      {extend: 'print',
      customize: function (win){
        $(win.document.body).addClass('white-bg');
        $(win.document.body).css('font-size', '10px');
        $(win.document.body).find('table')
        .addClass('compact')
        .css('font-size', 'inherit');
      }
    }
  ],
  columns: [
    {data: 'InterviewID', name: 'InterviewID', orderable: true, searchable: true},
    {data: 'CV_ID', name: 'CV_ID', orderable: true, searchable: true},
    {data: 'JobAD_ID', name: 'JobAD_ID', orderable: true, searchable: true},
    {data: 'StaffID', name: 'StaffID', orderable: false, searchable: false},
    {data: 'Status', name: 'Status', orderable: true, searchable: false},
    {data: 'InterviewDate', name: 'InterviewDate', orderable: true, searchable: false},
    {data: 'Actions', name: 'tools', orderable: false, searchable: false,"defaultContent": "<i>Not set</i>"},
  ],
});
});
</script>

<script>
  $(function() {

  $(".progress").each(function() {

    var value = $(this).attr('data-value');
    var left = $(this).find('.progress-left .progress-bar');
    var right = $(this).find('.progress-right .progress-bar');

    if (value > 0) {
      if (value <= 50) {
        right.css('transform', 'rotate(' + percentageToDegrees(value) + 'deg)')
      } else {
        right.css('transform', 'rotate(180deg)')
        left.css('transform', 'rotate(' + percentageToDegrees(value - 50) + 'deg)')
      }
    }
  })
  function percentageToDegrees(percentage) 
  {
    return percentage / 100 * 360
  }
});
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<script>
//Start daterange plugin
$(document).ready(function(){
  $('input#daterange').daterangepicker({autoUpdateInput: false});

  $('input#daterange').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
  });

  $('input#daterange').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
  });
  
  
  $('input#clientrange').daterangepicker({autoUpdateInput: false});

  $('input#clientrange').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
  });

  $('input#clientrange').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
  });
  
  
  $('input#interviewrange').daterangepicker({autoUpdateInput: false});

  $('input#interviewrange').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
  });

  $('input#interviewrange').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
  });
  
  $('input#ticketrange').daterangepicker({autoUpdateInput: false});

  $('input#ticketrange').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
  });

  $('input#ticketrange').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
  });
});
</script>
@endsection