@extends('layouts.admin')

@section('title','Professional Qualification Titles')


@section('content')
 <div class="row " style="margin-left:8px">
        <a href="{{route('home')}}" style="color:#858796" >
            <span class="text">Home</span> </a> &nbsp; | &nbsp;
        <a href="{{route('profqualtitles.index')}}" >
            <span class="text">Professional Qualification Title List</span> </a>
    </div>
    <br>
<div class="kt-portlet">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Add a Professional Qualification Title
            </h3>
        </div>
    </div>
    <!--begin::Form-->
    <form class="kt-form" method="POST" action="{{ route('profqualtitles.store') }}"
          enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="profqualtitlesname">Professional Title Name</label>
            <input id="profqualtitlesname" type="text" class="form-control{{ $errors->has('profqualtitlesname') ? ' is-invalid' : '' }}" name="profqualtitlesname" value="{{ old('profqualtitlesname')  }}" required>

            @if ($errors->has('profqualtitlesnamee'))
            <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('profqualtitlesname') }}</strong>
            </span>
            @endif
        </div>

    

        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <button type="submit" class="btn btn-primary">Submit</button>
                <button type="reset" class="btn btn-secondary">Cancel</button>
            </div>
        </div>
    </form>
    <!--end::Form-->




    @endsection
