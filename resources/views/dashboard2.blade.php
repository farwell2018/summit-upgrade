@extends('layouts.admin')
@section('title','Admin-Dashboard')
@section('css')

<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
@endsection
@section('content')

<!--dashboard breadcrumb-->

<div class="row" style="margin-left:8px">
    <span class="text">Consultant Dashboard Panel</span> </a> &nbsp;
 </div><br>
 <div class="card" style="background-color: #1C2E5C">
  <div class="card-body">
    <label style="color: #80CFFF">MY JOB ADS</label>
    <div id="filterForm">
      <div class="row">
        <div class="col">
          <div class="form-group form-spacing">
              <label for="company_name" style="color: #80CFFF">Job Title</label>
                <input type="text" name="jobtitle" class="form-control" value="">
          </div>
        </div>
        <div class="col">
           <div class="form-group form-spacing">
            <label for="educationLevel" style="color: #80CFFF">Education Level</label>
            <select id="education" name="education" class="form-control{{$errors->has('education')?'is-invalid':''}}">
              <option value=""> Select Education Level</option>
              <option value="Certificate">Certificate</option>
              <option value="Diploma">Diploma</option>
              <option value="Bachelor's Degree">Bachelor's Degree</option>
              <option value="Master's Degree">Master's Degree</option>
              <option value="Doctrate Degree">Doctrate Degree</option>
            </select>
           </div>
        </div>
        <div class="col">
          <div class="form-group form-spacing">
            <label for="JobType" style="color: #80CFFF">Job Type</label>
              <select class="form-control" name="jobtype" id="jobtype" value="">
                <option value="">Filter By Job Type</option>
                <option value="Permanent">Permanent</option>
                <option value="Part Time">Part Time</option>
                <option value="Contract">Contract</option>
              </select>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col">
           <div class="form-group form-spacing">
                    <label for="company_name" style="color: #80CFFF">Career Level</label>
                      <select name="careerlevel" class="form-control" value="">
                        <option value="" >Select Career Level</option>
                        <option value="Entry Level">Entry Level</option>
                        <option value="Mid level">Mid level</option>
                        <option value="Management">Management</option>
                        <option value="Senior Management">Senior Management</option>
                        <option value="Executive">Executive</option>
                    </select>
                  </div>
        </div>
        <div class="col">
         <div class="form-group form-spacing">
            <label for="company_name" style="color: #80CFFF">Client</label>
              <select name="clients" class="form-control" value="">
                <option value="">Select Client</option>
                  @foreach($clients as $client)
                <option value="{{$client->ClientID}}">{{$client->CompanyName}}</option>
                  @endforeach
              </select>
        </div>
        </div>
        <div class="col">
            <div class="form-group form-spacing">
                    <label for="company_name" style="color: #80CFFF">Industry</label>
                     <select name="industry" class="form-control" value="">
                         <option  value="">Select Job Industry</option>
                        @foreach($industries as $industry)

              <option value="{{$industry->Name}}">{{$industry->Name}}</option>
              @endforeach
            </select>
               </div>
        </div>
      </div>

      <div class="text-right">
         <button class="btn pull-right"  onclick="filterJobs()" style="background-color: #FFCB00;color: #000" >SEARCH</button>
         <button class="btn pull-right"  onClick="window.location.reload();" style="background-color: #FFCB00;color: #000" >RESET</button>
       </div>
    </div>
  </div> 
 </div>
   <div class="card">
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered table-striped table-hover" id="dataTable" width="100%" cellspacing="0">
        <thead style="background-color: #FFCB00">
          <tr>
          <th style="color: #000">ID</th>
          <th style="color: #000">Job Title</th>
          <th style="color: #000">Category</th>
          <th style="color: #000">Client</th>
          <th style="color: #000">Job Type</th>
          <th style="color: #000">Country</th>         
          <th style="color: #000">Salary</th>
          <th style="color: #000">Deadline</th>
          <th style="color: #000">Actions</th>
            </tr>
        </thead>
        </table>
    </div>  
  </div>
 </div><br>
 <div class="card" style="background-color: #1C2E5C">
  <div class="card-body">
    <label style="color: #80CFFF">MY INTERVIEW MANAGEMENT</label>
    <div id="filterForm7">
    <div class="card-body">
    <div class="row">
      <div class="col">
        <div class="form-group form-spacing">
          <label style="color: #80CFFF">First Name</label>
          <input type="text" name="firstname" class="form-control" placeholder="First Name">
        </div>
      </div>
      <div class="col">
        <div class="form-group form-spacing">
          <label style="color: #80CFFF">Last Name</label>
          <input type="text" name="lastname" class="form-control" placeholder="Last Name">
        </div>
      </div>
      <div class="col">
        <div class="form-group form-spacing">
          <label style="color: #80CFFF">Job Title</label>
          <select id="jobad" name="jobad" class="form-control">
            <option value="">Select Job Title</option>
            @foreach($jobs as $job)
            <option value="{{$job->ID}}">{{$job->JobTitle}}</option>
            @endforeach
          </select>
        </div>
      </div>
    </div>

    <div class="row">

      <div class="col">
        <div class="form-group form-spacing">
          <label style="color: #80CFFF">Interview Date</label>
          <input type="date" name="interviewdate" class="form-control">
        </div>
      </div>
      <div class="col">
             <div class="form-group form-spacing">
                <label style="color: #80CFFF">Status</label>
                  <select class="form-control" name="status" id="status" value="">
                    <option value="">Filter By Status</option>
                    <option value="Scheduled">Scheduled</option>
                    <option value="Interviewed">Interviewed</option>
                    <option value="Shortlisted">Shortlisted</option>
                    <option value="Recommended">Recommended</option>
                    <option value="Placed">Placed</option>
                    <option value="Discard">Discard</option>
                    <option value="Blacklist">Blacklist</option>
                  </select>
            </div>
      </div>
   </div>
   {{Auth::user()->SummitStaff->StaffID}}

  <div class="col text-right">
      <button class="btn  pull-right" onclick="filterInterview()" style="background-color:  #FFCB00;color:#3069AB">SEARCH</button>
      <button class="btn pull-right"  onClick="window.location.reload();" style="background-color: #FFCB00;color: #000" >RESET</button>
  </div>
  </div>
  </div>
  </div>
 </div><br>
  <div class="card">
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered table-striped table-hover" id="interviews" width="100%" cellspacing="0">
        <thead style="background-color: #FFCB00">
          <tr>
          <th style="color: #000">#</th>
          <th style="color: #000">Candidate</th>
          <th style="color: #000">Job Ad</th>
          <th style="color: #000">Consultant</th>         
          <th style="color: #000">Status</th>          
          <th style="color: #000">Interview Date</th>
          <th style="color: #000">Actions</th>
          </tr>
        </thead>
      </table>
    </div>  
  </div>
 </div>

<style>
.progress {
  width: 150px;
  height: 150px;
  background: none;
  position: relative;
}
.progress::after {
  content: "";
  width: 100%;
  height: 100%;
  border-radius: 50%;
  border: 6px solid #eee;
  position: absolute;
  top: 0;
  left: 0;
}
.progress>span {
  width: 50%;
  height: 100%;
  overflow: hidden;
  position: absolute;
  top: 0;
  z-index: 1;
}
.progress .progress-left {
  left: 0;
}
.progress .progress-bar {
  width: 100%;
  height: 100%;
  background: none;
  border-width: 6px;
  border-style: solid;
  position: absolute;
  top: 0;
}
.progress .progress-left .progress-bar {
  left: 100%;
  border-top-right-radius: 80px;
  border-bottom-right-radius: 80px;
  border-left: 0;
  -webkit-transform-origin: center left;
  transform-origin: center left;
}
.progress .progress-right {
  right: 0;
}
.progress .progress-right .progress-bar {
  left: -100%;
  border-top-left-radius: 80px;
  border-bottom-left-radius: 80px;
  border-right: 0;
  -webkit-transform-origin: center right;
  transform-origin: center right;
}
.progress .progress-value {
  position: absolute;
  top: 0;
  left: 0;
}
body {
  background: #ff7e5f;
  background: -webkit-linear-gradient(to right, #ff7e5f, #feb47b);
  background: linear-gradient(to right, #ff7e5f, #feb47b);
  min-height: 100vh;
}
.rounded-lg {
  border-radius: 1rem;
}
.text-gray {
  color: #aaa;
}
div.h4 {
  line-height: 1rem;
}

.daterangepicker.dropdown-menu.ltr.show-calendar.opensright{
    top: 220px;
    left: 547.812px !important;
    right: auto;
    
}
.daterangepicker:before, .daterangepicker:after {
    position: absolute;
    display: inline-block;
    border-bottom-color: rgba(0, 0, 0, 0.2);
    content: none !important;
}

</style>


@endsection

@section('footer-js')
<script>
var dataTable;
let baseUrl = "{!! route('dashboard.json') !!}?";

function encodeQueryData(data) {
 const ret = [];
 for (let d in data)
   ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
 return ret.join('&');
}

function filterJobs(){
  const type = document.querySelector('#filterForm select[name=jobtype]').value

  const careerlevel = document.querySelector('#filterForm select[name=careerlevel]').value

  const education = document.querySelector('#filterForm select[name=education]').value

  const jobtitle = document.querySelector('#filterForm input[name=jobtitle]').value

  const category = document.querySelector('#filterForm select[name=industry]').value

  const clients = document.querySelector('#filterForm select[name=clients]').value


  const url = baseUrl  + "&" + encodeQueryData({type,careerlevel,education,jobtitle,category,clients})

  console.log(url);
  dataTable.ajax.url( url ).load();
}


  $(document).ready(function(){
dataTable= $('#dataTable').DataTable({
    responsive: true,
    "deferRender": true,
    "processing": true,
    "serverSide": true,
    "ordering": true, //disable column ordering
    "lengthMenu": [
      [50, 100, 150, 200, 250, -1],
      [50, 100, 150, 200, 250, "All"] // change per page values here
    ],
    "pageLength": 10,
    "ajax": {
       url: baseUrl,
      method: 'GET'
    },
    // dom: '<"html5buttons"B>lTfgitp',
    "dom": "<'row' <'col-md-12'>><'row'<'col-md-8 col-sm-12'lB><'col-md-4 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
    buttons: [
      { extend: 'copy',exportOptions: {columns: [0, 1, 2, 3,4,5,6]}},
      {extend: 'csv',exportOptions: {columns: [0, 1, 2, 3,4,5,6]}},
      {extend: 'excel', title: '{{ config('app.name', 'Summit') }} - List of all Users',exportOptions: {columns: [0, 1, 2, 3,4,5]}},
      {extend: 'pdf', title: '{{ config('app.name', 'Summit') }} - List of all Users',exportOptions: {columns: [0, 1, 2, 3,4,5]}},
      {extend: 'print',
      customize: function (win){
        $(win.document.body).addClass('white-bg');
        $(win.document.body).css('font-size', '10px');
        $(win.document.body).find('table')
        .addClass('compact')
        .css('font-size', 'inherit');
      }
    }
  ],
  columns: [
    {data: 'ID', name: 'ID', orderable: true, searchable: true},
    {data: 'JobTitle', name: 'JobTitle', orderable: true, searchable: true},
    {data: 'Category', name: 'Category', orderable: true, searchable: true},
    {data: 'client', name: 'client', orderable: true, searchable: false},
    {data: 'JobType', name: 'JobType', orderable: false, searchable: false},
    {data: 'LocationCountry', name: 'LocationCountry', orderable: true, searchable: false},
    {data: 'full_salary', name: 'full_salary', orderable: true, searchable: false},
    {data: 'Deadline', name: 'Deadline', orderable: true, searchable: false},
    {data: 'Actions', name: 'tools', orderable: false, searchable: false,"defaultContent": "<i>Not set</i>"},
  ],
});
});

</script>
<script>
var dataTables;

let baUrl = "{!! route('dashboard2.json') !!}?";

function encodeQueryData(data)
{
 const ret = [];
 for (let d in data)
   ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
 return ret.join('&');
}

//Filter method
function filterInterview(){
    //Get company value

    const interviewdate = document.querySelector('#filterForm7 input[name=interviewdate]').value
    const firstname = document.querySelector('#filterForm7 input[name=firstname]').value
    const lastname = document.querySelector('#filterForm7 input[name=lastname]').value
    const jobad = document.querySelector('#filterForm7 select[name=jobad]').value
    const status = document.querySelector('#filterForm7 select[name=status]').value

    //Add to URL
      const url = baUrl  + "&" + encodeQueryData({jobad,interviewdate,status,firstname,lastname})


  console.log(url);
  dataTables.ajax.url( url ).load();
}
$(document).ready(function(){
 dataTables= $('#interviews').DataTable({
    responsive: true,
    "deferRender": true,
    "processing": true,
    "serverSide": true,
    "ordering": true, //disable column ordering
    "lengthMenu": [
      [5, 10, 15, 20, 25, -1],
      [5, 10, 15, 20, 25, "All"] // change per page values here
    ],
    "pageLength": 10,
    "ajax":  {
      url: baUrl,
      method: 'GET'
    },
    // dom: '<"html5buttons"B>lTfgitp',
    "dom": "<'row' <'col-md-12'>><'row'<'col-md-8 col-sm-12'lB><'col-md-4 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
    buttons: [
      { extend: 'copy',exportOptions: {columns: [0, 1, 2, 3,4,5,6]}},
      {extend: 'csv',exportOptions: {columns: [0, 1, 2, 3,4,5,6]}},
      {extend: 'excel', title: '{{ config('app.name', 'Summit') }} - List of all Users',exportOptions: {columns: [0, 1, 2, 3,4,5]}},
      {extend: 'pdf', title: '{{ config('app.name', 'Summit') }} - List of all Users',exportOptions: {columns: [0, 1, 2, 3,4,5]}},
      {extend: 'print',
      customize: function (win){
        $(win.document.body).addClass('white-bg');
        $(win.document.body).css('font-size', '10px');
        $(win.document.body).find('table')
        .addClass('compact')
        .css('font-size', 'inherit');
      }
    }
  ],
  columns: [
    {data: 'InterviewID', name: 'InterviewID', orderable: true, searchable: true},
    {data: 'CV_ID', name: 'CV_ID', orderable: true, searchable: true},
    {data: 'JobAD_ID', name: 'JobAD_ID', orderable: true, searchable: true},
    {data: 'StaffID', name: 'StaffID', orderable: false, searchable: false},
    {data: 'Status', name: 'Status', orderable: true, searchable: false},
    {data: 'InterviewDate', name: 'InterviewDate', orderable: true, searchable: false},
    {data: 'Actions', name: 'tools', orderable: false, searchable: false,"defaultContent": "<i>Not set</i>"},
  ],
});
});
</script>

<script>
  $(function() {

  $(".progress").each(function() {

    var value = $(this).attr('data-value');
    var left = $(this).find('.progress-left .progress-bar');
    var right = $(this).find('.progress-right .progress-bar');

    if (value > 0) {
      if (value <= 50) {
        right.css('transform', 'rotate(' + percentageToDegrees(value) + 'deg)')
      } else {
        right.css('transform', 'rotate(180deg)')
        left.css('transform', 'rotate(' + percentageToDegrees(value - 50) + 'deg)')
      }
    }
  })
  function percentageToDegrees(percentage) 
  {
    return percentage / 100 * 360
  }
});
</script>


@endsection