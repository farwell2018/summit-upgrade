@extends('layouts.admin')

@section('title','Subject')


@section('content')

     <div class="row " style="margin-left:8px">
        <a href="{{route('home')}}" style="color:#858796" >
            <span class="text">Home</span> </a> &nbsp; | &nbsp;
        <a href="{{route('subject.index')}}" >
            <span class="text">Subject List</span> </a>
    </div>
    <br>

    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Edit a Subject
                </h3>
            </div>
        </div>
        <!--begin::Form-->
        <form class="kt-form" method="POST" action="{{ route('subject.update',$subject->SubjectID) }}"
              enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="subjectname">Subject Name</label>
                <input id="subjectname" type="text" class="form-control{{ $errors->has('subjectname') ? ' is-invalid' : '' }}" name="subjectname" value="{{ $subjectname->SubjectTitle  }}" required>

                @if ($errors->has('subjectname'))
                    <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('subjectname') }}</strong>
            </span>
                @endif
            </div>

            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="reset" class="btn btn-secondary">Cancel</button>
                </div>
            </div>
        </form>
        <!--end::Form-->
@endsection
