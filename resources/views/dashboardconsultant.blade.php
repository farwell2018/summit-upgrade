@extends('layouts.admin')
@section('title','Consultant-Dashboard')
@section('content')
<div class="row" style="margin-left:8px">
    <h4><span class="text">Consultant Dashboard Panel</span> </a> &nbsp;
 </div><br>
 <div class="card" style="background-color: #1C2E5C">
 	<div class="card-body">
 		<label style="color: #80CFFF">MY JOB ADS</label>
 		<div class="row">
 			<div class="col">
              <div class="form-group form-spacing">
                  <label for="company_name" style="color: #80CFFF">Job Title</label>
                    <input type="text" name="company_name" class="form-control" value="">
             </div>
           </div> 
 			<div class="col">
 				<div class="form-group form-spacing">
 					<label for="education Level" style="color: #80CFFF">Education Level</label>
 					<select name="education" class="form-control" value="">
            <option value=#>Select Education Level</option>
 						<option value="Certificate">Certificate</option>
 						<option value="Diploma">Diploma</option>
 						<option value="Associate Degree">Associate Degree</option>
 						<option value="Bachelor's Degree">Bachelor's Degree</option>
 						<option value="Master's Degree">Master's Degree</option>
 						<option value="Doctorate Degree">Doctorate Degree</option>
 					</select>
 			    </div>
 			 </div>   
 			<div class="col">
 				<div class="form-group form-spacing">
 					<label for="JobType" style="color: #80CFFF">Job Type</label>
 					<select name="" class="form-control" value="">
            <option value="#">Select Job Type</option>
 						<option value="Permanent">Permanent</option>
 						<option value="Part Time">Part Time</option>
 						<option value="Contract">Contract</option>
 					</select>
 			    </div>
 			</div>
 		</div>
 		<div class="row"> 			
 		    <div class="col">
                <div class="form-group form-spacing">
                  <label for="company_name" style="color: #80CFFF">Career Level</label>
                    <select name="careerlevel" class="form-control" value="">
                      <option value="#">Select Career Level</option>
           						<option value="Entry Level">Entry Level</option>            
           						<option value="Mid level">Mid level</option>
           						<option value="Management">Management</option>
           						<option value="Senior Management">Senior Management</option>
           						<option value="Executive">Executive</option>
 					        </select>
                </div>          
 			</div> 			
 			<div class="col">
                <div class="form-group form-spacing">
                  <label for="company_name" style="color: #80CFFF">Client</label>
                    <select name="clients" class="form-control" value="">
                    	@foreach($clients as $client)
                      <option value="#">Select Client</option>
 						<option value="{{$client->ClientID}}">{{$client->CompanyName}}</option>
 						@endforeach
 					</select>
                </div>           
 			</div> 			
 			<div class="col">
                <div class="form-group form-spacing">
                  <label for="company_name" style="color: #80CFFF">Staff</label>
                    <select name="staff" class="form-control" value="">
                    	@foreach($staff as $consultants)
                      <option>Select Consultant</option>
 						<option value="{{$consultants->id}}">{{$consultants->Firstname}}&nbsp{{$consultants->Lastname}}</option>
 						@endforeach
 					</select>
                </div>          
 			</div>
 		</div>
 		<div class="row">
 			<div class="col">
 				  <div class="form-group form-spacing">
                  <label for="company_name" style="color: #80CFFF">Industry</label>
                    <select name="" class="form-control" value="">
                    	@foreach($industries as $industry)
                      <option>Select Job Industry</option>
 						<option value="{{$industry->ID}}">{{$industry->Name}}</option>
 						@endforeach
 					</select>
             </div>
 			</div>
 			<div class="col">
 				<label>View closed jobs</label>
 				
 			</div>
 			<div class="col">
 				<label>View all jobs</label>
 			</div>
 		</div>
    <div class="text-right">
    <button class="btn pull-right" style="background-color: #FFCB00;color: #000" >SEARCH</button>
  </div>
 	</div> 	
 </div><br>
  <div class="card">
 	<div class="card-body">
 		<div class="table-responsive">
	 		<table class="table table-bordered table-striped table-hover" id="dataTable" width="100%" cellspacing="0">
	 			<thead style="background-color: #FFCB00">
	 				<tr>
	 				<th style="color: #000">Job Title</th>
	 				<th style="color: #000">Category</th>
	 				<th style="color: #000">Client</th>
	 				<th style="color: #000">Job Type</th>
	 				<th style="color: #000">City</th>	 				
	 				<th style="color: #000">Salary</th>
	 				<th style="color: #000">Deadline</th>
	 				<th style="color: #000">Actions</th>
	 			    </tr>
	 			</thead>
	 			<tbody>
	 				@foreach($jobs as $row)
	 				<tr>
	 				<td>{{$row->JobTitle}}</td>
	 				<td>{{$row->Category}}</td>
	 				<td>{{$row->Clients->CompanyName}}</td>
	 				<td>{{$row->JobType}}</td>
	 				<td>{{$row->LocationCity}}</td>	 				
	 				<td>{{$row->SalCurrency}}{{$row->GrossMonthSal}}</td>
	 				<td>{{$row->Deadline}}</td>
	 				<td>
	 					<span style="overflow: visible; position: relative; width: 110px;">
	 						<a title="View details" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="#">
                               <i class="fas fa-eye" style="color:#80CFFF"></i>
                            </a>
                            <a title="Edit details" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="#">
                               <i class="fas fa-edit" style="color: #3069AB"></i>
                            </a>
                            <a title="View Applications" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="#">
                               <i class="fas fa-tasks" style="color: #000"></i>
                            </a>
	 					</span>
	 				</td>
	 			    </tr>
	 			    @endforeach
	 			</tbody>
	 		</table>
	 	</div>
	 	<div class="row pagination-row">
           {{$jobs->links()}}
        </div>	
 	</div>
 </div><br><
 <div class="card" style="background-color: #1C2E5C">
 	<div class="card-body">
 		<label style="color: #80CFFF">MY INTERVIEW MANAGEMENT</label>
 		<div class="row">
 			<div class="col">
              <div class="form-group form-spacing">
                  <label for="company_name" style="color: #80CFFF">First Name</label>
                    <input type="text" name="company_name" class="form-control" value="">
             </div>
           </div>
           <div class="col">
              <div class="form-group form-spacing">
                  <label for="company_name" style="color: #80CFFF">Last Name</label>
                    <input type="text" name="company_name" class="form-control" value="">
             </div>
           </div>
           <div class="col">
              <div class="form-group form-spacing">
                  <label for="company_name" style="color: #80CFFF">Job Title</label>                  
                    <select name="" class="form-control" value="">
                    	@foreach($jobs as $job)
 						<option value="{{$job->ID}}">{{$job->JobTitle}}</option>
 						@endforeach
 					</select>
             </div>          
 			</div>
 		</div>
 		<div class="row">
 			<div class="col">
              <div class="form-group form-spacing">
                  <label for="company_name" style="color: #80CFFF">Interviewer</label>                  
                    <select name="" class="form-control" value="">
                    	@foreach($staff as $consultants)
 						<option value="{{$consultants->id}}">{{$consultants->Firstname}}&nbsp{{$consultants->Lastname}}</option>
 				        @endforeach		
 					</select>
             </div>          
 			</div>
 			<div class="col">
              <div class="form-group form-spacing">
                  <label for="company_name" style="color: #80CFFF">Interview Date</label>
                    <input type="date" name="company_name" class="form-control" value="">
             </div>
           </div>
 			<div class="col">
              <div class="form-group form-spacing">
                  <label for="company_name" style="color: #80CFFF">Pay Status</label>
                    <select name="" class="form-control" value="">                    	
 						<option value="Yes">Yes</option>
 						<option value="No">No</option> 					
 					</select>
             </div>          
 			</div>
 		</div>
 		<div class="row">
 			<div class="col-4">
 				<div class="form-group form-spacing">
 			       <label for="company_name" style="color: #80CFFF">Application Status</label>
                       <select name="" class="form-control" value="" width="50">
                       	@foreach($interview as $interviews)
						 <option value="{{$interviews->InterviewID}}">{{$interviews->Status}}</option>
						@endforeach
					   </select>
 		        </div>
 			</div>
 		</div> 		
 		<div class="text-right">
 		<button class="btn pull-right" style="background-color:#FFCB00;color:#000;padding-right: " >SEARCH</button>
 	</div>
 	</div>
 </div><br>
 <div class="card">
 	<div class="card-body">
 		<div class="table-responsive">
	 		<table class="table table-bordered table-striped table-hover" id="dataTable" width="100%" cellspacing="0">
	 			<thead style="background-color: #FFCB00">
	 				<tr>
	 				<th style="color: #000">Candidate</th>
	 				<th style="color: #000">Job Ad</th>	 				
	 				<th style="color: #000">Status</th>
	 				<th style="color: #000">Consultant</th>
	 				<th style="color: #000">Interview Date</th>
	 				<th style="color: #000">Actions</th>
	 			    </tr>
	 			</thead>
	 			<tbody>
	 				@foreach($interview as $row)
	 				<tr>
	 				<td>{{$row->candidates->RegDetails->Firstname}}&nbsp{{$row->candidates->RegDetails->Lastname}}</td>
	 				<td>{{$row->jobs->JobTitle}}</td>
	 				<td>{{$row->Status}}</td>
	 				<td></td>	 				
	 				<td>{{$row->InterviewDate}}</td>
	 				<td>
	 					<span style="overflow: visible; position: relative; width: 110px;">
	 						<a title="View details" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="#">
                               <i class="fas fa-eye" style="color:#80CFFF"></i>
                            </a>
                            <a title="Edit details" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="#">
                               <i class="fas fa-edit" style="color: #3069AB"></i>
                            </a>                            
	 					</span>
	 				</td>
	 			    </tr>
	 			    @endforeach
	 			</tbody>
	 		</table>
	 	</div>
	 	<div class="row pagination-row">
          {{$interview->links()}}
        </div>	
 	</div>
 </div>
@endsection