@extends('layouts.admin')

@section('title','Industryfunctions')


@section('content')
 <div class="row " style="margin-left:8px">
        <a href="{{route('home')}}" style="color:#858796" >
            <span class="text">Home</span> </a> &nbsp; | &nbsp;
        <a href="{{route('industryfunctions.index')}}" >
            <span class="text">Industry Function List</span> </a>
    </div>
    <br>
<div class="kt-portlet">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                Add an Industry Function
            </h3>
        </div>
    </div>
    <!--begin::Form-->
    <form class="kt-form" method="POST" action="{{ route('industryfunctions.store') }}"
          enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="industryfunctionsname">Industry Function Name</label>
            <input id="industryfunctionsname" type="text" class="form-control{{ $errors->has('industryfunctionsname') ? ' is-invalid' : '' }}" name="industryfunctionsname" value="{{ old('industryfunctionsname')  }}" required>

            @if ($errors->has('industryfunctionsname'))
            <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('industryfunctions') }}</strong>
            </span>
            @endif
        </div>

    

        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <button type="submit" class="btn btn-primary">Submit</button>
                <button type="reset" class="btn btn-secondary">Cancel</button>
            </div>
        </div>
    </form>
    <!--end::Form-->




    @endsection
