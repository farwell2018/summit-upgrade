@extends('layouts.admin')

@section('title','Job Ads')


@section('content')

<div class="row profile bg-darkblue">
    <div class="col-md-12">
        <div class="container">
            <div id='responseMsg' style="color:red"></div>
        <div class="row profile-details bg-white">
            <div class="col-md-7">
                <h4 class="text-primary">{{$job->JobTitle}} </h4>
                <p class="details-span-first"><b>Monthly Salary:</b> {{$job->SalCurrency}} {{$job->GrossMonthSal}} </p>
                <p class="details-span-first"><b>Location:</b> {{$job->LocationCity}} {{$job->LocationCountry}}</p>
                <p class="details-span-first"><b>Career Level:</b> {{$job->CareerLevel}}</p>
                <p class="details-span-first"><b>Job Category:</b> {{$job->Category}}</p>
                <p class="details-span-first"><b>Education:</b> {{$job->MinEduReq}}</p>
            </div>
            <div class="col-md-5 profile-details-right">
                <a href="{{ route('jobAds.edit',$job->ID) }}"><span class="text-primary edit-contact" style="cursor: pointer;position: absolute;right: 0"><i class="fas fa-edit" style="color: #3069AB"></i></span></a>
                <h5 class="text-primary">Other Information</h5>
                <p class="details-span-first"><b>Job Type:</b> {{$job->JobType}} </p>
                <p class="details-span-first"><b>Deadline: </b> {{$job->Deadline}} </p>
                <p class="details-span-first"><b>Revenue </b> {{$job->Revenue}} </p>
                <p class="details-span-first"><b>Annual Percentage Revenue: {{$job->AnnualPercentageRevenue}} </b> </p>
                @if(!empty($staffassigned))<p class="details-span-first"><b></b>Consultant Assigned:@if(!empty($staffassigned->Firstname)){{$staffassigned->Firstname}}@else No Consultant Firstname @endif @if(!empty($staffassigned->Lastname)){{$staffassigned->Lastname}}@else No Consultant Lastname @endif</p>@endif
            </div>
        </div>
        </div>
    </div>
</div>
    <div class="row">
      <div class="col-md-12">
        <div class="card mb-3">
            <div class="card-header tab-form-header text-white" style="background-color: #1C2E5C">
              Jobs Summary
            </div>
            <div class="card-body">
             <p> {{$job->Summary}}</p>
            </div>
          </div>
      </div>
      <div class="col-md-6">
        <div class="card mb-5">
            <div class="card-header tab-form-header text-white" style="background-color: #1C2E5C">
              Job's Requirements
            </div>
            <div class="card-body jobad-box">
             <ul>
               @foreach($requirements as $requirement)
                <li>{{$requirement->Requirement}}</li>
               @endforeach
              <ul>
            </div>
          </div>
        </div>
        <div class="col-md-6">
        <div class="card mb-5">
            <div class="card-header tab-form-header text-white" style="background-color: #1C2E5C">
              Job's Duties
            </div>
            <div class="card-body jobad-box">
              <ul>
                @foreach($duties as $duty)
                <li>{{$duty->Duty}}</li>
               @endforeach
             </ul>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          
        <div class="card mb-3">
            <div class="card-header tab-form-header text-white" style="background-color: #1C2E5C">
              Job Applicants<a href="#" class="btn pull-right" style="background-color: #FFCB00;color: #000" onclick="jobApplicants(<?php echo $job->ID;?>)">Email Candidates</a></span>
            </div>
            <div class="card-body">
              <table class="table">
                <thead style="background-color: #FFCB00">
                  <th style="color: #000"><input type="checkbox" class="selectall">&nbspFull Name</th>
                  <th style="color: #000">Gender</th>
                  <th style="color: #000">Phone Number</th>
                  <th style="color: #000">Status</th>
                  <th style="color: #000">Email Address</th>
                  <th style="color: #000">Actions</th>
                </thead>
                <tbody>
               @foreach($applications_all as $application)
                <tr>

                 <td><input type="checkbox"  class="applicants">&nbsp @if($application->candidates->RegDetails) {{$application->candidates->RegDetails->Firstname}}&nbsp{{$application->candidates->RegDetails->Lastname}} @endif</td>
                 <td>{{$application->candidates->Gender}}</td>
                 <td>{{$application->candidates->PhoneNumber}}</td>

                 <td>@if(!empty($status)) {{$status->Status}} @else {{'Application Sent'}} @endif</td>
                 @if($application->candidates->RegDetails) <td><a href="mailto:{{$application->candidates->RegDetails->EmailAddress}}">  {{$application->candidates->RegDetails->EmailAddress}} </a></td>@endif
                 <td>
                  <span style="overflow: visible; position: relative; width: 110px;">
                  <a title="View details" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="{{route('candidateCV',[$application->CV_ID, $application->Job_adID])}}">
                    <i class="fas fa-eye" style="color:#80CFFF"></i></a>
                    <a title="Prequal Questions" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="{{route('jobtests.view',[$application->CV_ID, $application->Job_adID])}}"><i class="fas fa-tasks" style="color: #3069AB"></i></a>
                    @if($application->MarkRead == 0)
                    <a href="{{route('candidateCV.mark' ,$application->JobCVID )}}" class="btn btn-sm btn-clean btn-icon btn-icon-sm"  title="Mark as Read" >
                    <i class="fa fa-check" aria-hidden="true" style="color: #5cb85c"></i>
                  </a>
                    @else
                    <a href="{{ route('candidate.unread', $application->JobCVID) }}" class="btn btn-sm btn-clean btn-icon btn-icon-sm"  title="Marked as Read" >
                    <i class="fa fa-times" aria-hidden="true" style="color:#FA0000" ></i>
                  </a>
                    @endif
                    @php $inter = \App\Interview::where('CV_ID',$application->CV_ID)->first(); @endphp 
                    @if($inter)
                    <a title="Set Action" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="{{route('interviewmanagement.edit',[$inter->InterviewID])}}">
                    <i class="fas fa-calendar" style="color: #5cb85c"></i></a>
                    @else
                      <a title="Set Action" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="{{route('candidateCV',[$application->CV_ID, $application->Job_adID])}}">
                    <i class="fas fa-calendar" style="color: #FFCB00"></i></a>
                    @endif
                    <!-- <a title="Send email regrets" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="#"><i class="fas fa-envelope-open-text" style="color: #000"> </i></a> -->
                  </span>
                 </td>
                </tr>
            
                  @endforeach
                </tbody>
              </table>
            <div class="row pagination-row">
            {{$applications_all->links()}}
            </div>
            </div>
          </div>
          <div class="card mb-3">
             <div class="card-header tab-form-header text-white" style="background-color: #1C2E5C">
              Candidates Interviewed<span><a href="#" class="btn pull-right" style="background-color: #FFCB00;color: #000" onclick="jobInterviewed(<?php echo $job->ID;?>)">Email Candidates</a></span>
             </div>
             <div class="card-body">
               <table class="table">
                <thead style="background-color: #FFCB00">
                  <th style="color: #000"><input type="checkbox" class="select-interviewed">&nbspFull Name</th>
                  <th style="color: #000">Gender</th>
                  <th style="color: #000">Phone Number</th>
                  <th style="color: #000">Status</th>
                  <th style="color: #000">Email Address</th>
                  <th style="color: #000">Actions</th>
                </thead>
                <tbody>
                    @foreach($interviewed as $application)
                <tr>
                  <td><input type="checkbox"  class="applicants">&nbsp @if($application->candidates->RegDetails){{$application->candidates->RegDetails->Firstname}}&nbsp{{$application->candidates->RegDetails->Lastname}} @endif</td>
                 <td>{{$application->candidates->Gender}}</td>
                 <td>{{$application->candidates->PhoneNumber}}</td>

                 <td>@if(!empty($status)) {{$status->Status}} @else {{'Application Sent'}} @endif</td>
                 @if($application->candidates->RegDetails)<td><a href="mailto:{{$application->candidates->RegDetails->EmailAddress}}"> {{$application->candidates->RegDetails->EmailAddress}}</a></td>@endif
                 <td>
                  <span style="overflow: visible; position: relative; width: 110px;">
                  <a title="View details" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="{{route('candidateCV',[$application->CV_ID, $application->Job_adID])}}">
                    <i class="fas fa-eye" style="color:#80CFFF"></i></a>
                    <a title="Prequal Questions" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="#"><i class="fas fa-tasks" style="color: #3069AB"></i></a>
                    @if($application->MarkRead == 0)
                    <a href="{{route('candidateCV.mark' ,$application->JobCVID )}}" class="btn btn-sm btn-clean btn-icon btn-icon-sm"  title="Interviewed" >
                    <i class="fa fa-check" aria-hidden="true" style="color: #5cb85c"></i>
                  </a>
                    @else
                    <a href="{{ route('candidate.unread', $application->JobCVID) }}" class="btn btn-sm btn-clean btn-icon btn-icon-sm"  title="Not Interviewed" >
                    <i class="fa fa-times" aria-hidden="true" style="color:#FA0000" ></i>
                  </a>
                    @endif
                    @php $inter = \App\Interview::where('CV_ID',$application->CV_ID)->first(); @endphp 
                    @if($inter)
                    <a title="Set Action" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="{{route('interviewmanagement.edit',[$inter->InterviewID])}}">
                    <i class="fas fa-calendar" style="color: #5cb85c"></i></a>
                    @else
                      <a title="Set Action" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="{{route('candidateCV',[$application->CV_ID, $application->Job_adID])}}">
                    <i class="fas fa-calendar" style="color: #FFCB00"></i></a>
                    @endif
                    <!-- <a title="Send email regrets" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="#"><i class="fas fa-envelope-open-text" style="color: #000"> </i></a> -->
                  </span>
                 </td>
                </tr>
              
                  @endforeach
                </tbody>
               </table>
               <div class="row pagination-row">
              {{$interviewed->links()}}
               </div>
             </div>
          </div>
          <div class="card mb-3">
            <div class="card-header tab-form-header text-white" style="background-color: #1C2E5C">
              Candidates Shortlisted<span><a href="#" class="btn pull-right" style="background-color: #FFCB00;color: #000" onclick="jobShortlisted(<?php echo $job->ID;?>)">Email Candidates</a></span>
            </div>
            <div class="card-body">
              <table class="table">
                <thead style="background-color: #FFCB00">
                  <th style="color: #000"><input type="checkbox" class="select-shortlisted">&nbspFull Name</th>
                  <th style="color: #000">Gender</th>
                  <th style="color: #000">Phone Number</th>
                  <th style="color: #000">Status</th>
                  <th style="color: #000">Email Address</th>
                  <th style="color: #000">Actions</th>
                </thead>
                <tbody>
                    @foreach($shortlisted as $application)
                <tr>
                  <td><input type="checkbox"  class="applicants">&nbsp @if($application->candidates->RegDetails){{$application->candidates->RegDetails->Firstname}}&nbsp{{$application->candidates->RegDetails->Lastname}} @endif</td>
                 <td>{{$application->candidates->Gender}}</td>
                 <td>{{$application->candidates->PhoneNumber}}</td>

                 <td>@if(!empty($status)) {{$status->Status}} @else {{'Application Sent'}} @endif</td>
                 @if($application->candidates->RegDetails)<td><a href="mailto:{{$application->candidates->RegDetails->EmailAddress}}"> {{$application->candidates->RegDetails->EmailAddress}}</a></td>@endif
                 <td>
                  <span style="overflow: visible; position: relative; width: 110px;">
                  <a title="View details" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="{{route('candidateCV',[$application->CV_ID, $application->Job_adID])}}">
                    <i class="fas fa-eye" style="color:#80CFFF"></i></a>
                    <a title="Prequal Questions" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="#"><i class="fas fa-tasks" style="color: #3069AB"></i></a>
                    @if($application->MarkRead == 0)
                    <a href="{{route('candidateCV.mark' ,$application->JobCVID )}}" class="btn btn-sm btn-clean btn-icon btn-icon-sm"  title="Interviewed" >
                    <i class="fa fa-check" aria-hidden="true" style="color: #5cb85c"></i>
                  </a>
                    @else
                    <a href="{{ route('candidate.unread', $application->JobCVID) }}" class="btn btn-sm btn-clean btn-icon btn-icon-sm"  title="Not Interviewed" >
                    <i class="fa fa-times" aria-hidden="true" style="color:#FA0000" ></i>
                  </a>
                    @endif
                    @php $inter = \App\Interview::where('CV_ID',$application->CV_ID)->first(); @endphp 
                    @if($inter)
                    <a title="Set Action" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="{{route('interviewmanagement.edit',[$inter->InterviewID])}}">
                    <i class="fas fa-calendar" style="color: #5cb85c"></i></a>
                    @else
                      <a title="Set Action" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="{{route('candidateCV',[$application->CV_ID, $application->Job_adID])}}">
                    <i class="fas fa-calendar" style="color: #FFCB00"></i></a>
                    @endif
                    <!-- <a title="Send email regrets" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="#"><i class="fas fa-envelope-open-text" style="color: #000"> </i></a> -->
                  </span>
                 </td>
                </tr>
               
                  @endforeach
                </tbody>
               </table>
               <div class="row pagination-row">
                  {{$shortlisted->links()}}
               </div>
            </div>
          </div>
          <div class="card mb-3">
            <div class="card-header tab-form-header text-white" style="background-color: #1C2E5C">
              Candidates Placed<span><a href="#" class="btn pull-right" style="background-color: #FFCB00;color: #000" onclick="jobPlaced(<?php echo $job->ID;?>)">Email Candidates</a></span>
            </div>
            <div class="card-body">
              <table class="table">
                <thead style="background-color: #FFCB00">
                  <th style="color: #000"><input type="checkbox" class="select-placed">&nbspFull Name</th>
                  <th style="color: #000">Gender</th>
                  <th style="color: #000">Phone Number</th>
                  <th style="color: #000">Status</th>
                  <th style="color: #000">Email Address</th>
                  <th style="color: #000">Actions</th>
                </thead>
                <tbody>
                  @foreach($placed as $application)
                <tr>
                  <td><input type="checkbox"  class="applicants">&nbsp @if($application->candidates->RegDetails){{$application->candidates->RegDetails->Firstname}}&nbsp{{$application->candidates->RegDetails->Lastname}} @endif</td>
                 <td>{{$application->candidates->Gender}}</td>
                 <td>{{$application->candidates->PhoneNumber}}</td>

                 <td>@if(!empty($status)) {{$status->Status}} @else {{'Application Sent'}} @endif</td>
                 @if($application->candidates->RegDetails)<td><a href="mailto:{{$application->candidates->RegDetails->EmailAddress}}"> {{$application->candidates->RegDetails->EmailAddress}}</a></td>@endif
                 <td>
                  <span style="overflow: visible; position: relative; width: 110px;">
                  <a title="View details" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="{{route('candidateCV',[$application->CV_ID, $application->Job_adID])}}">
                    <i class="fas fa-eye" style="color:#80CFFF"></i></a>
                    <a title="Prequal Questions" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="#"><i class="fas fa-tasks" style="color: #3069AB"></i></a>
                    @if($application->MarkRead == 0)
                    <a href="{{route('candidateCV.mark' ,$application->JobCVID )}}" class="btn btn-sm btn-clean btn-icon btn-icon-sm"  title="Interviewed" >
                    <i class="fa fa-check" aria-hidden="true" style="color: #5cb85c"></i>
                  </a>
                    @else
                    <a href="{{ route('candidate.unread', $application->JobCVID) }}" class="btn btn-sm btn-clean btn-icon btn-icon-sm"  title="Not Interviewed" >
                    <i class="fa fa-times" aria-hidden="true" style="color:#FA0000" ></i>
                  </a>
                    @endif
                    @php $inter = \App\Interview::where('CV_ID',$application->CV_ID)->first(); @endphp 
                    @if($inter)
                    <a title="Set Action" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="{{route('interviewmanagement.edit',[$inter->InterviewID])}}">
                    <i class="fas fa-calendar" style="color: #5cb85c"></i></a>
                    @else
                      <a title="Set Action" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="{{route('candidateCV',[$application->CV_ID, $application->Job_adID])}}">
                    <i class="fas fa-calendar" style="color: #FFCB00"></i></a>
                    @endif
                    <!-- <a title="Send email regrets" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="#"><i class="fas fa-envelope-open-text" style="color: #000"> </i></a> -->
                  </span>
                 </td>
                </tr>
              
                  @endforeach
                </tbody>
               </table>
               <div class="row pagination-row">
                  {{$placed->links()}}
               </div>
            </div>
          </div>
      </div>
@endsection
@section('footer-js')
<script>
$(document).ready(function(){
  $(".selectall").click(function(){
  $(".applicants").prop("checked",$(this).prop("checked"));
  });

  $(".select-interviewed").click(function(){
  $(".interviewed").prop("checked",$(this).prop("checked"));
  });

  $(".select-shortlisted").click(function(){
  $(".shortlisted").prop("checked",$(this).prop("checked"));
  });

  $(".select-placed").click(function(){
  $(".placed").prop("checked",$(this).prop("checked"));
  });
});
</script>

<script>
function jobApplicants(jobid){

    var url = "{{url('/admin/jobAds/sendmail')}}";
    var jobid = jobid;
    var group = 'applicants';
    var token =  "{{ csrf_token()}}"

  $.ajax({
      url: url,
      type: 'POST',
      data:{'jobid':jobid, 'group':group, '_token':token},
      success: function(res)
      {
          if(res.msg){
              $('#responseMsg').html(res.msg);
          }else if(res.error){
              $('#errorMsg').html(res.error);
          }

      },

  });
}

function jobInterviewed(jobid){

    var url = "{{url('/admin/jobAds/sendmail')}}";
    var jobid = jobid;
    var group = 'interviewed';
    var token =  "{{ csrf_token()}}"

  $.ajax({
      url: url,
      type: 'POST',
      data:{'jobid':jobid, 'group':group, '_token':token},
      success: function(res)
      {
          if(res.msg){
              $('#responseMsg').html(res.msg);
          }else if(res.error){
              $('#errorMsg').html(res.error);
          }

      },

  });
}
function jobShortlisted(jobid){

    var url = "{{url('/admin/jobAds/sendmail')}}";
    var jobid = jobid;
    var group = 'shortlisted';
    var token =  "{{ csrf_token()}}"

  $.ajax({
      url: url,
      type: 'POST',
      data:{'jobid':jobid, 'group':group, '_token':token},
      success: function(res)
      {
          if(res.msg){
              $('#responseMsg').html(res.msg);
          }else if(res.error){
              $('#errorMsg').html(res.error);
          }

      },

  });
}
function jobPlaced(jobid){

    var url = "{{url('/admin/jobAds/sendmail')}}";
    var jobid = jobid;
    var group = 'placed';
    var token =  "{{ csrf_token()}}"

  $.ajax({
      url: url,
      type: 'POST',
      data:{'jobid':jobid, 'group':group, '_token':token},
      success: function(res)
      {
          if(res.msg){
              $('#responseMsg').html(res.msg);
          }else if(res.error){
              $('#errorMsg').html(res.error);
          }

      },

  });
}
</script>

@endsection
