@extends('layouts.admin')

@section('title','Job Ads')


@section('content')
<div class="row">
  <div class="col">
<div class="row " style="margin-left:8px">
        <a href="{{route('home')}}" style="color:#858796" >
            <span class="text">Dashboard</span> </a> &nbsp; | &nbsp;
            <span class="text">Job Ads Management Board</span> </a>&nbsp;

    </div>
  </div>
  <div class="col">
 <div class="row user-add-button">
  <a href="{{route('jobAds.create')}}" class="btn btn-icon-split"  style="margin-right: 15px;background-color: #FFCB00;color:#3069AB">
  <span class="icon" style="color:"><i class="fas fa-plus"></i></span>
  <span class="text">New Job Ad</span> </a>
</div>
</div>
</div><br>
<div class="card" style="background-color: #1C2E5C;align-content: center;">
<div class="card-body">
  <label style="color: #80CFFF">JOB ADS</label>
  <div id="filterForm">
    <div class="row">
      <div class="col">
        <div class="form-group form-spacing">
            <label for="company_name" style="color: #80CFFF">Job Title</label>
              <input type="text" name="jobtitle" class="form-control" value="">
        </div>
      </div>
      <div class="col">
         <div class="form-group form-spacing">
          <label for="educationLevel" style="color: #80CFFF">Education Level</label>
          <select id="education" name="education" class="form-control{{$errors->has('education')?'is-invalid':''}}">
            <option value=""> Select Education Level</option>
            <option value="Certificate">Certificate</option>
            <option value="Diploma">Diploma</option>
            <option value="Bachelor's Degree">Bachelor's Degree</option>
            <option value="Master's Degree">Master's Degree</option>
            <option value="Doctrate Degree">Doctrate Degree</option>
          </select>
         </div>
      </div>
      <div class="col">
        <div class="form-group form-spacing">
          <label for="JobType" style="color: #80CFFF">Job Type</label>
            <select class="form-control" name="jobtype" id="jobtype" value="">
              <option value="">Filter By Job Type</option>
              <option value="Permanent">Permanent</option>
              <option value="Part Time">Part Time</option>
              <option value="Contract">Contract</option>
            </select>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col">
         <div class="form-group form-spacing">
                  <label for="company_name" style="color: #80CFFF">Career Level</label>
                    <select name="careerlevel" class="form-control" value="">
                      <option value="" >Select Career Level</option>
                      <option value="Entry Level">Entry Level</option>
                      <option value="Mid level">Mid level</option>
                      <option value="Management">Management</option>
                      <option value="Senior Management">Senior Management</option>
                      <option value="Executive">Executive</option>
                  </select>
                </div>
      </div>
      <div class="col">
       <div class="form-group form-spacing">
          <label for="company_name" style="color: #80CFFF">Client</label>
            <select name="clients" class="form-control" value="">
              <option value="">Select Client</option>
                @foreach($clients as $client)
              <option value="{{$client->ClientID}}">{{$client->CompanyName}}</option>
                @endforeach
            </select>
      </div>
      </div>
      <div class="col">
         <div class="form-group form-spacing">
            <label for="company_name" style="color: #80CFFF">Staff</label>
              <select name="staff" class="form-control" value="">
                <option value="">Select Consultant</option>
                @foreach($staff as $staffs)
                <option value="{{$staffs->StaffID}}">{{$staffs->Firstname}}&nbsp{{$staffs->Lastname}}</option>
                 @endforeach
              </select>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col">
          <div class="form-group form-spacing">
                  <label for="company_name" style="color: #80CFFF">Industry</label>
                   <select name="industry" class="form-control" value="">
                       <option  value="">Select Job Industry</option>
                    	@foreach($industries as $industry)

 						<option value="{{$industry->Name}}">{{$industry->Name}}</option>
 						@endforeach
 					</select>
             </div>
      </div>
      <div class="col" style="padding-left: 45px ">
       <label style="color: #80CFFF">View Closed Jobs</label>
       <div class="form-group form-spacing">

         <select name="closed_jobs" class="form-control" value="">
             <option  value="">Select </option>
            <option value="yes">Yes</option>
            <option value="no">No</option>
        </select>
       </div>
     </div>
      <div class="col" style="padding-left: 30px ">
       <label style="color: #80CFFF">View all jobs</label>
       <div class="form-group form-spacing">
           <select name="all_jobs" class="form-control" value="">
             <option  value="">Select </option>
            <option value="yes">Yes</option>
            <option value="no">No</option>
        </select>
         <!--<input type="radio" class="radio" name="all" value="all" id="all" />-->
         <!--   <label for="all" style="color: #80CFFF">Yes</label>-->
         <!--   <input type="radio" class="radio" name="all" value="no" id="no" />-->
         <!--   <label for="no" style="color: #80CFFF">No</label>-->
       </div>
     </div>
    </div>
    <div class="text-right">
       <button class="btn pull-right"  onclick="filterJobs()" style="background-color: #FFCB00;color: #000" >SEARCH</button>
       <button class="btn pull-right"  onClick="window.location.reload();" style="background-color: #FFCB00;color: #000;margin-right:5px" >RESET</button>
     </div>
  </div>
</div>
</div><br>
<div class="row">
  <div class="col">
    <div class="buttonfilter">
      <div class="row">
        <div class="col">
          <p>
             <!--<a href="javascript:;" class="btn" onmousedown="toggleDiv('mydiv');"style="background-color: #FFCB00;color: #000" >Switch Orientation</a>-->
            <!-- <button class="btn pull-right" style="background-color: #FFCB00;color: #000" >TABULAR VIEW</button>
             <button class="btn pull-right" onclick="myFunction()"style="background-color: #FFCB00;color: #000" >LIST VIEW</button>  -->
          <!--</p>          -->
        </div>
        <div class="col">
            <p>  
           
         </p>
        </div>
      </div>
    </div>
  </div>
  <div class="col">
    
  </div>
</div>
<div class="buttonorientation"> 
</div><br>
<!--  <p>
<button class="btn pull-right" style="background-color: #FFCB00;color: #000" >SWITCH ORIENTATION</button>  
</p><br> -->
<div class="card mb-5">
    <div class="card-header tab-form-header text-white" style="background-color: #1C2E5C">
      JOB AD MANAGEMENT
    </div>
    <div id="mydivon" style="display:block">
      <div class="card-body">
        <table class="table table-bordered table-striped table-hover dt-responsive" id="dataTable" width="100%">
          <thead style="background-color: #FFCB00">
            <tr>
              <th>#</th>
              <th style="color: #000">Job Title</th>
              <th style="color: #000">Category</th>
              <th style="color: #000">Clients</th>
              <th style="color: #000">Job type</th>
              <th style="color: #000">Country</th>
              <th style="color: #000">Salary</th>
              <th style="color: #000">Deadline Date</th>
              <th style="color: #000">Actions</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
    <div id="mydivoff" style="display:none">
    <div class="row">
      @foreach($jobs as $job)
        <div class="col-6">
          <div class="card">
            <div class="card-header tab-form-header text-white" style="background-color: #1C2E5C ">
              {{$job->JobTitle}}
              @if(Auth::user()->role == 0)
              <div class="row">
                <div class="col">
                  <a title="View Job" href="{{route('jobAds.show',$job->ID)}}"><span class="text-primary view-contact"><i class="fas fa-eye" style="color: #fff"></i></span></a><br>
                </div>
                <div class="col">
                  <a title="Edit Job" href="{{route('jobAds.edit', $job->ID)}}"><span class="text-primary edit-contact"><i class="far fa-edit" style="color: #fff"></i></span></a><br>
                </div>
                <div class="col">
                  <a title="Delete Job" href="#"><span class="text-primary delete-contact" ><i class="fas fa-trash-alt" style="color:#fff"></i></span></a>
                </div>
              </div>
              @else
              <div class="row">
                <div class="col">
                  <a title="View Job" href="{{route('jobAds.show',$job->ID)}}"><span class="text-primary view-contact"><i class="fas fa-eye" style="color: #fff"></i></span></a><br>
                </div>
                <div class="col">
                  <a title= "Edit Job" href="{{route('jobAds.edit', $job->ID)}}"><span class="text-primary edit-contact"><i class="far fa-edit" style="color: #fff"></i></span></a><br>
                </div>
              </div>
              @endif
            </div>
            <div class="card-body">
                <ul>
                   <p><strong>Company Name:</strong>&nbsp{{$job->Clients->CompanyName}}</p>
                   <p><strong>Salary:</strong>&nbsp{{$job->SalCurrency.','.$job->GrossMonthSal}}</p>
                   <p><strong>Job Location:</strong>&nbsp{{$job->LocationCountry}}</p>
                   <p><strong>Career Level:</strong>&nbsp{{$job->CareerLevel}}</p>
                   <p><strong>Job Category:</strong>&nbsp{{$job->Category}}</p>
                   <p><strong>Requirements:</strong>&nbsp{{$job->MinEduReq}}</p>
                   <p><strong>Job Details: </strong>&nbsp{{$job->Summary}}</p>
                   <div class="text-center">
                     <a href="{{route('jobAds.viewApplicants',$job->ID)}}" class="btn"align="center" style="background-color: #0EA616;color: #fff">View Applicants</a>
                   </div>          
                </ul>
            </div>
          </div>
        </div>
      @endforeach       
    </div>
    <div class="row pagination-row">
             {{$jobs->links()}}
      </div>  
    </div>
</div>
@endsection
@section('footer-js')
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<style>
    .bs-example{
        margin: 20px;
    }
    .accordion .fa{
        margin-right: 0.5rem;
    }
</style>
<script>
  function toggleDiv(divid)
  {
 
    varon = divid + 'on';
    varoff = divid + 'off';
 
    if(document.getElementById(varon).style.display == 'block')
    {
    document.getElementById(varon).style.display = 'none';
    document.getElementById(varoff).style.display = 'block';
    }
   
    else
    {  
    document.getElementById(varoff).style.display = 'none';
    document.getElementById(varon).style.display = 'block'
    }
} 
</script>
<script>
function myFunction() {
  var x = document.getElementById("listview");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
</script>
<script>
var dataTable;
let baseUrl = "{!! route('jobAds.json') !!}?";

function encodeQueryData(data) {
 const ret = [];
 for (let d in data)
   ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
 return ret.join('&');
}
//Filter method
function filterJobs(){
  const type = document.querySelector('#filterForm select[name=jobtype]').value

  const careerlevel = document.querySelector('#filterForm select[name=careerlevel]').value

  const education = document.querySelector('#filterForm select[name=education]').value

  const jobtitle = document.querySelector('#filterForm input[name=jobtitle]').value

  const category = document.querySelector('#filterForm select[name=industry]').value

  const clients = document.querySelector('#filterForm select[name=clients]').value

  const staff = document.querySelector('#filterForm select[name=staff]').value

  const closed_jobs = document.querySelector('#filterForm select[name=closed_jobs]').value
  
  const all_jobs = document.querySelector('#filterForm select[name=all_jobs]').value


  const url = baseUrl  + "&" + encodeQueryData({type,careerlevel,education,jobtitle,category,clients,staff,closed_jobs,all_jobs})

  console.log(url);
  dataTable.ajax.url( url ).load();
}
$(document).ready(function(){
    dataTable= $('#dataTable').DataTable({
    responsive: true,
    "deferRender": true,
    "processing": true,
    "serverSide": true,
    "ordering": false, //disable column ordering
    "lengthMenu": [
      [25, 50, 100,  -1],
      [25, 50, 100, "All"] // change per page values here
    ],
    "pageLength": 25,
    "ajax": {
       url: baseUrl,
      method: 'GET'
    },
    // dom: '<"html5buttons"B>lTfgitp',
    "dom": "<'row' <'col-md-12'>><'row'<'col-md-8 col-sm-12'lB><'col-md-4 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
    buttons: [
      { extend: 'copy',exportOptions: {columns: [0, 1, 2, 3,4,5,6]}},
      {extend: 'csv',exportOptions: {columns: [0, 1, 2, 3,4,5,6]}},
      {extend: 'excel', title: '{{ config('app.name', 'Summit') }} - List of all Users',exportOptions: {columns: [0, 1, 2, 3,4,5]}},
      {extend: 'pdf', title: '{{ config('app.name', 'Summit') }} - List of all Users',exportOptions: {columns: [0, 1, 2, 3,4,5]}},
      {extend: 'print',
      customize: function (win){
        $(win.document.body).addClass('white-bg');
        $(win.document.body).css('font-size', '10px');
        $(win.document.body).find('table')
        .addClass('compact')
        .css('font-size', 'inherit');
      }
    }
  ],
  columns: [
    {data: 'ID', name: 'ID', orderable: true, searchable: true},
    {data: 'JobTitle', name: 'JobTitle', orderable: true, searchable: true},
    {data: 'Category', name: 'Category', orderable: true, searchable: true},
    {data: 'client', name: 'client', orderable: true, searchable: true},
    {data: 'JobType', name: 'JobType', orderable: false, searchable: true},
    {data: 'LocationCountry', name: 'LocationCountry', orderable: true, searchable: true},
    {data: 'full_salary', name: 'full_salary', orderable: true, searchable: false},
    {data: 'Deadline', name: 'Deadline', orderable: true, searchable: false},
    {data: 'tools', name: 'tools', orderable: false, searchable: false,"defaultContent": "<i>Not set</i>"},
  ],
});
});
</script>
@endsection
