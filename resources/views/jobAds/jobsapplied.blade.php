@extends('layouts.admin')

@section('title','Job Ads')


@section('content')

 <div class="card mb-3">
    <div class="card-header tab-form-header text-white" style="background-color: #1C2E5C">
      Jobs Applied
    </div>
    <div class="card-body">
        <table class="table table table-bordered table-striped table-hover dt-responsive wrap" id="jobsapplied" width="100%">
          <thead style="background-color: #FFCB00">
            <tr>              
              <th style="color: #000">Job Title:</th>
              <th style="color: #000">Category</th>         
              <th style="color: #000">Job Type</th>
              <th style="color: #000">Country</th>
              <th style="color: #000">Salary</th>
              <th style="color: #000">Career Level</th>
              <th style="color: #000">Deadline</th>
              <th style="color: #000">Actions</th>
            </tr>
          </thead>
          <tbody>
              @foreach($jobCV as $jobcvs)
          	<tr>          		
          		<td>{{$jobcvs->JobTitle}}</td>
          		<td>{{$jobcvs->Category}}</td>
          		<td>{{$jobcvs->JobType}}</td>
          		<td>{{$jobcvs->LocationCountry }}</td>
          		<td>{{$jobcvs->GrossMonthSal}}</td>          		
          		<td>{{$jobcvs->CareerLevel}}</td>
          		<td>{{$jobcvs->Deadline}}</td>
          		<td>
          			<span style="overflow: visible; position: relative; width: 110px;">
          			<a title="View details" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="{{route('jobAds.show',[$jobcvs->ID])}}">
                           <i class="fas fa-eye" style="color:#80CFFF"></i></a>
                    <a title="Edit Job" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="{{route('jobAds.edit',[$jobcvs->ID])}}">
                           <i class="fas fa-edit" style="color:#3069AB"></i></a>
                    <a title="View Applications" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="{{route('jobAds.viewApplicants',[$jobcvs->ID])}}">
                            <i class="fas fa-users" style="color:##000"></i></a>              
          			</span>
          		</td>          		
          	</tr>
          	@endforeach
          </tbody>
        </table>
    </div>
</div>


@endsection