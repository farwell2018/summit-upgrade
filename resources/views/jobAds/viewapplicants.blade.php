@extends('layouts.admin')

@section('title','Job Ads')
@php
$skill = array(
array("id" => "Business_Management ", "name" => "Business Management"),
array("id" => "Computer", "name" => "Computer"),
array("id" => "Construction", "name" => "Construction"),
array("id" => "Customer_Service", "name" => "Customer Service"),
array("id" => "Diplomacy", "name" => "Diplomacy"),
array("id" => "Effective_Listening", "name" => "Effective Listening"),
array("id" => "Financial_Management", "name" => "Financial Management"),
array("id" => "Interpersonal", "name" => "Interpersonal"),
array("id" => "Multi-tasking", "name" => "Multi-tasking"),
array("id" => "Negotiating", "name" => "Negotiating"),
array("id" => "Organisation", "name" => "Organisation"),
array("id" => "People_Management", "name" => "People Management"),
array("id" => "Planning", "name" => "Planning"),
array("id" => "Presentation", "name" => "Presentation"),
array("id" => "Problem_Solving", "name" => "Problem Solving"),
array("id" => "Programming", "name" => "Programming"),
array("id" => "Report_Writing", "name" => "Report Writing"),
array("id" => "Research", "name" => "Research"),
array("id" => "Resourcefulness", "name" => "Resourcefulness"),
array("id" => "Sales_Ability", "name" => "Sales Ability"),
array("id" => "Technical", "name" => "Technical"),
array("id" => "Time_Management", "name" => "Time Management"),
array("id" => "Training", "name" => "Training"),
array("id" => "Verbal_Communication", "name" => "Verbal Communication"),
array("id" => "Written_Communication", "name" => "Written Communication"),
);
@endphp

@section('content')
<h4> Applicants for {{$job->JobTitle}}</h4>
<div class="card" style="background-color: #1C2E5C;align-content: center;">
  <div id="filterForm4">
    <div class="card-body">
      <label style="color: #80CFFF">SEARCH CANDIDATES</label>
      <div class="row">
        <div class="col">
         <div class="form-group form-spacing">
            <label for="First Name" style="color: #80CFFF">First Name</label>
              <input type="text" name="firstname" class="form-control" value="">
          </div>
        </div>
        <div class="col">
          <div class="form-group form-spacing">
              <label for="lastname" style="color: #80CFFF">Last Name</label>
                <input type="text" name="lastname" class="form-control" value="">
          </div>
        </div>
        <div class="col">
          <div class="form-group form-spacing">
              <label for="company_name" style="color: #80CFFF">Phone Number</label>
                <input type="text" name="phonenumber" class="form-control" value="">
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col">
           <div class="form-group form-spacing">
               <label for="gender" style="color: #80CFFF">Gender</label>
              <select name="gender" class="form-control" value="">
                <option value="">Select Gender</option>
                <option value="Male">Male</option>
                <option value="Female">Female</option>
              </select>
          </div>
        </div>
        <div class="col">
             <div class="form-group form-spacing">
               <label for="nationality" style="color: #80CFFF">Nationality</label>
                  <select name="nationality" class="form-control" value="">
                    <option value=""> Select Nationality</option>
                    <option value="Kenyan">Kenyan</option>
                    <option value="Non-Kenyan">Non-Kenyan</option>
                  </select>
             </div>
        </div>
        <div class="col">
           <div class="form-group form-spacing">
             <label for="gender" style="color: #80CFFF">Skills</label>
                <select name="skills" class="form-control" value="">
                  <option value=""> Select Skills</option>
                     @foreach($skill as $skill)
                     <option value="{{$skill['id']}} ">{{$skill['name']}}</option>
                     @endforeach
                </select>

          </div>
        </div>
      </div>
      <div class="row">
          <div class="col">
             <div class="form-group form-spacing">
               <label for="education" style="color: #80CFFF">Education Level</label>
                <select name="education" class="form-control" value="">
                  <option value=""> Select Education Level</option>
                  <option value="Certificate">Certificate</option>
                  <option value="Diploma">Diploma</option>
                  <option value="Bachelor's Degree">Bachelor's Degree</option>
                  <option value="Master's Degree">Master's Degree</option>
                  <option value="Doctrate Degree">Doctrate Degree</option>
                </select>
             </div>
          </div>
          <div class="col">
             <div class="form-group form-spacing">
               <label for="hardskill" style="color: #80CFFF">Hard Skills</label>
                <select name="hardskill" class="form-control" value="">
                    <option value="">Select Hard Skill</option>
                  	@foreach($hardskills as $hardskill)
                  	<option value="{{$hardskill->ID}}">{{$hardskill->Name}}</option>
                  @endforeach
                </select>
             </div>
          </div>
          <div class="col">
             <div class="form-group form-spacing">
               <label for="prequaltestresults" style="color: #80CFFF">Pre Qual Test Results</label>
                <select name="marks" class="form-control" value="">
                  <option value="">Select Pre Qual Test</option>
                  <option value="40">Below 50</option>
                  <option value="50">50-69</option>
                  <option value="70">70-89</option>
                  <option value="90">90-100</option>
                </select>
             </div>
          </div>
        </div>
        <div class="text-right">
       <button class="btn pull-right"  onclick="filterCandidates()" style="background-color: #FFCB00;color: #000;margin-left: 15px;margin-bottom: 15px;" >SEARCH</button>
        <button class="btn pull-right"  onClick="window.location.reload();" style="background-color: #FFCB00;color: #000;margin-right:5px" >RESET</button>
     </div>
    </div>    
  </div>
</div><br>
 <div class="card mb-3">
    <div class="card-header tab-form-header text-white" style="background-color: #1C2E5C">
      Candidate Applicants
    </div>
    <div class="card-body">
        <table class="table table table-bordered table-striped table-hover dt-responsive" id="viewapplicants" width="100%">
          <thead style="background-color: #FFCB00">
            <tr>
              <th style="color: #000">Full Name:</th>
              <th style="color: #000">Gender</th>         
              <th style="color: #000">Status</th>
              <th style="color: #000">Candidate Mark</th>
              <th style="color: #000">Phone Number</th>
              <th style="color: #000">Actions</th>
            </tr>
          </thead>
        </table>
    </div>
</div>
@endsection
@section('footer-js')
<script>
var dataTable;
let baseUrl = "{!! route('jobAds.json2', $job->ID) !!}?";

function encodeQueryData(data) {
 const ret = [];
 for (let d in data)
   ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
 return ret.join('&');
}
//Filter method
function filterCandidates(){
  const firstname = document.querySelector('#filterForm4 input[name=firstname]').value

  const lastname = document.querySelector('#filterForm4 input[name=lastname]').value

  const phonenumber = document.querySelector('#filterForm4 input[name=phonenumber]').value

  const gender = document.querySelector('#filterForm4 select[name=gender]').value

  const nationality = document.querySelector('#filterForm4 select[name=nationality]').value

  const education = document.querySelector('#filterForm4 select[name=education]').value

  const marks = document.querySelector('#filterForm4 select[name=marks]').value

  const hardskill = document.querySelector('#filterForm4 select[name=hardskill]').value

  const skills = document.querySelector('#filterForm4 select[name=skills]').value


  const url = baseUrl  + "&" + encodeQueryData({firstname,lastname,phonenumber,gender,nationality,education,skills,marks,hardskill})


  console.log(url);
  dataTable.ajax.url( url ).load();
}
$(document).ready(function(){
    dataTable= $('#viewapplicants').DataTable({
    responsive: true,
    "deferRender": true,
    "processing": true,
    "serverSide": true,
    "ordering": false, //disable column ordering
    "lengthMenu": [
      [25, 50, 100,  -1],
      [25, 50, 100, "All"] // change per page values here
    ],
    "pageLength": 25,
    "ajax": {
       url: baseUrl,
      method: 'GET'
    },
    // dom: '<"html5buttons"B>lTfgitp',
    "dom": "<'row' <'col-md-12'>><'row'<'col-md-8 col-sm-12'lB><'col-md-4 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
    buttons: [
      { extend: 'copy',exportOptions: {columns: [0, 1, 2, 3,4,5,6]}},
      {extend: 'csv',exportOptions: {columns: [0, 1, 2, 3,4,5,6]}},
      {extend: 'excel', title: '{{ config('app.name', 'Summit') }} - List of all Users',exportOptions: {columns: [0, 1, 2, 3,4,5]}},
      {extend: 'pdf', title: '{{ config('app.name', 'Summit') }} - List of all Users',exportOptions: {columns: [0, 1, 2, 3,4,5]}},
      {extend: 'print',
      customize: function (win){
        $(win.document.body).addClass('white-bg');
        $(win.document.body).css('font-size', '10px');
        $(win.document.body).find('table')
        .addClass('compact')
        .css('font-size', 'inherit');
      }
    }
  ],
  columns: [
    {data: 'full_name', name: 'full_name', orderable: true, searchable: true},
    {data: 'Gender', name: 'Gender', orderable: true, searchable: false},
    {data: 'Status', name: 'Status', orderable: true, searchable: false},
    {data: 'CandidateMarks', name: 'CandidateMarks', orderable: true, searchable: false},
    {data: 'PhoneNumber', name: 'PhoneNumber', orderable: true, searchable: false},
    {data: 'Actions', name: 'Actions', orderable: false, searchable: false,"defaultContent": "<i>Not set</i>"},
  ],
});
});
</script>

<script>
$(document).ready(function() {
  $('#viewapplicants tbody').on( 'click', 'td a.mark-reading', function (e) {
    e.preventDefault();
    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
  var element = $(this);
    var url= "{{ route('candidateCV.mark2')}}";
    var id = $(this).attr('data-url');
    var token = "{{ csrf_token() }}";

    $.ajax({
    url: url,
    type: 'POST',
    dataType: 'JSON',
    data: {'id':id},
    success: function(res) {
    if (res.msg) {
      element.removeClass('mark-reading');
      element.empty();
      element.append( ' <i class="fa fa-times" aria-hidden="true" style="color:#FA0000"></i>');
       element.addClass('mark-unreading');
    } else if (res.error) {
       
    }

    },

});



});

});
  </script>


<script>
  $(document).ready(function() {
    $('#viewapplicants tbody').on( 'click', 'td a.mark-unreading', function (e) {
      e.preventDefault();
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var element = $(this);
      var url= "{{ route('candidate.unread2')}}";
      var id = $(this).attr('data-url');
      var token = "{{ csrf_token() }}";
  
      $.ajax({
      url: url,
      type: 'POST',
      dataType: 'JSON',
      data: {'id':id},
      success: function(res) {
      if (res.msg) {
        element.removeClass('mark-unreading');
        element.empty();
        element.append( '<i class="fa fa-check" style="color: #3069AB"></i>');
         element.addClass('mark-reading');
      } else if (res.error) {
         
      }
  
      },
  
  });
  });
  
  });
    </script>
    
    
    
    <script>
$(document).ready(function() {
  $('#viewapplicants tbody').on( 'click', 'td a.mark-blacklisting', function (e) {
    e.preventDefault();
    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
  var element = $(this);
    var url= "{{ route('jobAds.blacklist')}}";
    var id = $(this).attr('data-url');
      var val = 1;
    var token = "{{ csrf_token() }}";

    $.ajax({
    url: url,
    type: 'POST',
    dataType: 'JSON',
    data: {'id':id},
    success: function(res) {
    if (res.msg) {
      element.removeClass('mark-blacklisting');
      element.empty();
      element.append( ' <i class="fa fa-user-times" style="color: #FA0000"></i>');
       element.addClass('mark-unblacklisting');
    } else if (res.error) {
       
    }

    },

});



});

});
  </script>


<script>
  $(document).ready(function() {
    $('#viewapplicants tbody').on( 'click', 'td a.mark-unblacklisting', function (e) {
      e.preventDefault();
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var element = $(this);
      var url= "{{ route('jobAds.blacklist')}}";
      var id = $(this).attr('data-url');
      var val = 0;
      var token = "{{ csrf_token() }}";
  
      $.ajax({
      url: url,
      type: 'POST',
      dataType: 'JSON',
      data: {'id':id},
      success: function(res) {
      if (res.msg) {
        element.removeClass('mark-unblacklisting');
        element.empty();
        element.append( '<i class="fa fa-user" style="color: #5cb85c"></i>');
         element.addClass('mark-blacklisting');
      } else if (res.error) {
         
      }
  
      },
  
  });
  });
  
  });
    </script>
@endsection




