@extends('layouts.admin')

@section('title','JobAds')


@section('content')

 <div class="row " style="margin-left:8px">
        <a href="{{route('home')}}" style="color:#858796" >
            <span class="text">Home</span> </a> &nbsp; | &nbsp;
        <a href="{{route('jobAds.index')}}" >
            <span class="text">Job Ads Management Board  </span> </a>&nbsp; | &nbsp;
            <span class="text">Job Ads Create  </span> </a>
    </div>
    <br>
<div class="kt-portlet">
  <div class="kt-portlet__head">
    <div class="kt-portlet__head-label">
      <h3 class="kt-portlet__head-title">
        Add a New Job Ad
      </h3>
    </div>
  </div>
      @if (session('status'))
          <div class="alert alert-success" role="alert">
              {{ session('status') }}
          </div>
      @endif
  <!--begin::Form-->
  <form class="kt-form" method="POST" action="{{ route('jobAds.store') }}"  enctype="multipart/form-data">
    <div class="kt-portlet__body">

      @csrf
      <div class="row">
          <div class="col">
                 <div class="form-group">
            <label for="client_name">{{ __('Client Name') }}<span class="asterick" style="color:red">*</span></label>
            <select id="client_name" name="client_name" class="form-control{{ $errors->has('client_name') ? ' is-invalid' : '' }}" required>
            <option disabled selected>Select Client</option>
            @foreach($clients as $client)
             @if($id == $client->ClientID)
              <option value="{{$client->ClientID}}" selected>{{$client->CompanyName}}</option>
             @else
              <option value="{{$client->ClientID}}">{{$client->CompanyName}}</option>
             @endif
             @endforeach
          </select>
            @if ($errors->has('client_name'))
            <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('client_name') }}</strong>
            </span>
            @endif
      </div>
          </div>
          <div class="col">
               <div class="form-group">
            <label for="competencies">Key Competencies<span class="asterick" style="color:red">*</span></label><br>
            <select id="competencies" name="competencies[]" class="form-control{{ $errors->has('competencies') ? ' is-invalid' : '' }} selectpicker selectdropdown" required  multiple="multiple" data-width="200px;" data-live-search="true" data-size="10">
                <option value="">Select Competency</option>
              @foreach($competencies as $competency)
              <option value="{{$competency->ID}}">{{$competency->Name}}</option>
              @endforeach
            </select>
             @if ($errors->has('competencies'))
            <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('competencies') }}</strong>
            </span>
            @endif
          </div>

          </div>
      </div>

      <div class="row">
        <div class="col">
             <div class="form-group">
            <label for="job_title">{{ __('Job Title') }}<span class="asterick" style="color:red">*</span></label>
            <input id="job_title" type="name" class="form-control{{ $errors->has('job_title') ? ' is-invalid' : '' }}" name="job_title" value="{{ old('job_title')  }}" required>
            @if ($errors->has('job_title'))
            <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('job_title') }}</strong>
            </span>
            @endif
             </div>
        </div>
        <div class="col">
            <div class="form-group">
              <label for="job_category">{{ __('Industry') }}<span class="asterick" style="color:red">*</span></label>
                <select id="job_category" name="job_category" class="form-control{{ $errors->has('job_category') ? ' is-invalid' : '' }}" required>
                  @foreach($industries as $industry)
                    @if($c->IndustrySector === $industry->Name)
                    <option value="{{$industry->Name}}" selected>{{$industry->Name}}</option>
                    @else
                    <option value="{{$industry->Name}}">{{$industry->Name}}</option>
                    @endif
                    @endforeach
                </select>
                   @if ($errors->has('job_category'))
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('job_category') }}</strong>
                </span>
                @endif
            </div>
        </div>

      </div>
      <div class="form-group">
            <label for="email">{{ __('Summary') }}<span class="asterick" style="color:red">*</span></label>
              <textarea name="summary" row="50" cols="80" class="form-control" required></textarea>
              @if ($errors->has('summary'))
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('summary') }}</strong>
              </span>
              @endif
      </div>
      <div class="form-group">
         <label><strong>Requirements:</strong></label>
            <div id="requirements">
              <div class=" form-group">
                <label for="JobFields_requirements_1" class="required">
                  Requirement 1 <span class="asterick" style="color:red">*</span>
                </label>
                <input size="60" maxlength="255" name="JobFields[requirements][1]" id="JobFields_requirements_1" type="text" value="" class="form-control">
              </div>
            </div><!--requirements-->
            <div class=" form-group">
              <input type="button" value="+ Add a Requirement" class="btn btn-primary pull-right" id="addrequirements" />
            </div>
      </div>
    <div class="form-group">
    <label><Strong>Duties:</Strong></label>
  <div id="duties">
    <div class="form-group">
      <label for="JobFields_duties_1" class="required">
        Duty 1 <span class="asterick" style="color:red">*</span>
      </label>
      <input size="60" maxlength="255" name="JobFields[duties][1]" id="JobFields_duties_1" type="text" value="" class="form-control">
    </div>
  </div><!--duties-->
    <div class=" form-group">
      <input type="button" value="+Add a Duty" class="btn btn-primary pull-right" id="addduty" />
    </div>
  </div>
  <div class="label">
    <legend>Add Other Job Details:</legend>
  </div>
  <div class="row">
    <div class="col">
        <div class="form-group">
        <label for="education_level">{{ __('Min Education Level') }}<span class="asterick" style="color:red">*</span></label>
          <select id="education_level" name="education_level" class="form-control{{ $errors->has('education_level') ? ' is-invalid' : '' }}" required>
            <option disabled selected>Select Education Level</option>
            <option value="Certificate">Certificate</option>
            <option value="Diploma">Diploma</option>
            <option value="Bachelor's Degree">Bachelor's Degree</option>
            <option value="Master's Degree">Master's Degree</option>
            <option value="Doctrate Degree">Doctrate Degree</option>
          </select>
            @if ($errors->has('education_level'))
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('education_level') }}</strong>
              </span>
            @endif
    </div>
    </div>
    <div class="col">
      <div class="form-group">
          <label for="location_city">{{ __('Location City') }}</label>
          <select id="location_city" name="location_city" class="form-control" >
            <option disabled selected>Select Location</option>
            @foreach($locations as $location)
             <option value="{{$location->LocationName}}">{{$location->LocationName}}
            @endforeach
          </select>
            @if ($errors->has('location_city'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('location_city') }}</strong>
                </span>
                @endif
    </div>
    </div>
    <div class="col">
       <div class="form-group">
          <label for="location_country">{{ __('Location Country') }}<span class="asterick" style="color:red">*</span></label>
          <select id="location_country" name="location_country" class="form-control{{ $errors->has('location_country') ? ' is-invalid' : '' }}" required>
            <option disabled selected>Select Country</option>
            @foreach($countries as $country)
             <option value="{{$country->CountryName}}">{{$country->CountryName}}
            @endforeach
          </select>
            @if ($errors->has('location_country'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('location_country') }}</strong>
                </span>
                @endif
    </div>
    </div>
  </div>
  <div class="row">
    <div class="col">
        <div class="form-group ">
          <label for="salary_currency">{{ __('Salary Currency') }}<span class="asterick" style="color:red">*</span></label>
          <select id="salary_currency" name="salary_currency" class="form-control{{ $errors->has('salary_currency') ? ' is-invalid' : '' }}" required>
            <option disabled selected>Select Currency</option>
             @foreach($currencies as $currency)
             <option value="{{$currency->CurrencyID}}">{{$currency->CurrencyName}}
            @endforeach
          </select>
            @if ($errors->has('salary_currency'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('salary_currency') }}</strong>
                </span>
                @endif
    </div>
    </div>
    <div class="col">
       <div class="form-group">
          <label for="monthly_salary">{{ __('Gross Monthly Salary') }}<span class="asterick" style="color:red">*</span></label>
           <input id="monthly_salary" type="numeric" class="form-control{{ $errors->has('monthly_salary') ? ' is-invalid' : '' }}" name="monthly_salary" value="{{ old('monthly_salary')  }}" required>
            @if ($errors->has('monthly_salary'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('monthly_salary') }}</strong>
                </span>
                @endif
    </div>
    </div>
    <div class="col">
      <div class="form-group">
          <label for="show_salary">{{ __('Show Salary') }}<span class="asterick" style="color:red">*</span></label>
          <select id="show_salary" name="show_salary" class="form-control{{ $errors->has('show_salary') ? ' is-invalid' : '' }}" required>
            <option disabled selected>Show Salary</option>
            <option value="1">Yes</option>
            <option value="0">No</option>
          </select>
            @if ($errors->has('show_salary'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('show_salary') }}</strong>
                </span>
                @endif
    </div>
    </div>
  </div>
  <div class="row">
    <div class="col">
      <div class="form-group">
          <label for="career_level">{{ __('Career Level') }}<span class="asterick" style="color:red">*</span></label>
          <select id="career_level" name="career_level" class="form-control{{ $errors->has('career_level') ? ' is-invalid' : '' }}" required>
            <option disabled selected>Select Career Level</option>
            <option value="Entry Level">Entry Level</option>
            <option value="Mid Level">Mid Level</option>
            <option value="Management">Management</option>
            <option value="Senior Management">Senior management</option>
            <option value="Executive">Executive</option>
          </select>
            @if ($errors->has('career_level'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('career_level') }}</strong>
                </span>
                @endif
    </div>
    </div>
<!--     <div class="col">
        <div class="form-group">
          <label for="career_level">{{ __('Career Level') }}</label>
          <select id="career_level" name="career_level" class="form-control{{ $errors->has('career_level') ? ' is-invalid' : '' }}" required>
            <option disabled selected>Select Career Level</option>
            <option value="Entry Level">Entry Level</option>
            <option value="Mid Level">Mid Level</option>
            <option value="Management">Management</option>
            <option value="Senior Management">Senior management</option>
            <option value="Executive">Executive</option>
          </select>
            @if ($errors->has('career_level'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('career_level') }}</strong>
                </span>
                @endif
    </div>
    </div> -->
    <div class="col">
      <div class="form-group">
          <label for="deadline">{{ __('Deadline') }}<span class="asterick" style="color:red">*</span></label>
          <input id="mydate" type="date" name="deadline" class=" form-control{{ $errors->has('deadline') ? ' is-invalid' : '' }} "  required>
    </div>
    </div>
          <div class="col">
          <div class="form-group row">
              <label for="job_type">{{ __('Job Type') }}<span class="asterick" style="color:red">*</span></label>
                <select id="job_type" name="job_type" class="form-control{{ $errors->has('job_type') ? ' is-invalid' : '' }}" required>
                  <option disabled selected>Select Job Type</option>
                  <option value="Full Time">Full Time</option>
                  <option value="Part Time">Part Time</option>
                  <option value="Internship">Internship</option>
                  <option value="Interim">Interim</option>
                  <option value="Consulting">Consulting</option>
                  <option value="Contract">Contract</option>
                </select>
                @if ($errors->has('job_type'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('job_type') }}</strong>
                </span>
                @endif
          </div>
        </div>
  </div>
  <div class="row">
    <div class="col">
          <div class="form-group">
          <label for="revenue">{{ __('Revenue') }}<span class="asterick" style="color:red">*</span></label>
          <select id="id_revenue" name="revenue" class="form-control{{ $errors->has('revenue') ? ' is-invalid' : '' }}" required>
            <option disabled selected>Select Revenue</option>
            <option value="0" >Annual Percentage</option>
            <option value="1">1 Month Salary</option>
          </select>
            @if ($errors->has('revenue'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('revenue') }}</strong>
              </span>
            @endif
    </div>
    <div id="rev" class="form-group" style="display: none;">
        <label>Annual Percentage Revenue <small>*Only Input Numbers. The percentage sign is included*</small>:</label>
        <!--This may end up flagging errors!!-->
        <input type="numeric" class="form-control{{ $errors->has('annualpercentagerevenue') ? ' is-invalid' : '' }}" id="id_annualpercentagerevenue" name="annualpercentagerevenue" pattern="\d*"  /><br/>
    </div>

    </div>
    <div class="col">
       <div class="form-group">
          <label for="candidate_placed">{{ __('Candidate Placed') }}<span class="asterick" style="color:red">*</span></label>
          <select id="candidate_placed" name="candidate_placed" class="form-control{{ $errors->has('candidate_placed') ? ' is-invalid' : '' }}" required>
            <option disabled selected>Select Placement Status</option>
            <option value="0">No</option>
            <option value="1">Yes</option>
          </select>
            @if ($errors->has('candidate_placed'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('candidate_placed') }}</strong>
                </span>
                @endif
    </div>
    </div>
    <div class="col">
       <div class="form-group">
          <label for="staff_assigned">{{ __('Assign to Staff') }}<span class="asterick" style="color:red">*</span></label>
          <select name="staff_assigned" class="form-control" id="staff_assigned">
         <option value="{{Auth::user()->SummitStaff->StaffID}}" selected>{{Auth::user()->SummitStaff->Firstname.' '. Auth::user()->SummitStaff->Lastname}} </option>
           @foreach($staff as $staf)
           <option value="{{$staf->StaffID}}">{{$staf->Firstname.' '.$staf->Lastname}} </option>
           @endforeach
          </select>

    </div>
    </div>
  </div>
    </div>
    <div class="kt-portlet__foot">
      <div class="kt-form__actions">
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="reset" class="btn btn-secondary">Cancel</button>
      </div>
    </div>
  </form>
  <!--end::Form-->
@endsection
@section('footer-js')
<script>
  $(document).on("click", ".btneditsec1_2", function (e) {
    e.preventDefault();
    var _self = $(this);
    var myacc = _self.data('revenue');
    var mywacc = _self.data('wacc');
    var myitc = _self.data('itc');
        $('#id_revenue').val(myacc).change(); //trigger the change event so that associated event handler will get called
        $("#id_annualpercentagerevenue").val(mywacc);
    });
  $('select[name=revenue]').on('change', function()
   {
    if (this.value == '0') {
      $("#rev").show();
    } else {
      $("#rev").hide();
    }
  });
</script>
<script>
    $(document).ready(function(){
        $('#revenue').on('change',function(){
            var x = $(this).val();
            console.log(x);
            if( x === '0'){
                $('#myModal').modal('show');
            }else{
                $('#myModal').modal('hide');
            }
        });
    });
</script>
<script>
  var el = document.getElementById("mydate");
      el.onchange = onChangeDate;
       function onChangeDate(e)
       {
         if(new Date(el.value) < new Date())
         {
         alert("invalid date");
         }
       }
</script>
@endsection
