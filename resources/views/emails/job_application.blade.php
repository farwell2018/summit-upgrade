@extends('layouts.email')

@section('content')

<p style="font-family:'proxima-nova', sans-serif;font-size: 28px;line-height:1.6;font-weight:normal;margin:0 0 30px;padding:0;color:#662F8E;text-align:center;">Job Application</p>
<hr class="line-footer">
<p class="bigger-bold" style="font-size: 18px;font-family: 'proxima-nova', sans-serif;margin: 30px 0 30px;padding: 0;color:#7C7C7C;text-align:center;">Hello {{ ucwords(strtolower($name)) ?: '' }},</p>

<p style="font-family: 'proxima-nova', sans-serif;font-size: 18px;line-height: 1.6;font-weight: normal;margin: 30px 0 30px;padding: 0;color:#7C7C7C;text-align:center;">
    Thank you for submitting your application for {{$job_title}}. If you fit the minimum requirement & qualification a member of our team will be getting in touch.  </p>

  <p style="font-family: 'proxima-nova', sans-serif;font-size: 18px;line-height: 1.6;font-weight: normal;margin: 30px 0 30px;padding: 0;color:#7C7C7C;text-align:center;">
    We endeavour to contact all our applicants, but unfortunately, high volumes of applications make this unrealistic. If you do not hear from us within two weeks, your application has not been successful on this occasion.
    This does not mean you will not be considered for future roles, so please keep an eye on our job board and apply for positions that match your skills and experience.</p>

<p style="font-family: 'proxima-nova', sans-serif;font-size: 18px;line-height: 1.6;font-weight: bold;margin: 20px 0 0;padding: 0;color:#7C7C7C;text-align:center;">
Best Regards,
</p>
<p style="font-family:'proxima-nova', sans-serif;font-size: 18px;line-height: 1.6;font-weight: bold;margin: 0 0 10px;padding: 0;color:#7C7C7C;text-align:center;">
Summit Recruitment & Search Team. 
</p>

<p style="font-family:'proxima-nova', sans-serif;font-size: 18px;line-height: 1.6;font-weight: bold;margin: 0 0 10px;padding: 0;color:#662F8E;text-align:center;">
 <!-- support@thebrandinside.com -->
</p>

@endsection
