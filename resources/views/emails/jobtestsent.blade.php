@extends('layouts.email')
@section('content')
<p style="font-family:'proxima-nova', sans-serif;font-size: 28px;line-height:1.6;font-weight:normal;margin:0 0 30px;padding:0;color:#3069AB;text-align:center;">Job Test Done</p>
<hr class="line-footer">
<p class="bigger-bold" style="font-size: 18px;font-family: 'proxima-nova', sans-serif;text-align:center;">Hello {{ ucwords(strtolower($name)) ?: '' }},</p>

<p style="font-family: 'proxima-nova', sans-serif;font-size: 18px;line-height: 1.6;font-weight: normal;margin: 30px 0 30px;padding: 0;color:#7C7C7C;text-align:center;">
    {{ config('app.name', 'Summit Recruitment and Search Portal') }}</p>

<p style="font-family: 'proxima-nova', sans-serif;margin: 30px 0 30px; text-align:center;">
 
</p>
<p style="font-family: 'proxima-nova', sans-serif;font-size: 18px;line-height: 1.6;font-weight: bold;margin: 20px 0 0;padding: 0;color:#7C7C7C;text-align:center;">
Best Regards,
</p>
<p style="font-family:'proxima-nova', sans-serif;font-size: 18px;line-height: 1.6;font-weight: bold;margin: 0 0 10px;padding: 0;color:#7C7C7C;text-align:center;">
  Summit Recruitment & Search Portal
</p>
<p style="font-family:'proxima-nova', sans-serif;font-size: 18px;line-height: 1.6;font-weight: bold;margin: 0 0 10px;padding: 0;color:#662F8E;text-align:center;">
</p>

@endsection
