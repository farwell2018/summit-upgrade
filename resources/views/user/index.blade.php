@extends('layouts.admin')
@section('title','Users')
@section('content')
<div class="row " style="margin-left:8px">
  <div class="col">
        <a href="{{route('home')}}" style="color:#858796" >
            <span class="text">Dashboard</span> </a> &nbsp; | &nbsp;
            <span class="text">Consultant Management Board</span> </a>&nbsp;

    </div>
<br>
  <div class="col">
  <div class="row user-add-button">
  <a href="{{route('users.create')}}" class="btn btn-icon-split" style="margin-right: 15px;background-color: #FFCB00;color:#3069AB">
  <span class="icon"><i class="fas fa-plus"></i></span>
  <span class="text">New Consultant</span> </a>
</div>
 </div>
</div>
 <div class="row profile">
                <ul class="nav nav-pills nav-justified mb-3" id="pills-tab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true" style="background-color: #1C2E5C;color: #fff">Consultants</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false" style="background-color: #1C2E5C;color: #fff">Performance Management</a>
                  </li>
                </ul>
                <div class="tab-content" id="pills-tabContent">
                  <div class="tab-pane fade active in show" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                     <div class="container-fluid">
                         <div class="row ">
                          @foreach($users as $user)
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 user-row">
                              <div class="card @if($user->status == 1) border-right-success @else border-right-secondary @endif  h-100000 py-10 user-py-100">
                                      <div class="card-body user-body">
                                        <div class="row no-gutters align-items-center">
                                          <div class="col-auto">
                                            <a href="{{ route('users.show',$user->id) }}" ><img class="img-profile" src="@if(isset($user->SummitStaff->profilePhoto)) {{ asset('/public/uploads/staff/'. $user->SummitStaff->profilePhoto.'')}} @else {{asset('/public/admin-assets/img/default.jpg')}} @endif" style="width:80px; height:80px"></a>
                                          </div>
                                          <div class="col user-detail-col">
                                            <div class="font-weight-bold text-primary mb-1">
                                             <a href="{{ route('users.show',$user->id) }}" ><span class="user-fullname"> @if(!empty($user->SummitStaff)){{$user->SummitStaff->Firstname}} {{$user->SummitStaff->Lastname}} @endif</span></a>

                                              <span class="user-role"> @if(($user->role == 0)) {{'Administrator'}} @else {{'Staff'}} @endif</span>

                                              <span class="user-nationality">{{'Kenyan'}}</span>

                                            </div>
                                            <div class="mb-0 text-gray-800">{{$user->email}}</div>
                                            @if($user->status == 1)
                                            <div class="mb-0" style="float:right;margin-right:10px"><a href="{{route('users.delete',$user->id)}}">Deactivate </a></div>
                                            @else
                                            <div class="mb-0" style="float:right;margin-right:10px"><a href="{{route('users.activate',$user->id)}}">Activate </a></div>
                                            @endif
                                          </div>

                                        </div>
                                      </div>
                                    </div>
                            </div>
                         @endforeach
                       </div>
                       <div class="row pagination-row">
                        {{$users->links()}}
                       </div>
                </div>
                  </div>
                  <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <!--Performance review-->
                    <div class="card mb-5">
                        <div class="card-header tab-form-header text-white" style="background-color: #1C2E5C">
                         Performance Management Metrics
                        </div>
                        <div class="card-body">
                          <table class="table table-bordered table-striped table-hover dt-responsive" id="consultantmetrics">
                            <thead>
                               <tr>
                                <th>Staff Name</th>
                                <th>On boarded Clients</th>
                                <th>SLA signed Clients</th>
                                <th>Jobs Assigned</th>
                                <th>Interviewed Candidates</th>
                                <th>Placed Candidates</th>
                              </tr>
                            </thead>
                            <tbody>
                              
                              @foreach($users as $user)
                              <tr>
                              @if(!empty($user->SummitStaff))
                                <td>{{$user->SummitStaff->Firstname}}&nbsp{{$user->SummitStaff->Lastname}}</td>
                                
                                @php
                                  $interview = App\Interview::where('staffID','=',$user->SummitStaff->StaffID)->get();
                                  $jobsassigned = App\JobAd::where('staffID','=',$user->SummitStaff->StaffID)->get();
                                  $placedcandidates = App\Interview::where('staffID','=',$user->SummitStaff->StaffID)->get();
                                  $onboardedclients = App\Clients::where('staff','=',$user->SummitStaff->StaffID)->get();
                                 @endphp
                                <td>{{count($onboardedclients)}}</td>
                                <td></td>
                                <td>{{count($jobsassigned)}}</td>
                                <td>{{count($interview)}}</td>
                                <td>{{count($placedcandidates)}}</td>
                                
                                @endif
                              </tr>
                               @endforeach
                                
                            </tbody>
                          </table>
                           <div class="row pagination-row">
                       
                       </div>
                        </div>
                   </div>
                  </div>
                </div>
            </div>
@endsection
