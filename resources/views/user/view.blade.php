@extends('layouts.admin')

@section('title','Users')


@section('content')
 <div class="row " style="margin-left:8px">
        <a href="{{route('home')}}" style="color:#858796" >
            <span class="text">Home</span> </a> &nbsp; | &nbsp;
            <span class="text"></span><a>{{$user->SummitStaff->Firstname}}{{$user->SummitStaff->Lastname}}</a>

    </div><br>

     <div class="row profile bg-darkblue">
                <div class="col-md-3 profile-image">
                	<div class="row justify-content-center">
                	<img class="img-profile" src="@if(isset($user->SummitStaff->profilePhoto)) {{ asset('uploads/staff/'. $user->SummitStaff->profilePhoto.'')}} @else {{asset('admin-assets/img/default.jpg')}} @endif">
                </div>
                </div>
                <div class="col-md-9">
                    <div class="container">
                    <div class="row profile-details bg-white">
                        <div class="col-md-6">
                            <h3 class="text-primary">{{$user->SummitStaff->Firstname}} {{$user->SummitStaff->Lastname}}</h3>
                            <h5 class="text-primary">{{$user->SummitStaff->designation}} </h5>

                            <p>Mobile No | @if(isset($user->SummitStaff->PhoneNumber)){{$user->SummitStaff->PhoneNumber}} @endif </p>

                            <p>{{$user->email}}</p>

                            <p></p>
                        </div>
                        <div class="col-md-6 profile-details-right">
                            <a href="{{ route('users.edit',$user->SummitStaff->StaffID) }}"><span class="text-primary edit-contact" style="cursor: pointer;position: absolute;right: 0"><i class="fas fa-edit"></i></span></a>
                            <h5 class="text-primary"><strong>{{$user->SummitStaff->Firstname}}'s Performance Metrics</strong></h5>
                            <p><span class="details-span-first">Candidates Interviewed | {{(count($interviewed))}} </span> <span class="details-span"> Candidates Placed |  {{(count($placed))}}  </span></p>
                            <p></p>
                            <!-- <p>Total Success Rate | </p> -->
                            <p><span class="details-span-first">Clients Onboarded | {{(count($clients))}}</span> &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<span class="details-span-first">Jobs Assigned | {{(count($jobss))}}</span> </p>
                            <p></p>
                            <p> <!-- <span class="details-span">Clients Re-assigned |</span> --></p>
                            <p></p>
                        </div>
                    </div>
                    </div>
                </div>
            </div>

	 <div class="card" style="background-color: #1C2E5C">
  <div class="card-body">
    <label style="color: #80CFFF">{{$user->SummitStaff->Firstname}}&nbsp{{$user->SummitStaff->Lastname}}'s &nbspJob Ads</label>
    <div id="Filterform1">
    <div class="row" >
      <div class="col">
              <div class="form-group form-spacing">
                  <label for="company_name" style="color: #80CFFF">Job Title</label>
                    <input type="text" name="jobtitle" class="form-control" value="">
             </div>
           </div> 
      <div class="col">
        <div class="form-group form-spacing">
          <label for="educationLevel" style="color: #80CFFF">Education Level</label>
          <select name="education" class="form-control" value="">
            <option value="">Select Education Level</option>
            <option value="Certificate">Certificate</option>
            <option value="Diploma">Diploma</option>
            <option value="Associate Degree">Associate Degree</option>
            <option value="Bachelor's Degree">Bachelor's Degree</option>
            <option value="Master's Degree">Master's Degree</option>
            <option value="Doctorate Degree">Doctorate Degree</option>
          </select>
          </div>
       </div>   
      <div class="col">
        <div class="form-group form-spacing">
          <label for="JobType" style="color: #80CFFF">Job Type</label>
          <select name="jobtype" id="jobtype" class="form-control" >
            <option value="">Select Job Type</option>
            <option value="Permanent">Permanent</option>
            <option value="Part Time">Part Time</option>
            <option value="Contract">Contract</option>
          </select>
          </div>
      </div>
    </div>
    <div class="row">       
        <div class="col">
                <div class="form-group form-spacing">
                  <label for="company_name" style="color: #80CFFF">Career Level</label>
                    <select name="careerlevel" class="form-control" value="">
                      <option value="" >Select Career Level</option>
                      <option value="Entry Level">Entry Level</option>            
                      <option value="Mid level">Mid level</option>
                      <option value="Management">Management</option>
                      <option value="Senior Management">Senior Management</option>
                      <option value="Executive">Executive</option>
                  </select>
                </div>          
      </div>      
      <div class="col">
                <div class="form-group form-spacing">
                  <label for="company_name" style="color: #80CFFF">Client</label>
                    <select name="clients" class="form-control" value="">
                      <option value="">Select Client</option>
                                          
            <option value=""></option>
            
          </select>
                </div>           
      </div>      
      <div class="col">
                <div class="form-group form-spacing">
                  <label for="company_name" style="color: #80CFFF">Staff</label>
                    <select name="staff" class="form-control" value="">
                      <option value="">Select Consultant</option>
                                         
            <option value=""></option>
           
          </select>
                </div>          
      </div>
    </div>
    <div class="row">
      <div class="col">
          <div class="form-group form-spacing">
                  <label for="company_name" style="color: #80CFFF">Category</label>
                    <select name="category" class="form-control" value="">
                      <option value="">Select Job Category</option>
                                          
            <option value=""></option>
          
          </select>
             </div>
      </div>
      <!-- <div class="col">
        <label>View closed jobs</label>
        
      </div>
      <div class="col">
        <label>View all jobs</label>
      </div> -->
    </div>
    <div class="text-right">
    <button class="btn pull-right"  onclick="filterClients()" style="background-color: #FFCB00;color: #000" >SEARCH</button>
  </div>
</div>
  </div>  
 </div>
  <div class="card">
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered table-striped table-hover" id="dataTable" width="100%" cellspacing="0">
        <thead style="background-color: #FFCB00">
          <tr>
          <th style="color: #000">ID</th>
          <th style="color: #000">Job Title</th>
          <th style="color: #000">Category</th>
          <th style="color: #000">Client</th>
          <th style="color: #000">Job Type</th>
          <th style="color: #000">Country</th>         
          <th style="color: #000">Salary</th>
          <th style="color: #000">Deadline</th>
          <th style="color: #000">Actions</th>
            </tr>
        </thead>
        </table>
    </div>  
  </div>
 </div><br>
 <div class="card" style="background-color: #1C2E5C">
  <div class="card-body">
    <label style="color: #80CFFF">{{$user->SummitStaff->Firstname}}&nbsp{{$user->SummitStaff->Lastname}} &nbsp-&nbspInterview Management</label>
    <div id="filterform2">
    <div class="row">
      <div class="col">
              <div class="form-group form-spacing">
                  <label for="company_name" style="color: #80CFFF">First Name</label>
                    <input type="text" name="company_name" class="form-control" value="">
             </div>
           </div>
           <div class="col">
              <div class="form-group form-spacing">
                  <label for="company_name" style="color: #80CFFF">Last Name</label>
                    <input type="text" name="company_name" class="form-control" value="">
             </div>
           </div>
           <div class="col">
              <div class="form-group form-spacing">
                  <label for="company_name" style="color: #80CFFF">Job Title</label>                  
                    <select name="jobad" class="form-control" value="">
                      <option value=""></option>
                     
            <option value=""></option>
           
          </select>
             </div>          
      </div>
    </div>
    <div class="row">
      <div class="col">
              <div class="form-group form-spacing">
                  <label for="company_name" style="color: #80CFFF">Interviewer</label>                  
                    <select name="consultant" class="form-control" value="">
                      <option value="consultant">Select Consultant</option>
                      <option value=""></option>
                      
            <option value=""></option>
                  
          </select>
             </div>          
      </div>
      <div class="col">
              <div class="form-group form-spacing">
                  <label for="company_name" style="color: #80CFFF">Interview Date</label>
                    <input type="date" name="company_name" class="form-control" value="">
             </div>
           </div>
      <!-- <div class="col">
              <div class="form-group form-spacing">
                  <label for="company_name" style="color: #80CFFF">Pay Status</label>
                    <select name="" class="form-control" value="">                      
            <option value="Yes">Yes</option>
            <option value="No">No</option>          
          </select>
             </div>          
      </div> -->
    </div>
    <div class="row">
      <div class="col">     
               <div class="form-group form-spacing">
                <label for="company_name" style="color: #80CFFF">Application Status</label>
                  <select class="form-control" name="status" id="status">
                     <option value="">Filter By Status</option>
                     <option value="Scheduled">Scheduled</option>
                     <option value="Interviewed">Interviewed</option>
                     <option value="Shortlisted">Shortlisted</option>
                     <option value="Recommended">Recommended</option>
                     <option value="Placed">Placed</option>
                     <option value="Discard">Discard</option>
                     <option value="Blacklist">Blacklist</option>
                  </select>
               </div>
      </div>
    </div>    
    <div class="text-right">
    <button class="btn pull-right" onclick="filterInterview()" style="background-color:#FFCB00;color:#000;padding-right: " >SEARCH</button>
  </div>
</div>
  </div>
 </div><br>
  <div class="card">
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered table-striped table-hover" id="interviews" width="100%" cellspacing="0">
        <thead style="background-color: #FFCB00">
          <tr>
          <th style="color: #000">#</th>
          <th style="color: #000">Candidate</th>
          <th style="color: #000">Job Ad</th>
          <th style="color: #000">Consultant</th>         
          <th style="color: #000">Status</th>          
          <th style="color: #000">Interview Date</th>
          <th style="color: #000">Actions</th>
          </tr>
        </thead>
      </table>
    </div>  
  </div>
 </div>
@endsection
@section('footer-js')
<script>
var dataTable;
let baseUrl = "{!! route('dashboard.json') !!}?";

function encodeQueryData(data) {
 const ret = [];
 for (let d in data)
   ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
 return ret.join('&');
}

  function filterClients(){
    //Get company value

  const type = document.querySelector('#Filterform1 select[name=jobtype]').value
  //Get course value
  const category = document.querySelector('#Filterform1 select[name=category]').value

  const education = document.querySelector('#Filterform1 select[name=education]').value

  const careerlevel = document.querySelector('#Filterform1 Select[name=careerlevel]').value

  const clients = document.querySelector('#Filterform1 select[name=clients]').value

  const staff = document.querySelector('#Filterform1 select[name=staff]').value
  //Add to URL
    const url = baseUrl  + "&" + encodeQueryData({type,category,staff,careerlevel,clients,education})

  console.log(url);
  dataTable.ajax.url( url ).load();
}


  $(document).ready(function(){
dataTable= $('#dataTable').DataTable({
    responsive: true,
    "deferRender": true,
    "processing": true,
    "serverSide": true,
    "ordering": true, //disable column ordering
    "lengthMenu": [
      [25, 50, 100, -1],
      [25, 50, 100,  "All"] // change per page values here
    ],
    "pageLength": 25,
    "ajax": {
       url: baseUrl,
      method: 'GET'
    },
    // dom: '<"html5buttons"B>lTfgitp',
    "dom": "<'row' <'col-md-12'>><'row'<'col-md-8 col-sm-12'lB><'col-md-4 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
    buttons: [
      { extend: 'copy',exportOptions: {columns: [0, 1, 2, 3,4,5,6]}},
      {extend: 'csv',exportOptions: {columns: [0, 1, 2, 3,4,5,6]}},
      {extend: 'excel', title: '{{ config('app.name', 'Summit') }} - List of all Users',exportOptions: {columns: [0, 1, 2, 3,4,5]}},
      {extend: 'pdf', title: '{{ config('app.name', 'Summit') }} - List of all Users',exportOptions: {columns: [0, 1, 2, 3,4,5]}},
      {extend: 'print',
      customize: function (win){
        $(win.document.body).addClass('white-bg');
        $(win.document.body).css('font-size', '10px');
        $(win.document.body).find('table')
        .addClass('compact')
        .css('font-size', 'inherit');
      }
    }
  ],
  columns: [
    {data: 'ID', name: 'ID', orderable: true, searchable: true},
    {data: 'JobTitle', name: 'JobTitle', orderable: true, searchable: true},
    {data: 'Category', name: 'Category', orderable: true, searchable: true},
    {data: 'client', name: 'client', orderable: true, searchable: false},
    {data: 'JobType', name: 'JobType', orderable: false, searchable: false},
    {data: 'LocationCountry', name: 'LocationCountry', orderable: true, searchable: false},
    {data: 'full_salary', name: 'full_salary', orderable: true, searchable: false},
    {data: 'Deadline', name: 'Deadline', orderable: true, searchable: false},
    {data: 'Actions', name: 'tools', orderable: false, searchable: false,"defaultContent": "<i>Not set</i>"},
  ],
});
});

</script>
<script>  
var dataTables;

let baUrl = "{!! route('users2.json') !!}?";

function encodeQueryData(data)
{
 const ret = [];
 for (let d in data)
   ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
 return ret.join('&');
}

//Filter method
function filterInterview(){
    //Get company value
  
  const status = document.querySelector('#filterForm2 select[name=status]').value

  //Add to URL
    const url = baUrl  + "&" + encodeQueryData({jobad,consultant,status})

  //console.log(consultant);
  dataTables.ajax.url( url ).load();
}
$(document).ready(function(){
 dataTables= $('#interviews').DataTable({
    responsive: true,
    "deferRender": true,
    "processing": true,
    "serverSide": true,
    "ordering": true, //disable column ordering
    "lengthMenu": [
      [25, 50, 100, -1],
      [25, 50, 100,  "All"] // change per page values here
    ],
    "pageLength": 25,
    "ajax":  {
      url: baUrl,
      method: 'GET'
    },
    // dom: '<"html5buttons"B>lTfgitp',
    "dom": "<'row' <'col-md-12'>><'row'<'col-md-8 col-sm-12'lB><'col-md-4 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
    buttons: [
      { extend: 'copy',exportOptions: {columns: [0, 1, 2, 3,4,5,6]}},
      {extend: 'csv',exportOptions: {columns: [0, 1, 2, 3,4,5,6]}},
      {extend: 'excel', title: '{{ config('app.name', 'Summit') }} - List of all Users',exportOptions: {columns: [0, 1, 2, 3,4,5]}},
      {extend: 'pdf', title: '{{ config('app.name', 'Summit') }} - List of all Users',exportOptions: {columns: [0, 1, 2, 3,4,5]}},
      {extend: 'print',
      customize: function (win){
        $(win.document.body).addClass('white-bg');
        $(win.document.body).css('font-size', '10px');
        $(win.document.body).find('table')
        .addClass('compact')
        .css('font-size', 'inherit');
      }
    }
  ],
  columns: [
    {data: 'InterviewID', name: 'InterviewID', orderable: true, searchable: true},
    {data: 'CV_ID', name: 'CV_ID', orderable: true, searchable: true},
    {data: 'JobAD_ID', name: 'JobAD_ID', orderable: true, searchable: true},
    {data: 'StaffID', name: 'StaffID', orderable: false, searchable: false},
    {data: 'Status', name: 'Status', orderable: true, searchable: false},
    {data: 'InterviewDate', name: 'InterviewDate', orderable: true, searchable: false},
    {data: 'Actions', name: 'tools', orderable: false, searchable: false,"defaultContent": "<i>Not set</i>"},
  ],
});
});
</script>
@endsection