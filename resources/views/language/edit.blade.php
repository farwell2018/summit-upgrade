@extends('layouts.admin')

@section('title','language')


@section('content')

     <div class="row " style="margin-left:8px">
        <a href="{{route('home')}}" style="color:#858796" >
            <span class="text">Home</span> </a> &nbsp; | &nbsp;
        <a href="{{route('language.index')}}" >
            <span class="text">LanguageList</span> </a>
    </div>
    <br>

    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Edit a Language
                </h3>
            </div>
        </div>
        <!--begin::Form-->
        <form class="kt-form" method="POST" action="{{ route('language.update',$language->LanguageListID) }}"
              enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="language">Language Name</label>
                <input id="languagename" type="text" class="form-control{{ $errors->has('languagename') ? ' is-invalid' : '' }}" name="languagename" value="{{ $language->LanguageName  }}" required>

                @if ($errors->has('languagename'))
                    <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('languagename') }}</strong>
            </span>
                @endif
            </div>

            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="reset" class="btn btn-secondary">Cancel</button>
                </div>
            </div>
        </form>
        <!--end::Form-->
@endsection
