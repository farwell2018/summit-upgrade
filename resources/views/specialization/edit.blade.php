@extends('layouts.admin')

@section('title','Specialization')


@section('content')

     <div class="row " style="margin-left:8px">
        <a href="{{route('home')}}" style="color:#858796" >
            <span class="text">Home</span> </a> &nbsp; | &nbsp;
        <a href="{{route('specialization.index')}}" >
            <span class="text">Specialization List</span> </a>
    </div>
    <br>

    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Edit a Specialization
                </h3>
            </div>
        </div>
        <!--begin::Form-->
        <form class="kt-form" method="POST" action="{{ route('specialization.update',$specialization->SpecializationID) }}"
              enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="specialization">Specialization Name</label>
                <input id="specializationname" type="text" class="form-control{{ $errors->has('specializationname') ? ' is-invalid' : '' }}" name="specializationname" value="{{ $specialization->SpecializationTitle  }}" required>

                @if ($errors->has('specializationname'))
                    <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('specializationname') }}</strong>
            </span>
                @endif
            </div>

            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="reset" class="btn btn-secondary">Cancel</button>
                </div>
            </div>
        </form>
        <!--end::Form-->
@endsection
