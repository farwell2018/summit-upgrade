@extends('layouts.admin')

@section('title','Client Management - Edit Pane')

@section('content')

    <div class="row " style="margin-left:8px">
        <a href="{{route('home')}}" style="color:#858796" >
            <span class="text">Home</span> </a> &nbsp; | &nbsp;
            <a href="{{route('clientmanagement.index')}}" style="color:#858796" >
       <!--  <a href="{{route('clientmanagement.index')}}" style="color:#858796" > -->
            <span class="text">Client Management Board</span> </a> &nbsp; | &nbsp;
        
            <span class="text">{{$client->CompanyName}}</span>
    </div>
    <br>
<div class="kt-portlet">
  <div class="kt-portlet__head">
    <div class="kt-portlet__head-label">
      <h3 class="kt-portlet__head-title"><!--Color change depending on lead-->
        Edit {{$client->CompanyName}} @if($client->status === "Cold Lead")<span class="badge badge-primary">{{ $client->status }}<span>@elseif($client->status === "Warm Lead")<span class="badge badge-warning">{{ $client->status }}<span>@elseif($client->status === "Hot Lead")<span class="badge badge-danger">{{ $client->status }}@elseif ($client->status === "Active Loyal")<span class="badge badge-success">{{ $client->status }}<span>@elseif ($client->status === "Active Not Loyal")<span class="badge badge-success">{{ $client->status }}@elseif ($client->status === "Inactive Loyal")<span class="badge badge-success">{{ $client->status }}@elseif ($client->status === "Inactive Not Loyal")<span class="badge badge-primary">{{ $client->status }}@endif
      </h3>
    </div>
  </div><br>

  <!--begin::Form-->
  <!-- The update instance being called from the ClientManagementController via the edit function-->
  <form class="kt-form" method="POST" action="{{ route('clientmanagement.update', $client->ClientID ) }}"  enctype="multipart/form-data" id="clientmanagement">
    <div class="kt-portlet__body">
        <div class="card shadow mb-4">
           <div class="card shadow mb-4">
                <div class="progress">
             @if($client->status === "Cold Lead")
                  <div class="progress-bar progress-bar-striped progress-bar-animated " role="progressbar" style="width: 30%;background-color:#FFCB00;color:#000" aria-valuenow="25" aria-valuemin="0"aria-valuemax="100" >30% Complete</div>
              @elseif($client->status === "Warm Lead")
                  <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width: 70% ;background-color:#FFCB00;color:#000" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">70% Complete</div>
              @elseif($client->status === "Hot Lead")
                  <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width: 100% ;background-color:#FFCB00;color:#000" aria-valuenow="25"aria-valuemin="0" aria-valuemax="100">100% Complete</div>
              @endif
            </div>
           </div>&nbsp
        @csrf
       
         <div class="tab-form ">
          <div class="tab-form-header text-white">
            <h4>Company Information</h4>
          </div>&nbsp
        <div class="row">
          <div class="col">
          <div class="form-group form-spacing">
            <label for="company_name">{{ __('Company Name') }}<span class="asterick" style="color:red">*</span></label>
            <input type="text" name="company_name" class="form-control{{ $errors->has('company_name') ? ' is-invalid' : ''}}" value="{{$client->CompanyName}}" required>
            @if ($errors->has('company_name'))
            <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('company_name') }}</strong>
            </span>
            @endif
          </div>
        </div>
        <div class="col">
          <div class="form-group form-spacing">
            <label for="industry">{{ __('Industry Sector') }}<span class="asterick" style="color:red">*</span></label>
            
            
            <select id="industry" name="industry" class="form-control{{ $errors->has('industry') ? ' is-invalid' : '' }}" required>
                
                @foreach($industries as $industry)
            @if($industry['Name'] == $client->IndustrySector)
             <option value="{{$industry->Name}}" selected="selected">{{$industry->Name}}</option>
              @else
             <option value="{{$industry->Name}}">{{$industry->Name}}</option>
             @endif
            @endforeach
              
            </select>
            @if ($errors->has('industry'))
            <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('industry') }}</strong>
            </span>
            @endif
          </div>
        </div>
      </div>
          <div class="row">
            <div class="col">
              <div class="form-group form-spacing">
                <label for="physical_address">{{ __('Physical Address') }}<span class="asterick" style="color:red">*</span></label>
                <textarea name="physical_address" row="50" cols="80" class="form-control" required>{{$client->PhysAdd}}</textarea>
                    @if ($errors->has('physical_address'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('physical_address') }}</strong>
                    </span>
                    @endif
              </div>
            </div>
            <div class="col">
              <div class="form-group form-spacing">
                <label for="postal_address">{{ __('Postal Address') }}</label>
                <textarea name="postal_address" row="50" cols="80" class="form-control">{{$client->PostalAdd}}</textarea>
                  @if ($errors->has('postal_address'))
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('postal_address') }}</strong>
                  </span>
                  @endif
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col">
              <div class="form-group form-spacing">
                <label for="contact_person">{{ __('Contact Person') }}<span class="asterick" style="color:red">*</span></label>
                <input type="text" name="contact_person" class="form-control{{ $errors->has('contact_person') ? ' is-invalid' : ''}}" value="{{$client->ContactPerson}}" required>

                @if ($errors->has('contact_person'))
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('contact_person') }}</strong>
                </span>
                @endif
              </div>
            </div>
            <div class="col">
              <div class="form-group form-spacing">
                <label for="contact_title">{{ __('Contact Title') }}<span class="asterick" style="color:red">*</span></label>
                <input type="text" name="contact_title" class="form-control{{ $errors->has('contact_title') ? ' is-invalid' : ''}}" value="{{$client->ContactTitle}}" required>

                @if ($errors->has('contact_title'))
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('contact_title') }}</strong>
                </span>
                @endif
              </div>
            </div>
            <div class="col">
              <div class="form-group form-spacing">
                <label for="contact_email">{{ __('Email Address') }}<span class="asterick" style="color:red">*</span></label>
                <input type="email" name="contact_email" class="form-control{{ $errors->has('contact_email') ? ' is-invalid' : ''}}" value="{{$client->EmailAdd}}" required>
                @if ($errors->has('contact_email'))
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('contact_email') }}</strong>
                </span>
                @endif
              </div>
           </div>
           <div class="col">
              <div class="form-group form-spacing">
                <label for="phone_number">{{ __('Phone Number') }}<span class="asterick" style="color:red">*</span></label>
                <input type="tel" name="phone_number" class="form-control{{ $errors->has('phone_number') ? ' is-invalid' : ''}}" value="{{$client->PhoneNumber}}" required>
                @if ($errors->has('phone_number'))
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('phone_number') }}</strong>
                </span>
                @endif
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col">
              <div class="form-group form-spacing">
                <label for="second_contact_person">{{ __('Second Contact Person') }}</label>
                <input type="text" name="second_contact_person" class="form-control{{ $errors->has('second_contact_person') ? ' is-invalid' : ''}}" value="{{$client->AContactPerson}}" >
                @if ($errors->has('second_contact_person'))
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('second_contact_person') }}</strong>
                </span>
                @endif
              </div>
            </div>
          <div class="col">
            <div class="form-group form-spacing">
              <label for="second_contact_title">{{ __('Second Contact Title') }}</label>
              <input type="text" name="second_contact_title" class="form-control{{ $errors->has('second_contact_title') ? ' is-invalid' : ''}}" value="{{$client->AContactTitle}}" >
              @if ($errors->has('second_contact_title'))
              <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('second_contact_title') }}</strong>
              </span>
              @endif
            </div>
          </div>
            <div class="col">
              <div class="form-group form-spacing">
                <label for="second_contact_email">{{ __('Second Email Address') }}</label>
                <input type="email" name="second_contact_email" class="form-control{{ $errors->has('second_contact_email') ? ' is-invalid' : ''}}" value="{{$client->AEmailAdd}}" >
                @if ($errors->has('second_contact_email'))
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('second_contact_email') }}</strong>
                </span>
                @endif
              </div>
            </div>
            <div class="col">
              <div class="form-group form-spacing">
                <label for="second_phone_number">{{ __('Second Phone Number') }}</label>
                <input type="tel" name="second_phone_number" class="form-control{{ $errors->has('second_phone_number') ? ' is-invalid' : ''}}" value="{{$client->APhoneNumber}}" >
                @if ($errors->has('second_phone_number'))
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('second_phone_number') }}</strong>
                </span>
                @endif
              </div>
            </div>
          </div>
                <div class="row">
        <div class="col">
          <div class="form-group form-spacing">
            <label for="second_contact_person">{{ __('Third Contact Person') }}</label>
            <input type="text" name="third_contact_person" class="form-control{{ $errors->has('third_contact_person') ? ' is-invalid' : ''}}" value="{{$client->AAContactPerson}}" >

            @if ($errors->has('third_contact_person'))
            <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('third_contact_person') }}</strong>
            </span>
            @endif
          </div>
       </div>
       <div class="col">
          <div class="form-group form-spacing">
            <label for="second_contact_title">{{ __('Third Contact Title') }}</label>
            <input type="text" name="third_contact_title" class="form-control{{ $errors->has('third_contact_title') ? ' is-invalid' : ''}}" value="{{$client->AAContactTitle}}" >

            @if ($errors->has('second_contact_title'))
            <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('third_contact_title') }}</strong>
            </span>
            @endif
          </div>
        </div>
       <div class="col">
         <div class="form-group form-spacing">
            <label for="second_contact_email">{{ __('Third Email Address') }}</label>
            <input type="email" name="third_contact_email" class="form-control{{ $errors->has('third_contact_email') ? ' is-invalid' : ''}}" value="{{$client->AAEmailAdd}}" >

            @if ($errors->has('third_contact_email'))
            <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('third_contact_email') }}</strong>
            </span>
            @endif
          </div>
        </div>
    <div class="col">
          <div class="form-group form-spacing">
            <label for="second_phone_number">{{ __('Third Phone Number') }}</label>
            <input type="tel" name="third_phone_number" class="form-control{{ $errors->has('third_phone_number') ? ' is-invalid' : ''}}" value="{{$client->AAPhoneNumber}}" >

            @if ($errors->has('third_phone_number'))
            <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('third_phone_number') }}</strong>
            </span>
            @endif
          </div>
        </div>
   </div>
        <div class="row">
          <div class="col">
            <div class="form-group form-spacing">
              <label for="website">{{ __('Website') }}<span class="asterick" style="color:red">*</span></label>
              <input type="text" name="website" class="form-control{{ $errors->has('website') ? ' is-invalid' : ''}}" value="{{$client->Website}}" >
                @if ($errors->has('website'))
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $errors->first('website') }}</strong>
                </span>
                @endif
            </div>
          </div>
          <div class="col">
            <div class="form-group form-spacing">
               <label for="hq">Head Quarters:<span class="asterick" style="color:red">*</span></label>
               <input type="text" class="form-control" name="hq" id="hq" placeholder="" value="{{$client->hq}}" required>
            </div>
          </div>
           <div class="col">
            <div class="form-group form-spacing">
                 <label for="no_offices">No. of Offices:</label>
                 <input type="numeric" class="form-control" name="no_offices" id="no_offices" value="{{$client->offices}}" placeholder="" pattern="\d*">
            </div>
          </div>
          <div class="col">
            <div class="form-group form-spacing">
                 <label for="no_staff">No. of Staff:</label>
                 <input type="numeric" class="form-control" name="no_Staff" id="no_Staff" value="{{$client->staff}}" placeholder="" pattern="\d*">
            </div>
          </div>
          <div class="col">
            <div class="form-group form-spacing">
                 <label for="input_kra_pin">KRA PIN:</label>
                 <input type="text" class="form-control" name="input_kra_pin" id="input_kra_pin" value="{{$client->kra_pin}}" placeholder="">
            </div>
          </div>                    
        </div>
        <div class="row">
            <div class="col">
                <div class="form-group form-spacing">
                    <label for="clientregistered">Client Registered On:<span class="asterick" style="color:red">*</span></label>
                     <input class="form-control" name="dateclientregistered" value="{{$client->client_regon}}" disabled>
                </div>
            </div>
            <div class="col">
                <div class="form-group form-spacing">
                     <label for="datelast">Date of Last Communication:<span class="asterick" style="color:red">*</span></label>
                       <input class="form-control" name="lastcommdate" value="{{$client->lastcommunication}}" disabled>
                </div>
            </div>
            <div class="col">
                <div class="form-group form-spacing">
                    <label for="clientsource">Client Source</label>
                    <select class="form-control" name="sourceSelect1" id="sourceSelect1" required>
                        <option disabled selected>Select Client Source</option>
                        @if(!empty($client->source))
                        <option value="{{$client->source}}" selected="selected">{{$client->source}}</option>
                        <option value="1">Website</option>
                        <option value="2">Recommendation</option>
                        <option value="3">Referal</option>
                        <option value="4">Mainline</option>
                        <option value="5">Cold Approach / BDM</option>
                        <option value="6">LPO / Tender</option>
                        <option value="7">Others</option>
                        @else
                        <option value="1">Website</option>
                        <option value="2">Recommendation</option>
                        <option value="3">Referal</option>
                        <option value="4">Mainline</option>
                        <option value="5">Cold Approach / BDM </option>
                        <option value="6">LPO / Tender</option>
                        <option value="7">Others</option>
                        @endif
                    </select>
                </div>
            </div>
        </div>
    </div>
                    @if($client->status == "Cold Lead")
                    @include('clientmanagement.editfirstcomm')                   
                    @elseif($client->status == "Warm Lead")
                    @include('clientmanagement.editfirstcomm')
                    @include('clientmanagement.editschedulemeet')
                    @elseif($client->status == "Hot Lead")
                    @include('clientmanagement.editfirstcomm')
                    @include('clientmanagement.editschedulemeet')
                    @include('clientmanagement.editslasigned')
                    @elseif($client->status == "Active Loyal" || $client->status == "Active Not Loyal" || $client->status == "Inactive Loyal" || $client->status == "Inactive Not Loyal")
                    @include('clientmanagement.editfirstcomm')
                    @include('clientmanagement.editschedulemeet')
                    @include('clientmanagement.editslasigned')
                    @include('clientmanagement.editactivestatus')
                    @endif
            </div>
      </div>
                    @if($client->status == "Cold Lead")
                       <div class="row justify-content-center">
                          <button type="submit" class="btn btn-primary">Submit & Continue</button>
                       </div>
                    @elseif($client->status == "Warm Lead")
                       <div class="row justify-content-center">
                          <button type="submit" class="btn btn-primary">Submit & Continue</button>
                       </div>
                    @elseif($client->status == "Hot Lead")
                    <div class="row justify-content-center">
                          <button type="submit" class="btn btn-primary">Submit & Continue</button>
                       </div>
                    @elseif($client->status == "Active Loyal")
                    <div class="row justify-content-center">
                          <button type="submit" class="btn btn-primary">Submit</button>
                       </div>
                    @elseif($client->status == "Active Not Loyal")
                    <div class="row justify-content-center">
                          <button type="submit" class="btn btn-primary">Submit</button>
                       </div>
                    @elseif($client->status == "Inactive Loyal")
                    <div class="row justify-content-center">
                          <button type="submit" class="btn btn-primary">Submit</button>
                       </div>
                    @elseif($client->status == "Inactive Not Loyal")
                    <div class="row justify-content-center">
                          <button type="submit" class="btn btn-primary">Submit</button>
                       </div>
                       @endif
    </form>   
  <!--end::Form-->
 <script>
  $(document).ready(function(){
    $("#myModal").modal('show');
  });
</script>
<style>
    .bs-example{
      margin: 20px;
    }
</style>
@if($client->status == "Cold Lead")
 <div class="bs-example">
    <div id="myModal" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered modal-dialog-centered-lg">
            <div class="modal-content">
                <div class="modal-header justify-content-center">
                    <h6 class="modal-header justify-content-center">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<strong>Convert To Warm Lead</strong></h6>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>              
                <div class="modal-body">
                  <div class="row d-flex justify-content-center align-items-center">
                    <h6>
                     Has the conversation with this customer progressed?
                    </h6>                   
                    <h6>You may convert their profile to a warm lead by doing the following:</h6>                 
                    <h6>
                    &nbsp&nbsp1. Indicating your client source.<br>
                    &nbsp&nbsp2. Confirming your first communication date.<br>
                    &nbsp&nbsp3. Confirming your first communication medium.</h6>
                    <p class="text-secondary"><small>If you don't save, your changes will be lost.</small></p>
                </div>               
            </div>
        </div>
    </div>
</div>
@elseif($client->status == "Warm Lead")
 <div class="bs-example">     
    <div id="myModal" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered">
              <div class="modal-content">
                <div class="modal-header justify-content-center">
                    <h6 class="modal-header justify-content-center">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<strong>Convert To Hot Lead</strong></h6>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>              
                <div class="modal-body">
                  <div class="row d-flex justify-content-center align-items-center">
                    <h6>
                    Has the conversation has turned positive?
                   </h6>
                    <h6>Is the Customer Ready to sign?</h6>
                    </h6>
                    <h6>Kindly change the lead status to hot by:</h6>
                    <h6>
                    &nbsp&nbsp1. Scheduling the client meeting.<br>
                    &nbsp&nbsp2. Sending a proposal.<br>
                    &nbsp&nbsp3. Upload the signed proposal. <br>
                    &nbsp&nbsp3. Convert to Hot lead..</h6>                    
                </div>                
            </div>
        </div>
        </div>
    </div>
</div>
@elseif($client->status == "Hot Lead")
 <div class="bs-example">
    <div id="myModal" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header justify-content-center">
                    <h6 class="modal-header justify-content-center">&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<strong>Convert To Active Status</strong></h6>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>              
                <div class="modal-body">
                  <div class="row d-flex justify-content-center align-items-center">
                    <h6>
                    At this juncture an Agreement has been reached. 
                    </h6>
                    <h6>
                    &nbsp&nbspOn board the client by:<br><br>
                    &nbsp&nbsp1. Ensuring An SLA Agreement Is Signed<br>                  
                    &nbsp&nbsp2. SLA comments have been recorded.</h6>
                    <p class="text-secondary"><small>If you don't save, your changes will be lost.</small></p>
                </div>
               <!--  <div class="modal-footer row justify-content-center">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>                    
                </div> -->
            </div>
        </div>
        </div>
    </div>
</div>
 @elseif($client->status == "Active Loyal")
 <div class="bs-example">
    <div id="myModal" class="modal fade" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered">
               <div class="modal-content">
                <div class="modal-header justify-content-center">
                    <h6 class="modal-header"></h6>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>              
                <div class="modal-body">
                  <div class="row d-flex justify-content-center align-items-center">
                    <h6>
                    <!--<span class="badge info">Onboarding Process Complete</span>-->
                    Onboarding Process Complete
                    </h6>
                    <h6>Please Change Active Status When Need Be
                   </h6>
                    <p class="text-secondary"><small>If you don't save, your changes will be lost.</small></p>
                </div>
                <div class="modal-footer row justify-content-center">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>                    
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
@endif
@endsection
<style>
/* Caret Breadcrumb */
.breadcrumb-caret .breadcrumb-item+.breadcrumb-item::before {
  content: '\f0da';
  font-family: 'FontAwesome';
}

/* Bullet */
.breadcrumb-bullet .breadcrumb-item+.breadcrumb-item::before {
  content: '•';
}

/* Arrow Breadcrumb */
.breadcrumb-arrow .breadcrumb-item+.breadcrumb-item::before {
  content: '';
}

.breadcrumb-arrow {
  height: 40px;
  line-height: 40px;
}

.breadcrumb-arrow .breadcrumb-item:first-child a {
  border-radius: 4px 0 0 4px;
}

.breadcrumb-arrow .breadcrumb-item,
.breadcrumb-arrow .breadcrumb-item a,
.breadcrumb-arrow .breadcrumb-item span {
  display: inline-block;
  vertical-align: top;
}

.breadcrumb-arrow .breadcrumb-item:not(:first-child) {
  margin-left: -1px;
}

.breadcrumb-arrow .breadcrumb-item+.breadcrumb-item:before {
  padding: 0;
  content: "";
}

.breadcrumb-arrow .breadcrumb-item a,
.breadcrumb-arrow .breadcrumb-item.active {
  height: 40px;
  padding: 0 10px 0 25px;
  line-height: 40px;
}

.breadcrumb-arrow .breadcrumb-item a {
  position: relative;
  color: #fff;
  text-decoration: none;
  background-color: #3bafda;
  border: 1px solid #3bafda;
}

.breadcrumb-arrow .breadcrumb-item a:after,
.breadcrumb-arrow .breadcrumb-item a:before {
  position: absolute;
  top: -1px;
  width: 0;
  height: 0;
  content: '';
  border-top: 20px solid transparent;
  border-bottom: 20px solid transparent;
}

.breadcrumb-arrow .breadcrumb-item a:before {
  right: -10px;
  z-index: 3;
  border-left-color: #3bafda;
  border-left-style: solid;
  border-left-width: 11px;
}

.breadcrumb-arrow .breadcrumb-item a:after {
  right: -11px;
  z-index: 2;
  border-left: 11px solid #00d2ff;
}

.breadcrumb-arrow .breadcrumb-item a:focus,
.breadcrumb-arrow li a:hover {
  background-color: #3069AB;
  border: 1px solid #3069AB;
}

.breadcrumb-arrow .breadcrumb-item a:focus:before,
.breadcrumb-arrow li a:hover:before {
  border-left-color: #3069AB;
}

.breadcrumb-arrow li.active:after,
.breadcrumb-arrow li.active:before {
  border-left-color: #3069AB;
}

body {
  min-height: 100vh;
  background-color: #3069AB;
  background-image: linear-gradient(147deg, #00d2ff 0%, #3a7bd5 100%);
}

.text-uppercase {
  letter-spacing: 0.15em;
}


</style>
<style>
  * {
    margin: 0;
    padding: 0
}

html {
    height: 100%
}

#grad1 {
    background-color: : #9C27B0;
    background-image: linear-gradient(120deg, #FF4081, #81D4FA)
}

#msform {
    text-align: center;
    position: relative;
    margin-top: 20px
}

#msform fieldset .form-card {
    background: white;
    border: 0 none;
    border-radius: 0px;
    box-shadow: 0 2px 2px 2px rgba(0, 0, 0, 0.2);
    padding: 20px 40px 30px 40px;
    box-sizing: border-box;
    width: 94%;
    margin: 0 3% 20px 3%;
    position: relative
}

#msform fieldset {
    background: white;
    border: 0 none;
    border-radius: 0.5rem;
    box-sizing: border-box;
    width: 100%;
    margin: 0;
    padding-bottom: 20px;
    position: relative
}

#msform fieldset:not(:first-of-type) {
    display: none
}

#msform fieldset .form-card {
    text-align: left;
    color: #9E9E9E
}

#msform input,
#msform textarea {
    padding: 0px 8px 4px 8px;
    border: none;
    border-bottom: 1px solid #ccc;
    border-radius: 0px;
    margin-bottom: 25px;
    margin-top: 2px;
    width: 100%;
    box-sizing: border-box;
    font-family: montserrat;
    color: #2C3E50;
    font-size: 16px;
    letter-spacing: 1px
}

#msform input:focus,
#msform textarea:focus {
    -moz-box-shadow: none !important;
    -webkit-box-shadow: none !important;
    box-shadow: none !important;
    border: none;
    font-weight: bold;
    border-bottom: 2px solid skyblue;
    outline-width: 0
}

#msform .action-button {
    width: 100px;
    background: skyblue;
    font-weight: bold;
    color: white;
    border: 0 none;
    border-radius: 0px;
    cursor: pointer;
    padding: 10px 5px;
    margin: 10px 5px
}

#msform .action-button:hover,
#msform .action-button:focus {
    box-shadow: 0 0 0 2px white, 0 0 0 3px skyblue
}

#msform .action-button-previous {
    width: 100px;
    background: #616161;
    font-weight: bold;
    color: white;
    border: 0 none;
    border-radius: 0px;
    cursor: pointer;
    padding: 10px 5px;
    margin: 10px 5px
}

#msform .action-button-previous:hover,
#msform .action-button-previous:focus {
    box-shadow: 0 0 0 2px white, 0 0 0 3px #616161
}

select.list-dt {
    border: none;
    outline: 0;
    border-bottom: 1px solid #ccc;
    padding: 2px 5px 3px 5px;
    margin: 2px
}

select.list-dt:focus {
    border-bottom: 2px solid skyblue
}

.card {
    z-index: 0;
    border: none;
    border-radius: 0.5rem;
    position: relative
}

.fs-title {
    font-size: 25px;
    color: #2C3E50;
    margin-bottom: 10px;
    font-weight: bold;
    text-align: left
}

#progressbar {
    margin-bottom: 30px;
    overflow: hidden;
    color: lightgrey
}

#progressbar .active {
    color: #000000
}

#progressbar li {
    list-style-type: none;
    font-size: 12px;
    width: 25%;
    float: left;
    position: relative
}

#progressbar #account:before {
    font-family: FontAwesome;
    content: "\f023"
}

#progressbar #personal:before {
    font-family: FontAwesome;
    content: "\f007"
}

#progressbar #payment:before {
    font-family: FontAwesome;
    content: "\f09d"
}

#progressbar #confirm:before {
    font-family: FontAwesome;
    content: "\f00c"
}

#progressbar li:before {
    width: 50px;
    height: 50px;
    line-height: 45px;
    display: block;
    font-size: 18px;
    color: #ffffff;
    background: lightgray;
    border-radius: 50%;
    margin: 0 auto 10px auto;
    padding: 2px
}

#progressbar li:after {
    content: '';
    width: 100%;
    height: 2px;
    background: lightgray;
    position: absolute;
    left: 0;
    top: 25px;
    z-index: -1
}

#progressbar li.active:before,
#progressbar li.active:after {
    background: skyblue
}

.radio-group {
    position: relative;
    margin-bottom: 25px
}

.radio {
    display: inline-block;
    width: 204;
    height: 104;
    border-radius: 0;
    background: lightblue;
    box-shadow: 0 2px 2px 2px rgba(0, 0, 0, 0.2);
    box-sizing: border-box;
    cursor: pointer;
    margin: 8px 2px
}

.radio:hover {
    box-shadow: 2px 2px 2px 2px rgba(0, 0, 0, 0.3)
}

.radio.selected {
    box-shadow: 1px 1px 2px 2px rgba(0, 0, 0, 0.1)
}

.fit-image {
    width: 100%;
    object-fit: cover
}
</style>
<script>
  $(document).ready(function(){

var current_fs, next_fs, previous_fs; //fieldsets
var opacity;

$(".next").click(function(){

current_fs = $(this).parent();
next_fs = $(this).parent().next();

//Add Class Active
$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

//show the next fieldset
next_fs.show();
//hide the current fieldset with style
current_fs.animate({opacity: 0}, {
step: function(now) {
// for making fielset appear animation
opacity = 1 - now;

current_fs.css({
'display': 'none',
'position': 'relative'
});
next_fs.css({'opacity': opacity});
},
duration: 600
});
});

$(".previous").click(function(){

current_fs = $(this).parent();
previous_fs = $(this).parent().prev();

//Remove class active
$("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

//show the previous fieldset
previous_fs.show();

//hide the current fieldset with style
current_fs.animate({opacity: 0}, {
step: function(now) {
// for making fielset appear animation
opacity = 1 - now;

current_fs.css({
'display': 'none',
'position': 'relative'
});
previous_fs.css({'opacity': opacity});
},
duration: 600
});
});

$('.radio-group .radio').click(function(){
$(this).parent().find('.radio').removeClass('selected');
$(this).addClass('selected');
});

$(".submit").click(function(){
return false;
})

});
</script>
