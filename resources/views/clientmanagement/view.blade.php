@extends('layouts.admin')

@section('title','Client Management ')


@section('content')
 <div class="row " style="margin-left:8px">
        <a href="{{route('home')}}" style="color:#858796" >
            <span class="text">Home</span> </a> &nbsp; | &nbsp;
            <a href="{{route('clientmanagement.index')}}" style="color:#858796" >
       <!--  <a href="{{route('clientmanagement.index')}}" style="color:#858796" > -->
            <span class="text">Client Management Board</span> </a> &nbsp; | &nbsp;

            <span class="text">{{$client->CompanyName}}</span>
    </div>
    <br>

      <!--#ROUTING BUTTONS VIA A CONTROLLER TO A BLADE-->
    @if($client->status == 1)
    <div class="row user-add-button">
          <a href="{{route('clientmanagement.edit', $client->ClientID)}}" class="btn btn-icon-split" style="margin-right: 15px;background-color: #FFCB00;color:#3069AB">
          <span class="icon"><i class="fas fa-plus"></i></span>
          <span class="text">Edit Client</span> </a>
        </div>
      <div class="row user-add-button">
        <a href="{{route('clientmanagement.edit', $client->ClientID)}}" data-toggle="tooltip" class="btn btn-secondary btn-icon-split" style="margin-right: 15px;">
        <span class="text">Proceed With Client Onboarding</span> </a>
      </div>
      
    @elseif($client->status == 2)
     <div class="row user-add-button">
          <a href="{{route('clientmanagement.edit', $client->ClientID)}}" class="btn btn-icon-split" style="margin-right: 15px;background-color: #FFCB00;color:#3069AB">

          <span class="text">Edit Client</span> </a>
        </div>
      <div class="row user-add-button">
      <a href="{{route('clientmanagement.edit', $client->ClientID)}}" class="btn btn-dark btn-icon-split" style="margin-right: 15px;">
      <span class="text">Proceed With Client Onboarding</span> </a>
    </div>
    @elseif($client->status == 3)
     <div class="row user-add-button">
          <a href="{{route('clientmanagement.edit', $client->ClientID)}}" class="btn btn-icon-split" style="margin-right: 15px;background-color: #FFCB00;color:#3069AB">
          <span class="text">Edit Client</span> </a>
        </div>
      <div class="row user-add-button">
      <a href="{{route('clientmanagement.edit', $client->ClientID)}}" class="btn btn-info btn-icon-split" style="margin-right: 15px;">
      <span class="text">Proceed With Client Onboarding</span> </a>
    </div>
    @elseif($client->status == 4)
     <div class="row user-add-button">
          <a href="{{route('clientmanagement.edit', $client->ClientID)}}" class="btn btn-icon-split" style="margin-right: 15px;background-color: #FFCB00;color:#3069AB">
          <span class="text">Edit Client</span> </a>
        </div>
    <div class="row user-add-button">
      <a href="{{route('client.create', $client->ClientID)}}" class="btn btn-primary btn-icon-split" style="margin-right: 15px;">
      <span class="icon"><i class="fas fa-plus"></i></span>
      <span class="text">New Job Ad</span> </a>
    </div>
    @elseif($client->status == 5)
     <div class="row user-add-button">
          <a href="{{route('clientmanagement.edit', $client->ClientID)}}" class="btn btn-icon-split" style="margin-right: 15px;background-color: #FFCB00;color:#3069AB">
          <span class="text">Edit Client</span> </a>
        </div>
    <div class="row user-add-button">
      <a href="{{route('client.create', $client->ClientID)}}" class="btn btn-primary btn-icon-split" style="margin-right: 15px;">
      <span class="icon"><i class="fas fa-plus"></i></span>
      <span class="text">New Job Ad</span> </a>
    </div>
    @elseif($client->status == 6)
     <div class="row user-add-button">
          <a href="{{route('clientmanagement.edit', $client->ClientID)}}" class="btn btn-icon-split" style="margin-right: 15px;background-color: #FFCB00;color:#3069AB">
          <span class="text">Edit Client</span> </a>
        </div>
    <div class="row user-add-button">
      <a href="{{route('client.create', $client->ClientID)}}" class="btn btn-primary btn-icon-split" style="margin-right: 15px;">
      <span class="icon"><i class="fas fa-plus"></i></span>
      <span class="text">New Job Ad</span> </a>
    </div>
    @elseif($client->status == 7)
     <div class="row user-add-button">
          <a href="{{route('clientmanagement.edit', $client->ClientID)}}" class="btn btn-icon-split" style="margin-right: 15px;background-color: #FFCB00;color:#3069AB">
          <span class="text">Edit Client</span> </a>
        </div>
    <div class="row user-add-button">
      <a href="{{route('client.create', $client->ClientID)}}" class="btn btn-primary btn-icon-split" style="margin-right: 15px;">
      <span class="icon"><i class="fas fa-plus"></i></span>
      <span class="text">New Job Ad</span> </a>
    </div>
    @endif
<div class="row profile bg-darkblue">
    <div class="col-md-12">
        <div class="container-fluid">
          <div class="col-md-12">
        <div class="row profile-details bg-white">
            <div class="col">
                <h4 class="text-primary">{{$client->CompanyName}} </h4>
                <p class="details-span-first"><b>Industry Sector:</b> {{$client->IndustrySector}} </p>
                <p class="details-span-first"><b>Physical Address:</b> {{$client->PhysAdd}}</p>
                <p class="details-span-first"><b>Postal Address:</b> {{$client->PostalAdd}}</p>
                <p class="details-span-first"><b>Website:</b> {{$client->Website}}</p>
                <p class="details-span-first"><b>Client Registered On:</b>{{$client->client_regon}}</p>

            </div>
            <div class="col profile-details-right">
               <br><br>
                <p class="details-span-first"><b>Contact Person:</b> {{$client->ContactPerson}} </p>
                <p class="details-span-first"><b>Contact Title:</b> {{$client->ContactTitle}} </p>
                <p class="details-span-first"><b>Contact Email:</b> {{$client->EmailAdd}} </p>
                <p class="details-span-first"><b>Contact Phone Number:</b> {{$client->PhoneNumber}}</p>
                <p class="details-span-first"><b>Date of Last Communication:</b>{{$client->lastcommunication}}</p>
            </div>
              <div class="col profile-details-right">
               <br><br>
                <p class="details-span-first"><b>Second Contact Person:</b> {{$client->AContactPerson}} </p>
                <p class="details-span-first"><b>Second Contact Title:</b> {{$client->AContactTitle}} </p>
                <p class="details-span-first"><b>Second Contact Email:</b> {{$client->AEmailAdd}} </p>
                <p class="details-span-first"><b>Second Contact Phone Number:</b> {{$client->APhoneNumber}}</p>
                <p class="details-span-first"><b>Consultant Assigned:</b> @if(!empty($staffassigned->Firstname)){{$staffassigned->Firstname}}@else No Consultant First Name @endif @if(!empty($staffassigned->Lastname)){{$staffassigned->Lastname}} @else No Consultant Last Name @endif </p>
            </div>
        </div>
        </div>
    </div>
</div>
</div>

    <div class="row">
        <div class="col-md-12">
        <div class="card mb-3">
            <div class="card-header tab-form-header">
             Client Jobs
            </div>
            <div class="card-body">
              <table class="table">
                <thead>
                  <th>ID</th>
                  <th>Job Title</th>
                  <th>Industry</th>
                  <th>Client</th>
                  <th>Job Type</th>
                  <th>Country</th>
                  <th>Salary</th>
                  <th>Deadline Date</th>
                  <th>Tools</th>
                </thead>
              <tbody>
                @foreach($jobAds as $jobAd)
                <tr>
                 <td>{{$jobAd->ID}}</td>
                 <td>{{$jobAd->JobTitle}}</td>
                 <td>{{$jobAd->Category}}</td>
                 <td>{{$jobAd->clients->CompanyName}}</td>
                 <td>{{$jobAd->JobType}}</td>
                 <td>{{$jobAd->LocationCountry}}</td>
                 <td>{{$jobAd->GrossMonthSal}}</td>
                 <td>{{$jobAd->Deadline}}</td>
                 <td>
                  <span style="overflow: visible; position: relative; width: 110px;">
                  <a title="View details" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="{{route('jobAds.show', $jobAd->ID)}}">
                    <i class="fas fa-eye"></i></a>
                  <a title="Edit details" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="{{route('jobAds.edit', $jobAd->ID)}}">
                    <i class="fas fa-edit"></i></a>

                </span>
                 </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            <div class="row pagination-row">
             {{$jobAds->links()}}
            </div>
            </div>
          </div>
      </div>
      </div>
<style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input {
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color:#3069AB;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>
 <script>
  $(document).ready(function(){
    $("#myModal").modal('show');
  });
</script>
<style>
    .bs-example{
      margin: 20px;
    }
</style>
@endsection
