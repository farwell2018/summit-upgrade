
      <div class="tab-form ">
          <div class="tab-form-header text-white">
            <h4>Active Status</h4>
          </div><br>
          <div class="row">
               <div class="col">
              <div class="form-group form-spacing">
                <label for="status_update">Status Update:<span class="asterick" style="color:red">*</span></label>
                <select class="form-control" name="status_update" id="status_update" value="">
                  <option value=""> -- Choose Option -- </option>
                  <option value="4">Active Loyal</option>
                  <option value="5">Active Not Loyal</option>
                  <option value="6">Inactive Loyal</option>
                  <option value="7">Inactive Not Loyal</option>
                </select>
                @if ($errors->has('status_update'))
                  <p class="text-danger"><b>{{ $errors->first('status_update') }}</b></p>
                @endif
              </div>
            </div>
            <div class="col">
              <!--<div class="form-group form-spacing">-->
              <!--  <label for="status_update">Status Update:</label>-->
              <!--  <select class="form-control" name="status_update_option" id="status_update" value="">-->
              <!--    <option value=""> -- Choose Option -- </option>-->
              <!--    <option value="1">Yes</option>-->
              <!--    <option value="0">No</option>-->
              <!--  </select>-->
              <!--</div>-->
            </div>
           
          </div>
      </div>
