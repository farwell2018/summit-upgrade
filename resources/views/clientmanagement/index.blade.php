@extends('layouts.admin')

@section('title','Client Management')


@section('content')
    @if (session('status'))
          <div class="alert alert-success" role="alert">
              {{ session('status') }}
          </div>
      @endif 
<div class="row">
  <div class="col">
<div class="row " style="margin-left:8px">
        <a href="{{route('home')}}" style="color:#858796" >
            <span class="text">Dashboard</span> </a> &nbsp; | &nbsp;
            <span class="text">Client Management Board</span> </a>&nbsp;
</div>
</div>
<div class="col">
 <div class="row user-add-button">
  <a href="{{route('clientmanagement.create')}}" class="btn btn-icon-split" style="margin-right: 15px;background-color: #FFCB00;color:#3069AB">
  <span class="icon"><i class="fas fa-plus"></i></span>
  <span class="text">New Client</span> </a>
</div>
</div>
</div>
<br>
<div class="card"style="background-color: #1C2E5C;align-content: center;">
<div class="card-body">
  <div id="filterForm">
    <div class="row">
      <div class="col">
         <div class="form-group form-spacing">
            <label for="company_name" style="color: #80CFFF">Company Name</label>
              <input type="company" name="company" class="form-control" placeholder="Company Name">
         </div>
      </div>
      <div class="col">
         <div class="form-group form-spacing">
            <label for="company_name" style="color: #80CFFF">Industry Sector</label>
            <select name="industry" class="form-control" value="">
                <option  value="">Select Job Industry</option>
               @foreach($industries as $industry)

     <option value="{{$industry->Name}}">{{$industry->Name}}</option>
     @endforeach
   </select>
         </div>
      </div>
      <div class="col">
         <div class="form-group form-spacing">
             <label for="staff" style="color: #80CFFF">Consultant Assigned</label>
             <select name="staff" class="form-control" value="">
               <option value="">Select Consultant</option>
               @foreach($staff as $staffs)
               <option value="{{$staffs->StaffID}}">{{$staffs->Firstname}}&nbsp{{$staffs->Lastname}}</option>
                @endforeach
             </select>
         </div>
     </div>
      <div class="col">
        <div class="form-group form-spacing">
            <label for="company_name" style="color: #80CFFF">Onboarding Status</label>
          <select name="status" class="form-control">
            <option value="">Select Status</option>
            <option value="1">Cold Clients</option>
            <option value="2">Warm Clients</option>
            <option value="3">Hot Clients</option>
            <option value="4">Active Clients</option>
            <option value="5">Active Not Loyal Clients</option>
            <option value="6">Inactive loyal Clients</option>
            <option value="7">Inactive Not Loyal Clients</option>
          </select>
        </div>
      </div>
    </div>
     <div class="text-right">
       <button class="btn pull-right"  onclick="filterClients()" style="background-color: #FFCB00;color: #000" >SEARCH</button>
       <button class="btn pull-right"  onClick="window.location.reload();" style="background-color: #FFCB00;color: #000" >RESET</button>
     </div>
  </div>
</div>
</div>
   <br>
   <div class="container">
        <div class="row">
          <div class="col">
            <span class="badge badge-primary">Cold Clients: {{(count($cold))}}</span>
          </div>
          <div class="col">
            <span class="badge badge-warning">Warm Clients: {{(count($warm))}}</span>
          </div>
          <div class="col">
            <span class="badge badge-danger">Hot Clients: {{(count($hot))}}</span>
          </div>
          <div class="col">
            <span class="badge badge-success">Active Clients: {{(count($active))}}</span>
          </div>
          <div class="col">
            <span class="badge badge-success">Active Not loyal Clients: {{(count($activenotloyal))}}</span>
          </div>
          <div class="col">
            <span class="badge badge-success">Inactive Loyal Clients: {{(count($inactiveloyal))}}</span>
          </div>
          <div class="col">
            <span class="badge badge-primary">Inactive Not Loyal Clients: {{(count($inactivenotloyal))}}</span>
          </div>
        </div>
  </div><br>
  <div class="row">
    <div class="col">
      <div class="buttonfilter">
        <div class="row">
          <div class="col-2">
            <p>
              <!--<a href="javascript:;" class="btn" onmousedown="toggleDiv('mydiv');"style="background-color: #FFCB00;color: #000" >Switch Orientation</a>-->
              <!-- <button class="btn pull-right" style="background-color: #FFCB00;color: #000" >TABULAR VIEW</button>
              <button class="btn pull-right" onclick="myFunction()" style="background-color: #FFCB00;color: #000" >LIST VIEW</button> -->
            <!--</p>-->
          </div>
          <div class="col">
            <p>
              
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
<div class="card mb-5">
  <div class="card-header tab-form-header text-white" style="background-color: #1C2E5C">
   CLIENT MANAGEMENT
  </div>
      <div id="mydivon" style="display:block">
        <div class="card-body">
          <table class="table table-bordered table-striped table-hover dt-responsive" id="clients" width="100%">
            <thead style="background-color: #FFCB00">
               <tr>
                <th style="color: #000">#</th>
                <th style="color: #000">Company Name</th>
                <th style="color: #000">Industry Sector</th>
                <th style="color: #000">Physical Address</th>
                <th style="color: #000">Email Address</th>
                <th style="color: #000">Contact Person</th>
                <th style="color: #000">Phone No:</th>
                <th style="color: #000">Status:</th>
                <th style="color: #000">Actions:</th>
              </tr>
            </thead>
          </table>
        </div>
    </div>
    <div id="mydivoff" style="display:none">
    <div class="row">
      @foreach($clients as $client)
        <div class="col-6">
          <div class="card">
            <div class="card-header tab-form-header text-white" style="background-color: #1C2E5C">
              {{$client->CompanyName}}
              @if(Auth::user()->role == 0)
              <div class="row">
                <div class="col">
                  <a href="{{route('clientmanagement.edit', $client->ClientID)}}"><span class="text-primary edit-contact"><i class="far fa-edit" style="color: #fff"></i></span></a><br>
                </div>
                <div class="col">
                  <a href="#"><span class="text-primary delete-client" ><i class="fas fa-trash-alt" style="color:#fff"></i></span></a>
                </div>
              </div>
              @else
              <div class="row">
                <div class="col">
                  <a href="{{route('clientmanagement.edit', $client->ClientID)}}"><span class="text-primary edit-contact"><i class="far fa-edit" style="color: #fff"></i></span></a><br>
                </div>
              </div>
              @endif
            </div>
            <div class="card-body">
                <ul>
                  <p><strong>Industry Sector:</strong>&nbsp{{$client->IndustrySector}}s</p>
                  <p><strong>Physical Address:</strong>&nbsp{{$client->PhysAdd}}</p>
                  <p><strong>Postal Address:</strong>&nbsp{{$client->PostalAdd}}</p>
                  <p><strong>Contact Person:</strong>&nbsp{{$client->ContactPerson}}</p>
                  <p><strong>Contact Title:</strong>&nbsp{{$client->ContactTitle}}</p>
                </ul>
            </div>
            <div class="text-center">
              <a href="{{route('clientmanagement.show',$client->ClientID)}}" class="btn" style="background-color:#0EA616;color: #fff ">View Client</a>
            </div>
          </div><br>
        </div>
      @endforeach
    </div><br>
    <div class="row pagination-row">
             {{$clients->links()}}
      </div>
    </div>
</div>
@endsection
@section('footer-js')
<script>
  function toggleDiv(divid)
  { 
    varon = divid + 'on';
    varoff = divid + 'off';
 
    if(document.getElementById(varon).style.display == 'block')
    {
    document.getElementById(varon).style.display = 'none';
    document.getElementById(varoff).style.display = 'block';
    }
   
    else
    {  
    document.getElementById(varoff).style.display = 'none';
    document.getElementById(varon).style.display = 'block'
    }
} 
</script>
<script>
function myFunction() {
  var x = document.getElementById("listview");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
</script>
<script>
var dataTable;
let baseUrl = "{!! route('clientmanagement.json') !!}?";
function encodeQueryData(data) {
 const ret = [];
 for (let d in data)
   ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
 return ret.join('&');
}
//Filter method
function filterClients(){
    //Get company value
  const industry = document.querySelector('#filterForm select[name=industry]').value
  const company = document.querySelector('#filterForm input[name=company]').value
  const staff = document.querySelector('#filterForm select[name=staff]').value
  //Get Status value
  const status = document.querySelector('#filterForm select[name=status]').value
  //Add to URL
    const url = baseUrl  + "&" + encodeQueryData({company,status,industry,staff})
  console.log(staff);
  dataTable.ajax.url( url ).load();
}
$(document).ready(function(){

    dataTable = $('#clients').DataTable({
    responsive: true,
    "deferRender": true,
    "processing": true,
    "serverSide": true,
    "ordering": false, //disable column ordering
    "lengthMenu": [
      [25, 50, 100, -1],
      [25, 50, 100, "All"] // change per page values here
    ],
    "pageLength": 25,
    "ajax": {
      url: baseUrl,
      method: 'GET'
    },
    // dom: '<"html5buttons"B>lTfgitp',
    "dom": "<'row' <'col-md-12'>><'row'<'col-md-8 col-sm-12'lB><'col-md-4 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
    buttons: [
      { extend: 'copy',exportOptions: {columns: [0, 1, 2, 3,4,5,6]}},
      {extend: 'csv', title: '{{ config('app.name', 'Summit') }} - List of all Clients', exportOptions: {columns: [0, 1, 2, 3,4,5,6]}},
      {extend: 'excel', title: '{{ config('app.name', 'Summit') }} - List of all Clients',exportOptions: {columns: [0, 1, 2, 3,4,5]}},
      {extend: 'pdf', title: '{{ config('app.name', 'Summit') }} - List of all Clients',exportOptions: {columns: [0, 1, 2, 3,4,5]}},
      {extend: 'print',
      customize: function (win){
        $(win.document.body).addClass('white-bg');
        $(win.document.body).css('font-size', '10px');
        $(win.document.body).find('table')
        .addClass('compact')
        .css('font-size', 'inherit');
      }
    }
  ],
  columns: [
    {data: 'ClientID', name: 'ClientID', orderable: true, searchable: true},
    {data: 'CompanyName', name: 'CompanyName', orderable: true, searchable: true},
    {data: 'IndustrySector', name: 'IndustrySector', orderable: true, searchable: true},
    {data: 'PhysAdd', name: 'PhysAdd', orderable: false, searchable: false},
    {data: 'EmailAdd', name: 'EmailAdd', orderable: true, searchable: false},
    {data: 'ContactPerson', name: 'ContactPerson', orderable: true, searchable: false},
    {data: 'PhoneNumber', name: 'PhoneNumber', orderable: true, searchable: false},
    {data: 'status', name: 'status', orderable: true, searchable: true},
    {data: 'tools', name: 'tools', orderable: false, searchable: false,"defaultContent": "<i>Not set</i>"},
  ],
  rowCallback: function(row,data,index){
  	if(data[7] == 4){
  		$(row).find('td:eq(7)').html('<span class="badge badge-secondary">Active Loyal</span>');
  	}

  }
});
});
</script>
@endsection
