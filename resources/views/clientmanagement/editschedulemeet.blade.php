
      <div class="tab-form ">
          <div class="tab-form-header text-white">
            <h4>Schedule Meeting And Send Proposal</h4>
          </div>&nbsp
          <div class="row">
            <div class="col">
              <!--client meetup modal-->
              <div class="form-group form-spacing">
                <label for="client_meet_option">Client Meeting:<span class="asterick" style="color:red">*</span></label>
                @if ($client->bdm_date)
                  <p><b> Yes </b></p>
                @elseif($client->bdm_date)
                  <p><b> No</b></p>
                @else 
                  <select class="form-control" name="client_meet_option" id="client_meet_option" value="" required>
                    <option value=""> -- Choose Option -- </option>
                    <option value="1">Yes</option>
                    <option value="0">No</option>
                  </select>
                @endif
              </div>
            </div>
            <div class="col">
              <div class="form-group form-spacing">
                <label for="client_meet">{{ __('Meeting Date') }}</label>
                <input  type="date" id="from"  name="client_meet"  class="form-control{{ $errors->has('client_meet') ? ' is-invalid' : ''}}" value="{{ $client->bdm_date}}">
                @if ($errors->has('client_meet'))
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('client_meet') }}</strong>
                  </span>
                @endif
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col">
              <div class="form-group form-spacing">
                <label for="client_proposal_action">Request for Proposal:<span class="asterick" style="color:red">*</span></label>
                @if ($client->bdm_proposal)
                  <p><b> Yes </b></p>
                @else
                <select class="form-control" name="client_proposal_option" id="client_proposal_action"  required>
                  <option value=""> -- Choose Option -- </option>
                 @if(empty($client->bdm_proposal))
                  <option value="1">Yes</option>
                  <option value="0" selected>No</option>
                  @endif
                </select>
                @endif
              </div>
            </div>
              <div class="col">
              <div class="form-group form-spacing">
                <label for="proposal_request">Attach Proposal</label><!--to find a route to the uploads-->
                @if($client->bdm_proposal) <a href="/summitre/public/clients/bdm_proposal/{{ $client->bdm_proposal }}" target="_blank">{{ $client->bdm_proposal }}</a> @endif
                <input type="file" name="proposal_request" class="form-control{{ $errors->has('proposal_request') ? ' is-invalid' : ''}}" value="">
                   @if ($errors->has('proposal_request'))
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('proposal_request') }}</strong>
                      </span>
                  @endif
              </div>
            </div>
          </div>
           <div class="form-group form-spacing">
                    <label for="comment">Consultant Remarks:<span class="asterick" style="color:red">*</span></label>
                    <textarea name="commentconsultant"class="form-control " rows="2" value="">{{$client->comment}}</textarea>
          </div>
      </div>
      <script type="text/javascript">
document.getElementById('from').max = new Date(new Date().getTime() - new Date().getTimezoneOffset() * 60000).toISOString().split("T")[0];

</script>
<script>
jQuery(document).ready(function($) {
if ( $('#from')[0].type != 'date' ) $('#from').datepicker();

});
</script>
