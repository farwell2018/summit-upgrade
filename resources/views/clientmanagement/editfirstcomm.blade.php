<div class="tab-form ">
          <div class="tab-form-header text-white">
            <h4>First Communication</h4>
           </div>&nbsp
  <div class="row">
    <div class="col">
      <div class="form-group form-spacing">
        <label for="date_first_communication">Date of First Communication:<span class="asterick" style="color:red">*</span></label>
        <input id="1comm" type="date" name="1stcommunication" class=" form-control"   value="{{$client->d_o_c}}" required>
     </div>
    </div>
    <div class="col">
      <div class="form-group form-spacing">
        <label for="initial_communication">Initial Communication:</label>
          <select class="form-control" name="initial_communication" id="initial_communication" value=""required>
            <option disabled selected>Select Initial Communication</option>
            @if(!empty($client->initial_comm))
            <option value="{{$client->initial_comm}}" selected="selected">{{$client->initial_comm}}</option>
            <option value="1">Email</option>
            <option value="2">Phone Call</option>
            <option value="3">Skype Call</option>
            <option value="4">Face to Face Meeting</option>
            @else
            <option value="1">Email</option>
            <option value="2">Phone Call</option>
            <option value="3">Skype Call</option>
            <option value="4">Face to Face Meeting</option>
            @endif
          </select>
      </div>
    </div>
    <div class="col">
        <label>Date of Last Communication<span class="asterick" style="color:red">*</span></label>
        <input  id="last" type="date"class="form-control" name="dateoflastcommunication" value="" required>
    </div>
  </div>
</div>
<script type="text/javascript">
document.getElementById('1comm').max = new Date(new Date().getTime() - new Date().getTimezoneOffset() * 60000).toISOString().split("T")[0];

document.getElementById('last').max = new Date(new Date().getTime() - new Date().getTimezoneOffset() * 60000).toISOString().split("T")[0];
</script>
<script>
jQuery(document).ready(function($) {
if ( $('#last')[0].type != 'date' ) $('#last').datepicker();

});
</script>

