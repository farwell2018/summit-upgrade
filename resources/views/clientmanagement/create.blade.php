@extends('layouts.admin')

@section('title','Client Management')


@section('content')

<!--This designed and implemented by Farwell-consultants-->
<div class="row " style="margin-left:8px">
        <a href="{{route('home')}}" style="color:#858796" >
            <span class="text">Home</span> </a> &nbsp; | &nbsp;
        <a href="{{route('clientmanagement.index')}}" >
            <span class="text">Client List</span> </a>
</div>
    <br>
<div class="kt-portlet">
  <div class="kt-portlet__head">
    <div class="kt-portlet__head-label">
      <h3 class="kt-portlet__head-title">
        Add a New Client
      </h3>
    </div>
  </div>
     @if (session('status'))
          <div class="alert alert-success" role="alert">
              {{ session('status') }}
          </div>
      @endif
  <!--begin::Form-->
  <form class="kt-form" method="POST" action="{{ route('clientmanagement.store') }}"  enctype="multipart/form-data">
    <div class="kt-portlet__body">
      @csrf
      @method('POST')
      <div class="row">
          <div class="col">
              <div class="form-group form-spacing">
            <label for="company_name">{{ __('Company Name') }}<span class="asterick" style="color:red">*</span></label>
            <input type="text" name="company_name" class="form-control{{ $errors->has('company_name') ? ' is-invalid' : ''}}" value="{{old('company_name')}}" required>

            @if ($errors->has('company_name'))
            <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('company_name') }}</strong>
            </span>
            @endif
          </div>
          </div>
          <div class="col">
            <div class="form-group form-spacing">
        <label for="industry">{{ __('Industry Sector') }}<span class="asterick" style="color:red">*</span></label>
        <select id="industry" name="industry" class="form-control" required>
            <option disabled selected>Select Industry</option>
            @foreach($industries as $industry)
             <option value="{{$industry->Name}}">{{$industry->Name}}
            @endforeach
          </select>
      </div>
          </div>
      </div>
      
   <div class="row">
        <div class="col">
          <div class="form-group form-spacing">
            <label for="physical_address">{{ __('Physical Address') }}<span class="asterick" style="color:red">*</span></label>
            <textarea name="physical_address" row="50" cols="80" class="form-control" required></textarea>
                @if ($errors->has('physical_address'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('physical_address') }}</strong>
                </span>
                @endif
        </div>
      </div>
      <div class="col">
         <div class="form-group form-spacing">
            <label for="postal_address">{{ __('Postal Address') }}</label>
            <textarea name="postal_address" row="50" cols="80" class="form-control"></textarea>
                @if ($errors->has('postal_address'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('postal_address') }}</strong>
                </span>
                @endif

            </div>
         </div>
   </div>       
         <div class="form-row">
             <div class="col">
                    <div class="form-group form-spacing">
                        <label for="contact_person">{{ __('Contact Person') }}<span class="asterick" style="color:red">*</span></label>
                        <input type="text" name="contact_person" class="form-control{{ $errors->has('contact_person') ? ' is-invalid' : ''}}" value="{{old('contact_person')}}" required>

                        @if ($errors->has('contact_person'))
                        <span class="invalid-feedback" role="alert">
                          <strong>{{ $errors->first('contact_person') }}</strong>
                        </span>
                    @endif
                  </div>
             </div>
             <div class="col">
                  <div class="form-group form-spacing">
                      <label for="contact_title">{{ __('Contact Title') }}<span class="asterick" style="color:red">*</span></label>
                      <input type="text" name="contact_title" class="form-control{{ $errors->has('contact_title') ? ' is-invalid' : ''}}" value="{{old('contact_title')}}" required>

                      @if ($errors->has('contact_title'))
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('contact_title') }}</strong>
                      </span>
                      @endif
                  </div>
            </div>
                 <div class="col">
                   <div class="form-group form-spacing">
                      <label for="contact_email">{{ __('Email Address') }}<span class="asterick" style="color:red">*</span></label>
                      <input type="email" name="contact_email" class="form-control{{ $errors->has('contact_email') ? ' is-invalid' : ''}}" value="{{old('contact_email')}}" required>

                      @if ($errors->has('contact_email'))
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('contact_email') }}</strong>
                      </span>
                      @endif
                    </div>
                </div>
              <div class="col">
                  <div class="form-group form-spacing">
                  <label for="phone_number">{{ __('Phone Number') }}<span class="asterick" style="color:red">*</span></label>
                  <input type="tel" name="phone_number" class="form-control{{ $errors->has('phone_number') ? ' is-invalid' : ''}}" value="{{old('phone_number')}}" required>

                  @if ($errors->has('phone_number'))
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('phone_number') }}</strong>
                  </span>
                  @endif
                </div>
              </div>
        </div>     
<div class="row">
        <div class="col">
          <div class="form-group form-spacing">
            <label for="second_contact_person">{{ __('Second Contact Person') }}</label>
            <input type="text" name="second_contact_person" class="form-control{{ $errors->has('second_contact_person') ? ' is-invalid' : ''}}" value="{{old('second_contact_person')}}" >

            @if ($errors->has('second_contact_person'))
            <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('second_contact_person') }}</strong>
            </span>
            @endif
          </div>
       </div>
       <div class="col">
          <div class="form-group form-spacing">
            <label for="second_contact_title">{{ __('Second Contact Title') }}</label>
            <input type="text" name="second_contact_title" class="form-control{{ $errors->has('second_contact_title') ? ' is-invalid' : ''}}" value="{{old('second_contact_title')}}" >

            @if ($errors->has('second_contact_title'))
            <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('second_contact_title') }}</strong>
            </span>
            @endif
          </div>
        </div>
       <div class="col">
         <div class="form-group form-spacing">
            <label for="second_contact_email">{{ __('Second Email Address') }}</label>
            <input type="email" name="second_contact_email" class="form-control{{ $errors->has('second_contact_email') ? ' is-invalid' : ''}}" value="{{old('second_contact_email')}}" >

            @if ($errors->has('second_contact_email'))
            <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('second_contact_email') }}</strong>
            </span>
            @endif
          </div>
        </div>
    <div class="col">
          <div class="form-group form-spacing">
            <label for="second_phone_number">{{ __('Second Phone Number') }}</label>
            <input type="tel" name="second_phone_number" class="form-control{{ $errors->has('second_phone_number') ? ' is-invalid' : ''}}" value="{{old('second_phone_number')}}" >

            @if ($errors->has('second_phone_number'))
            <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('second_phone_number') }}</strong>
            </span>
            @endif
          </div>
        </div>
   </div>
   
      <div class="row">
        <div class="col">
          <div class="form-group form-spacing">
            <label for="second_contact_person">{{ __('Third Contact Person') }}</label>
            <input type="text" name="third_contact_person" class="form-control{{ $errors->has('third_contact_person') ? ' is-invalid' : ''}}" value="{{old('third_contact_person')}}" >

            @if ($errors->has('third_contact_person'))
            <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('third_contact_person') }}</strong>
            </span>
            @endif
          </div>
       </div>
       <div class="col">
          <div class="form-group form-spacing">
            <label for="second_contact_title">{{ __('Third Contact Title') }}</label>
            <input type="text" name="third_contact_title" class="form-control{{ $errors->has('third_contact_title') ? ' is-invalid' : ''}}" value="{{old('third_contact_title')}}" >

            @if ($errors->has('second_contact_title'))
            <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('third_contact_title') }}</strong>
            </span>
            @endif
          </div>
        </div>
       <div class="col">
         <div class="form-group form-spacing">
            <label for="second_contact_email">{{ __('Third Email Address') }}</label>
            <input type="email" name="third_contact_email" class="form-control{{ $errors->has('third_contact_email') ? ' is-invalid' : ''}}" value="{{old('third_contact_email')}}" >

            @if ($errors->has('third_contact_email'))
            <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('third_contact_email') }}</strong>
            </span>
            @endif
          </div>
        </div>
    <div class="col">
          <div class="form-group form-spacing">
            <label for="second_phone_number">{{ __('Third Phone Number') }}</label>
            <input type="tel" name="third_phone_number" class="form-control{{ $errors->has('third_phone_number') ? ' is-invalid' : ''}}" value="{{old('third_phone_number')}}" >

            @if ($errors->has('third_phone_number'))
            <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('third_phone_number') }}</strong>
            </span>
            @endif
          </div>
        </div>
   </div>

<div class="row">
   <div class="col">
      <div class="form-group form-spacing">
          <label for="website">{{ __('Website') }}<span class="asterick" style="color:red">*</span></label>
          <input type="text" name="website" class="form-control{{ $errors->has('website') ? ' is-invalid' : ''}}" value="{{old('website')}}" required>
          @if ($errors->has('website'))
            <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('website') }}</strong>
            </span>
            @endif
      </div>
    </div>
    <div class="col">
      <div class="form-group form-spacing">
        <label for="consultant">Consultant Assigned <span class="asterick" style="color:red">*</span></label>
                  <select id="consultant" name="consultant" class="form-control {{ $errors->has('consultant') ? ' is-invalid' : '' }}" required>
                     <option value="{{Auth::user()->SummitStaff->StaffID}}" selected>{{Auth::user()->SummitStaff->Firstname.' '. Auth::user()->SummitStaff->Lastname}} </option>
                     @foreach($SummitStaff as $staff)
                    <option value="{{$staff->StaffID}}">{{$staff->Firstname}} {{$staff->Lastname}} </option>
                    @endforeach
                  </select>
                  @if ($errors->has('consultant'))
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('consultant') }}</strong>
                  </span>
                  @endif
      </div>
    </div>
    <div class="col"><!--the date type changed from date to text-->
        <div class="form-group form-spacing">
             <label for="regon">Client Registered On<span class="asterick"style="color:red">*</span></label>
             <input type="date" id="registred_on"  name="date" class=" form-control" placeholder="DD/MM/YYYY"  value="" required >          
        </div>       
    </div>
    <div class="col">
        <div class="form-group form-spacing">
            <label for="datelastcomm">Date of Last Communication:<span class="asterick"style="color:red">*</span></label>
            <input type="date" id="last_comm" class="form-control" name="datelastcomm" placeholder="DD/MM/YYYY"  value="" required>
        </div>
    </div>
        <div class="col">
      <div class="form-group form-spacing">       
        <label for="client_source">Client Source:<span class="asterick" style="color:red">*</span></label>
        <select class="form-control" name="sourceSelect1" id="sourceSelect1" required>
          <option value="" disabled selected>--Select Option--</option>
          <option value="1">Website</option>
          <option value="2">Recommendation</option>
          <option value="3">Referal</option>
          <option value="4">Mainline</option>
          <option value="5">Cold Approach / BDM</option>
          <option value="6">LPO / Tender</option>
          <option value="7">Others</option>
        </select>
      </div>
      <div id="rev" class="form-group form-spacing" style="display: none;">
        <label>Other Sources:</label>
        <!--This may end up flagging errors!!-->
        <input type="text" class="form-control" id="id_sourceselect1" name="annualpercentagerevenue"  /><br/>    
    </div>
    </div>
</div>


   <div class="form-group form-spacing">
                    <label for="comment">Consultant Remarks:<span class="asterick" style="color:red">*</span></label>
                    <textarea id="editor" name="comment"class="form-control description {{ $errors->has('comment') ? ' is-invalid' : ''}}" rows="4" value="{{old('comment')}}"></textarea>
                     @if ($errors->has('comment'))
                     <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('comment') }}</strong>
              </span>
            @endif
  </div>
 <div class="text-center">
<div class="kt-portlet__foot">
      <div class="kt-form__actions">
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="reset" class="btn btn-secondary">Cancel</button>
      </div>
</div>
</div>
<script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
  $(document).on("click", ".btneditsec1_2", function (e) {
    e.preventDefault();
    var _self = $(this);
    var myacc = _self.data('sourceSelect1');
    var mywacc = _self.data('wacc');
    var myitc = _self.data('itc');
        $('#sourceSelect1').val(myacc).change(); //trigger the change event so that associated event handler will get called
        $("#id_sourceselect1").val(mywacc);
    });
  $('select[name=sourceSelect1]').on('change', function()
   {
    if (this.value == '7') {
      $("#rev").show();
    } else {
      $("#rev").hide();
    }
  });
</script>
<script>
  tinymce.init({
    selector: 'textarea#editor',
    menubar: false
  });
</script>
<script>
    document.getElementById('dol').max = new Date(new Date().getTime() - new Date().getTimezoneOffset() * 60000).toISOString().split("T")[0];
</script>

<script>
jQuery(document).ready(function($) {
if ( $('#registred_on')[0].type != 'date' ) $('#registred_on').datepicker();

if ( $('#last_comm')[0].type != 'date' ) $('#last_comm').datepicker();

});
</script>
@endsection

<!--Date validation script function..future dates.  no accepted..-->