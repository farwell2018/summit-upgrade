
      <div class="tab-form ">
          <div class="tab-form-header text-white">
            <h4>SLA Signed</h4>
          </div>&nbsp
          <div class="row">
            <div class="col">
              <!--client meetup modal-->
              <div class="form-group form-spacing">
                <label for="sla_shared">SLA Shared:<span class="asterick" style="color:red">*</span></label>
                @if ($client->sla_date)
                  <p><b> Yes </b></p>
                @else
                <select class="form-control" name="sla_shared_option" id="sla_shared" value="">
                  <option value=""> -- Choose Option -- </option>
                  <option value="1">Yes</option>
                  <option value="0">No</option>
                </select>
                @endif
              </div>
            </div>
            <div class="col">
              <div class="form-group form-spacing">
                <label for="sla_shared">SLA Date Shared</label>
                <input type="date" id="frommm" name="sla_shared"  class="form-control{{ $errors->has('sla_shared') ? ' is-invalid' : ''}}" value="{{ $client->sla_date }}">
                @if ($errors->has('sla_shared'))
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('sla_shared') }}</strong>
                  </span>
               @endif
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col">
              <div class="form-group form-spacing">
                <label for="sla_sign_option">SLA Signed:<span class="asterick" style="color:red">*</span></label>
                @if ($client->sla_signed)
                  <p><b> Yes </b></p>
                @else
                 <select class="form-control" name="sla_signed_option" id="client_meet" value="">
                  <option value=""> -- Choose Option -- </option>
                  <option value="1">Yes</option>
                  <option value="0">No</option>
                </select>
                @endif
              </div>
               <div class="form-group form-spacing">
                <label for="sla_request">Attach SLA</label><!--to find a route to the uploads-->
                @if($client->sla_proposal) <a href="/summitre/public/clients/sla/{{$client->sla_proposal}}" target="_blank">{{ $client->sla_proposal }}</a> @endif
                <input type="file" name="sla_request" class="form-control{{ $errors->has('sla_request') ? ' is-invalid' : ''}}" value="">
                   @if ($errors->has('sla_request'))
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('sla_request') }}</strong>
                      </span>
                  @endif
              </div>
            </div>
            <div class="col">
              <div class="form-group form-spacing">
                  <label for="sla_expire">SLA Expiring Date</label>
                  <input type="date" id="expiry" class="form-control" name="slaexpirationdate" value="{{$client->sla_expirationdate}}"/>
              </div>
              <div class="form-group form-spacing">
                <label for="sla_sign">SLA Comments</label>
                <textarea name="sla_signed" class="form-control{{$errors->has('sla_signed')? 'is-invalid':''}}" placeholder="SLA Comments" value="">{{$client->sla_signed}}</textarea><br>
                @if ($errors->has('sla_signed'))
                  <span class="text-danger">
                    <strong>{{ $errors->first('sla_signed') }}</strong>
                  </span>
                @endif
              </div>
            </div>
          </div>
      </div>
<script type="text/javascript">
document.getElementById('frommm').max = new Date(new Date().getTime() - new Date().getTimezoneOffset() * 60000).toISOString().split("T")[0];
</script>
<script>
jQuery(document).ready(function($) {
if ( $('#frommm')[0].type != 'date' ) $('#frommm').datepicker();

if ( $('#expiry')[0].type != 'date' ) $('#expiry').datepicker();

});
</script>

