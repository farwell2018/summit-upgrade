@extends('layouts.admin')
@section('title','Candidate Management')
@section('content')
<div class="card" style="background-color: #1C2E5C;align-content: center;">
    <div id="filterForm5">
      <div class="card-body">
        <label style="color: #80CFFF">CANDIDATE MANAGEMENT</label>
        <div class="row">
          <div class="col">
            <div class="form-group form-spacing">
              <label style="color: #80CFFF">First Name</label>
              <input type="text" name="firstname" class="form-control" placeholder="First Name">
            </div>
          </div>
          <div class="col">
            <div class="form-group form-spacing">
              <label style="color: #80CFFF">Last Name</label>
              <input type="text" name="lastname" class="form-control" placeholder="Last Name">
            </div>
          </div>
          <div class="col">
            <div class="form-group form-spacing">
              <label style="color: #80CFFF">Phone Number</label>
              <input type="text" name="phonenumbercandidate" class="form-control" placeholder="Phone Number">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <div class="form-group form-spacing">
              <label style="color: #80CFFF">Gender</label>
              <select class="form-control" name="gender" >
              <option value="">Select Gender</option>
                <option value="Male">Male</option>
                <option value="Female">Female</option>
              </select>
            </div>
          </div>
          <div class="col">
            <div class="form-group form-spacing">
              <label style="color: #80CFFF">Nationality</label>
              <select class="form-control" name="nationality">
                <option value="">Select Nationality</option>
                <option value="Kenyan">Kenyan</option>
                <option value="Non-Kenyan">Non Kenyan</option>
              </select>
            </div>
          </div>
          <div class="col">
            <div class="form-group form-spacing">
              <label style="color: #80CFFF">Skills</label>
              <select class="form-control" name="skills">
                @php
            $skills = array(
          array("id" => "Business_Management ", "name" => "Business Management"),
          array("id" => "Computer", "name" => "Computer"),
          array("id" => "Construction", "name" => "Construction"),
          array("id" => "Customer_Service", "name" => "Customer Service"),
          array("id" => "Diplomacy", "name" => "Diplomacy"),
          array("id" => "Effective_Listening", "name" => "Effective Listening"),
          array("id" => "Financial_Management", "name" => "Financial Management"),
          array("id" => "Interpersonal", "name" => "Interpersonal"),
          array("id" => "Multi-tasking", "name" => "Multi-tasking"),
          array("id" => "Negotiating", "name" => "Negotiating"),
          array("id" => "Organisation", "name" => "Organisation"),
          array("id" => "People_Management", "name" => "People Management"),
          array("id" => "Planning", "name" => "Planning"),
          array("id" => "Presentation", "name" => "Presentation"),
          array("id" => "Problem_Solving", "name" => "Problem Solving"),
          array("id" => "Programming", "name" => "Programming"),
          array("id" => "Report_Writing", "name" => "Report Writing"),
          array("id" => "Research", "name" => "Research"),
          array("id" => "Resourcefulness", "name" => "Resourcefulness"),
          array("id" => "Sales_Ability", "name" => "Sales Ability"),
          array("id" => "Technical", "name" => "Technical"),
          array("id" => "Time_Management", "name" => "Time Management"),
          array("id" => "Training", "name" => "Training"),
          array("id" => "Verbal_Communication", "name" => "Verbal Communication"),
          array("id" => "Written_Communication", "name" => "Written Communication"),
          );
            @endphp
                <option value="">Skills</option>
                @foreach($skills as $skill)
                @if(in_array($skill['id'],array_values($skills)))
              <option value="{{$skill['id']}} ">{{$skill['name']}}</option>
              @else
              <option value="{{$skill['id']}} ">{{$skill['name']}}</option>
              @endif
              @endforeach
              </select>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <div class="form-group form-spacing">
              <label style="color: #80CFFF">Education</label>
              <select class="form-control" name="education">
                <option value="">Select Education</option>
                <option value="Certificate">Certificate</option>
                <option value="Degree">Bachelors</option>
                 <option value="Masters">Masters</option>
                 <option value="PHD">PHD</option> <option value="Diploma">Diploma</option>
                 <option value="Higher Diploma">Higher diploma</option>
                 <option value="PGDip">PGDip</option>
                 <option value="PGCert">PGCert</option>
              </select>
            </div>
          </div>
          <div class="col">
            <div class="form-group form-spacing">
              <label style="color: #80CFFF">Professional Qualification</label>
              <select class="form-control" name="prof">
                <option value="">Select Qualification</option>
                 @foreach($profqualtitles as $profqualtitle)
                 <option value="{{$profqualtitle->profqualtitle}}">{{$profqualtitle->profqualtitle}}</option>
                 @endforeach
              </select>
            </div>
          </div>
          <div class="col">
            <div class="form-group form-spacing">
              <label style="color: #80CFFF">Industry Sector</label>
              <select class="form-control" name="industrysector">
                <option value="">Select Industry Sector</option>
                 @foreach($industry as $industries)
                 <option value="{{$industries->Name}}">{{$industries->Name}}</option>
                   @endforeach
              </select>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <div class="form-group form-spacing">
              <label style="color: #80CFFF">Job Title</label>
              <select class="form-control" name="industryfunctions">
                <option value="">Select Job Title</option>
                @foreach($industryfunctions as $industryfunction)
                <option value="{{$industryfunction->Name}}">{{$industryfunction->Name}}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="col">
            <div class="form-group form-spacing">
              <label style="color: #80CFFF">Hard Skills</label>
              <select class="form-control" name="hardskills">
                <option value="">Select Hard Skill</option>
                @foreach($hardskills as $hardskill)
                <option value="{{$hardskill->ID}}">{{$hardskill->Name}}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="col">
            <div class="form-group form-spacing">
              <label style="color: #80CFFF">Company Name</label>
              <input type="text" name="companyname" class="form-control">
            </div>
          </div>
        </div>
        <div class="col text-right">
            <button class="btn  pull-right" onclick="filterCandidates()" style="background-color:  #FFCB00;color:#3069AB;margin-left: 15px;margin-bottom: 15px;">SEARCH</button>
               <button class="btn pull-right"  onClick="window.location.reload();" style="background-color: #FFCB00;color: #000" >RESET</button>
        </div>
      </div>
    </div>
  </div>
<div class="card mb-5">
  <div class="card-header tab-form-header text-white" style="background-color: #1C2E5C">
   CANDIDATE MANAGEMENT
  </div><br>
  <div class="card-body">
      <table class="table table table-bordered table-striped table-hover dt-responsive wrap width=100%" id="candidatestable">
        <thead style="background-color: #FFCB00">
           <tr>
              <th style="color: #000">Full Name</th>
              <th style="color: #000">Gender</th>
              <th style="color: #000">Phone Number</th>
              <th style="color: #000">Actions</th>
           </tr>
        </thead>
     </table>
  </div>
  <div class="row pagination-row">

  </div>
</div>
@endsection
@section('footer-js')

<script>
var dataTable;
let baseUrl = "{!! route('candidatemanagement.json') !!}?";

function encodeQueryData(data) {
 const ret = [];
 for (let d in data)
   ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
 return ret.join('&');
}
//Filter method
function filterCandidates(){
    //Get company value
    //console.log(document.querySelector('#filterForm5 input[name=firstname]').value);
    const firstname = document.querySelector('#filterForm5 input[name=firstname]').value

    const lastname = document.querySelector('#filterForm5 input[name=lastname]').value

    const phonenumbercandidate = document.querySelector('#filterForm5 input[name=phonenumbercandidate]').value

    const gender = document.querySelector('#filterForm5 select[name=gender]').value

    const nationality = document.querySelector('#filterForm5 select[name=nationality]').value

    const skills = document.querySelector('#filterForm5 select[name=skills]').value

    const education = document.querySelector('#filterForm5 select[name=education]').value

    const profqualtitles = document.querySelector('#filterForm5 select[name=prof]').value

    const industrysector = document.querySelector('#filterForm5 select[name=industrysector]').value

    const industryfunctions = document.querySelector('#filterForm5 select[name=industryfunctions]').value

    const hardskills = document.querySelector('#filterForm5 select[name=hardskills]').value

    const companyname = document.querySelector('#filterForm5 input[name=companyname]').value


    //Get Status value
    // const status = document.querySelector('#filterForm5 select[name=status]').value
    //Add to URL
    const url = baseUrl  + "&" + encodeQueryData({firstname,lastname,phonenumbercandidate,gender,nationality,skills,education,profqualtitles,industrysector,industryfunctions,hardskills,companyname})
  console.log(url);
  dataTable.ajax.url( url ).load();
}
$(document).ready(function(){

dataTable = $('#candidatestable').DataTable({
    responsive: true,
    "processing": true,
    "serverSide": true,
    "lengthMenu": [
      [50, 100, 150, -1],
      [50, 100, 150, "All"] // change per page values here
    ],
    "pageLength": 50,
    "ajax": {
      url: baseUrl,
      method: 'GET'
    },
    // dom: '<"html5buttons"B>lTfgitp',
    "dom": "<'row' <'col-md-12'>><'row'<'col-md-8 col-sm-12'lB><'col-md-4 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
    buttons: [
      { extend: 'copy',exportOptions: {columns: [0, 1, 2, 3,4,5,6]}},
      {extend: 'csv', title: '{{ config('app.name', 'Summit') }} - List of all Clients', exportOptions: {columns: [0, 1, 2, 3,4,5,6]}},
      {extend: 'excel', title: '{{ config('app.name', 'Summit') }} - List of all Clients',exportOptions: {columns: [0, 1, 2, 3,4,5]}},
      {extend: 'pdf', title: '{{ config('app.name', 'Summit') }} - List of all Clients',exportOptions: {columns: [0, 1, 2, 3,4,5]}},
      {extend: 'print',
      customize: function (win){
        $(win.document.body).addClass('white-bg');
        $(win.document.body).css('font-size', '10px');
        $(win.document.body).find('table')
        .addClass('compact')
        .css('font-size', 'inherit');
      }
    }
  ],
  columns: [
      {data: 'CV_ID', name: 'CV_ID', orderable: true, searchable: true},
      {data: 'Gender', name: 'Gender', orderable: true, searchable: true},
      {data: 'Phonenumber', name: 'Phonenumber', orderable: true, searchable: true},
      {data: 'Actions', name: 'Actions', orderable: true, searchable: true},
  ],
 
});
});
</script>
@endsection
