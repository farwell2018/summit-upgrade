@extends('layouts.admin')

@section('title','Create Job Test')


@section('content')
<div class="kt-portlet">
  <div class="kt-portlet__head">
    <div class="kt-portlet__head-label">
      <h3 class="kt-portlet__head-title">
        Add a New Test
      </h3>
    </div>
  </div>
  <form class="kt-form" method="POST" action="{{ route('jobtests.store') }}"  enctype="multipart/form-data">
    <div class="kt-portlet__body">
      @csrf
      <div class="row">
        <div class="form-group" style="width:100%">
            <label for="jobadselect">Job</label>
            <input type="hidden" value="{{$jobads->ID}}" name="jobadselect">
            <input type="text" size="60" value="{{$jobads->JobTitle}}" placeholder=""  class="form-control" readonly >
{{--            <select id="jobadselect" name="jobadselect" class="form-control">--}}
{{--            <option disabled selected>Select JobAd</option>--}}
{{--             @foreach($jobads as $jobad)           --}}
{{--             <option value="{{$jobad->ID}}">{{$jobad->JobTitle}}--}}
{{--             @endforeach           --}}
{{--            </select>           --}}
        </div>
        </div>
        <!-- <div id="questions">-->
        <!--<div class="form-group">-->
        <!--   <label for="question1">Question 1<span class="required" style="color: red">*</span></label>-->
        <!--      <input maxlength="255" name="QuestionFields[1][questions]" type="text" value="" class="form-control">-->
        <!--</div>-->
        <!-- <div class="form-group">-->
        <!--   <label for="answer">Answer1<span class="required" style="color: red">*</span></label>-->
        <!--      <input maxlength="255"name="QuestionFields[1][answers][1]" type="text" value="" class="form-control">-->
        <!--</div>-->
        <!-- <div class="form-group">-->
        <!--   <label for="marks">Marks1<span class="required" style="color: red">*</span></label>-->
        <!--      <input size="60"maxlength="255" name="QuestionFields[1][marks][1]" type="text" value="" class="form-control">-->
        <!--</div>-->


        <!-- </div>-->
        
         <div id="requirements">

            <div class="row">
              <div class="form-group" style="width:100%">
      <label for="PreQualTest_Question_1" class="required">
        Question 1 <span class="required">*</span>
      </label>
      <input   name="Question[1][Question]" id="PreQualTest_Question_1" type="text" value="" class="form-control">
    </div></div>
    <div class="row">
      <div class="form-group" style="width:100%">
      <label for="PreQualAnswers_Answers_1" class="required">
        Answer 1 <span class="required">*</span>
      </label>
      <input   name="Question[1][Answers][1]" id="PreQualAnswers_Answers_1" type="text" value="" class="form-control">
    </div></div>
                <div class="row">
                  <div class="form-group" style="width:100%">
      <label for="PreQualAnswers_Marks_1" class="required">
        Marks 1 <span class="required">*</span>
      </label>
      <input   name="Question[1][Marks][1]" id="PreQualAnswers_Marks_1" type="text" value="" class="form-control">
    </div></div>
    
  </div><!--requirements-->

            <div class=" form-group">
              <input type="button" value="+Add Answer" class="btn btn-primary pull-right" id="addanswer" />
            </div>
           <div class=" form-group">
              <input type="button" value="+Add Question" class="btn btn-primary pull-right" id="addquestion" />
            </div>


        <div class="kt-portlet__foot">
          <div class="kt-form__actions">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </div>
   </div>
  </form>
</div>
@endsection
