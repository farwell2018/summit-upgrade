@extends('layouts.admin')

@section('title','View Job Test Resource Management ')

@section('content')

 <div class="row user-add-button">
  <a href="{{route('jobtests.editviewquestions',[$job->ID])}}" class="btn btn-icon-split"  style="margin-right: 15px;background-color: #FFCB00;color:#3069AB">
  <span class="icon" style="color:"><i class="fas fa-plus"></i></span>
  <span class="text">Edit Job Test</span> </a>
</div>

<div class="card mb-3">
    <div class="card-header tab-form-header text-white" style="background-color: #1C2E5C">
        Questionaire for &nbsp|&nbsp {{$job->JobTitle}}
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <div class="questions">
                               @php $i = 0; @endphp
                        @php $j = 0; @endphp
                        @foreach($prequalquestions  as $key => $question)
                        @php $i++; @endphp
                        <label for="question">Question {{$i}}</label>
                        <br>
                        <input type="text" name="question" value="{{$question->Question}}" class="form-control" disabled="">
                        <br>
                        @foreach($answers[$key] as $answer) 
                        @php $j++; @endphp
                        <label for="answers">Answers {{$j}}</label>
                        <div class="row">
                        <div class="col">                   
                                <input type="text" name="{{$answer->PreQualTestID}}" value="{{$answer->Answers}}" class="form-control" disabled="">
                        </div>
                        <label>Mark {{$j}}</label>
                        <div class="col">       
                                <input type="text" name="{{$answer->PreQualTestID}}" value="{{$answer->Marks}}" class="form-control" disabled="">
                        </div>      
                        </div>  
                         
                        <br>                        
                        @endforeach     
                        <a href="{{route('jobtests.destroy',[$question->PreQualTestID, $job->ID])}}" class="btn btn-icon-split"  style="margin-right: 15px;background-color: #FFCB00;color:#3069AB;float:right"><span class="text">Delete Question {{$i}}</span> </a><br><br>
                        @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
