@extends('layouts.admin')
@section('title','jobtest')
@section('content')
<!--  <div class="row user-add-button">
  <a href="#" class="btn btn-primary btn-icon-split" style="margin-right: 15px;">
  <span class="icon"><i class="fas fa-plus"></i></span>
  <span class="text">New Job Test</span> </a>
</div> -->
<div class="card mb-5">
  <div class="card-header tab-form-header text-white" style="background-color: #1C2E5C">
    JOB TESTS MANAGEMENT
  </div>
  <div class="card-body">
      <table class="table" id="clients" width="100%">
        <thead>
           <tr>
              <th>Job:</th>
              <th>Questions:</th>
              <th>Action:</th>
           </tr>
        </thead>
     </table>
  </div>
</div>
@endsection
@section('footer-js')
<script>
var dataTable;
let baseUrl = "{!! route('jobtests.json') !!}?";
function encodeQueryData(data) {
 const ret = [];
 for (let d in data)
   ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
 return ret.join('&');
}
//Filter method
// function filterClients(){
//     //Get company value
//   const company = document.querySelector('#filterForm select[name=company]').value
//   //Get course value
//   const industry = document.querySelector('#filterForm select[name=industry]').value
//   //Get Status value
//   const status = document.querySelector('#filterForm select[name=status]').value
//   //Add to URL
//     const url = baseUrl  + "&" + encodeQueryData({company,industry,status})
//   //console.log(url);
//   dataTable.ajax.url( url ).load();
// }
$(document).ready(function(){

    dataTable = $('#clients').DataTable({
    responsive: true,
    "deferRender": true,
    "processing": true,
    "serverSide": true,
    "ordering": false, //disable column ordering
    "lengthMenu": [
      [25, 50, 100,  -1],
      [25, 50, 100, "All"] // change per page values here
    ],
    "pageLength": 10,
    "ajax": {
      url: baseUrl,
      method: 'GET'
    },
    // dom: '<"html5buttons"B>lTfgitp',
    "dom": "<'row' <'col-md-12'>><'row'<'col-md-8 col-sm-12'lB><'col-md-4 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
    buttons: [
      {extend: 'copy',exportOptions: {columns: [0, 1, 2, 3,4,5,6]}},
      {extend: 'csv', title: '{{ config('app.name', 'Summit') }} - List of all Clients', exportOptions: {columns: [0, 1, 2, 3,4,5,6]}},
      {extend: 'excel', title: '{{ config('app.name', 'Summit') }} - List of all Clients',exportOptions: {columns: [0, 1, 2, 3,4,5]}},
      {extend: 'pdf', title: '{{ config('app.name', 'Summit') }} - List of all Clients',exportOptions: {columns: [0, 1, 2, 3,4,5]}},
      {extend: 'print',
      customize: function (win){
        $(win.document.body).addClass('white-bg');
        $(win.document.body).css('font-size', '10px');
        $(win.document.body).find('table')
        .addClass('compact')
        .css('font-size', 'inherit');
      }
    }
  ],
  columns: [
    {data: 'ID', name: 'ID', orderable: true, searchable: true},
    {data: 'Question', name: 'Question', orderable: true, searchable: true},
    {data: 'Actions', name: 'Action', orderable: false, searchable: false,"defaultContent": "<i>Not set</i>"},
  ],
  rowCallback: function(row,data,index){
    if(data[7] == 4){
      $(row).find('td:eq(7)').html('<span class="badge badge-secondary">Active Loyal</span>');
    }
  }
});
});
</script>
@endsection
