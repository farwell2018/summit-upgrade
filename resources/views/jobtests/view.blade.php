@extends('layouts.admin')

@section('title','View Job Test Management ')


@section('content')
 <div class="row " style="margin-left:8px">
        <a href="{{route('home')}}" style="color:#858796" >
            <span class="text">Home</span> </a> &nbsp; | &nbsp;
            <a href="{{route('jobAds.index')}}" style="color:#858796" >
       <!--  <a href="{{route('clientmanagement.index')}}" style="color:#858796" > -->
            <span class="text">Job Test Management</span> </a> &nbsp; | &nbsp;

            <span class="text">Pre Qual Questions and Answers</span>
    </div><br>
    <div class="card mb-3">
    	<div class="card-header tab-form-header text-white" style="background-color: #1C2E5C">
    		Questionaire for the &nbsp&nbsp{{$job->JobTitle}}'s&nbsp Job
    	</div>

    	<div class="card-body">
            <div class="row">
               <div class="col-md-12">
                   <div class="questions">
                     @php $i = 0; @endphp
                     @php
                       $q=array();
                       @endphp
                       @foreach($prequalquestions as $prequalquestion)
                       @php $i++; @endphp
                       @php $prequalanswers= \App\PreQualAnswers::where('PreQualTestID','=',$prequalquestion->PreQualTestID)->get(); @endphp
                         @foreach($prequalanswers as $ans)
                           @php $Candidateanswers= \App\CandidateAnswers::where('AnswerID','=',$ans->AnswersID)->where('CV_ID','=',$id)->get(); @endphp

                           @foreach($Candidateanswers as $candidate)


                            <?php  $q[]= '<p><b>Question'. $i .'</b> '.$prequalquestion->Question.'<p><p>'.$candidate->Answer.'</p>';?>

                           @endforeach



                         @endforeach



                       @endforeach
                         {!!  $a = implode(' ', $q)!!}
                   </div>
               </div>
    	</div>
    </div>
@endsection
