@extends('layouts.admin')
@section('title','Edit Job Test Management ')
@section('content')
<div class="card mb-3">
	<div class="card-header tab-form-header text-white" style="background-color: #1C2E5C">
		Edit Questionaire for &nbsp|&nbsp {{$job->JobTitle}}
	</div>
	<div class="card-body">
		<form class="kt-form" method="POST" action="{{ route('jobtests.updateviewquestions', $job->ID) }}"  enctype="multipart/form-data">
			@method('PUT')
			@csrf
		<div class="row">
			<div class="col-md-12">
				<div class="questions">
					    @php $i = 0; @endphp
					    @php $j = 0; @endphp
		  				@foreach($prequalquestions  as $key => $question)
		  				@php $i++; @endphp
		  				<label for="question">Question {{$i}}</label>
		  				<br>
		  				<input type="text" name="Question[{{$i}}][Question]" value="{{$question->Question}}" class="form-control" id="PreQualTest_Question_{{$i}}">
		  				<br>
		  				@foreach($answers[$key] as $answer)
		  				@php $j++; @endphp

		  				<div class="row">
		  				<div class="col">
								<label for="answers">Answers {{$j}}</label>
		  						<input type="text" name="Question[{{$i}}][Answers][{{$j}}]" value="{{$answer->Answers}}" class="form-control" id="PreQualAnswers_Answers_{{$j}}">
		  				</div>
		  				<div class="col">
								<label for="answers">Marks {{$j}}</label>
		  						<input type="text" name="Question[{{$i}}][Marks][{{$j}}]" value="{{$answer->Marks}}" class="form-control" id="PreQualAnswers_Marks_{{$j}}">
		  						
		  						<script>
		  						    var phpAnsId = {{$j}};
		  						     var phpQId = {{$i}};
		  						</script>
		  				</div>
						</div><br>
						

		  				@endforeach
		  				<div id="answers{{$i}}">

						</div>
							<div class="row text-right">
								<div class="col">

								</div>
								<div class="col">
									<div class=" form-group">
												<input type="button" value="+Add Answer" class="btn btn-primary addanswers" id="addanswers" onclick="addQAnswer(<?php echo $i; ?>, <?php echo $j; ?>)"/>
									</div>
								</div>
							</div>

                         	<script>
		  						    var phpQuestId = {{$i}};
		  						</script>
		  				@endforeach
						</div>
									<br>
		  				          <div id="requirements">

            <!-- <div class="row">
              <div class="form-group" style="width:100%">
      <label for="PreQualTest_Question_1" class="required">
        Question 1 <span class="required">*</span>
      </label>
      <input  maxlength="255" name="Question[1][Question]" id="PreQualTest_Question_1" type="text" value="" class="form-control">
    </div></div>
    <div class="row">
      <div class="form-group" style="width:100%">
      <label for="PreQualAnswers_Answers_1" class="required">
        Answer 1 <span class="required">*</span>
      </label>
      <input  maxlength="25" name="Question[1][Answers][1]" id="PreQualAnswers_Answers_1" type="text" value="" class="form-control">
    </div></div>
                <div class="row">
                  <div class="form-group" style="width:100%">
      <label for="PreQualAnswers_Marks_1" class="required">
        Marks 1 <span class="required">*</span>
      </label>
      <input  maxlength="255" name="Question[1][Marks][1]" id="PreQualAnswers_Marks_1" type="text" value="" class="form-control">
    </div></div> -->


				</div>
				<div class="row text-right">
					<div class="col">

					</div>
					<div class="col">
						<div class=" form-group">
															 <input type="button" value="+Add Question" class="btn btn-primary" id="addquestion" />
													</div>
						<div class=" form-group">
															 <input type="button" value="+Add Answer" class="btn btn-primary" id="addanswer" />
													</div>
					</div>
				</div>
			</div>
		</div>
		<br><br>
		<div class="row text-center">
			<div class="col-md-12">
			    <a href="javascript:history.back()" class="btn btn-light">Back</a>
				
				<button type="submit" class="btn btn-success">Submit</button>
			</div>
		</div>
	</form>
	</div>
</div>
@endsection
