@extends('layouts.admin')

@section('title','Country')


@section('content')

     <div class="row " style="margin-left:8px">
        <a href="{{route('home')}}" style="color:#858796" >
            <span class="text">Home</span> </a> &nbsp; | &nbsp;
        <a href="{{route('country.index')}}" >
            <span class="text">Country List</span> </a>
    </div>
    <br>

    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Edit a Country
                </h3>
            </div>
        </div>
        <!--begin::Form-->
        <form class="kt-form" method="POST" action="{{ route('country.update',$country->CountryID) }}"
              enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="country">Country Name</label>
                <input id="countryname" type="text" class="form-control{{ $errors->has('countryname') ? ' is-invalid' : '' }}" name="countryname" value="{{ $country->CountryName  }}" required>

                @if ($errors->has('countryname'))
                    <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('countryname') }}</strong>
            </span>
                @endif
            </div>

            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="reset" class="btn btn-secondary">Cancel</button>
                </div>
            </div>
        </form>
        <!--end::Form-->
@endsection
