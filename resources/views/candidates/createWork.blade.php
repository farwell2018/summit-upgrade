@extends('layouts.candidate')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
        <div id="regForm">
             @csrf
                      <!-- Circles which indicates the steps of the form: -->
          <div class="steps">
            <ul id="progressbar">
                <li class="step finish"><span>Personal Details</span></li>
                <li class="step finish"><span>Education</span></li>
                <li class="step active"><span>Work</span></li>
            </ul>
          </div>
          <div class="progress">
              <div class="progress-bar" role="progressbar" aria-valuenow="75"
              aria-valuemin="0" aria-valuemax="100" style="width:75%">
                75%
              </div>
          </div>
          <!-- One "tab" for each step in the form: -->
          <div class="tab">
            @if(session()->has('register_error'))
                <div class="form-group row">
                    <div class="col-md-12">
                        <div class="form-check">
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session()->get('register_error') }}
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        </div>
                    </div>
                </div>
            @endif
            @include('includes.components.work')
          </div>
         <div class="row justify-content-center">
              <a href="{{ route('profile.create.education')}}" class="btn btn-secondary">Back</a>&nbsp;&nbsp;
                  <form class="" action="{{route('candidate.profile.home')}}" method="post">
                @csrf
                <button type="submit" class="btn btn-success">Finish</button>
              </form>
          </div>
        </div>
        </div>
    </div>
</div>
@if(!empty($work))
@include('includes.modals.editWork')
@endif
@include('includes.modals.work')

@endsection
@section('js')
<script src="{{asset('public/js/script.js')}}"></script>
<script type="text/javascript">
$(document).ready(function() 
{
var max_fields = 5;    
if( typeof phpRespId !== 'undefined' ) 
{
var respId =phpRespId;
}
else
{
var respId =1;
}
if( typeof phpAchievId !== 'undefined' )
{
    var achievId =phpAchievId;
}
else
{
  var achievId = 1;  
}
//****************************Start Responsibilities*******************************************************//
 $('#addresponsibilities').click( function(j) {
     j.preventDefault();
     if(respId<=max_fields)
  {
 	$("#requirements").append
 	(
       '<input name="responsible[requirements]['+respId+']" id="responsible_'+respId+'" type="text" value="" placeholder="Add Responsibility' +respId+' " class="form-control text-center"/ style="margin-top:5px">'+
       '</div>'
 	);
 	respId++;
 	}});
//****************************end responsibilites*********************************************************// 
//****************************Start Achievements*********************************************************//
$('#addachievements').click( function(k) {
   k.preventDefault(); 
    if(achievId<=max_fields)
    {  
      $("#achievements").append
      (  
       '<input name="achievement[achievements]['+achievId+']" id="achievement_'+achievId+'" type="text" value="" placeholder="Add Achievement' +achievId+' " class="form-control text-center"/ style="margin-top:5px">'+
       '</div>'
      );
        achievId++;
  }});
//**************************************End Achievements**********************************************//  

});  
</script>

<script>
jQuery(document).ready(function($) {

if ( $('#wstart_date')[0].type != 'date' ) $('#wstart_date').datepicker();

if ( $('#wend_date')[0].type != 'date' ) $('#wend_date').datepicker();

});
</script>
@endsection
