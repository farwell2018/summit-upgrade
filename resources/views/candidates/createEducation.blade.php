@extends('layouts.candidate')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
        <div id="regForm">
        			  <!-- Circles which indicates the steps of the form: -->
		  <div class="steps">
            <ul id="progressbar">
                <li class="step finish"><span>Personal Details</span></li>
                <li class="step active"><span>Education</span></li>
                <li class="step"><span>Work</span></li>
            </ul>
		  </div>
		  <div class="progress">
			  <div class="progress-bar" role="progressbar" aria-valuenow="45"
			  aria-valuemin="0" aria-valuemax="100" style="width:45%">
			    45%
			  </div>
		  </div>
		  <!-- One "tab" for each step in the form: -->
		  <div class="tab">
		  	@if(session()->has('register_error'))
			    <div class="form-group row">
			        <div class="col-md-12">
			            <div class="form-check">
			            <div class="alert alert-success alert-dismissible fade show" role="alert">
			                {{ session()->get('register_error') }}
			              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			                <span aria-hidden="true">&times;</span>
			              </button>
			            </div>
			            </div>
			        </div>
			    </div>
			@endif
		  	@include('includes.components.education')
		  </div>
  		 <div class="row justify-content-center">
		      <a href="{{ route('profile.create.summary')}}" class="btn btn-secondary">Back</a>&nbsp;&nbsp;
		      <a href="{{ route('profile.create.work')}}" class="btn btn-success">Go to next</a>
		  </div>
		</div>

        </div>
    </div>
</div>
@if(!empty($education))
@include('includes.modals.editEducation')
@endif
@if(!empty($qualifications))
@include('includes.modals.editProfession')
@endif
@if(!empty($profbodies))
@include('includes.modals.editProfessionBodies')
@endif
@include('includes.modals.education')
@include('includes.modals.profession')
@include('includes.modals.professionalBodies')
@endsection

@section('js')
<script src="{{asset('public/js/script.js')}}"></script>
<script>
jQuery(document).ready(function($) {
if ( $('#start_date')[0].type != 'date' ) $('#start_date').datepicker();

if ( $('#end_date')[0].type != 'date' ) $('#end_date').datepicker();

if ( $('#pstart_date')[0].type != 'date' ) $('#pstart_date').datepicker();

if ( $('#pend_date')[0].type != 'date' ) $('#pend_date').datepicker();


if ( $('#bstart_date')[0].type != 'date' ) $('#bstart_date').datepicker();

if ( $('#bend_date')[0].type != 'date' ) $('#bend_date').datepicker();


});
</script>
@endsection
