@extends('layouts.candidate')

@section('content')
 
   @if (session('status'))
          <div class="alert alert-success" role="alert">
              {{ session('status') }}
          </div>
      @endif
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-3">
            @include('includes.components.sidebar')
        </div>
        <div class="col-md-9">
            @if(auth()->user()->unreadNotifications->count() > 0)
            <div class="row notification-bg bg-white">
              <div class="col-md-12">
                <div class="notification">
                  <div class="notification-header"><span>Notifications</span></div>
                      <div class="notification-content">
                        <div class="row">
                      @foreach(auth()->user()->unreadNotifications as $notification)
                           <div class="col-md-3 border-right">
                            <h4 class="text-primary">{{ $notification->data['title'] }}</h4>
                            <span>{{ $notification->data['data'] }}</span>
                            @if($notification->type == 'App\Notifications\Tickets')
                            <p><a href="{{route('tickets')}}" class="text-info">View</a></p>
                            @endif
                          </div>
                      @endforeach
                        </div>
                      </div>
                  <div class="notification-footer"><span><a href="{{route('markAsRead')}}">Close <i class="fas fa-times"></i></a></span></div>
                </div>
              </div>
            </div>
            @endif
            @foreach ($errors->all() as $error)
                <span class="invalid-feedback" role="alert" style="display: block;">
                    <strong>{{ $error }}</strong>
                </span>
            @endforeach

            <div class="row profile bg-darkblue">
                <div class="col-md-3 profile-image">
                  <my-upload current="{{ $details->CandidatePhoto ?: 'your-picture.png' }}" action="{{ route('profile.ppic.change') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  </my-upload>
                </div>
                <div class="col-md-9">
                    <div class="container">
                    <div class="row profile-details bg-white">
                        <div class="col-md-6">
                            <h3 class="text-primary">{{$details->Firstname}} {{$details->Lastname}}</h3>
                            <p>Mobile no | {{$cvdets->PhoneNumber}}</p>
                            @if($cvdets->PhoneNumberOther != null)
                            <p>Other mobile no | {{$cvdets->PhoneNumberOther}}</p>
                            @endif
                            <p>Email address | {{$details->EmailAddress}}</p>
                            @if($cvdets->EmailAddressOther != null)
                            <p>Other email address | {{$cvdets->EmailAddressOther}}</p>
                            @endif
                            <p>Nationality | {{$cvdets->Nationality}} |  @if($cvdets->Identification == 'ID') {{$cvdets->ID_No}} @elseif($cvdets->Identification == 'Passport')  {{$cvdets->Passport_No}} @endif </p><!-- | @if($cvdets->Identification == 'ID') {{$cvdets->ID_No}} @else {{$cvdets->Passport_No}} @endif -->
                        </div>
                        <div class="col-md-6 border-left">
                            <span class="text-primary edit-contact" onclick="edit();" style="cursor: pointer;position: absolute;right: 0"><i class="fas fa-edit"></i></span>
                            <p>Driving license | {{$cvdets->DL}} </p>
                            <p>Car owner | {{$cvdets->CarOwner}} </p>
                            <p>Location | {{$cvdets->PhysicalAddress}}</p>
                            <p>Address | {{$cvdets->PO_BOX}}</p>
                            <p>D.O.B | {{$cvdets->DOB}}</p>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
               <div class="row">
            <div class="col-md-12">
          <div class="cvupload">


            @if(empty($details->CVUpload))
            <div class="right"><h4>Kindly upload your Resume</h4></div>
            @else
            <a href="{{ asset('public/uploads/'. $details->CVUpload.'')}}"  style="color: #000" target="_blank"> View Current CV</a>
            @endif
            <div class="left" style="display: flex;">
                    <cv-upload action="{{ route('upload.cv') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </cv-upload>
                </div>


          </div>
        </div>
      </div>
            <div class="row profile">
                <ul class="nav nav-pills nav-justified mb-3" id="pills-tab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Personal Details</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Education</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Work</a>
                  </li>
                </ul>
                <div class="tab-content" id="pills-tabContent">
                  <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                    @include('includes.profile.personaldetails')
                  </div>
                  <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                    @include('includes.profile.education')
                  </div>
                  <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                    @include('includes.profile.work')
                  </div>
                </div>
            </div>

        </div>
    </div>
</div>
@include('includes.modals.editContact')
@include('includes.modals.editSummary')
@include('includes.modals.editAttributes')
@include('includes.modals.editSkills')
@include('includes.modals.editHardSkills')
@include('includes.modals.editLanguages')
@include('includes.modals.editSocialMedia')
@include('includes.modals.editEducation')
@include('includes.modals.editProfession')
@include('includes.modals.editProfessionBodies')
@include('includes.modals.education')
@include('includes.modals.profession')
@include('includes.modals.professionalBodies')
@include('includes.modals.editInterests')
@if(!empty($work))
@include('includes.modals.editWork')
@endif
@include('includes.modals.morework')





@endsection
@section('js')

<script src="{{asset('public/js/script.js')}}"></script>

<script>
$(document).ready(function() 
{
var max_fields = 5;    
if( typeof phpRespId !== 'undefined' ) 
{
var respId =phpRespId;
}
else
{
var respId =2;
}
if( typeof phpAchievId !== 'undefined' )
{
    var achievId =phpAchievId;
}
else
{
  var achievId = 2;  
}
//****************************Start Responsibilities*******************************************************//
 $('.add-responsibles').click( function(j) {
     console.log('yes');
     j.preventDefault();
     if(respId<= max_fields){
     $("#required").append
 	(
       '<input name="responsible[requirements]['+respId+']" id="responsible_'+respId+'" type="text" value="" placeholder="Add Responsibility' +respId+' " class="form-control text-center"/ style="margin-top:5px">'+
       '</div>'
 	);
  
 	respId++;
 	}});
//****************************end responsibilites*********************************************************// 
//****************************Start Achievements*********************************************************//
$('.add-achievements').click( function(k) {
   k.preventDefault(); 
    if(achievId<=max_fields)
    {  
      $("#achievements").append
      (  
       '<input name="achievement[achievements]['+achievId+']" id="achievement_'+achievId+'" type="text" value="" placeholder="Add Achievement' +achievId+' " class="form-control"/ style="margin-top:5px">'+
       '</div>'
      );
        achievId++;
  }});
//**************************************End Achievements**********************************************//  

}); 
    
    
    
    
    
</script>



<script>
$(document).ready(function() 
{
var max = 5;    
if( typeof phpResId !== 'undefined' ) 
{
var resId =phpResId;
}
else
{
var resId =2;
}
if( typeof phpAchieId !== 'undefined' )
{
    var achieId =phpAchieId;
}
else
{
  var achieId = 2;  
}
//****************************Start Responsibilities*******************************************************//
 $('#moreresponsibilities').click( function(j) {
     j.preventDefault();
     if(resId<= max){
     $("#more-r").append
 	(
       '<input name="responsible[requirements]['+resId+']" id="responsible_'+resId+'" type="text" value="" placeholder="Add Responsibility' +resId+' " class="form-control text-center"/ style="margin-top:5px">'+
       '</div>'
 	);
  
 	respId++;
 	}});
//****************************end responsibilites*********************************************************// 
//****************************Start Achievements*********************************************************//
$('#moreachievements').click( function(k) {
   k.preventDefault(); 
    if(achieId<=max)
    {  
      $("#more-a").append
      (  
       '<input name="achievement[achievements]['+achieId+']" id="achievement_'+achieId+'" type="text" value="" placeholder="Add Achievement' +achieId+' " class="form-control"/ style="margin-top:5px">'+
       '</div>'
      );
        achievId++;
  }});
//**************************************End Achievements**********************************************//  

}); 
    
    
    
    
    
</script>

<script type="text/javascript">

    var d = 5;
    var c = 5;

$(document).ready(function() {
    var max_fields = 2; //maximum input boxes allowed
    var wrapper = $("#mobile"); //Fields wrapper
    var add_button = $(".add-mobile"); //Add button ID
    var wrapper1 = $("#email"); //Fields wrapper
    var add_button1 = $(".add-email"); //Add button ID

    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
    e.preventDefault();
    if(x < max_fields){ //max input box allowed
    x++; //text box increment
    $(wrapper).append('<div class="form-group row"><label for="colFormLabel" class="col-md-4 text-center text-white col-form-label label label-primary">Other Number</label>' +
    '<div class="col-md-4"><input class="form-control" id="other_number" type="tel" placeholder="" name="phone1" required/></div>' +
    '<div class="col-md-4"><a href="#" class="remove_field"><i class="fa fa-times"></i></a></div></div>'); //add input box

    $('.add-mobile').hide();
     setTimeout(function(){
    var input = document.querySelector('input[name="phone1"]');
    var hidden = "full_phone1";
    international(input, hidden);
      }, 600);
    }
    });

    var y = 1;
    $(add_button1).click(function(e){ //on add input button click
    e.preventDefault();
    if(y < max_fields){ //max input box allowed
    y++; //text box increment
    $(wrapper1).append('<div class="form-group row"><label for="colFormLabel" class="col-md-4 text-center text-white col-form-label label label-primary">Other Email</label>' +
    '<div class="col-md-4"><input type="email" class="form-control" name="email1" required></div>' +
    '<div class="col-md-4"><a href="#" class="remove_field"><i class="fa fa-times"></i></a></div></div>'); //add input box

    $('.add-email').hide();
    }
    });
    var l = '<?php echo $y; ?>';
    $(".add-language").click(function() {
    console.log("click");
    l ++;
    console.log(x);
    if(l > 5){
    $('.alert').addClass('show');
    }else{
      if($('#language'+ l).length){
        $('#language'+ l).show();
      }else{
        l++;
        $('#language'+ l).show();
      }
    }
    });

    $(".remLamguage").on("click",".remove_field", function(e){//user click on remove field
    e.preventDefault();
    l--;
    $(this).closest( ".form-group" ).remove();
    })

    $(wrapper).on("click",".remove_field", function(e){ //user click on remove field
    e.preventDefault();
     $('input[name=phone1]').val('');
    $(wrapper).find('.row').remove();
    $('.add-mobile').show();
    x--;
    })

    $(wrapper1).on("click",".remove_field", function(e){ //user click on remove field
    e.preventDefault();
     $('input[name=email1]').val('');
    $(wrapper1).find('.row').remove();
    $('.add-email').show();
    y--;
    })

     var z = 1;
    $(".add-referee").click(function() {
    z = z + 1;

    if(z > 3){
        $('.ref-alert').addClass('show');
    }else{
        $(this).find('#referee'+ z).css( "background-color", "green" );
    $('#referee'+ z).append($('<div class="form-group row border-line"><label for="name" class="col-md-4 text-center text-white col-form-label label label-primary">Other Referee Details</label><div class="col-md-4"><input type="text" class="form-control" placeholder="Name" name="referee_name'+ z +'" required></div><div class="col-md-4"><input type="text" class="form-control" placeholder="Designation" name="referee_desg'+ z +'"></div></div>'));


    $('#referee'+ z).append($('<div class="form-group row"><label for="name" class="col-md-4 text-center text-white col-form-label label label-primary">Other Referee Contact</label><div class="col-md-4"><input type="email" class="form-control" name="referee_email'+ z +'" placeholder="Email Address"></div><div class="col-md-4"><input type="text" class="form-control" name="referee_phone'+ z +'" placeholder="Mobile Number"></div></div><span class="text-primary remove_referee" style="cursor: pointer;"><i class="fas fa-trash"></i> Delete</span>'));

      var input = document.querySelector('input[name="referee_phone'+ z +'"]');
      var hidden = "full_phone";
      international(input, hidden);
    }

    });

     var r = 3;
    $(".add-editreferee").click(function() {
        console.log("okat");
    r = r + 1;

    if(r > 6){
        $('.ref-alert').addClass('show');
    }else{
        $(this).find('#referee'+ z).css( "background-color", "green" );
    $('#referee'+ r).append($('<div class="form-group row border-line"><label for="name" class="col-md-4 text-center text-white col-form-label label label-primary">Other Referee Details</label><div class="col-md-4"><input type="text" class="form-control" placeholder="Name" name="referee_name'+ r +'" required></div><div class="col-md-4"><input type="text" class="form-control" placeholder="Designation" name="referee_desg'+ r +'"></div></div>'));


    $('#referee'+ r).append($('<div class="form-group row"><label for="name" class="col-md-4 text-center text-white col-form-label label label-primary">Other Referee Contact</label><div class="col-md-4"><input type="email" class="form-control" name="referee_email'+ r +'" placeholder="Email Address"></div><div class="col-md-4"><input type="tel" class="form-control" name="referee_phone'+ r +'" placeholder="Mobile Number"></div></div><span class="text-primary remove_referee" style="cursor: pointer;"><i class="fas fa-trash"></i> Delete</span>'));

      var input = document.querySelector('input[name="referee_phone'+ r +'"]');
      var hidden = "full_phone";
      international(input, hidden);
    }

    });

    wrapp2 = $("#referee2");
    wrapp3 = $("#refere3");

    wrapp4 = $("#referee4");
    wrapp5 = $("#referee5");

    $(wrapp2).on("click",".remove_referee", function(e){ //user click on remove field
    e.preventDefault();
     $('input[name=referee_name2]').val('');
     $('input[name=referee_desg2]').val('');
     $('input[name=referee_email2]').val('');
     $('input[name=referee_phone2]').val('');
    $(wrapp2).find('.row').remove();
    $(this).remove();
        s--;
    })
    $(wrapp3).on("click",".remove_referee", function(e){ //user click on remove field
    e.preventDefault();
     $('input[name=referee_name3]').val('');
     $('input[name=referee_desg3]').val('');
     $('input[name=referee_email3]').val('');
     $('input[name=referee_phone3]').val('');
    $(wrapp3).find('.row').remove();
    $(this).remove();
        s--;
    })
    $(wrapp4).on("click",".remove_referee", function(e){ //user click on remove field
    e.preventDefault();
     $('input[name=referee_name2]').val('');
     $('input[name=referee_desg2]').val('');
     $('input[name=referee_email2]').val('');
     $('input[name=referee_phone2]').val('');
    $(wrapp4).find('.row').remove();
    $(this).remove();
        s--;
    })
    $(wrapp5).on("click",".remove_referee", function(e){ //user click on remove field
    e.preventDefault();
     $('input[name=referee_name3]').val('');
     $('input[name=referee_desg3]').val('');
     $('input[name=referee_email3]').val('');
     $('input[name=referee_phone3]').val('');
    $(wrapp5).find('.row').remove();
    $(this).remove();
        s--;
    })

      //for the work edit module....for the achievements and responsibilies
    $(document).ready(function() {
      var max_fields = 5; //maximum input boxes allowed
      var wrapper3 = $("#achievements"); //Fields wrapper
      var add_button3 = $("#add-achievements"); //Add button ID


      var x = 1; //initlal text box count
          $(add_button3).click(function(e){ //on add input button click
            e.preventDefault();

      if( typeof achDutyId !== 'undefined' ) {
          	var achId =achDutyId;
          }
          else{
          var achId =  $('#ach_value').val();
          }
          $(wrapper3).append('<div class="form-group row"><label for="colFormLabel" class="col-md-4 text-center text-white col-form-label label label-primary">Other achievements</label>' +
          '<div class="col-md-4"><input class="form-control"  type="text" name="achievements['+achId+']" id="achivements_'+achId+'"></div>' +
          '<div class="col-md-4"><a href="#" class="remove_field"><i class="fa fa-times"></i></a></div></div>');  //add input box


             $('#ach_value').val(achId++);
            //$('.add-achievements').hide();

          });

          $(wrapper3).on("click",".remove_field", function(e){ //user click on remove field
          e.preventDefault();
          $('input[name=achievements1]').val('');
          $(wrapper3).find('.row').remove();
          $('.add-achievements').show();
          x--;
          })
    });


//end of edit work module

    var s = 1;
    $(".add-interest").click(function() {
    s = s + 1;
    if(s > 6){
        $('.interest-alert').addClass('show');
    }else{

    $('#interests'+ s).append($('<div class="form-group row"><label for="colFormLabel" class="col-md-5 text-center text-white col-form-label label label-primary">Other Interest</label><div class="col-md-5"><input class="form-control" type="text" name="Interest'+ s +'"/></div><div class="col-md-2"><a href="#" class="remove_field"><i class="fa fa-times"></i></a></div></div>'));

    }
    });
    wrapper2 = $("#interests2");
    wrapper3 = $("#interests3");
    wrapper4 = $("#interests4");

    $(wrapper2).on("click",".remove_field", function(e){ //user click on remove field
    e.preventDefault();
     $('input[name=interest2]').val('');
    $(wrapper2).find('.row').remove();
        s--;
    })
    $(wrapper3).on("click",".remove_field", function(e){ //user click on remove field
    e.preventDefault();
     $('input[name=interest3]').val('');
    $(wrapper3).find('.row').remove();
        s--;
    })
    $(wrapper4).on("click",".remove_field", function(e){ //user click on remove field
    e.preventDefault();
     $('input[name=interest4]').val('');
    $(wrapper4).find('.row').remove();
        s--;
    })
    $('.notification-footer').on("click","span", function(e){

    $('.notification').remove();
    })
    //
//     var dateToday = new Date();
//     var dates = $("#from, #to").datepicker({
//     defaultDate: "+1w",
//     changeMonth: true,
//     numberOfMonths: 3,
//     minDate: dateToday,
//     onSelect: function(selectedDate) {
//         var option = this.id == "from" ? "minDate" : "maxDate",
//             instance = $(this).data("datepicker"),
//             date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
//         dates.not(this).datepicker("option", option, date);
//     }
// });

})

    function addresp(id) {
    d = d + 1;
    console.log(x);
    if(d > 10){
        $('.ref-alert').addClass('show');
    }else{
    $('#responsibility'+ id).append($('<div id="responsibility'+ d +'" class="responsible"><div class="form-group row border-line"><div class="col-md-8"><input type="text" class="form-control" placeholder="Tell briefly about your work: your role and responsibilities." name="responsibility'+ d +'"></div></div></div>'));
    }
    }


    function addachieve(id) {
    c = c + 1;
    if(c > 10){
        $('.ref-alert').addClass('show');
    }else{
    $('#achievements'+ id).append($('<div id="achievements'+ c +'" class="responsible"><div class="form-group row border-line"><div class="col-md-8"><input type="text" class="form-control" placeholder="Tell briefly about your work: your daily tasks and main achievements." name="achievements'+ c +'"></div></div></div>'));

    }

    }
</script>

// <script>
// jQuery(document).ready(function($) {
// if ( $('#DOBdate')[0].type != 'date' ) $('#DOBdate').datepicker();
// if ( $('#EODdate')[0].type != 'date' ) $('#EODdate').datepicker();



// if ( $('#edstart_date')[0].type != 'date' ) $('#edstart_date').datepicker();
// if ( $('#edend_date')[0].type != 'date' ) $('#edend_date').datepicker();



// });
// </script>
@endsection
