@extends('layouts.admin')

@section('title','Hardskill')


@section('content')

     <div class="row " style="margin-left:8px">
        <a href="{{route('home')}}" style="color:#858796" >
            <span class="text">Home</span> </a> &nbsp; | &nbsp;
        <a href="{{route('hardskill.index')}}" >
            <span class="text">Hardskill List</span> </a>
    </div>
    <br>

    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Edit a Hardskill
                </h3>
            </div>
        </div>
        <!--begin::Form-->
        <form class="kt-form" method="POST" action="{{ route('hardskill.update',$hardskill->ID) }}"
              enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="hardskill">Hardskill Name</label>
                <input id="hardskillname" type="text" class="form-control{{ $errors->has('hardskillname') ? ' is-invalid' : '' }}" name="hardskillname" value="{{ $hardskill->Name  }}" required>

                @if ($errors->has('hardskillname'))
                    <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('hardskillname') }}</strong>
            </span>
                @endif
            </div>

            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="reset" class="btn btn-secondary">Cancel</button>
                </div>
            </div>
        </form>
        <!--end::Form-->
@endsection
