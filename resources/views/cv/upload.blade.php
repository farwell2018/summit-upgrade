@extends('layouts.candidate')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-3">
            @include('includes.components.sidebar')
        </div>
        <div class="col-md-9">
		  <div class="row">

			@if($errors)
			<div class="row"> 
			<div class="col-md-12 justify-content-center" style="text-align: center; margin-bottom: 2rem;">
			@foreach ($errors->all() as $error)
		        <span class="invalid-feedback" role="alert" style="display: block;">
		            <strong>{{ $error }}</strong>
		        </span>
		    @endforeach
		   </div> </div>
		   @endif
		   
		   <!--<div class="right"><h4>Kindly upload your Resume before downloading your custom CV</h4></div>-->
    	<!--			<div class="left" style="display: flex;">-->
     <!--         			<cv-upload action="{{ route('upload.cv') }}">-->
		   <!--                 <input type="hidden" name="_token" value="{{ csrf_token() }}">-->
		   <!--             </cv-upload>-->
		   <!--         </div>-->
		   	<div class="col-md-12">
		  		<div class="cvupload">
    			
		  			<div class="right"><h4>Download your Custom CV</h4></div>
    				<div class="left" style="display: flex;">
    					<a href="{{route('export.cv')}}" class="btn buttn-download"><i class="fa fa-download"></i> Download CV</a>
					</div>
		          
		               
		  		</div>
		  	</div>
		  
		  </div>
		  <div class="row">
		  	<div class="col-md-3">
		  		<div class="container cv-sidebar text-center text-white">

		  			<div class="form-group">
		  				<div class="cv-image">
		  				<div class="image" style="background-image: url('../public/uploads/{{$details->CandidatePhoto}}');"></div>
                            <h3>{{$details->Firstname}} {{$details->Lastname}}</h3>
		  				</div>
		  			</div>

		  			<div class="form-group">
		  				<div class="cv-contact">
                            <p>{{$cvdets->PhoneNumber}}</p>
                            <p>{{$details->EmailAddress}}</p>
                            <p>{{$cvdets->Nationality}} | @if($cvdets->Identification == 'ID') {{$cvdets->ID_No}} @else {{$cvdets->Passport_No}} @endif</p>
		  				</div>
		  			</div>
					<div class="nav-border"></div>
		  			<div class="form-group">
		  				<div class="cv-contact">
                            <p>{{$cvdets->PhysicalAddress}} </p>
                            <p>P.O. Box | {{$cvdets->PO_BOX}}</p>
                            <p>D.O.B | {{$cvdets->DOB}} </p>
		  				</div>
		  			</div>
					<div class="nav-border"></div>
		  			<div class="form-group">
		  				<div class="cv-contact">
		  					<p class="text-white"><i class="fa fa-skype"></i></p>
		  					<p class="text-white">{{$cvdets->SkypeContact}}</p>
		  				</div>
		  				<div class="social">
		  					<p class="text-white"><i class="fa fa-linkedin"></i></p>
		  					<p class="text-white">{{$cvdets->LinkedInContact}}</p>
		  				</div>
		  			</div>

			


		  		</div>
		  	</div>
		  	<div class="col-md-9"><div class="container">

		  		<div class="form-group row"><div class="logo">
                    <img src="{{ asset('public/images/icons/summit-logo.png') }}" class="float-right" style="width: 20%">
		  		</div></div>

		  		<div class="form-group">
		  		    <div class="cvheadline"><h2>Personal Summary</h2></div>
		  		    
		  		    @php $summary = strip_tags($cvdets->PersonalSummary); @endphp
		  			<p>{!! $summary !!}</p>
		  		</div>

		  		@if(count($work) > 0)
		  		<div class="form-group">
		  			<div class="workexp">
		  				<div class="cvheadline"><h2>Work Experience</h2></div>
		  				<hr class="cvline" />
						@foreach($work as $work)
							<div class="education">
							<h4 class="text-secondary">{{$work->Title}}</h4>
							<p>Company: {{$work->Company}}</p>
							<p><span>From - @php echo date("d-m-Y", strtotime($work->StartDate)); @endphp To - @if($work->CurrentDate == 'Yes')  now @endif</span></p>
						  	 
			 <h4 class="text-primary">Responsibilites:</h4>
			 
				@foreach($workresps as $res)
				 @foreach($res as $resp)
				@if($resp->WorkExpID == $work->WorkExpID)
				 @if(empty($resp->Responsibility))
				 @else
				 
			{!! $resp->Responsibility !!}
				@endif
				@endif
				@endforeach
				@endforeach
				
			<h4 class="text-primary">Achievements:</h4>
			
				@foreach($workresps as $res)
				@foreach($res as $resp)
				@if($resp->WorkExpID == $work->WorkExpID)
				@if(empty($resp->Achievement))
				
				@else
			{!! $resp->Achievement !!}
				@endif
				@endif
				@endforeach
				@endforeach
						
				<hr/>
						@endforeach	
		  			</div>
		  		</div>
				@endif

				@if(count($education) > 0)
		  		<div class="form-group">
		  			<div class="workexp">
		  				<div class="cvheadline"><h2>Education</h2></div>
		  				<hr class="cvline" />
						@foreach($education as $edu)
							<div class="education">
							@if($edu->FurtherEducation != null)
							<h4 class="text-secondary">{{$edu->FurtherEducation}} {{$edu->Subjects}}</h4>
							<p>Specialization: {{$edu->Specialization}}</p>
							<p><span>{{$edu->Institution}} |</span> <span>From @php echo date("d-m-Y", strtotime($edu->QualStartGradDate)) @endphp - To @php echo date("d-m-Y", strtotime($edu->QualEndGradDate)) @endphp</span></p>
							@else
							<h4 class="text-secondary">{{$edu->Institution}}</h4>
							<p><span>From @php echo date("d-m-Y", strtotime($edu->QualStartGradDate)) @endphp - To @php echo date("d-m-Y", strtotime($edu->QualEndGradDate)) @endphp</span></p>
							@endif 
								</div>
							<hr/>
						@endforeach	
		  			</div>
		  		</div>
				@endif

				@if(count($qualifications) > 0)
		  		<div class="form-group">
		  			<div class="workexp">
		  				<div class="cvheadline"><h2>Professional Qualifications</h2></div>
		  				<hr class="cvline" />
						@foreach($qualifications as $value => $qual)
							<div class="education">
							<h4 class="text-secondary">{{$qual->ProfessionalQualifications}}</h4>
							<p>Title: {{$qual->ProfQualTitle}}</p>
							<p> <span>From @php echo date("d-m-Y", strtotime($qual->StartDate)) @endphp - To @php echo date("d-m-Y", strtotime($qual->EndDate)) @endphp</span></p>
								</div>
							<hr/>
						@endforeach	
		  			</div>
		  		</div>
				@endif

				@if(count($profbodies) > 0)
		  		<div class="form-group">
		  			<div class="workexp">
		  				<div class="cvheadline"><h2>Professional Bodies</h2></div>
		  				<hr class="cvline" />
						@foreach($profbodies as $value => $bodies)
						@foreach($profBodiesTitles as $key => $prof)
						@if($bodies->ProfessionalBody == $prof->id)
							<div class="education">
							<h4 class="text-secondary">{{$prof->name}}</h4>
							<p>Title: {{$prof->name}}</p>
							<p>Membership No/Certificate No:{{$profbodies[$value]->Membership_number}} </p>
							</div>
								<hr/>
						@endif
						@endforeach	
						@endforeach	
		  			</div>
		  		</div>
				@endif

				@if(count($skills) > 0)
		  		<div class="form-group">
		  			<div class="skills">
		  				<div class="cvheadline"><h2>Key Skills</h2></div>
		  				<hr class="cvline" />

			@php 
	  		$checks = array(
				array("id" => "Business_Management ", "name" => "Business Management"),
			array("id" => "Computer", "name" => "Computer"),
			array("id" => "Construction", "name" => "Construction"),
			array("id" => "Customer_Service", "name" => "Customer Service"),
			array("id" => "Diplomacy", "name" => "Diplomacy"),
			array("id" => "Effective_Listening", "name" => "Effective Listening"),
			array("id" => "Financial_Management", "name" => "Financial Management"),
			array("id" => "Interpersonal", "name" => "Interpersonal"),
			array("id" => "Multi-tasking", "name" => "Multi-tasking"),
			array("id" => "Negotiating", "name" => "Negotiating"),
			array("id" => "Organisation", "name" => "Organisation"),
			array("id" => "People_Management", "name" => "People Management"),
			array("id" => "Planning", "name" => "Planning"),
			array("id" => "Presentation", "name" => "Presentation"),
			array("id" => "Problem_Solving", "name" => "Problem Solving"),
			array("id" => "Programming", "name" => "Programming"),
			array("id" => "Report_Writing", "name" => "Report Writing"),
			array("id" => "Research", "name" => "Research"),
			array("id" => "Resourcefulness", "name" => "Resourcefulness"),
			array("id" => "Sales_Ability", "name" => "Sales Ability"),
			array("id" => "Technical", "name" => "Technical"),
			array("id" => "Time_Management", "name" => "Time Management"),
			array("id" => "Training", "name" => "Training"),
			array("id" => "Verbal_Communication", "name" => "Verbal Communication"),
			array("id" => "Written_Communication", "name" => "Written Communication"),
			);
	  		@endphp
				<ul class="keySkills" style="display:flex; list-style:none">
	  			@foreach($checks as $check)
	  			@if(in_array($check['id'],array_values($skills)))
					<li style="color: #fff;background-color: #9BBBD5;font-size: 12px;background: #9BBBD5 0% 0% no-repeat padding-box;border-radius: 8px;padding: 8px;margin: 4px;">{{$check['name']}}</li>
				@endif
				@endforeach	
				</ul>
		  			</div>
		  		</div>
				@endif

				@if(count($hardskills) > 0)
		  		<div class="form-group">
		  			<div class="skills" >
		  				<div class="cvheadline"><h2>Hard Skills</h2></div>
		  				<hr class="cvline" />
				<ul class="keySkills" style="display:flex; list-style:none">
	  			@foreach($hardskills as $hardskill)
	  			@if(in_array($hardskill->ID,array_values($hskills)))
					<li style="color: #fff;background-color: #9BBBD5;font-size: 12px;background: #9BBBD5 0% 0% no-repeat padding-box;border-radius: 8px;padding: 8px;margin: 4px;">{{$hardskill->Name}}</li>
				@endif
				@endforeach		
				</ul>
		  			</div>
		  		</div>
				@endif
              
					@if(count($attrs) > 0)
		  		<div class="form-group">
		  			<div class="skills" >
		  				<div class="cvheadline"><h2>Attributes</h2></div>
		  				<hr class="cvline" />
		  		<ul class="keySkills" style="display:flex; list-style:none">
		    	@foreach($attrs as $attr)
		    
		    	@if($attr != null)
					<li style="color: #fff;background-color: #9BBBD5;font-size: 12px;background: #9BBBD5 0% 0% no-repeat padding-box;border-radius: 8px;padding: 8px;margin: 4px;">{{$attr}}</li>
				@endif	
				@endforeach	
				</ul>
		  			</div>
		  		</div>
				@endif

			@if($lang != null)
		  		<div class="form-group">
		  		<div class="skills" >
		  				<div class="cvheadline"><h2>Languages</h2></div>
		  			<hr class="cvline" />
	  			@if(!empty($lang->Language1))
					<span>{{$lang->Language1}} - {{$lang->Fluency1}}</span><br>
		  		@endif
			 	@if(!empty($lang->Language2))
					<span>{{$lang->Language2}} - {{$lang->Fluency2}}</span><br>
		  		@endif
			 	@if(!empty($lang->Language3))
					<span>{{$lang->Language3}} - {{$lang->Fluency3}}</span><br>
		  		@endif
			 	@if(!empty($lang->Language4))
					<span>{{$lang->Language4}} - {{$lang->Fluency4}}</span><br>
		  		@endif	
		  			</div>
		  		</div>
				@endif
		  		
		  	</div></div>
		  </div>
        </div>
    </div>
</div>

@endsection
@section('js')
<script src="{{asset('public/js/custom-file-input.js')}}"></script>
@endsection