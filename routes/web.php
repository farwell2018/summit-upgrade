<?php
use Illuminate\Support\Facades\Route;
use Spatie\QueryBuilder\QueryBuilder;
use App\Clients;
Route::post('/user/create', 'Auth\RegisterController@create')->name('user.create');
Route::get('/verify/account/{token}', 'Auth\RegisterController@verifyAccount')->name('verify.account');
Route::any('/password/change/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.change');
Route::post('/password/update', 'Auth\ResetPasswordController@reset')->name('password.update');
Route::get('/terms-conditions', 'PageController@termsConditions')->name('terms.conditions');
Auth::routes();
Route::get('/', 'HomeController@index');
Route::middleware(['auth', 'verified'])->group(function()
{
Route::get('/home', 'HomeController@index')->name('home');
Route::any('/profile/ppic/change', 'EditProfileController@picture')->name('profile.ppic.change');
Route::get('/profile/create', 'CandidateProfileController@create')->name('profile.create');
Route::get('/profile/create/summary', 'CandidateProfileController@createSummary')->name('profile.create.summary');
Route::get('/profile/create/education', 'CandidateProfileController@createEducation')->name('profile.create.education');
Route::get('/profile/create/work', 'CandidateProfileController@createWork')->name('profile.create.work');
Route::post('/add/contact/details', 'CandidateProfileController@addcontact')->name('add.contact.details');
Route::post('/add/profile/summary', 'CandidateProfileController@addSummary')->name('add.profile.summary');
Route::post('/add/profile/feducation', 'CandidateProfileController@addFurtherEducation')->name('add.profile.feducation');
Route::post('/add/profile/proffesion', 'CandidateProfileController@addProffession')->name('add.profile.proffesion');
Route::post('/add/profile/proffesionBodies', 'CandidateProfileController@addProffessionBodies')->name('add.profile.proffesionBodies');
Route::post('/add/profile/work', 'CandidateProfileController@addWork')->name('add.profile.work');
Route::post('/add/profile/interest', 'CandidateProfileController@addInterest')->name('add.profile.interest');
Route::post('/candidate/home', 'CandidateProfileController@candidatehome')->name('candidate.profile.home');
Route::post('/delete/profile/feducation', 'CandidateProfileController@deleteFurtherEducation')->name('delete.profile.feducation');
Route::post('/delete/profile/proffesion', 'CandidateProfileController@deleteProffession')->name('delete.profile.proffesion');
Route::post('/delete/profile/proffesionBodies', 'CandidateProfileController@deleteProffessionBodies')->name('delete.profile.proffesionBodies');
Route::post('/delete/profile/work', 'CandidateProfileController@deleteWork')->name('delete.profile.work');
Route::post('/edit/contact/details', 'EditProfileController@editContact')->name('edit.contact.details');
Route::post('/edit/profile/summary', 'EditProfileController@editSummary')->name('edit.profile.summary');
Route::post('/edit/profile/feducation', 'EditProfileController@editFurtherEducation')->name('edit.profile.feducation');
Route::post('/edit/profile/proffesion', 'EditProfileController@editProffession')->name('edit.profile.proffesion');
Route::post('/edit/profile/proffesionBodies', 'EditProfileController@editProffessionBodies')->name('edit.profile.proffesionBodies');
Route::post('/edit/profile/work', 'EditProfileController@editWork')->name('edit.profile.work');
Route::post('/edit/profile/interest', 'EditProfileController@editInterest')->name('edit.profile.interest');
/*Export CV*/
Route::get('/view/cv', 'CurriculumVitaeController@view')->name('view.cv');
Route::any('/upload/cv', 'CurriculumVitaeController@upload')->name('upload.cv');
Route::get('/export/cv', 'CurriculumVitaeController@export')->name('export.cv');
Route::get('jobs/jobapply/{id}','JobAdController@apply')->name('jobs.jobapply');
Route::post('/jobs/jobapply/sendtest', 'JobAdController@sendTest')->name('jobs.jobapply.sendtest');
Route::get('jobs/my-applications','JobAdController@applications')->name('jobs.my-applications');
Route::get('jobs/applied/{id}','JobAdController@applied')->name('jobs.applied');
Route::post('ticket-send','NotificationController@send')->name('ticket.send');
Route::get('tickets','NotificationController@tickets')->name('tickets');
Route::get('/markAsRead', function(){
	auth()->user()->unreadNotifications->markAsRead();
	return redirect()->back();
})->name('markAsRead');
});
Route::middleware(['auth', 'verified','admin'])->prefix('admin')->group(function()
{
//
Route::post('/interview/export', 'InterviewExportController@export')->name('interview.export');
Route::get('/interviewdetails/export/{id}', 'InterviewExportController@detailsexport')->name('interview.interviewdetailsexport');
//



//admin board
Route::any('dashboard/json', 'DashboardController@json')->name('dashboard.json');
Route::any('dashboard/filter', 'DashboardController@dashboardFilter')->name('dashboard.filter');
Route::any('dashboard', 'DashboardController@dashboard')->name('dashboard');

//
//consultant board
Route::any('dashboard2/json','DashboardController@json2')->name('dashboard2.json');
Route::any('dashboard2', 'DashboardController@dashboard2')->name('dashboard2');
//
Route::any('jobAds/json', 'JobAdController@json')->name('jobAds.json');
Route::any('jobAds/viewApplicants/{id}','JobAdController@viewApplicants')->name('jobAds.viewApplicants');
Route::any('jobAds/json2/{id}','JobAdController@json2')->name('jobAds.json2');
Route::any('jobAds/jobsapplied/{id}','JobAdController@jobsapplied')->name('jobAds.jobsapplied');
Route::any('jobAds/client/create/{id}','JobAdController@ClientCreate')->name('client.create');
Route::resource('jobAds', 'JobAdController');
Route::resource('jobAdsviewapplicants','JobAdController');
Route::any('jobAds/blacklist','JobAdController@Blacklist')->name('jobAds.blacklist');
//
 Route::post('jobAds/sendmail','JobAdController@sendmail')->name('send.mail');
//
Route::get('users/json', 'UserController@json')->name('users.json');
Route::get('users2/json', 'UserController@json2')->name('users2.json');
// Route::resource('users', 'UserController');
Route::any('staff/profile', 'UserController@profile')->name('staff.profile');
Route::any('staff/profile/image/{id}', 'UserController@profileImage')->name('staff.profile.image');
Route::any('users','UserController@index')->name('users.index');
Route::any('users/create','UserController@create')->name('users.create');
Route::any('users/store','UserController@store')->name('users.store');
Route::any('users/edit/{id}','UserController@edit')->name('users.edit');
Route::any('users/update/{id}','UserController@update')->name('users.update');
Route::any('users/show/{id}','UserController@show')->name('users.show');
Route::any('users/delete/{id}','UserController@delete')->name('users.delete');
Route::any('users/activate/{id}','UserController@activate')->name('users.activate');
Route::delete('jobAds/remove_job/{id}','JobAdController@removeJob')->name('jobAds.remove_job');
/*Export details backend*/
/*prequaltest management route to confirm with Paul*/
Route::any('/jobtests/json','JobTestController@json')->name('jobtests.json');
Route::get('/jobtests','JobTestController@jobtestsindex')->name('jobtests.index');
Route::get('jobtests/add/{id}','JobTestController@jobtestscreate')->name('jobtests.create');
Route::post('jobtests/store','JobTestController@jobtestsstore')->name('jobtests.store');
Route::get('jobtests/view/{id}/{job}','JobTestController@jobtestsview')->name('jobtests.view');
Route::get('jobtests/viewquestions/{job}','JobTestController@jobtestsviewquestions')->name('jobtests.viewquestions');
Route::get('jobtests/viewquestions/edit/{job}','JobTestController@editjobtestsviewquestions')->name('jobtests.editviewquestions');
Route::any('jobtests/viewquestions/update/{job}','JobTestController@updatejobtestsviewquestions')->name('jobtests.updateviewquestions');
Route::get('jobtests/destroy/{id}/{job}','JobTestController@jobtestsdestroy')->name('jobtests.destroy');

/* Interview management route to confirm with paul*/
Route::any('interviewmanagement/json', 'InterviewManagementController@json')->name('interviewmanagement.json');
Route::get('interviewmanagement', 'InterviewManagementController@index')->name('interviewmanagement.index');
Route::get('interviewmanagement/add', 'InterviewManagementController@create')->name('interviewmanagement.create');
Route::post('interviewmanagement/store', 'InterviewManagementController@store')->name('interviewmanagement.store');
Route::get('interviewmanagement/edit/{id}', 'InterviewManagementController@edit')->name('interviewmanagement.edit');
Route::get('interviewmanagement/create/{id}','InterviewManagementController@create')->name('interviewmanagement.create');
Route::get('interviewmanagement/noCreate/{id}','InterviewManagementController@noCreate')->name('interviewmanagement.noCreate');
Route::post('interviewmanagement/update/{id}','InterviewManagementController@update')->name('interviewmanagement.update');
Route::get('interviewmanagement/show/{id}','InterviewManagementController@show')->name('interviewmanagement.show');
Route::delete('interviewmanagement/delete/{id}', 'InterviewManagementController@delete')->name('interviewmanagement.delete');
Route::get('interviewmanagement/blacklist/{id}/{val}','InterviewManagementController@Blacklist')->name('interviewmanagement.blacklist');

/*Candidate management route to confirm with Paul - this can be re routed via a specific controller. */
 Route::any('candidatemanagement/json', 'CandidateManagementController@json')->name('candidatemanagement.json');
 Route::get('candidatemanagement', 'CandidateManagementController@index')->name('candidatemanagement.index');
 Route::get('candidatemanagement/edit/{id}', 'CandidateManagementController@edit')->name('candidatemanagement.edit');
 Route::post('candidatemanagement/update/{id}','CandidateManagementController@update')->name('candidatemanagement.update');
 Route::get('candidatemanagement/show/{id}','CandidateManagementController@show')->name('candidatemanagement.show');
 Route::get('candidatemanagement/blacklist/{id}/{val}','CandidateManagementController@Blacklist')->name('candidatemanagement.blacklist');
 
/*Client management route to confirm with Paul*/
Route::any('clientmanagement/json', 'ClientManagementController@json')->name('clientmanagement.json');
Route::get('clientmanagement', 'ClientManagementController@index')->name('clientmanagement.index');
Route::get('clientmanagement/add', 'ClientManagementController@create')->name('clientmanagement.create');
Route::any('clientmanagement/store', 'ClientManagementController@store')->name('clientmanagement.store');
Route::get('clientmanagement/edit/{id}', 'ClientManagementController@edit')->name('clientmanagement.edit');
Route::post('clientmanagement/update/{id}','ClientManagementController@update')->name('clientmanagement.update');
Route::get('clientmanagement/show/{id}','ClientManagementController@show')->name('clientmanagement.show');
Route::delete('clientmanagement/delete/{id}','ClientManagementController@delete')->name('clientmanagement.delete');
/**end of client management**/

Route::any('candidate/cv/read', 'CVManagementController@candidateRead')->name('candidateCV.mark');
Route::any('candidate/cv/unread', 'CVManagementController@candidateUnRead')->name('candidate.unread');

Route::any('candidate/cv/read2', 'CVManagementController@candidateRead2')->name('candidateCV.mark2');
Route::any('candidate/cv/unread2', 'CVManagementController@candidateUnRead2')->name('candidate.unread2');
Route::get('candidate/cv/{id}/{job}', 'CVManagementController@candidateCV')->name('candidateCV');
Route::get('candidate/cv/{id}', 'CVManagementController@candidatesCV')->name('candidatesCV');
Route::any('comment/cv', 'CVManagementController@commentCV')->name('comment.cv');
Route::get('tickets','NotificationController@admintickets')->name('tickets');
Route::post('response/ticket','NotificationController@response')->name('response.ticket');
Route::get('staff/notification/{id}', 'NotificationController@staffNotification')->name('staff.notifications');

/* industry */
Route::get('industry/add', 'IndustryController@create')->name('industry.create');
Route::post('industry/store', 'IndustryController@store')->name('industry.store');
Route::get('industry/json', 'IndustryController@json')->name('industry.json');
Route::get('industry', 'IndustryController@index')->name('industry.index');
Route::get('industry/edit/{id}', 'IndustryController@edit')->name('industry.edit');
Route::get('industry/view/{id}', 'IndustryController@view')->name('industry.view');
Route::post('industry/update/{id}', 'IndustryController@update')->name('industry.update');
Route::delete('industry/delete/{id}','IndustryController@delete')->name('industry.delete');
/**end of industry**/

/**Key Competencies**/
Route::get('competencies','KeyCompetenciesController@index')->name('competencies.index');
Route::get('competencies/add','KeyCompetenciesController@create')->name('competencies.create');
Route::post('competencies/store','KeyCompetenciesController@store')->name('competencies.store');
Route::get('competencies/edit/{id}','KeyCompetenciesController@edit')->name('competencies.edit');
Route::post('competencies/update/{id}','KeyCompetenciesController@update')->name('competencies.update');
Route::delete('competencies/delete/{id}','KeyCompetenciesController@delete')->name('competencies.delete');
/**end of competencies**/

/* currency to confirm with paul*/
Route::get('currency/add', 'CurrencyController@create')->name('currency.create');
Route::post('currency/store', 'CurrencyController@store')->name('currency.store');
Route::get('currency', 'CurrencyController@index')->name('currency.index');
Route::get('currency/edit/{id}', 'CurrencyController@edit')->name('currency.edit');
Route::get('currency/view/{id}', 'CurrencyController@view')->name('currency.view');
Route::post('currency/update/{id}', 'CurrencyController@update')->name('currency.update');
Route::delete('currency/delete/{id}','CurrencyController@delete')->name('currency.delete');
/*location routes to confirm with paul*/
Route::get('location/add', 'LocationController@create')->name('location.create');
Route::post('location/store', 'LocationController@store')->name('location.store');
Route::get('location', 'LocationController@index')->name('location.index');
Route::get('location/edit/{id}', 'LocationController@edit')->name('location.edit');
Route::get('location/view/{id}', 'LocationController@view')->name('location.view');
Route::post('location/update/{id}', 'LocationController@update')->name('location.update');
Route::delete('location/delete/{id}','LocationController@delete')->name('location.delete');
/*language routes*to verify with paul*/
Route::get('language/add', 'LanguageController@create')->name('language.create');
Route::post('language/store', 'LanguageController@store')->name('language.store');
Route::get('language', 'LanguageController@index')->name('language.index');
Route::get('language/edit/{id}', 'LanguageController@edit')->name('language.edit');
Route::get('language/view/{id}', 'LanguageController@view')->name('language.view');
Route::post('language/update/{id}', 'LanguageController@update')->name('language.update');
Route::delete('language/delete/{id}','LanguageController@delete')->name('language.delete');
/*hardskills routes..to verify with paul*/
Route::get('hardskill/add', 'HardSkillController@create')->name('hardskill.create');
Route::post('hardskill/store', 'HardSkillController@store')->name('hardskill.store');
Route::get('hardskill/json', 'HardSkillController@json')->name('hardskill.json');
Route::get('hardskill', 'HardSkillController@index')->name('hardskill.index');
Route::get('hardskill/edit/{id}', 'HardSkillController@edit')->name('hardskill.edit');
Route::get('hardskill/view/{id}', 'HardSkillController@view')->name('hardskill.view');
Route::post('hardskill/update/{id}', 'HardSkillController@update')->name('hardskill.update');
Route::delete('hardskill/delete/{id}','HardSkillController@delete')->name('hardskill.delete');
/*countries routes..to verify with paul*/
Route::get('country/add', 'CountryController@create')->name('country.create');
Route::post('country/store', 'CountryController@store')->name('country.store');
Route::get('country', 'CountryController@index')->name('country.index');
Route::get('country/edit/{id}', 'CountryController@edit')->name('country.edit');
Route::get('country/view/{id}', 'CountryController@view')->name('country.view');
Route::post('country/update/{id}', 'CountryController@update')->name('country.update');
Route::delete('country/delete/{id}','CountryController@delete')->name('country.delete');
/*specialization routes..to verify with paul*/
Route::get('specialization/add', 'SpecializationController@create')->name('specialization.create');
Route::post('specialization/store', 'SpecializationController@store')->name('specialization.store');
Route::get('specialization', 'SpecializationController@index')->name('specialization.index');
Route::get('specialization/edit/{id}', 'SpecializationController@edit')->name('specialization.edit');
Route::get('specialization/view/{id}', 'SpecializationController@view')->name('specialization.view');
Route::post('specialization/update/{id}', 'SpecializationController@update')->name('specialization.update');
Route::delete('specialization/delete/{id}','SpecializationController@delete')->name('specialization.delete');
/*Industryfunctions routes..to verify with paul*/
Route::get('industryfunctions/add', 'IndustryFunctionsController@create')->name('industryfunctions.create');
Route::post('industryfunctions/store', 'IndustryFunctionsController@store')->name('industryfunctions.store');
Route::get('industryfunctions', 'IndustryFunctionsController@index')->name('industryfunctions.index');
Route::get('industryfunctions/edit/{id}', 'IndustryFunctionsController@edit')->name('industryfunctions.edit');
Route::get('industryfunctions/view/{id}', 'IndustryFunctionsController@view')->name('industryfunctions.view');
Route::post('industryfunctions/update/{id}', 'IndustryFunctionsController@update')->name('industryfunctions.update');
Route::delete('industryfunctions/delete/{id}','IndustryFunctionsController@delete')->name('industryfunctions.delete');
/*subject/discpline titles routes..to verify with paul*/
Route::get('subject/add', 'SubjectController@create')->name('subject.create');
Route::post('subject/store', 'SubjectController@store')->name('subject.store');
Route::get('subject', 'SubjectController@index')->name('subject.index');
Route::get('subject/edit/{id}', 'SubjectController@edit')->name('subject.edit');
Route::get('subject/view/{id}', 'SubjectControllerr@view')->name('subject.view');
Route::post('subject/update/{id}', 'SubjectController@update')->name('subject.update');
Route::delete('subject/delete/{id}','SubjectController@delete')->name('subject.delete');
/*pofessional qualification titles routes..to verify with paul*/
Route::get('profqualtitles/add', 'ProfQualTitlesController@create')->name('profqualtitles.create');
Route::post('profqualtitles/store', 'ProfQualTitlesController@store')->name('profqualtitles.store');
Route::get('profqualtitles', 'ProfQualTitlesController@index')->name('profqualtitles.index');
Route::get('profqualtitles/edit/{id}', 'ProfQualTitlesController@edit')->name('profqualtitles.edit');
Route::get('proflqualtitles/view/{id}', 'ProfQualTitlesController@view')->name('profqualtitles.view');
Route::post('profqualtitles/update/{id}', 'ProfQualTitlesController@update')->name('profqualtitles.update');
Route::delete('profqualtitles/delete/{id}','ProfQualTitlesController@delete')->name('profqualtitles.delete');
/*campaign management routes..to verify with paul*/
Route::get('communication/add', 'CreateCampaignsController@create')->name('communication.create');
Route::post('communication/store', 'CreateCampaignsController@store')->name('communication.store');
Route::get('communication/view/{id}', 'CreateCampaignsController@view')->name('communication.view');
Route::get('communication', 'CreateCampaignsController@index')->name('communication.index');
Route::get('communication/json', 'CreateCampaignsController@json')->name('communications.json');
Route::get('communication/edit/{id}', 'CreateCampaignsController@edit')->name('communication.edit');
Route::post('communication/update/{id}', 'CreateCampaignsController@update')->name('communication.update');
Route::delete('communication/delete/{id}','CreateCampaignsController@delete')->name('communication.delete');
Route::any('communication/show/{id}','CreateCampaignsController@show')->name('communication.show');
});
