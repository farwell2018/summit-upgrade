<?php

return [


  'profile' => [
    ['title' => 'My Profile', 'route' => 'home'],
    ['title' => 'My Applications', 'route' => 'jobs.my-applications'],
    ['title' => 'Export CV', 'route' => 'view.cv'],
  ],

  'support_email' => 'support@erevuka.farwell-consultants.com',

];
