<?php

return [
  /*
    Let's create the nav structure of the admin section.
  */
  'admin_nav' => [
    'Dashboard'=>[
      // ['title'=>'My Recruitment','icon'=>'<i class="fas fa-file"></i>','route'=>'dashboard'],

      ['title'=>'Job Ads Management','icon'=>'<i class="fas fa-briefcase"></i>','route'=>'jobAds.index'],

      ['title'=>'Interviews Management','icon'=>'<i class="fas fa-book"></i>','route'=>'interviewmanagement.index'],

      ['title'=>'Client Management','icon'=>'<i class="far fa-building" aria-hidden="true"></i>','route'=>'clientmanagement.index'],

      ['title'=>'Candidate Management','icon'=>'<i class="fas fa-users"></i>','route'=>'candidatemanagement.index'],     

      ['title'=>'Consultant Management','icon'=>'<i class="fas fa-users"></i>','route'=>'users.index'],     

      ['title'=>'Tickets Management','icon'=>'<i class="fas fa-ticket-alt"></i>','route'=>'tickets'], 
    ],
  ],

  'summit_staff' => [
    'Dashboard'=>[
        
      ['title'=>'Job Ad Management','icon'=>'<i class="fas fa-briefcase"></i>','route'=>'jobAds.index'],

      ['title'=>'Interviews Management','icon'=>'<i class="fas fa-book"></i>','route'=>'interviewmanagement.index'],

      ['title'=>'Client Management','icon'=>'<i class="far fa-building"></i>','route'=>'clientmanagement.index'],
      
      ['title'=>'Candidate Management','icon'=>'<i class="fas fa-users"></i>','route'=>'candidatemanagement.index'],  
      
      ['title'=>'Tickets','icon'=>'<i class="fas fa-ticket-alt"></i>','route'=>'tickets'],
    ],
  ],
  /*
    Theme specific styles
  */
  'styles'=>[
    "public/assets/vendors/general/bootstrap/dist/css/bootstrap.min.css",
    "public/assets/vendors/general/perfect-scrollbar/css/perfect-scrollbar.css",
    "public/assets/vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css",
    "public/admin-assets/css/sb-admin-2.min.css",
    "public/admin-assets/vendor/datatables/dataTables.bootstrap4.min.css",
    "public/css/admin.css",
  	//Global Mandatory Vendors
  ],

  /*
    Theme specific js
  */
  'js'=>[
  "public/admin-assets/vendor/jquery/jquery.min.js",
  "public/assets/vendors/general/bootstrap/dist/js/bootstrap.min.js",
  "public/admin-assets/vendor/bootstrap/js/bootstrap.bundle.min.js",
  "public/admin-assets/vendor/jquery-easing/jquery.easing.min.js",
  "public/admin-assets/js/sb-admin-2.min.js",
  "public/admin-assets/vendor/datatables/jquery.dataTables.min.js",
  "public/admin-assets/vendor/datatables/dataTables.bootstrap4.min.js",
  "public/admin-assets/js/demo/datatables-demo.js",
  "public/admin-assets/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js",
  ]

];
